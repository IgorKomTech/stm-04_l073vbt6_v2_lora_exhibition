/* 

while(){for(unlong i=0; i<0xFFFFFFF; i++);}
*/
//#include "stm32l0xx_hal.h"
#include "stm32l0xx.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "Global.h"
//############################################################
uint8_t CheckOneBitInByte(uint8_t Bit)
{
   uint8_t flag=0;
   for(uint8_t i=0; i<8; i++)
   {
      if((Bit & BIT0) && flag)
      {
         return 1;
      }
      else
      {
         if(Bit & BIT0)
         flag = 1;
         Bit >>= 1;
      }
   }
   return 0;
}
//##################################################################
uint16_t SwapBytes(uint16_t buf)
{
   uint16_t temp = buf << 8;
   temp |= (uint8_t) (buf >> 8);
   return temp;
}
//##################################################################
uint16_t Average(uint16_t *ptrData, uint8_t cnt_word)
{
   uint32_t sum=0;
   for(uint8_t i=0; i < cnt_word; i++)
   {
     sum += *ptrData++;
   }
   sum /= cnt_word;
   return (uint16_t)sum;
}
//##################################################################
uint8_t Calck_CSumme (uint8_t *ptr_cs, uint32_t counter)
{
   uint16_t i;
   uint8_t CS;
   CS=0;
   for (i = 0; i < counter; i++)
   {CS += *ptr_cs++;}
   CS=~CS;
   return CS;
}
//########################################################################################################################################
void Clear_memory(uint8_t *ptr, uint16_t n)
{
   for(uint16_t i=0; i < n; i++)
      *ptr++ = 0;
}
//########################################################################################################################################
uint8_t Number_digits_U32(uint32_t x)
{
   uint8_t r=0;
   if(x < 10)
     r = 1;
   else if(x < 100)
     r = 2;
   else if(x < 1000)
     r = 3;
   else if(x < 10000)
     r = 4; 
   else if(x < 100000)
     r = 5; 
   else if(x < 1000000)
     r = 6; 
   else if(x < 10000000)
     r = 7;
   else if(x < 100000000)
     r = 8;
   else if(x < 1000000000)
     r = 9;
   else if(x > 999999999)
     r = 10;
   return r;
}
//########################################################################################################################################
void     U32ToStr(uint8_t *ptr, uint32_t x)
{
   uint8_t Number_digits ;
   uint32_t temp1;
   // ���������� ����� ����
   Number_digits = Number_digits_U32(x);
   for(uint16_t i=Number_digits; i > 0; i--)
   {
      if(i != 1)
      {
         temp1 = (uint32_t)pow((double)10,i-1);
         temp1 = x / temp1;
         x %= (uint32_t)pow((double)10,i-1);
      }
      else
         temp1 = (uint8_t)x;
      *ptr++ = (uint8_t)temp1 + '0';
   }
}
//########################################################################################################################################
uint32_t StrToInt(const int8_t *str, uint8_t n)
{ // n - ����� ��������
   uint32_t x=0;
   uint8_t i, y=0;
   for(i = n; i > 0; i--)
   {
      switch (i)
      {
      case 1:
           x += (uint8_t)*str - 0x30;
           break;
      case 2:
           y = (uint8_t)*str - 0x30;
           x += 10 * y;
           break;
      case 3:
           y = (uint8_t)*str - 0x30;
           x += (uint16_t)100 * y;
           break;
      case 4:
           y = (uint8_t)*str - 0x30;
           x += 1000 * y;
           break;
      case 5:
           y = (uint8_t)*str - 0x30;
           x += (uint32_t)10000 * y;
           break;
      case 6:
           y = (uint8_t)*str - 0x30;
           x += 100000 * y;
           break;
      case 7:
           y = (uint8_t)*str - 0x30;
           x += 1000000 * y;
           break;
      case 8:
           y = (uint8_t)*str - 0x30;
           x += 10000000 * y;
           break;
      case 9:
           y = (uint8_t)*str - 0x30;
           x += 100000000 * y;
           break;
      case 10:
           y = (uint8_t)*str - 0x30;
           if((uint8_t)*str > 0x2F && (uint8_t)*str < 0x35)
              x += (uint32_t)1000000000 * y;
           break;
      default:break;
      }
      ++str;
   }
   return x;
}
//########################################################################################################################################
uint8_t Find_null_bite(uint8_t x, uint8_t n)
{
   if(n < 9)
      for(uint8_t i=1; i <= n; i++)
      {
         if(!(x & BIT0))
            return i; // ���������� ����� ���������� ����
         x >>= 1;
      }
   return 0; // ��� �� ������ �������� ����
}
//########################################################################################################################################
uint8_t Find_null_byte(uint8_t *ptr, uint8_t n)
{
   for(uint8_t i=1; i <= n; i++)
   {
      if(!*ptr++)
         return i;
   }
   return 0;
}
//########################################################################################################################################
void Change_bite(uint8_t *ptr_x, uint8_t n, uint8_t flag)
{
   if(flag)
   {
      *ptr_x |= BIT0 << n-1;
   }
   else
   {
      *ptr_x &= ~(BIT0 << n-1);
   }
}
//########################################################################################################################################
uint16_t StrToHex(uint8_t *OutBuf, uint8_t * InBuf, uint16_t inc)
{
   int i,
       len,
       r_len=0,
       hex;
   
   len = strlen((char*)InBuf);
   for(i=0; i<len;)
   {
      sscanf((char *)InBuf+i,"%02X",&hex);
      OutBuf[r_len++]=hex;
      i += inc;
   }
   return r_len;
}
//########################################################################################################################################
void AnsiToUnicod(uint8_t *ptrbuf, uint8_t *ptrstring)
{
   uint8_t len_str;
   char *ptr;
   ptr = strchr((char*)ptrbuf, 0);
   if(ptr == 0)
     return;
   len_str = strlen((char*)ptrstring);
   for(uint8_t i=0; i < len_str; i++)
   {
      *ptr++ = '0';
      *ptr++ = '0';
      sprintf((char*)ptr,"%02X", *ptrstring++);
      ptr += 2;
   }
}
//########################################################################################################################################
void LongToUnicod(uint8_t *ptrbuf, uint32_t numbber)
{
   uint8_t buf[12];
   sprintf((char*)buf, "%d", numbber);
   AnsiToUnicod(ptrbuf, buf);
}

/*
//########################################################################################################################################
uint32_t DateToInt(DateType dateType)
{// ������ ������
   uint32_t dateInt;
   dateInt = (uint32_t)dateType.Year * 10000;
   dateInt += (uint32_t)dateType.Date * 100;
   dateInt += (uint32_t)dateType.Month;
   return dateInt;
}
//########################################################################################################################################
DateType IntToDate(uint32_t dateInt)
{// ������ ������
   DateType dateType;
   dateType.Year = (uint8_t)(dateInt / 10000);
   dateType.Date = (uint8_t)((dateInt % 10000) / 100);
   dateType.Date = (uint8_t)(dateInt % 100);
   return dateType;
}
*/
//####################################################################################################################3
/*
1 - ���� 1 > 2
0 - ���� 1 == 2
-1 - ���� 1 < 2
*/
int8_t compareDateTime(_DateTimeType DateType1, _DateTimeType DateType2)
{
    if(DateType1.Date.Year > DateType2.Date.Year)
       return MORE_YEAR;
    if(DateType1.Date.Year < DateType2.Date.Year)
       return LESS_YEAR;
    
    if(DateType1.Date.Month > DateType2.Date.Month)
       return MORE_MONTH;
    if(DateType1.Date.Month < DateType2.Date.Month)
       return LESS_MONTH;
    
    if(DateType1.Date.Date > DateType2.Date.Date)
       return MORE_DAY;
    if(DateType1.Date.Date < DateType2.Date.Date)
       return LESS_DAY;
    
    if(DateType1.Time.Hours > DateType2.Time.Hours)
       return MORE_HOUR;
    if(DateType1.Time.Hours < DateType2.Time.Hours)
       return LESS_HOUR;
    
    if(DateType1.Time.Minutes > DateType2.Time.Minutes)
       return MORE_MINUTE;
    if(DateType1.Time.Minutes < DateType2.Time.Minutes)
       return LESS_MINUTE;
    
    if(DateType1.Time.Seconds > DateType2.Time.Seconds)
       return MORE_SECOND;
    if(DateType1.Time.Seconds < DateType2.Time.Seconds)
       return LESS_SECOND;
    
    return EQUALLY;
}
//###########################################################################
uint32_t DifferenceTickCount(uint32_t TickCount, uint32_t temp)
{
   uint32_t buf = 0;
   if(TickCount < temp)
   {
      buf = (0xFFFFFFFF - temp) + 1 + TickCount;
   }
   else
   {
      buf =  TickCount - temp;
   } 
   return buf;
}
//###########################################################################
uint8_t check_crc32_str(uint8_t *ptr_str)
{
   char *ptr_temp_for_crc32;
   uint8_t rez = _FALSE;
   uint32_t crc32, crc32_str;
   
   ptr_temp_for_crc32 = strchr((char*)ptr_str, '\t');
   if(ptr_temp_for_crc32)
   {
       crc32_str = CalculateCRC32((uint8_t*)ptr_str, (uint32_t)ptr_temp_for_crc32 - (uint32_t)ptr_str, CRC_CTART_STOP);
       sscanf(ptr_temp_for_crc32 + 1,"%X",&crc32);
       if(crc32_str == crc32)
          rez = _OK;
   }
   return rez;
}
/*!
Calculate CRC32
\param[in] 
\param[in] 
\param[out]
\return 
*/   
uint32_t CalculateCRC32(uint8_t *ptr_buf, uint32_t cnt_byte_data, uint8_t flag_process)
{
   uint8_t *pBuffer = (uint8_t *)ptr_buf;
   uint32_t crc = 0;
   uint32_t tempdate;
   uint32_t i; /* input data buffer index */   

   if(flag_process == CRC_NON || !cnt_byte_data)
     return 0;
     
   if(flag_process == CRC_CTART || flag_process == CRC_CTART_STOP)
      CRC_Start();

   for(i = 0U; i < (cnt_byte_data / 4U); i++)
   {
     tempdate = ((uint32_t)pBuffer[ 4U * i] << 24U) 
                    | ((uint32_t)pBuffer[ 4U * i + 1U ] << 16U) 
                    | ((uint32_t)pBuffer[ 4U * i + 2U ] << 8U) 
                    | (uint32_t)pBuffer[ 4U * i + 3U ];
     CRC->DR = tempdate;
   }
      
   if(flag_process == CRC_STOP || flag_process == CRC_CTART_STOP)
     crc = CRC_Stop(pBuffer, cnt_byte_data, i);
   else
   {
      if((cnt_byte_data % 4U) != 0U)
      {
	   if(cnt_byte_data % 4U == 1U)
           {
	      *(uint8_t volatile*) (&CRC->DR) = pBuffer[4U*i];
	   }
	   if(cnt_byte_data % 4U == 2U)
           {
		*(uint16_t volatile*) (&CRC->DR) = ((uint32_t)pBuffer[4U*i]<<8U) | (uint32_t)pBuffer[4U*i+1U];
	   }
	   if(cnt_byte_data % 4U == 3U)
	   {
		*(uint16_t volatile*) (&CRC->DR) = ((uint32_t)pBuffer[4U*i]<<8U) | (uint32_t)pBuffer[4U*i+1U];
		*(uint8_t volatile*) (&CRC->DR) = pBuffer[4U*i+2U];
	   }
      }
   }
   
   crc = ~crc;
   return crc;
}

void CRC_Start()
{
   SET_BIT(RCC->AHBENR, RCC_AHBENR_CRCEN);
   CRC->INIT = 0xFFFFFFFFU;   
   CRC->POL = 0x04C11DB7U;
   CRC->CR = (uint32_t)(CRC_CR_REV_OUT | CRC_CR_REV_IN_0);
   CRC->CR |= CRC_CR_RESET;  
}

uint32_t CRC_Stop(uint8_t *pBuffer, uint32_t cnt_byte_data, uint32_t i)
{
  uint32_t crc = 0;
      if((cnt_byte_data % 4U) != 0U)
      {
	   if(cnt_byte_data % 4U == 1U)
           {
	      *(uint8_t volatile*) (&CRC->DR) = pBuffer[4U*i];
	   }
	   if(cnt_byte_data % 4U == 2U)
           {
		*(uint16_t volatile*) (&CRC->DR) = ((uint32_t)pBuffer[4U*i]<<8U) | (uint32_t)pBuffer[4U*i+1U];
	   }
	   if(cnt_byte_data % 4U == 3U)
	   {
		*(uint16_t volatile*) (&CRC->DR) = ((uint32_t)pBuffer[4U*i]<<8U) | (uint32_t)pBuffer[4U*i+1U];
		*(uint8_t volatile*) (&CRC->DR) = pBuffer[4U*i+2U];
	   }
      }
   
   crc = CRC->DR;
   CLEAR_BIT(RCC->AHBENR, RCC_AHBENR_CRCEN);  
   return crc;
}