#include "main_function.h"
#include "main.h"
#include "GSMSIM800C.h"
#include "string.h"
#include "Global.h"
#include "stdio.h"
#include "stdlib.h"
#include "SensorTypeParameters.h"
#include "GasMeter.h"
#include "cmsis_os.h"
#include "Time_g.h"
#include "OptoComand.h"
#include "USART_Modem.h"
#include "Archiv.h"
#include "ValveTask.h"
#include <time.h>

//#include "main_function.h"
#define BUF_MAS_SIZE 40
//uint8_t buf_mas[BUF_MAS_SIZE];
#define CRYPTSTR_SIZE 40
uint8_t CryptStr[BUF_MAS_SIZE];
uint8_t partParsingMessageTMR;
uint8_t flag_end_transmit_parts;
uint16_t cnt_str_transmit=0;
TArchiv_type flag_detection_archive;
uint8_t flag_archive; // ��� ��������� ������ �� TCP
uint8_t  successful_transfer_data_to_server = _OK;
uint8_t cnt_GSM_TCP_Connect_FALSE=0;
uint8_t cnt_seans_TCP_mode_2=0;
uint8_t flag_change_server_url_valve=_FALSE;
int8_t flag_event=0;
int8_t flag_event_menu=0;
uint8_t cnt_error_SGM=0;
uint8_t flagPowerONGSMOneTime = 0;
uint8_t  cntQueueGSMModemHandle=0;
uint8_t CRC_old_buf[4];
uint8_t flag_check_crc_dQf=0;
uint8_t Qmax_warning_temp=0;
uint8_t going_beyond_T = 0;
uint8_t going_beyond_Q = 0;
uint8_t going_beyond_T_warning = 0;
uint8_t source_change_CLB_LOCK = CHANGE_PROGRAM;
uint8_t number_server_transmit=0;

int8_t HiPower_ON = 0;


_ResultReply resultReplyGSM;


//uint16_t code_event_buf=EVENT_CHANGE_STATUS;
uint16_t NoLogToOptik = 0; // debag info for GSM to optic off (=0 on)

uint32_t PressKeyTime;
uint32_t stack_task_GSM=0;
uint32_t stack_GasmeterTask=0;
uint32_t stack_OPTO_ReadTask=0;
uint32_t stack_ReadWriteARC=0;
uint32_t stack_GSM_RX_UARTTask=0;
uint32_t stack_IDLE=0;
uint32_t event_int_story=0;
uint32_t temp_time_stop_mode;
uint16_t difference_lptim;
IntArcTypeDef frameArchiveInt;
//EventArcTypeDef frameArchiveEvent;
//ChangeArcTypeDef frameArchiveChange;
uint8_t *ptr_frameArchive;

TypeStatus StatusDevice;
TypeStatus StatusDevice_buf;
TypeStatus StatusDevice_old_connect_TCP;
TypeStatusWarning StatusWarning;
TypeLoraStatus LoraStatus;

uint32_t tick_count_message_from_server_msec;
uint32_t numberArcRecFind;
uint32_t currentArcRecNo;
//uint32_t reserved_int;
uint32_t number_communication_gsm_fail;
const  uint32_t number_communication_gsm_fail_eeprom @".eeprom" = 0; 
const  uint32_t current_communication_gsm_number_eeprom @".eeprom" = 0; 
const  uint32_t cnt_fail_sim_card_eeprom @".eeprom" = 0; 
const  uint32_t cnt_fail_speed_gsm_modem_eeprom @".eeprom" = 0; 
const  uint16_t hw_version @".eeprom" = 208; 
const  double pressure_abs @".eeprom" = 101.325;
const  uint8_t valve_presence  @".eeprom" = 0; // 1 - there is
const  uint8_t valve_status @".eeprom" = VALVE_UNKNOWN;  // 0 - close, 1 - open
const  uint8_t valve_resolution @".eeprom" = 1; // 0 - close, 1 - open
const char EEPROM_SN_SGM [SIZE_EEPROM_SN_SGM] @".eeprom";
const char MdmURL_Server[3][SIZE_MdmURL_Server] @".eeprom" = {{"089.109.032.058"},0, 0} ;
const char MdmPort_Server[3][SIZE_MdmPort_Server] @".eeprom" = {{"40050"},0,0} ;//40200
const char MdmAPN_Adress[SIZE_MdmAPN_Adress] @".eeprom";
const char MdmAPN_Login [SIZE_MdmAPN_Login] @".eeprom";
const char MdmAPN_Password[SIZE_MdmAPN_Password] @".eeprom";
const char MdmNumberBalans[SIZE_MdmNumberBalans] @".eeprom";
const uint32_t reserved_int @".eeprom" = 10;//43200 300
const uint32_t auto_switching_mode @".eeprom" = 1;
const struct _Mode_Transfer Mode_Transfer @".eeprom" = {1,10,0,1}; 
const  uint8_t hard_fault @".eeprom" = 0; 
const uint8_t telephone_number_SMS1[15] @".eeprom" = {"+79307000107"}; // 14 + null, user server
const uint8_t telephone_number_SMS2[15] @".eeprom" = {"+79307000107"}; // 14 + null, user server
const int8_t type_device @".eeprom" = SGM_TYPE_UNDEFINED;
const int32_t max_cnt_communication_gsm @".eeprom" = 3000;
const int8_t CLB_lock_eeprom @".eeprom" = 0x55;
const int8_t optic_kontrast @".eeprom" = 7;
const _TypeMotoHoursToMils MotoHoursToMilsEEPROM @".eeprom";
const LL_RTC_DateTypeDef DateVerification @".eeprom";
const LL_RTC_DateTypeDef DateNextVerification @".eeprom";
const float Qmax_warning @".eeprom" = 1.2;
const float Qmax_min_warning @".eeprom" = 0.8;
const uint16_t Qmax_warning_cnt_measurement @".eeprom" = 20;
const uint16_t Qmax_cnt_measurement @".eeprom" = 20;
const uint16_t cnt_measurement_temper_eeprom @".eeprom" = 20;
const int8_t Tmax_warning @".eeprom" = 50;
const int8_t Tmin_warning @".eeprom" = -20;
const uint16_t cnt_measurement_temper_warning_eeprom @".eeprom" = 10;
const uint32_t crc_dQf_eeprom @".eeprom";
const uint16_t cnt_reverse_flow_eeprom @".eeprom" = 10;
const uint16_t Qfl_reverse_flow @".eeprom" = 9800;
const uint8_t serial_number_board[15] @".eeprom" = {0}; //
const uint16_t cnt_error_SGM_eeprom @".eeprom" = 20;
const uint16_t Kfactor_min @".eeprom" = 32000;
const uint16_t Kfactor_average @".eeprom" = 39000;
const uint16_t Kfactor_max @".eeprom" = 40700;
const uint8_t Kfactor_measure_cnt_eeprom @".eeprom" = 20;
const  uint8_t EnableKalmanFilter @".eeprom" = 1; 

uint8_t type_device_int;

double pressure_abs_gas = 96.325;
float residual_capacity_battery_percent;

uint32_t numFindArcBeg;
uint32_t numFindArcEnd;
uint32_t numFindArcCur;

const char str_end[]={"\x7D\r\n\x1A"};

uint64_t TimeFindUnixBegin;
uint64_t TimeFindUnixEnd;
uint64_t TimeFindUnixCurrent;
uint64_t   timeSystemUnix;
uint64_t   timeGSMUnixStart;
uint64_t   timeUnixGSMNextConnect;
uint64_t   time_record_Volume;
_TypeMotoHoursToMils MotoHoursToMils;
_TypeMotoHoursTempToMils MotoHoursTempToMils;

extern osMessageQId QueueGSMModemHandle;

//-----------------------------------------------
const TypePtrParserCmdTCP PtrParserCmdTCP[]=
{
  {"Server_MT",           MESSAGE_TCP_KEY}, 
  {"GET_GROUP_INFO",      MESSAGE_TCP_GET_GROUP_INFO}, 
  {"ARCHIVE",             MESSAGE_TCP_ARCHIVE}, 
  {"DATETIME",            MESSAGE_TCP_DATETIME}, 
  {"GET_GSM_INFO",        MESSAGE_GET_GSM_INFO},
  {"MOTO_INFO",           MESSAGE_MOTO_INFO},  
  {"EXIT",                MESSAGE_TCP_EXIT},  
  {"CLOSED",              MESSAGE_TCP_CLOSED},    
  {"RESERVED_INT",        MESSAGE_RESERVED_INT},
  {"AUTO_SWITCH_MODE",    MESSAGE_AUTO_SWITCH_MODE},  
  {"GAS_DAY",             MESSAGE_GAS_DAY},  
  {"BALANCE_PHONE",       MESSAGE_BALANCE_PHONE}, 
  {"MODE_TRANSFER",       MESSAGE_TCP_MODE_TRANSFER},
  {"SERVER_URL2",         MESSAGE_TCP_SERVER_URL2},
  {"SERVER_URL3",         MESSAGE_TCP_SERVER_URL3},
  {"SERVER_URL",          MESSAGE_TCP_SERVER_URL},
  {"LOCK_STATE",          MESSAGE_TCP_LOCK_STATE},
  {"APN_ADDRESS",         MESSAGE_TCP_APN_ADDRESS},
  {"APN_LOGIN",           MESSAGE_TCP_APN_LOGIN},
  {"APN_PASSWORD",        MESSAGE_TCP_APN_PASSWORD},  
  {"PABS",                MESSAGE_TCP_PABS},
  {"COUNT_SESSION",       MESSAGE_TCP_COUNT_SESSION},  
  {"ERROR_SESSION",       MESSAGE_TCP_ERROR_SESSION},
  {"MAX_SESSION",         MESSAGE_TCP_MAX_SESSION},  
  {"VALVE",               MESSAGE_TCP_VALVE},
  {"COUNT_FAIL_SIM",      MESSAGE_TCP_COUNT_FAIL_SIM},  
  {"COUNT_FAIL_SPEED",    MESSAGE_TCP_COUNT_FAIL_SPEED},  
  {"QMAX_WAR_CNT_MEAS",   MESSAGE_TCP_QMAX_WAR_CNT_MEAS}, 
  {"QMAX_MIN_WARNING",    MESSAGE_TCP_QMAX_MIN_WARNING},   
  {"TMAX_WARNING",        MESSAGE_TCP_TMAX_WARNING}, 
  {"TMIN_WARNING",        MESSAGE_TCP_TMIN_WARNING},    
  {"T_WARN_CNT",          MESSAGE_TCP_T_WARN_CNT},    
  {"N_GAS_REC_TIME",      MESSAGE_TCP_N_GAS_REC_TIME}, 
  {"GAS_RANGE",           MESSAGE_TCP_GAS_RANGE},    
  {"N_GAS_REC",           MESSAGE_TCP_N_GAS_REC},
  {"N_GAS_REC",           MESSAGE_TCP_N_GAS_REC},  
};

const uint8_t CNT_STR_TCP = sizeof(PtrParserCmdTCP)/sizeof(PtrParserCmdTCP[0]);

//SMT-G
const uint8_t SMT_type[] = "0053004d0054002d0047";
//���.��.��.-
const uint8_t tek_znach_[] = "002004420435043A002E0437043D002E04410447002E002D";
//��� ������
const uint8_t net_interneta_[] = "0020043D0435044200200438043D044204350440043D043504420430";
// ������ �� ���������
const uint8_t server_non_reply[] = "00200441043504400432043504400020043D04350020043E0442043204350447043004350442";
// ����� ���������
const uint8_t break_gprs[] = "0020043E04310440044B043200200438043D044204350440043D043504420430";
//���. ��� � ������
const uint8_t mode_time_in_week[] = "0020044004350436002E0020044004300437002004320020043D043504340435043B044E";
//���. ��� � �����
const uint8_t mode_time_in_month[] = "0020044004350436002E0020044004300437002004320020043C04350441044F0446";
// ������� �������
const uint8_t text_triggered_sensor[] = "00200421044004300431043E04420430043B002004340430044204470438043A0020";
// ������ ������� �������
const uint8_t text_end_triggered_sensor[] = "0020041E0442043C0435043D043000200442044004350432043E04330438002004340430044204470438043A04300020";
// ����
const uint8_t text_gas[] = "0433043004370430";
// ����������
const uint8_t text_smoke[] = "043704300434044B043C043B0435043D0438044F";
// �������
const uint8_t text_flames[] = "043F043B0430043C0435043D0438";
// �������� ����
const uint8_t text_carbon_monoxide[] = "0443043304300440043D043E0433043E00200433043004370430";
// �������� ����
const uint8_t text_water_leakage[] = "043F0440043E044204350447043A043800200432043E0434044B";
// 220V
const uint8_t text_220V[] = "0032003200300056";
// ����������� �������������
const uint8_t text_coolant_temperature[] = "04420435043C043F043504400430044204430440044B002004420435043F043B043E043D043E0441043804420435043B044F";
//�������� �������
const uint8_t text_gas_valve[] = "043304300437043E0432043E0433043E0020043A043B0430043F0430043D0430";


//_Setting Setting;
struct _FlagMessageTMR FlagMessageTMR;
//struct _Mode_Transfer Mode_Transfer;



extern TSens_Const Sensor_Const;
extern const TDevice Device_STM;
extern xQueueHandle xARCQueue;
extern SemaphoreHandle_t xMdmArcRecReadySemaphore;

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/

uint8_t ReadMessageTMR(char *ptrMessageTMR, uint8_t cnt_char_message, TypSourceMessageTMR sourceMessageTMR)
{
  /*
        if(ReadMessageTMR((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], 
                         GSMcnt_string_GSMModule[temp_number_indicator_message_for_gsm-1],
                         GSM_TCP)) return;
   return 1;  - ���� ���������� ������
   return 0;  - ���  ���������� ������  
  */
 // 
  char *ptr_chr_write;
  char *ptr_begin, *ptr_end;
  FlagMessageTMR.source_message = sourceMessageTMR;
  
  ptr_chr_write = strchr(ptrMessageTMR, '=');
  for(uint8_t i = 0; i < CNT_STR_TCP; i++)
  {
     ptr_begin = strstr(ptrMessageTMR, PtrParserCmdTCP[i].commandStr);
     if(ptr_begin != 0)
     {// �������
       
       if(PtrParserCmdTCP[i].message_TCP == MESSAGE_TCP_ARCHIVE)
         strcpy((char*)&FlagMessageTMR.buf_message_tcp[0], ptr_begin + 7);
       if(PtrParserCmdTCP[i].message_TCP == MESSAGE_TCP_KEY)
       {
         ptr_begin = strchr(ptrMessageTMR, '"') + 1;
         ptr_end = strchr(ptr_begin, '"');
         memcpy((char*)&FlagMessageTMR.buf_message_tcp[0], ptr_begin, ptr_end - ptr_begin);
       }
       if(ptr_chr_write != 0)
       {// ������� �� ������
         ++ptr_chr_write;
         strcpy((char*)&FlagMessageTMR.buf_message_tcp[0], ptr_chr_write);
         FlagMessageTMR.command = PtrParserCmdTCP[i].message_TCP + 1;
       }
       else
         FlagMessageTMR.command = PtrParserCmdTCP[i].message_TCP;
       
       return 1;
     }
  }
  return 0;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
_ResultReply TransmitGROUP_INFO()
{
   _ResultReply  resultReply;
   resultReply.end_part_message = _FALSE;
   resultReply.end_message = _FALSE;
   memset((char*)FlagMessageTMR.buf_message_tcp, 0, sizeof(FlagMessageTMR.buf_message_tcp));
   ++cnt_str_transmit;
   FormationGROUP_INFO(FlagMessageTMR.buf_message_tcp, cnt_str_transmit);
   if(cnt_str_transmit == 0 || cnt_str_transmit > 7)
   {
       resultReply.end_part_message = _OK;
       resultReply.end_message = _OK;
       strcat((char*)FlagMessageTMR.buf_message_tcp,"\r\n");
       cnt_str_transmit = 0;
   }
   return resultReply;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
_ResultReply TransmitGSM_INFO(uint8_t *ptr_buf, uint8_t part)
{

   _ResultReply  resultReply;
   resultReply.end_part_message = _FALSE;
   resultReply.end_message = _FALSE;
   uint8_t flag_CRC32;
   
   memset((char*)ptr_buf, 0, strlen((char*)ptr_buf));
   //++cnt_str_transmit;
   
   switch(part)
   {
   case 1:
//        sprintf((char*)ptr_buf,"{%s;%s;%d;",
//                                                    //  GSM_network_name,
//                                                    //  CCID_SIM_card,
//                                                      
//                                                       GSM_Get_ptr_network_name(),
//                                                       GSM_Get_ptr_CCID_SIM_card()
//                                                       GSM_Get_level_gsm_signal()
//                                                       );
//        flag_CRC32 = CRC_CTART;
        break;
   case 2:
        sprintf((char*)ptr_buf,"%s}", GSM_Get_ptr_message_SIM_card_balance());
        flag_CRC32 = CRC_STOP;
        resultReply.end_part_message = _OK;
        resultReply.end_message = _OK;          
        break;
   default:
        strcat((char*)ptr_buf,"\r\n\x1A");
        resultReply.end_part_message = _OK;
        resultReply.end_message = _OK;
     break;
   }
   if(flag_CRC32 != CRC_NON)
   {
      uint32_t rez_crc32;
      uint8_t *ptr_str;
      rez_crc32 = CalculateCRC32(ptr_buf, strlen((char*)ptr_buf), flag_CRC32);
      if(flag_CRC32 == CRC_STOP)
      {
        ptr_str = ptr_buf + strlen((char*)ptr_buf);
        sprintf((char*)ptr_str, "\t%08X\r\n\x1A", rez_crc32);
      }
   }
   return resultReply;
}
/*!
Read record without compare through queue
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/         
_ResultReply TransmitARCHIVE(uint8_t *ptr_buf_in, uint8_t *ptr_buf_out, uint32_t size_buf_out)
{
   char *end_ptr, *beg_ptr;
   uint8_t flag_CRC32;
   
   _ResultReply resultReply;
   ++cnt_str_transmit;
   if(cnt_str_transmit == 1)
   {
     flag_detection_archive = (TArchiv_type)strtoul((char*)(ptr_buf_in + 1), &end_ptr, 10);
     numFindArcBeg = 1;
     switch(flag_detection_archive)
     {
     case  TYPE_ARCHIVE_HOURLY_TIME:
     case  TYPE_ARCHIVE_HOURLY_NUMBER:       
           if(IntArcRecNo > 744 )
              numFindArcBeg = IntArcRecNo - 744;
           numFindArcEnd = IntArcRecNo - 1;
           break;
     case  TYPE_ARCHIVE_DAILY_TIME:
     case  TYPE_ARCHIVE_DAILY_NUMBER:       
           if(IntArcRecNo > 366 )
              numFindArcBeg = DayArcRecNo - 366;
           numFindArcEnd = DayArcRecNo - 1;
           break;
     case  TYPE_ARCHIVE_CHANG_TIME:
     case  TYPE_ARCHIVE_CHANG_NUMBER:       
           if(ChangeArcRecNo > 300 )
              numFindArcBeg = ChangeArcRecNo - 300;
           numFindArcEnd = ChangeArcRecNo - 1;
           break;
   //  case  TYPE_ARCHIVE_EVENT_TIME:
   //  case  TYPE_ARCHIVE_EVENT_NUMBER:       
   //        if(EvArcRecNo > 300 )
   ///           numFindArcBeg = EvArcRecNo - 300;
  //         numFindArcEnd = EvArcRecNo - 1;
  //        break;  
     case  TYPE_ARCHIVE_TELEMETRY_TIME:
     case  TYPE_ARCHIVE_TELEMETRY_NUMBER:       
           if(TelemetryArcRecNo > 890 )
              numFindArcBeg = TelemetryArcRecNo - 890;
           numFindArcEnd = TelemetryArcRecNo - 1;
           break;       
     case  TYPE_ARCHIVE_SYSTEM_TIME:
     case  TYPE_ARCHIVE_SYSTEM_NUMBER:       
           if(SystemArcRecNo > 890 )
              numFindArcBeg = SystemArcRecNo - 890;
           numFindArcEnd = SystemArcRecNo - 1;
           break;           
           
     default:break;
     }     
     
     if(flag_detection_archive == TYPE_ARCHIVE_HOURLY_TIME 
        || flag_detection_archive == TYPE_ARCHIVE_DAILY_TIME
        || flag_detection_archive == TYPE_ARCHIVE_CHANG_TIME
      //  || flag_detection_archive == TYPE_ARCHIVE_EVENT_TIME
        || flag_detection_archive == TYPE_ARCHIVE_SYSTEM_TIME)
     {
        LL_RTC_DateTypeDef lo_ArcDate;   
        LL_RTC_TimeTypeDef lo_ArcTime;
        
        lo_ArcDate.Day = strtoul((char*)(ptr_buf_in + 3), &end_ptr, 10);
        lo_ArcDate.Month = strtoul((char*)(ptr_buf_in + 6), &end_ptr, 10);
        lo_ArcDate.Year = strtoul((char*)(ptr_buf_in + 9), &end_ptr, 10);
        lo_ArcTime.Hours = strtoul((char*)(ptr_buf_in + 12), &end_ptr, 10);
        lo_ArcTime.Minutes = strtoul((char*)(ptr_buf_in + 15), &end_ptr, 10);
        lo_ArcTime.Seconds = strtoul((char*)(ptr_buf_in + 18), &end_ptr, 10);     
        TimeFindUnixBegin = Get_IndexTime(lo_ArcDate, lo_ArcTime);
 
        lo_ArcDate.Day = strtoul((char*)(ptr_buf_in + 21), &end_ptr, 10);
        lo_ArcDate.Month = strtoul((char*)(ptr_buf_in + 24), &end_ptr, 10);
        lo_ArcDate.Year = strtoul((char*)(ptr_buf_in + 27), &end_ptr, 10);
        lo_ArcTime.Hours = strtoul((char*)(ptr_buf_in + 30), &end_ptr, 10);
        lo_ArcTime.Minutes = strtoul((char*)(ptr_buf_in + 33), &end_ptr, 10);
        lo_ArcTime.Seconds = strtoul((char*)(ptr_buf_in + 36), &end_ptr, 10);     
        TimeFindUnixEnd = Get_IndexTime(lo_ArcDate, lo_ArcTime);
        

     }
     else
     {// old type archive
         uint32_t numFindArcEnd_buf;
         
         ptr_buf_in = (uint8_t*)strchr((char*)ptr_buf_in, ';') + 1;
         numFindArcBeg = strtoul((char*)ptr_buf_in, &end_ptr, 10);
         beg_ptr = end_ptr + 1;
         numFindArcEnd_buf = strtoul(beg_ptr, &end_ptr, 10);
         
         if(numFindArcEnd_buf < numFindArcEnd)
           numFindArcEnd = numFindArcEnd_buf;
         
         TimeFindUnixBegin = 0;
         TimeFindUnixEnd = 0;
     }
     memset((char*)ptr_buf_out, 0, size_buf_out);
     *ptr_buf_out = '{';
     resultReply.end_part_message = _FALSE;
     resultReply.end_message = _FALSE;
     flag_CRC32 = CRC_CTART;
   //  return resultReply;
   }
   //----------------------------------------------------------------------------------------------
   if(cnt_str_transmit > 1)
   {
      uint8_t result;
      uint16_t strlen_buf;
      
      while(1)
      {
         memset((char*)ptr_buf_out, 0, size_buf_out);
         if(numFindArcBeg <= numFindArcEnd)
            result = ReadArchiveQueue(flag_detection_archive, numFindArcBeg, TimeFindUnixBegin, TimeFindUnixEnd, ptr_buf_out);
         else
         {
            result = ARC_READ_STATUS_END_FIND;
            break;
         }
         ++numFindArcBeg;
         //--------------------------------------------------------------------------      
         if(result == ARC_READ_STATUS_TIME_FAIL_ERROR)
         {
           result = ARC_READ_STATUS_END_FIND;
           break;
         }
         //--------------------------------------------------------------------------
         if(result == ARC_READ_STATUS_IN_RANGE_OK
            || (TimeFindUnixBegin == 0 && TimeFindUnixEnd == 0 && result == ARC_READ_STATUS_CRC_ERROR))
         {
            strlen_buf = strlen((char*)ptr_buf_out);
            if(strlen_buf < 1000)
            {
               resultReply.end_part_message = _FALSE;
               resultReply.end_message = _FALSE;
            }
            else
            {
               resultReply.end_part_message = _OK;
               resultReply.end_message = _FALSE;
               memset((char*)ptr_buf_out, 0, size_buf_out);
               --numFindArcBeg;
            }
            break;
         }
      }        
      //--------------------------------------------------------------------------
      if(result == ARC_READ_STATUS_END_FIND)
      {
         strcat((char*)ptr_buf_out, "}");
         resultReply.end_part_message = _OK;
         resultReply.end_message = _OK;
         flag_CRC32 = CRC_STOP;
      }    
      else
        flag_CRC32 = CRC_IN_PROCESSING;
   }
   if(flag_CRC32 != CRC_NON)
   {
      uint32_t rez_crc32;
      uint8_t *ptr_str;
      rez_crc32 = CalculateCRC32(ptr_buf_out, strlen((char*)ptr_buf_out), flag_CRC32);
      if(flag_CRC32 == CRC_STOP)
      {
        ptr_str = ptr_buf_out + strlen((char*)ptr_buf_out);
        sprintf((char*)ptr_str, "\t%08X\r\n", rez_crc32);
      }
   }
   return resultReply; 
}

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
/*
void sprintfArchive(uint8_t typeArchive, uint8_t flagBegin)
{
  uint8_t index=0;
   if(flagBegin == _OK)
     FlagMessageTMR.buf_message_tcp[index++] = '{';
   switch(typeArchive)
   {
   case TYPE_ARCHIVE_HOURLY:
   case TYPE_ARCHIVE_DAILY:
                 sprintf((char*)&FlagMessageTMR.buf_message_tcp[index],
                                                          "%d;%02d.%02d.%d,%02d:00:00;%d;%08X;%0.2f;%d\r\n", 
                                                          frameArchiveInt.ArcRecNo,                 
                                                          frameArchiveInt.ArcDay, frameArchiveInt.ArcMon, frameArchiveInt.ArcYear,
                                                          frameArchiveInt.ArcHour, 
                                                          (uint32_t)(frameArchiveInt.ArcVolume * 1000),
                                                       //   frameArchive.ArcFlag // 
                                                          frameArchiveInt.ArcStatus, //
                                                          frameArchiveInt.ArcTemp,
                                                          frameArchiveInt.ArcK
                                                            
                                                                                         );
                 break;
   case TYPE_ARCHIVE_EVENT:
                   sprintf((char*)&FlagMessageTMR.buf_message_tcp[index],
                                                          "%d;%02d.%02d.%04d,%02d:%02d:%02d;%02d.%02d.%04d,%02d:%02d:%02d;%d;%d;%d;%08X\r\n", 
                                                          frameArchiveEvent.ArcRecNo,                 
                                                          frameArchiveEvent.TimeBegine.Day, frameArchiveEvent.TimeBegine.Mon, frameArchiveEvent.TimeBegine.Year,
                                                          frameArchiveEvent.TimeBegine.Hour, frameArchiveEvent.TimeBegine.Minute, frameArchiveEvent.TimeBegine.Second,
                                                          frameArchiveEvent.TimeEnd.Day, frameArchiveEvent.TimeEnd.Mon, frameArchiveEvent.TimeEnd.Year,
                                                          frameArchiveEvent.TimeEnd.Hour, frameArchiveEvent.TimeEnd.Minute, frameArchiveEvent.TimeEnd.Second,                                                          
                                                          frameArchiveEvent.Event_Code,
                                                          frameArchiveEvent.Event_Result,
                                                          frameArchiveEvent.Sesion_Number, //
                                                          frameArchiveEvent.Status
                                                            
                                                                                         );       
        break;
   case TYPE_ARCHIVE_CHANG:
                   sprintf((char*)&FlagMessageTMR.buf_message_tcp[index],
                                                          "%d;%02d.%02d.%04d,%02d:%02d:%02d;%d;%d;%d;%s;%s\r\n", 
                                                          frameArchiveChange.ArcRecNo,                 
                                                          frameArchiveChange.Time.Day, frameArchiveChange.Time.Mon, frameArchiveChange.Time.Year,
                                                          frameArchiveChange.Time.Hour, frameArchiveChange.Time.Minute, frameArchiveChange.Time.Second,                                                     
                                                          frameArchiveChange.ArcCLB_Lock,
                                                          frameArchiveChange.ID_Change,
                                                          frameArchiveChange.ID_Param, //
                                                          frameArchiveChange.OldValue,
                                                          frameArchiveChange.NewValue
                                                            
                                                                                         );       
        break;        
   default:
        //sprintf((char*)&FlagMessageTMR.buf_message_tcp[0], "{}\r\n\x1A");
        sprintfEndMessage(STR_ALL_END);
        break;
     
   }
}
*/

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_RESERVED_INT_SET(char *ptr_buf, uint8_t checkcrc32)
{
   char* end_ptr;
   uint8_t rez = _FALSE;
   uint32_t reserved_int_buf;
   if(checkcrc32 == _OK)
   {
      reserved_int_buf = strtoul(ptr_buf, &end_ptr, 10);
      if(reserved_int_buf > 599 && reserved_int_buf < 86401)
      {
         sprintf((char*)OldValue,"%d", reserved_int);  

         Eeprom_WriteDword((uint32_t)&reserved_int, reserved_int_buf);
       
         sprintf((char*)NewValue,"%d", reserved_int); 
         WriteChangeArcRecord(CHANGE_RESERVED_INT, CHANGE_TCP); 
         rez = _OK;
      }
   }
   return rez;
}  
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_AUTO_SWITCH_MODE_SET(uint8_t checkcrc32)
{
   uint8_t rez, buf;
   if((FlagMessageTMR.buf_message_tcp[0] == '0' || FlagMessageTMR.buf_message_tcp[0] == '1') && checkcrc32 == _OK)
   {
      buf = FlagMessageTMR.buf_message_tcp[0] - 0x30;
         
      sprintf((char*)OldValue, "%d", auto_switching_mode);  

      Eeprom_WriteChar((uint32_t)&auto_switching_mode, buf);
       
      sprintf((char*)NewValue, "%d", auto_switching_mode);  
      WriteChangeArcRecord(CHANGE_AUTO_MODE, CHANGE_TCP);
      rez = _OK;
   }
   else
      rez = _FALSE;      

  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_GAS_DAY_SET(uint8_t checkcrc32)
{
   char *end_ptr/*, *ptr_begin*/;
   uint8_t rez, buf;
  
   buf = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   if(buf < 24 && checkcrc32 == _OK)
   {
      sprintf((char*)OldValue, "%d", GasDayBorder);  

      Eeprom_WriteWord((uint32_t)&GasDayBorder, (uint16_t)buf);
       
      sprintf((char*)NewValue, "%d", GasDayBorder);  
      WriteChangeArcRecord(CHANGE_GAS_DAY, CHANGE_TCP); 
      rez = _OK;
   }
   else
     rez = _FALSE;
  return rez;
}  

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_BALANCE_PHONE_SET(uint8_t checkcrc32)
{
   uint8_t rez;

   if(strlen((char*)&FlagMessageTMR.buf_message_tcp[0]) < sizeof(MdmNumberBalans) && checkcrc32 == _OK)
   {
         sprintf((char*)OldValue, "%s", MdmNumberBalans);  

         Eeprom_WriteString((uint8_t*)MdmNumberBalans,(uint8_t*)&FlagMessageTMR.buf_message_tcp[0], strlen((char*)&FlagMessageTMR.buf_message_tcp[0]));
       
         sprintf((char*)NewValue, "%s", MdmNumberBalans); 
         WriteChangeArcRecord(CHANGE_BALANCE_NUMBER, CHANGE_TCP);   
         rez = _OK;
   }
   else
      rez = _FALSE;  


  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_MODE_TRANSFER_SET(uint8_t checkcrc32)
{
   char *end_ptr, *begin_ptr, rez = 0;
   uint8_t buf_mode, buf_hour, buf_day, buf_minute;

   buf_mode = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   if(buf_mode ==2 || buf_mode == 4 || buf_mode == 5)
       //Setting.Mode_Transfer.mode = buf;
     __NOP();
   else
   {
      rez = 1;
   }
   //---
   begin_ptr = end_ptr;
   buf_hour = strtoul(begin_ptr+1, &end_ptr, 10);  
   if(buf_hour <= 23)
     // Setting.Mode_Transfer.hour = buf; 
      __NOP();
   else
   {
       rez += 2;
   }
   //---
   begin_ptr = end_ptr;
   buf_minute = strtoul(begin_ptr+1, &end_ptr, 10);
   if(buf_minute <= 59)
      //Setting.Mode_Transfer.minute = buf;
      __NOP();
   else
   {
       rez += 4;
   }
   //---
   begin_ptr = end_ptr;
   buf_day = strtoul(begin_ptr+1, &end_ptr, 10);
   if(buf_day == 0)
     buf_day = 1;
   if(   /*(buf_mode == 3 && buf_day > 0 && buf_day < 8)*/
         (buf_mode == 4 && buf_day > 0 && buf_day < 29)
      || (buf_mode == 2 && buf_day > 0 && buf_day < 9)
      || (buf_mode == 5 && buf_day > 0 && buf_day < 9))
      //Setting.Mode_Transfer.day = buf; 
      __NOP();
   else
   {
       rez += 8;
   }
   if(rez == 0 && checkcrc32 == _OK)
   {
  /*    if(StatusDevice.connection_limit_100
         || (StatusDevice.connection_limit_300
              && buf_mode == 2))
      {
         rez = _FALSE;
      }
      else*/
  //    {
         if(buf_mode == 2)
            cnt_seans_TCP_mode_2 = 0;
         
         sprintf((char*)OldValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  

         Eeprom_WriteChar((uint32_t)&Mode_Transfer.mode, buf_mode);
         Eeprom_WriteChar((uint32_t)&Mode_Transfer.hour, buf_hour);
         Eeprom_WriteChar((uint32_t)&Mode_Transfer.minute, buf_minute);
         Eeprom_WriteChar((uint32_t)&Mode_Transfer.day, buf_day);   
       
         sprintf((char*)NewValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  
         WriteChangeArcRecord(CHANGE_TRANSFER_MODE, CHANGE_TCP);     
         
         
         rez = _OK;
    //  }
   }
   else
   {
     rez = _FALSE;
   }
   
  
   return rez;
}   

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_SERVER_URL_SET(uint8_t index, uint8_t checkcrc32)
{
   uint8_t rez = _FALSE;
   if(checkcrc32 == _OK)
      rez = ChangeURLPortServer(&FlagMessageTMR.buf_message_tcp[0], index, CHANGE_TCP);
  return rez;
}   
/*!
Edite parametr setting server
\param[in] pointer string
\param[in] number server
\param[in] source change
\return _OK or _FALSE
*/  
uint8_t ChangeURLPortServer(uint8_t *ptr_str, uint8_t index_server, uint8_t source)
{
   char *end_ptr, *ptr_begin;
   uint8_t rez;

   ptr_begin = (char*)ptr_str;
   end_ptr = strchr((char*)ptr_str, ':');
   
   if(end_ptr != 0 && (end_ptr - ptr_begin) < SIZE_MdmURL_Server)
   {
      uint8_t str_len_port;
      str_len_port = strlen((char*)end_ptr + 1);
      if(*(end_ptr + str_len_port) == 0x0A)
        str_len_port -= 2;
      sprintf((char*)OldValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]);  

      Eeprom_WriteString((uint8_t*)&MdmURL_Server[index_server][0], ptr_str, end_ptr - ptr_begin);
      Eeprom_WriteString((uint8_t*)&MdmPort_Server[index_server][0], (uint8_t*)end_ptr + 1, str_len_port);
       
      sprintf((char*)NewValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]); 
      WriteChangeArcRecord((TCHANG_Type)(CHANGE_TCP_SERVER1 + index_server), source);   
     
      rez = _OK;
      if(source == CHANGE_TCP)
         flag_change_server_url_valve = _OK;
   }
   else
      rez = _FALSE;   
   if(end_ptr == 0)
   {
      sprintf((char*)OldValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]);  

      Eeprom_WriteChar((uint32_t)&MdmURL_Server[index_server][0], 0);
      Eeprom_WriteChar((uint32_t)&MdmPort_Server[index_server][0], 0);
       
      sprintf((char*)NewValue,"%s:%s", &MdmURL_Server[index_server][0], &MdmPort_Server[index_server][0]); 
      WriteChangeArcRecord((TCHANG_Type)(CHANGE_TCP_SERVER1 + index_server), source);   
     
      rez = _OK;
   }
  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_LOCK_STATE_SET(uint8_t checkcrc32)
{
  uint8_t rez = _FALSE;
  if(checkcrc32 == _OK)
  {
    //    sprintf((char*)OldValue,"%d", CLB_lock);  

        CLB_lock = 0;
        
     //   sprintf((char*)NewValue, "%d", CLB_lock); 
     //  WriteChangeArcRecord(CHANGE_CLB_LOCK, CHANGE_TCP); 
        source_change_CLB_LOCK = CHANGE_TCP;
        rez = _OK;
  }
   return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_PABS(uint8_t checkcrc32)
{
    double pressure_abs_buf;

        sscanf((char*)&FlagMessageTMR.buf_message_tcp[0],"%lf", &pressure_abs_buf);
        if(pressure_abs_buf >= 96.325 && pressure_abs_buf <= 106.325 && checkcrc32 == _OK)
        {
           sprintf((char*)OldValue,"%f", pressure_abs);
           Eeprom_WriteDouble((uint32_t)&pressure_abs, pressure_abs_buf);
           sprintf((char*)NewValue,"%f", pressure_abs);
           WriteChangeArcRecord(CHANGE_PRESSURE_ABS, CHANGE_TCP); 
           return _OK;
        }
        else
           return _FALSE;
}

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_APN_SET(TCHANG_Type MdmAPN, uint8_t checkcrc32)
{
  
   uint8_t strlen_;
   uint8_t *ptrAPN;
   uint8_t sizeAPN;
   switch(MdmAPN)
   {
   case CHANGE_APN_ADDRESS:
        ptrAPN = (uint8_t*)MdmAPN_Adress;
        sizeAPN = sizeof(MdmAPN_Adress);
        break;
   case CHANGE_APN_LOGIN:
        ptrAPN = (uint8_t*)MdmAPN_Login;
        sizeAPN = sizeof(MdmAPN_Login);
        break;     
   case CHANGE_APN_PASWORD:
        ptrAPN = (uint8_t*)MdmAPN_Password;
        sizeAPN = sizeof(MdmAPN_Password);
        break;      
   default: return _FALSE; break;
   }
   
   strlen_ = strlen((char*)&FlagMessageTMR.buf_message_tcp[0]);
   if(strlen_ < sizeAPN && checkcrc32 == _OK)
   {

       sprintf((char*)OldValue,"%s", ptrAPN);  

       Eeprom_WriteString((uint8_t*)ptrAPN,(uint8_t*)&FlagMessageTMR.buf_message_tcp[0], strlen_);
       
       sprintf((char*)NewValue,"%s", ptrAPN); 
       WriteChangeArcRecord(MdmAPN, CHANGE_TCP); 

       return _OK;
   }
   else
       return _FALSE;
}                     
                     
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_SMS_PHONE1_SET()
{
   char *ptr_begin;
   uint8_t rez , cnt_char;

   ptr_begin = (char*)&FlagMessageTMR.buf_message_tcp[0];
   cnt_char = strlen(ptr_begin);
   if(cnt_char < sizeof(telephone_number_SMS1))
   {
      sprintf((char*)OldValue,"%s", telephone_number_SMS1);  

      Eeprom_WriteString((uint8_t*)&telephone_number_SMS1, (uint8_t*)ptr_begin, cnt_char);
       
      sprintf((char*)NewValue,"%s", telephone_number_SMS1); 
      WriteChangeArcRecord(CHANGE_SMS_NUMBBER, CHANGE_TCP);   

      rez = _OK;
      flag_change_server_url_valve = _OK;
   }
   else
      rez = _FALSE;

  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_SMS_PHONE2_SET()
{
   char *ptr_begin;
   uint8_t rez , cnt_char;

   ptr_begin = (char*)&FlagMessageTMR.buf_message_tcp[0];
   cnt_char = strlen(ptr_begin);
   if(cnt_char < sizeof(telephone_number_SMS2))
   {
      sprintf((char*)OldValue,"%s", telephone_number_SMS2);  

      Eeprom_WriteString((uint8_t*)&telephone_number_SMS2, (uint8_t*)ptr_begin, cnt_char);
       
      sprintf((char*)NewValue,"%s", telephone_number_SMS2); 
      WriteChangeArcRecord(CHANGE_SMS_NUMBBER2, CHANGE_TCP);   

      rez = _OK;
      flag_change_server_url_valve = _OK;
   }
   else
      rez = _FALSE;

  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_COUNT_FAIL_SIM_SET(uint8_t checkcrc32)
{
   uint8_t rez;
   uint16_t buf;
   char *end_ptr;
   
   buf = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   
   if(buf < 65000 && checkcrc32 == _OK)
   {
         ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, buf, cnt_fail_speed_gsm_modem_eeprom); // cnt_fail_sim_card_eeprom
         rez = _OK;
   }
   else
      rez = _FALSE;  
  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_COUNT_FAIL_SPEED_SET(uint8_t checkcrc32)
{
   uint8_t rez;
   uint16_t buf;
   char *end_ptr;
   
   buf = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   
   if(buf < 65000 && checkcrc32 == _OK)
   {
         ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, cnt_fail_sim_card_eeprom, buf); // cnt_fail_sim_card_eeprom
         rez = _OK;
   }
   else
      rez = _FALSE;  
  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
void CalculationNextCommunicationSession()
{
   if(Mode_Transfer.mode == MODE_ONCE_AN_HOUR)
     timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
   //----------------------------------
   if(Mode_Transfer.mode == MODE_ONCE_A_DAY)
     ++cnt_seans_TCP_mode_2;   
   //----------------------------------
   if(successful_transfer_data_to_server == _OK)
   {
      flagPowerONGSMOneTime = 0;
      cnt_GSM_TCP_Connect_FALSE = 0;
      switch(Mode_Transfer.mode)
      {
      case MODE_ONCE_A_DAY: 
           CalculationTimeOnceADay();
           break;
      case MODE_ONCE_A_DECADE:
           CalculationTimeOnceADecade();
           break;           
      case MODE_ONCE_A_MONTH: 
           CalculationTimeOnceAMonth();
           break;
      default:break;
      }
   }
   else
   {
     if(flagPowerONGSMOneTime == 1)
     {
        timeUnixGSMNextConnect = timeGSMUnixStart;
     }
     else
     {
       flagPowerONGSMOneTime = 0;
    
       ++cnt_GSM_TCP_Connect_FALSE;
       switch(Mode_Transfer.mode)
       {
       case MODE_ONCE_A_DAY: 
           if(cnt_GSM_TCP_Connect_FALSE > 1)
           {
              CalculationTimeOnceADay();
           }
           else
           {
             timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
           }
           
           if(cnt_GSM_TCP_Connect_FALSE > 5 && auto_switching_mode)
           {           
              change_transfermode_to_5(CHANGE_PROGRAM);

              cnt_GSM_TCP_Connect_FALSE = 0;
              CalculationTimeOnceADecade();
           }
           break;
       case MODE_ONCE_A_DECADE:
           if(cnt_GSM_TCP_Connect_FALSE > 2)
           {
              CalculationTimeOnceADecade();
           }
           else
           {
             timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
           }           
           break;           
       case MODE_ONCE_A_MONTH: 
           if(cnt_GSM_TCP_Connect_FALSE > 4)
           {
              CalculationTimeOnceAMonth();
           }
           else
           {
             timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
           }               
           break;
       default:break;
       }     
     }
   }
   
   if(flag_change_server_url_valve == _OK)
   {
     flag_change_server_url_valve = _FALSE;
     timeUnixGSMNextConnect = timeGSMUnixStart;
   }
   
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/       
void  CalculationTimeOnceADay()
{
   struct tm lo_tm;
   memcpy(&lo_tm, gmtime((time_t*)&timeGSMUnixStart), sizeof(lo_tm));
   lo_tm.tm_hour = Mode_Transfer.hour;
   lo_tm.tm_min = Mode_Transfer.minute;
   lo_tm.tm_sec = 0;
   timeUnixGSMNextConnect = mktime(&lo_tm) + 86400;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/       
void  CalculationTimeOnceADecade()
{
     LL_RTC_DateTypeDef lo_ArcDate;   
     LL_RTC_TimeTypeDef lo_ArcTime;
     uint64_t Ts;
     uint64_t T;
     
     GetDate_Time(&lo_ArcDate, &lo_ArcTime);
     lo_ArcTime.Minutes = Mode_Transfer.minute;
     lo_ArcTime.Hours = Mode_Transfer.hour;     
     lo_ArcDate.Day = Mode_Transfer.day;
     Ts = Get_IndexTime(lo_ArcDate, lo_ArcTime);
     
     for(uint8_t i = 0; i < 3; i++)
     {
        T = Ts + 864000 * i;
        if(T < timeSystemUnix)
        {
          if(i == 2)
          {
            ++lo_ArcDate.Month;
            if(lo_ArcDate.Month == 13)
            {
              lo_ArcDate.Month = 1;
              ++lo_ArcDate.Year;
            }
            timeUnixGSMNextConnect = Get_IndexTime(lo_ArcDate, lo_ArcTime);
          }
        }
        else
        {
           timeUnixGSMNextConnect = T;
           break;
        }
     }
}
              /**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/       
void  CalculationTimeOnceAMonth()
{
     LL_RTC_DateTypeDef lo_ArcDate;   
     LL_RTC_TimeTypeDef lo_ArcTime;
     
     GetDate_Time(&lo_ArcDate, &lo_ArcTime);
     lo_ArcTime.Minutes = Mode_Transfer.minute;
     lo_ArcTime.Hours = Mode_Transfer.hour;     
     lo_ArcDate.Day = Mode_Transfer.day;
     
     ++lo_ArcDate.Month;
     if(lo_ArcDate.Month == 13)
     {
       lo_ArcDate.Month = 1;
       ++lo_ArcDate.Year;
     }
     timeUnixGSMNextConnect = Get_IndexTime(lo_ArcDate, lo_ArcTime);
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/  
void Response_TCP(uint8_t response)
{
   memset((char*)FlagMessageTMR.buf_message_tcp, 0, sizeof(FlagMessageTMR.buf_message_tcp));
   if(response == _OK)
      strcat((char*)FlagMessageTMR.buf_message_tcp,"OK\tD736D92D\r\n\x1A");
   else
      strcat((char*)FlagMessageTMR.buf_message_tcp,"ERROR\tAA897EC1\r\n\x1A");
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/  
void TransmitDebugMessageOptic(const uint8_t *ptr_message)
{
   if(OptoPort == OPTO_PORT_ENABLE /*&& FlagMessageTMR.command != MESSAGE_TCP_ARCHIVE */ &&  !NoLogToOptik )
   {
      if(strlen((char*)ptr_message) > TX_BUFFER_MAX_SIZE-1)
        return;
      for(uint32_t i = 0; i < 0xFFFFF; i++)
      {
        if(TransmitEnable == TRANSMIT_ENABLE)
        {
           sprintf((char*)TransmitBuffer,"LORA %s\r\n", ptr_message);
           Opto_Uart_Transmit(); 
           return;
        }
      }
   }
   else
     osDelay(5);
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/  
void WriteTelemetry(uint16_t p_Event_Code, int8_t p_Event_Result, uint16_t p_Sesion_Number, uint8_t type_record)
{
 //  LL_RTC_DateTypeDef lo_ArcDate;   
 //  LL_RTC_TimeTypeDef lo_ArcTime; 
   
 //  GetDate_Time(&lo_ArcDate,&lo_ArcTime);  
 //  code_event_buf = p_Event_Code;
   if(type_record == EVENT_BEGIN)
   {
 //     code_event_buf = p_Event_Code;
      EventTimeBegin = timeSystemUnix;
   }
   if(type_record == EVENT_END)
   {
      EventTimeEnd = timeSystemUnix;
   //   if(p_Event_Code == 0)
   //     p_Event_Code = code_event_buf;
      WriteTelemetryArcRecord(p_Event_Code, p_Event_Result, p_Sesion_Number);
   }
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/  
uint8_t QueueSendToBack(uint8_t* ptr_buf_arh, uint8_t _flag_detection_archive, uint32_t _numberArcRecFind)
{
  /*
     TARC_Queue lo_ArcRec_Queue;   // ������� �������   
     
          lo_ArcRec_Queue.Arc_Buffer = ptr_buf_arh; // frameArchiveInt
          lo_ArcRec_Queue.Arc_RW = ARC_READ;
          lo_ArcRec_Queue.Arc_Type = (TArchiv_type) _flag_detection_archive ; // flag_detection_archive
          lo_ArcRec_Queue.Arc_RecNo = _numberArcRecFind;
          lo_ArcRec_Queue.Arc_Source = SOURCE_MDM;
          xQueueSendToBack(xARCQueue, &lo_ArcRec_Queue, 1000);  
    
          if(xSemaphoreTake(xMdmArcRecReadySemaphore, 10000) == pdFALSE)
          {
              return _FALSE;             
          }
  */
          return _OK;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/ 
void FormationGROUP_INFO(uint8_t *ptr_buf, uint8_t part)
{
   uint8_t flag_CRC32;
   switch(part)
   {
   case 1:
        {
           uint32_t cntGas;
           uint32_t *ptrStatus;
           
           cntGas = (uint32_t)(GasMeter.VE_Lm3 * 1000);
           GetDate_Time(&SystDate,&SystTime);
           
           ptrStatus = (uint32_t*)&StatusDevice_old_connect_TCP;
           sprintf((char*)ptr_buf,"{%08X;1000;%lu;%02d.%02d.%02d,%02d:%02d:%02d;", 
                                                        *ptrStatus,
                                                        // 1000 - ��� ��������
                                                        cntGas,
                                                        SystDate.Day, SystDate.Month, SystDate.Year, 
                                                        SystTime.Hours, SystTime.Minutes, SystTime.Seconds);
           flag_CRC32 = CRC_CTART;
        }
        break;
   case 2:
        sprintf((char*)ptr_buf,"%s;%s;%s;%s:%s;", (uint8_t*)MdmAPN_Adress, 
                                            (uint8_t*)MdmAPN_Login, 
                                            (uint8_t*)MdmAPN_Password,
                                            (uint8_t*)MdmURL_Server,
                                            (uint8_t*)MdmPort_Server);
        flag_CRC32 = CRC_IN_PROCESSING;
        break;
   case 3:
        sprintf((char*)ptr_buf,"%s;%s;%d,",  telephone_number_SMS1,
                                           (uint8_t*)MdmNumberBalans,
                                           Mode_Transfer.mode);
        flag_CRC32 = CRC_IN_PROCESSING;
        break;
   case 4:
        sprintf((char*)ptr_buf,"%02d:%02d,%d;4000;%.0f;%d;%d;%d;%d;", 
                                                            
                                                           Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day,
                                                           //    (uint16_t)battery_voltage_mv_main,
                                                           //   (uint16_t)battery_voltage_mv_rezerv,
                                                           GasMeter.var_T,
                                                           reserved_int,
                                                           max_cnt_communication_gsm,
                                                         //  GSM_Get_current_communication_gsm_number(),
                                                           current_communication_gsm_number_eeprom,
                                                           number_communication_gsm_fail_eeprom
                                                           );
        flag_CRC32 = CRC_IN_PROCESSING;
        break;
   case 5:
        sprintf((char*)ptr_buf,"%d;%d;%d;%d;%d;%d;%d;" , 
                                                                GasDayBorder,
                                                                hw_version,
                                                                auto_switching_mode,
                                                                ChangeArcRecNo - 1,
                                                                SystemArcRecNo - 1,
                                                                IntArcRecNo - 1,
                                                                DayArcRecNo - 1
                                                                );
        flag_CRC32 = CRC_IN_PROCESSING;
        break;
   case 6:
     sprintf((char*)ptr_buf,"%f;%d;%d;%d;%d;%d" , 
                                                          pressure_abs,
                                                          valve_resolution,
                                                          valve_status,
                                                          cnt_fail_sim_card_eeprom, 
                                                          cnt_fail_speed_gsm_modem_eeprom,
                                                          TelemetryArcRecNo - 1 
                                                                );
        flag_CRC32 = CRC_IN_PROCESSING;
        break;        
   case 7:
     sprintf((char*)ptr_buf,";%.2f;%s:%s;%s:%s;%s;" , 
                                                          residual_capacity_battery_percent,
                                                          (uint8_t*)MdmURL_Server[1],
                                                          (uint8_t*)MdmPort_Server[1],
                                                          (uint8_t*)MdmURL_Server[2],
                                                          (uint8_t*)MdmPort_Server[2],
                                                          serial_number_board
                                                                );
        flag_CRC32 = CRC_IN_PROCESSING;
        break;            
    case 8:
     sprintf((char*)ptr_buf,"%s}" , 
                                                          telephone_number_SMS2 );
        flag_CRC32 = CRC_STOP;
        break;            
        
   default:break;
   }
   if(flag_CRC32 != CRC_NON)
   {
      uint32_t rez_crc32;
      uint8_t *ptr_str;
      rez_crc32 = CalculateCRC32(ptr_buf, strlen((char*)ptr_buf), flag_CRC32);
      if(flag_CRC32 == CRC_STOP)
      {
        ptr_str = ptr_buf + strlen((char*)ptr_buf);
        sprintf((char*)ptr_str, "\t%08X\r\n", rez_crc32);
      }
   }
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
void sprintfEndMessage(uint8_t flag)
{
  if(flag == STR_ALL_END)
     sprintf((char*)&FlagMessageTMR.buf_message_tcp[0], "{}\r\n\x1A");
  else
     sprintf((char*)&FlagMessageTMR.buf_message_tcp[0], "}\r\n\x1A");
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
void ClearCntSession(uint8_t change_source, uint32_t cnt_all, uint32_t cnt_fail, uint32_t cnt_fail_sim, uint32_t cnt_fail_speed)
{
       sprintf((char*)OldValue,"%d;%d;%d;%d",current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);  

       current_communication_gsm_number = cnt_all;
       Eeprom_WriteDword((uint32_t)&current_communication_gsm_number_eeprom,  current_communication_gsm_number);
       number_communication_gsm_fail = cnt_fail;
       Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
       Eeprom_WriteDword((uint32_t)&cnt_fail_sim_card_eeprom,  cnt_fail_sim);
       Eeprom_WriteDword((uint32_t)&cnt_fail_speed_gsm_modem_eeprom,  cnt_fail_speed);
       
       sprintf((char*)NewValue,"%d;%d;%d;%d",current_communication_gsm_number_eeprom, number_communication_gsm_fail_eeprom, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);  
       WriteChangeArcRecord(CHANGE_CNT_SESSION, change_source); 
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_COUNT_SESSION_SET(uint8_t checkcrc32)
{
   uint8_t rez;
   uint16_t buf;
   char *end_ptr;
   
   buf = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   
   if(buf < 65000 && checkcrc32 == _OK)
   {
         ClearCntSession(CHANGE_TCP, buf, number_communication_gsm_fail_eeprom, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);
         rez = _OK;
   }
   else
      rez = _FALSE;  
  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_ERROR_SESSION_SET(uint8_t checkcrc32)
{
   uint8_t rez;
   uint16_t buf;
   char *end_ptr;
   
   buf = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   
   if(buf < 65000 && checkcrc32 == _OK)
   {
         ClearCntSession(CHANGE_TCP, current_communication_gsm_number_eeprom, buf, cnt_fail_sim_card_eeprom, cnt_fail_speed_gsm_modem_eeprom);
         rez = _OK;
   }
   else
      rez = _FALSE;  
  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_MAX_SESSION_SET(uint8_t checkcrc32)
{
   uint8_t rez;
   uint16_t buf;
   char *end_ptr;
   
   buf = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   
   if(buf > 300 && buf < 65000 && checkcrc32 == _OK)
   {
       sprintf((char*)OldValue,"%d", max_cnt_communication_gsm);  

       Eeprom_WriteDword((uint32_t)&max_cnt_communication_gsm,  buf);
       
       sprintf((char*)NewValue,"%d", max_cnt_communication_gsm);  
       WriteChangeArcRecord(CHANGE_MAX_CNT_COMMUNICATION_GSM, CHANGE_TCP); 
         
       rez = _OK;
   }
   else
      rez = _FALSE;  
  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_TCP_VALVE_SET(uint8_t checkcrc32)
{
   uint8_t rez;
   uint16_t buf;
   char *end_ptr;
   
   buf = strtoul((char*)&FlagMessageTMR.buf_message_tcp[0], &end_ptr, 10);
   
   if((buf == 0 || buf == 1) && checkcrc32 == _OK)
   {
       sprintf((char*)OldValue,"%d", valve_resolution);  

       Eeprom_WriteChar((uint32_t)&valve_resolution,  buf);
       
       sprintf((char*)NewValue,"%d", valve_resolution);  
       WriteChangeArcRecord(CHANGE_VALVE_RESOLUTION, CHANGE_TCP); 
       
       if(valve_resolution == 1)
          valve_action = VALVE_ACTION_OPEN_TCP;
       else
          valve_action = VALVE_ACTION_CLOSE_TCP; 
     // if(valve_resolution == 0)
     //    rez = _OK; //### CLOSE VALVE
       rez = _OK;
     flag_change_server_url_valve = _OK;
   }
   else
      rez = _FALSE;  
  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
_ResultReply  ParsingMESSAGE_MOTO_INFO(uint8_t *ptr_buf, uint8_t part)
{
   _ResultReply  resultReply;
   resultReply.end_part_message = _FALSE;
   resultReply.end_message = _FALSE;
   uint8_t flag_CRC32;
   
   memset((char*)ptr_buf, 0, strlen((char*)ptr_buf));
   
   switch(part)
   {
   case 1:
        sprintf((char*)ptr_buf,"{%llu;%llu;%llu;%llu;%llu;",
                             //   MotoHoursToMils.time_workingCPU,
                                MotoHoursToMils.time_StopCPU,
                                MotoHoursToMils.time_workingGSM,
                                MotoHoursToMils.time_workingOPTIC,
                                MotoHoursToMils.time_workingSGM,
                                MotoHoursToMils.time_workingValve
                                                       );
        flag_CRC32 = CRC_CTART;
        break;
   case 2:
        sprintf((char*)ptr_buf,"%f;%f;%f;%f;%f;%u}",
                              //  MotoHoursToMils.workingCPU_ma,
                                MotoHoursToMils.StopCPU_mkA,
                                MotoHoursToMils.workingGSM_ma,
                                MotoHoursToMils.workingOPTIC_ma,
                                MotoHoursToMils.workingSGM_ma,
                                MotoHoursToMils.workingValve_ma,
                                MotoHoursToMils.capacity_battery_ma
                                                       );
        flag_CRC32 = CRC_STOP;
        resultReply.end_part_message = _OK;
        resultReply.end_message = _OK;          
        break;
   default:
        strcat((char*)ptr_buf,"\r\n\x1A");
        resultReply.end_part_message = _OK;
        resultReply.end_message = _OK;
     break;
   }
   if(flag_CRC32 != CRC_NON)
   {
      uint32_t rez_crc32;
      uint8_t *ptr_str;
      rez_crc32 = CalculateCRC32(ptr_buf, strlen((char*)ptr_buf), flag_CRC32);
      if(flag_CRC32 == CRC_STOP)
      {
        ptr_str = ptr_buf + strlen((char*)ptr_buf);
        sprintf((char*)ptr_str, "\t%08X\r\n\x1A", rez_crc32);
      }
   }
   return resultReply;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
uint8_t ParsingMESSAGE_MOTO_INFO_SET(char *ptr_buf_str, uint8_t checkcrc32, uint8_t flag_source)
{
   uint8_t rez = _FALSE;
   uint8_t i=0;
   char *ptr_buf=ptr_buf_str;
 //  char *ptr_param = (char *)&MotoHoursToMils.time_workingCPU;
   char *ptr_param = (char *)&MotoHoursToMils.time_workingGSM;
   if(checkcrc32 == _OK)
   {
      sprintf((char*)OldValue,"%0.1f", residual_capacity_battery_percent); 
      for( ; i < 13; i++)
      {
         rez = _OK;
	 if(strlen(ptr_buf)>0) // � ������ ���-�� ����
	 {
	    if(i < 6) // 0 - 5
	    {
	       *((uint64_t*)ptr_param) = atoll(ptr_buf);
		ptr_param += 8;
	    }
	    else 
               if(i > 5 && i < 12) // 6 - 11
	       {
		  *((float*)ptr_param) = atof(ptr_buf);
	          ptr_param += 4;
	       }
	       else // 12
		*((uint16_t*)ptr_param) = atoi(ptr_buf);
	 }
	 else // ������ ������
	     break;
	 ptr_buf = strchr(ptr_buf, ';');
	 if(ptr_buf) // ����������� ������
 	    ptr_buf++;
	 else
	    break;
      }
      Calculation_residual_capacity_battery_percent();
      sprintf((char*)NewValue,"%0.1f", residual_capacity_battery_percent); 
      WriteChangeArcRecord(CHANGE_MOTO_INFO, flag_source); 
      
  }

  return rez;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :   STR_ALL_END         STR_PART_END                                                                               **
** Out :                                                                                     **
**********************************************************************************************************/
void change_transfermode_to_5(uint8_t flag_change)
{
         sprintf((char*)OldValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  

       Eeprom_WriteChar((uint32_t)&Mode_Transfer.mode, MODE_ONCE_A_DECADE); // every 10 days
       Eeprom_WriteChar((uint32_t)&Mode_Transfer.day,  1);
       
         sprintf((char*)NewValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  
         WriteChangeArcRecord(CHANGE_TRANSFER_MODE, flag_change);
}
/*!
Setting CRC32 prerphi
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/       
void CRC_Init(void)
{
  /*
    hcrc.Instance = CRC;
  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_WORDS;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  */
  //------------------
   SET_BIT(RCC->AHBENR, RCC_AHBENR_CRCEN);
   // DEFAULT_POLYNOMIAL_ENABLE;
   WRITE_REG(CRC->POL, 0x04C11DB7U);  //  DEFAULT_CRC32_POLY
   MODIFY_REG(CRC->CR, CRC_CR_POLYSIZE, 0); // CRC_POLYLENGTH_32B
   // DEFAULT_INIT_VALUE_ENABLE
   WRITE_REG(CRC->INIT, 0xFFFFFFFFU); // DEFAULT_CRC_INITVALUE
   // CRC_INPUTDATA_INVERSION_NONE
   MODIFY_REG(CRC->CR, CRC_CR_REV_IN, 0x00000000U); 
   // CRC_OUTPUTDATA_INVERSION_DISABLE
   MODIFY_REG(CRC->CR, CRC_CR_REV_OUT, 0x00000000U); 
}


void CalculateMotoHourceGSM(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START)
  {
    MotoHoursTempToMils.time_workingGSM = xTaskGetTickCount();
    MotoHoursTempToMils.flag_start_time_workingGSM = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingGSM == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingGSM = _FALSE;
     MotoHoursTempToMils.time_workingGSM = xTaskGetTickCount() - MotoHoursTempToMils.time_workingGSM;
     MotoHoursToMils.time_workingGSM += MotoHoursTempToMils.time_workingGSM;
  }
}

void CalculateMotoHourceOPTIC(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START)
  {
    MotoHoursTempToMils.time_workingOPTIC = xTaskGetTickCount();
    MotoHoursTempToMils.flag_start_time_workingOPTIC = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingOPTIC == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingOPTIC = _FALSE;
     MotoHoursTempToMils.time_workingOPTIC = xTaskGetTickCount() - MotoHoursTempToMils.time_workingOPTIC;
     MotoHoursToMils.time_workingOPTIC += MotoHoursTempToMils.time_workingOPTIC;
  }
}

void CalculateMotoHourceSGM(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START)
  {
    MotoHoursTempToMils.time_workingSGM = xTaskGetTickCount();
    MotoHoursTempToMils.flag_start_time_workingSGM = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP && MotoHoursTempToMils.flag_start_time_workingSGM == _OK)
  {
     MotoHoursTempToMils.flag_start_time_workingSGM = _FALSE;
     MotoHoursTempToMils.time_workingSGM = xTaskGetTickCount() - MotoHoursTempToMils.time_workingSGM;
     MotoHoursToMils.time_workingSGM += MotoHoursTempToMils.time_workingSGM;
  }
}

void CalculateMotoHourceValve(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START)
  {
    MotoHoursTempToMils.time_workingValve = xTaskGetTickCount();
    MotoHoursTempToMils.flag_start_time_sworkingValve = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP &&  MotoHoursTempToMils.flag_start_time_sworkingValve == _OK)
  {
     MotoHoursTempToMils.flag_start_time_sworkingValve = _FALSE;
     MotoHoursTempToMils.time_workingValve = xTaskGetTickCount() - MotoHoursTempToMils.time_workingValve;
     MotoHoursToMils.time_workingValve += MotoHoursTempToMils.time_workingValve;
  }
}

void CalculateMotoHourceStopCPU(uint8_t flag)
{
  if(flag == MOTO_HOURCE_START)
  {
    difference_lptim = LL_LPTIM_GetCounter(LPTIM1);
    MotoHoursTempToMils.flag_start_time_stopCPU = _OK;
  }
    
  if(flag == MOTO_HOURCE_STOP &&  MotoHoursTempToMils.flag_start_time_stopCPU == _OK)
  {
     MotoHoursTempToMils.flag_start_time_stopCPU = _FALSE;
     difference_lptim = (uint16_t)LL_LPTIM_GetCounter(LPTIM1) - difference_lptim;
     temp_time_stop_mode = ((uint32_t)difference_lptim * (uint32_t)305) / (uint32_t)10000;
     MotoHoursToMils.time_StopCPU += temp_time_stop_mode;
  }
}

void Calculation_residual_capacity_battery_percent()
{
 // float residual_capacity_battery_percent_temp;
  uint64_t energy_temp;
  energy_temp = (uint64_t)(MotoHoursToMils.time_StopCPU * MotoHoursToMils.StopCPU_mkA / 1000);
  energy_temp += MotoHoursToMils.time_workingGSM * MotoHoursToMils.workingGSM_ma;
  energy_temp += MotoHoursToMils.time_workingOPTIC * MotoHoursToMils.workingOPTIC_ma;
  energy_temp += MotoHoursToMils.time_workingSGM * MotoHoursToMils.workingSGM_ma;  
  energy_temp += MotoHoursToMils.time_workingValve * MotoHoursToMils.workingValve_ma;   
  residual_capacity_battery_percent = 100 - energy_temp * 100 / ((uint64_t)MotoHoursToMils.capacity_battery_ma * 3600);
  if(residual_capacity_battery_percent < 10.0)
  {
    if(!StatusDevice.residual_capacity_battery_min10)
    {
       StatusDevice.residual_capacity_battery_min10 = 1;
       WriteIntEventArcRecord(EVENT_INT_BATTERY_TELEMETRY_MIN_10, EVENT_INT_BEGIN);
    }
  }
  else
  {
    if(StatusDevice.residual_capacity_battery_min10)
    {
       StatusDevice.residual_capacity_battery_min10 = 0;
       WriteIntEventArcRecord(EVENT_INT_BATTERY_TELEMETRY_MIN_10, EVENT_INT_END);
    }
  }
}
/*!
Initilization LPTIM
\param[in] 
\param[out] 
\return 
*/ 
void MX_LPTIM1_Init(void)
{
  // Select the LSE clock as LPTIM peripheral clock
  MODIFY_REG(RCC->CCIPR, RCC_CCIPR_LPTIM1SEL, RCC_CCIPR_LPTIM1SEL);
  
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_LPTIM1);
  //   SET_BIT(RCC->APB1ENR, Periphs);
  
  RCC->APB1SMENR |= RCC_APB1SMENR_LPTIM1SMEN;
    LPTIM1->CR |= LPTIM_CR_ENABLE;
    
  LL_LPTIM_SetClockSource(LPTIM1, LL_LPTIM_CLK_SOURCE_INTERNAL);
  // MODIFY_REG(LPTIMx->CFGR, LPTIM_CFGR_CKSEL, ClockSource);
  LL_LPTIM_SetPrescaler(LPTIM1, LL_LPTIM_PRESCALER_DIV1);
  // MODIFY_REG(LPTIMx->CFGR, LPTIM_CFGR_PRESC, Prescaler);
  LL_LPTIM_SetPolarity(LPTIM1, LL_LPTIM_OUTPUT_POLARITY_REGULAR);
  // MODIFY_REG(LPTIMx->CFGR, LPTIM_CFGR_WAVPOL, Polarity);
  LL_LPTIM_SetUpdateMode(LPTIM1, LL_LPTIM_UPDATE_MODE_IMMEDIATE);
  // MODIFY_REG(LPTIMx->CFGR, LPTIM_CFGR_PRELOAD, UpdateMode);
  LL_LPTIM_SetCounterMode(LPTIM1, LL_LPTIM_COUNTER_MODE_INTERNAL);
  // MODIFY_REG(LPTIMx->CFGR, LPTIM_CFGR_COUNTMODE, CounterMode);
  LL_LPTIM_TrigSw(LPTIM1);
  // CLEAR_BIT(LPTIMx->CFGR, LPTIM_CFGR_TRIGEN);
  LL_LPTIM_SetAutoReload(LPTIM1, 0xFFFF);
  // MODIFY_REG(LPTIMx->ARR, LPTIM_ARR_ARR, AutoReload);
  LL_LPTIM_StartCounter(LPTIM1, LL_LPTIM_OPERATING_MODE_CONTINUOUS);
  // MODIFY_REG(LPTIMx->CR, LPTIM_CR_CNTSTRT | LPTIM_CR_SNGSTRT, OperatingMode);
  
  LPTIM1->CR |= LPTIM_CR_ENABLE;
  LPTIM1->CR |= LPTIM_CR_CNTSTRT;
}

int ToHex(uint8_t *In, uint8_t *Out)
{
int i,index=0,len;
len =strlen((char*)In);
Out[0]=0;
for(i=0;i<len;i++)
	{
	sprintf((char*)Out+index,"%02X", In[i]);
	index+=2;
	}
Out[index]=0;
return index;
}

