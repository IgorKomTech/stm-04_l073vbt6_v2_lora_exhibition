//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    DataStorage.h
/// \author  RFU
/// \date    09-Feb-2016
/// \brief   Functions to store and load data for the gasmeter.
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "DataStorage.h"
#include "Nvm.h"
#include "Utils.h"

#define CONFIG_USE_CATEGORY_DATASTORAGE
#include "Configuration.h"

static Error LoadAndVerifyBackupBlock(BackupBlock* backup, uint8_t index);
static Error GetLatestBackupBlockIndex(uint8_t* index);
static Error GetOldestBackupBlockIndex(uint8_t* index);

//-----------------------------------------------------------------------------
Error DataStorage_LoadData(double* volume, uint32_t* operationTime,
                           uint32_t* batteryReplaceTime)
{
  Error       error;
  uint8_t     index;
  BackupBlock backup;
  
  error = GetLatestBackupBlockIndex(&index);
  
  if(Error_IsNone(error)) {
    error = LoadAndVerifyBackupBlock(&backup, index);
  }
  
  if(Error_IsNone(error)) {
    *volume = backup.Itmes.Volume;
    *operationTime = backup.Itmes.Timestamp;
    error = Nvm_ReadLastBatteryReplacement(batteryReplaceTime);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error DataStorage_StoreVolumeAndOperationTime(double volume, uint32_t operationTime)
{
  Error       error;
  uint8_t     index;
  BackupBlock backup;
  
  error = GetOldestBackupBlockIndex(&index);
  
  if(Error_IsNone(error)) {
    backup.Itmes.Volume = volume;
    backup.Itmes.Timestamp = operationTime;
    backup.Itmes.Crc32 = Utils_CalculateCrc32(backup.Data, 12, Config_DataStorageCrc32Polynominal);
    error = Nvm_WriteBackupBlock(backup, index);
  }
  
  return error;
}

Error DataStorage_StoreBatteryReplaceTime(uint32_t batteryReplaceTime)
{
  return Nvm_WriteLastBatteryReplacement(batteryReplaceTime);
}

//-----------------------------------------------------------------------------
Error DataStorage_ClearAllData(void)
{
  Error       error;
  BackupBlock emptyBackup;
  int         i;
  
  emptyBackup.Itmes.Volume = 0.0;
  emptyBackup.Itmes.Timestamp = 0;
  emptyBackup.Itmes.Crc32 = Utils_CalculateCrc32(emptyBackup.Data, 12, Config_DataStorageCrc32Polynominal);
  
  for(i = 0; i < 8; i++)
  {
    error = Nvm_WriteBackupBlock(emptyBackup, i);
    if(Error_IsError(error)) break;
  }
  
  Nvm_WriteLastBatteryReplacement(0);
  
  return error;
}

//-----------------------------------------------------------------------------
static Error LoadAndVerifyBackupBlock(BackupBlock* backup, uint8_t index)
{
  Error    error;
  uint32_t crc;
  
  error = Nvm_ReadBackupBlock(backup, index);
  
  if(Error_IsNone(error)) {
    crc = Utils_CalculateCrc32(backup->Data, 12, Config_DataStorageCrc32Polynominal);
    error = Utils_Crc32Check(crc, backup->Itmes.Crc32);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error GetLatestBackupBlockIndex(uint8_t* index)
{
  Error       error;
  BackupBlock block;
  uint32_t    latestTimestamp = 0x00000000;
  int         i;
  bool        indexFound = false;
  
  for(i = 0; i < 8; i++) {
    error  = LoadAndVerifyBackupBlock(&block, i);
    if(error.Items.LastError == ERROR_UTILS_CRC) {
      error = Error_None();
      continue; // ignore faulty backup blocks
    }
    if(Error_IsError(error)) break;
    if(latestTimestamp <= block.Itmes.Timestamp) {
      latestTimestamp = block.Itmes.Timestamp;
      *index = i;
      indexFound = true;
    }
  }
  
  if(!indexFound) {
    Error_Add(&error, ERROR_DATASTORAGE_NO_BACKUP_BLOCK_FOUND);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error GetOldestBackupBlockIndex(uint8_t* index)
{
  Error       error;
  BackupBlock block;
  uint32_t    oldestTimestamp = 0xFFFFFFFF;
  int         i;
  bool        indexFound = false;
  
  for(i = 0; i < 8; i++) {
    error  = LoadAndVerifyBackupBlock(&block, i);
    if(error.Items.LastError == ERROR_UTILS_CRC) {
      error = Error_None();
      continue; // ignore faulty backup blocks
    }
    if(Error_IsError(error)) break;
    if(oldestTimestamp > block.Itmes.Timestamp) {
      oldestTimestamp = block.Itmes.Timestamp;
      *index = i;
      indexFound = true;
    }
  }
  
  if(!indexFound) {
    Error_Add(&error, ERROR_DATASTORAGE_NO_BACKUP_BLOCK_FOUND);
  }
  
  return error;
}
