//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Display.h
/// \author  LWI
/// \date    18-Dec-2014
/// \brief   Module for Display modifications
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef DISPLAY_H
#define DISPLAY_H

#include "Typedef.h"
#include "Error.h"
#include "LCD.h"

//-----------------------------------------------------------------------------
/// \brief Fixed symbols on display
typedef enum{
  // Symbol Top
  SYMBOL_SENSIRION, 
  SYMBOL_RANGE,
  SYMBOL_CYCLE,
  SYMBOL_DOT,
  // Symbol Line 1
  SYMBOL_PA,    
  SYMBOL_RH1,
  SYMBOL_M3_1,
  SYMBOL_PH,
  SYMBOL_C1, 
  SYMBOL_U, 
  SYMBOL_L11, 
  SYMBOL_PMIN11, 
  SYMBOL_M1, 
  SYMBOL_L12, 
  SYMBOL_N1, 
  SYMBOL_PMIN12,
  // Symbol Line 2
  SYMBOL_RH2,   
  SYMBOL_M3_2,
  SYMBOL_C2,
  SYMBOL_M2, 
  SYMBOL_L2,
  SYMBOL_N2,
  // Symbol Bot
  SYMBOL_GAS,   
  SYMBOL_NAT,
  SYMBOL_AIR,
  SYMBOL_FLOW,
  SYMBOL_MAX,
  SYMBOL_MIN,
  SYMBOL_LOWBATT          
} DisplaySymbol;

//-----------------------------------------------------------------------------
/// \brief Available digits for 7 segment display
typedef enum{
  DIGIT_0        =  0,
  DIGIT_1        =  1,        
  DIGIT_2        =  2,
  DIGIT_3        =  3,
  DIGIT_4        =  4,
  DIGIT_5        =  5,
  DIGIT_6        =  6,
  DIGIT_7        =  7,
  DIGIT_8        =  8,
  DIGIT_9        =  9,
  DIGIT_A        = 10,
  DIGIT_B        = 11,
  DIGIT_C        = 12,
  DIGIT_D        = 13,
  DIGIT_E        = 14,
  DIGIT_F        = 15,
  DIGIT_CLEAR    = 16, 
  DIGIT_NEGATIV  = 17, 
  DIGIT_CHAR_U   = 18, 
  DIGIT_CHAR_r   = 19,  
  DIGIT_CHAR_H   = 20,
  DIGIT_CHAR_L   = 21,
  DIGIT_CHAR_P   = 22,
  DIGIT_CHAR_I   = 23
}DisplayDigit;

//----------------------------------------------------------------------------
/// \brief Init LCD hardware.
void Display_InitHardware(void);

//----------------------------------------------------------------------------
/// \brief Clear all digits in Ram.
void Display_ClearRam(void);

//----------------------------------------------------------------------------
/// \brief Enable all digits in Ram.
void Display_AllOnInRam(void);

//----------------------------------------------------------------------------
/// \brief Load the content from Ram to display, execute Display_Enable()
/// before if display is disabled for correct update.
void Display_Update(void);

//----------------------------------------------------------------------------
/// \brief Enable the display, the current consumtion is increased.
void Display_Enable(void);

//----------------------------------------------------------------------------
/// \brief Disable the display for saving Power.
void Display_Disable(void);

//----------------------------------------------------------------------------
/// \brief Write a digit on the given position in display Ram.
/// \param position   Position on display [0...10]
/// \n                Line 1:  7 8 9 10
/// \n                Line 2:  0 1 2 3 4 5 6
/// \param digit      Digit for display 
void Display_WriteDigitInRam(uint8_t position, DisplayDigit digit);

//----------------------------------------------------------------------------
/// \brief Enable or disable a decimal point on the given position in display.
/// Ram.
/// \param position  Position on display [0...5, 6...9]
/// \n               Line 1:  7 8 9 -
/// \n               Line 2:  0 1 2 3 4 5 -
/// \param enabled   Enable or disable decimal point
void Display_WriteDpInRam(uint8_t position, bool enabled);

//----------------------------------------------------------------------------
/// \brief Enable or disable a dymbol in display Ram.
/// \param Symbol    Symbol
/// \param enabled   Enable or disable Symbol
void Display_WriteSymbolInRam(DisplaySymbol Symbol, bool enabled);

//----------------------------------------------------------------------------
/// \brief Clear all digits and decimal points and set complet float number  
/// with decimal point on Line1.
/// \param maxDecs   Maximum number of decimal places [0...3]
/// \param number    float number for Display
void Display_WriteFloat1InRam(uint8_t maxDecs, float number);

//----------------------------------------------------------------------------
/// \brief Clear all digits and decimal points and set complete integer number  
/// with optinal minus sign and decimal point on Line1.
/// Zeros on small numbers are displayed before and after decimal point:   
/// \n      false, 2, 1001 => 10.01
/// \n      false, 1, 1000 => 100.0
/// \n      false, 2,    0 =>  0.00
/// \n      true,  3,  999 => -.999
/// \n      true,  2,   99 => -0.99
/// \param negative set true for negative numbers
/// \param decs     select number of decimal places after decimal point [0...3]
/// \param number   unsigned 16bit value for write to display, is limited to
/// \n              [0...9999] for positiv and [0...999] for negative numbers.
void Display_WriteLine1InRam(bool negative, uint8_t decs, uint16_t number);

//----------------------------------------------------------------------------
/// \brief Clear all digits and decimal points and set complete integer number  
/// with optinal minus sign and decimal point on Line2.
/// Zeros on small numbers are displayed before and after decimal point: 
/// \n      false, 2,    1001 =>    10.01
/// \n      false, 1,    1000 =>    100.0
/// \n      false, 2,       0 =>     0.00
/// \n      true,  3,     999 =>   -0.999
/// \n      true,  2,      99 =>    -0.99
/// \n      true,  6,  999999 => -.999999
/// \param decs     select number of decimal places after decimal point [0...6]
/// \param number   unsigned 16bit value for write to display, is limited to
/// \n              [0...9999999] for positiv and [0...999999] for negative numbers.
void Display_WriteLine2InRam(bool negative, uint8_t decs, uint32_t number, DisplayDigit prefix);

//-----------------------------------------------------------------------------
void Display_EnableLogo(void);
void Display_WriteFwVersion(uint8_t major, uint8_t minor, uint8_t debug);

void Display_WriteCurrentFlow(float flow);
void Display_WriteTotalVolume(double totalVolume, uint8_t decimalPlaces);

void Display_WriteError(Error error, uint8_t page);
  
//-----------------------------------------------------------------------------
/// \brief Function to display a program or parameter memory checksum error.
/// \param error Error code to display in the upper line.
/// \param crc   Calculated CRC
/// \param page  [0,1] Defines whether page is 0 or 1 of the error is displayed.
void Display_WriteCrcError(Error error, uint32_t crc, uint8_t page);


#endif /* DISPLAY_H */
