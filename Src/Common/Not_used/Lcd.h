//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Lcd.h
/// \author  LWI
/// \date    18-Dec-2014
/// \brief   Module for low level LCD functions
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef LCD_H
#define LCD_H

#include "Typedef.h"
#include "stm32l1xx.h"

//----------------------------------------------------------------------------
/// \brief Init all required MCU-Pins for LCD function and init LCD block.
void Lcd_Init(void);

//----------------------------------------------------------------------------
/// \brief Enable the LCD, the current consumtion is increased.
void Lcd_Enable(void);

//----------------------------------------------------------------------------
/// \brief Disable the LCD for saving Power.
void Lcd_Disable(void);

//----------------------------------------------------------------------------
/// \brief Load the content from Ram to display, execute Lcd_Enable()before if 
///  LCD is disabled for correct function
void Lcd_Update(void);

//----------------------------------------------------------------------------
/// \brief After calling Lcd_Update() the LCD is busy for max one LCD Frame,  
/// the next update could only be perfomed if LCD is not busy.
/// \retval State of LCD:
/// \n  True : LCD busy, no Update could be performed
/// \n  False: LCD idle, Update could be performed
bool Lcd_IsBusy(void);

//----------------------------------------------------------------------------
/// \brief Write all segment for the given COM Line in Ram.
/// \param Com    Com line for set all segment [0...7]
/// \param Data   New Segment data
void Lcd_WriteCOMDataInRam(uint8_t Com, uint32_t Data);

//----------------------------------------------------------------------------
/// \brief Enable or disable the segment for the given COM and Seg Line in Ram.
/// \param Com       Com line for set all segment [0...7]
/// \param Seg       Seg for set or clear
/// \param enabled   Enable or disable the segment
void Lcd_WriteSegmentInRam(uint8_t Com, uint8_t Seg, bool enabled);

#endif /* LCD_H */
