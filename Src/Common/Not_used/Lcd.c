//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Lcd.c
/// \author  LWI
/// \date    18-Dec-2014
/// \brief   Module for low level LCD functions
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "Lcd.h"

static void InitLcdPin(GPIO_TypeDef* Port, uint8_t Pin);
static void WriteRam2Lcd(void);

static uint32_t lcdRam[8];

//-----------------------------------------------------------------------------
void Lcd_Init()
{  
  // enable clock for Port A,B,C,D
  SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN | RCC_IOPENR_GPIODEN);
  // enable LCD clk 
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_LCDEN); 

  // Set Pin for Port A
  InitLcdPin(GPIOA, 1);
  InitLcdPin(GPIOA, 2);
  InitLcdPin(GPIOA, 3);
  InitLcdPin(GPIOA, 6);
  InitLcdPin(GPIOA, 7);
  InitLcdPin(GPIOA, 8);
  InitLcdPin(GPIOA, 9);
  InitLcdPin(GPIOA, 10);
  // Set Pin for Port B
  InitLcdPin(GPIOB, 0);
  InitLcdPin(GPIOB, 1);
  InitLcdPin(GPIOB, 3);
  InitLcdPin(GPIOB, 4);
  InitLcdPin(GPIOB, 5);
  InitLcdPin(GPIOB, 9);
  InitLcdPin(GPIOB, 10);
  InitLcdPin(GPIOB, 11);
  InitLcdPin(GPIOB, 12);
  InitLcdPin(GPIOB, 13);
  InitLcdPin(GPIOB, 14);
  InitLcdPin(GPIOB, 15);
  // Set Pin for Port C
  InitLcdPin(GPIOC, 10);
  InitLcdPin(GPIOC, 11);
  InitLcdPin(GPIOC, 12);
  // Set Pin for Port D
  InitLcdPin(GPIOD, 2);

  // Disable LCD
  Lcd_Disable();
  // LCD control: 1/4 Bias, 1/8 Duty, VSel internal,
  MODIFY_REG(LCD->CR, LCD_CR_BIAS | LCD_CR_DUTY | LCD_CR_VSEL, LCD_CR_DUTY_2);
  // Configure LCD Frame register for 40Hz Frame frequency
  MODIFY_REG(LCD->FCR, LCD_FCR_PS, 2 << 22);    // PS = 2
  MODIFY_REG(LCD->FCR, LCD_FCR_DIV, 9 << 18);   // DIV = 9
  MODIFY_REG(LCD->FCR, LCD_FCR_BLINK, 0 << 16); // 0: Blink disabled
  MODIFY_REG(LCD->FCR, LCD_FCR_CC, 7 << 10);    // Contrast Control = 7
}

//-----------------------------------------------------------------------------
void Lcd_Enable(void)
{
  SET_BIT(LCD->CR, LCD_CR_LCDEN);
}

//-----------------------------------------------------------------------------
void Lcd_Disable(void)
{
  CLEAR_BIT(LCD->CR, LCD_CR_LCDEN);
}

//-----------------------------------------------------------------------------
void Lcd_Update(void)
{
  WriteRam2Lcd();
  SET_BIT(LCD->SR, LCD_SR_UDR); // Update Display request
}

//-----------------------------------------------------------------------------
bool Lcd_IsBusy(void)
{
  if((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS)) // LCD is only busy if enabled
    return true;
  else
    return false;
}

//-----------------------------------------------------------------------------
static void InitLcdPin(GPIO_TypeDef* Port, uint8_t Pin)
{
  // Set alternative function mode for given port
  MODIFY_REG(Port->MODER, GPIO_MODER_MODE0<<(Pin * 2), 0x02 << (Pin * 2));
  // Set Port to medium Speed
  MODIFY_REG(Port->OSPEEDR, GPIO_OSPEEDER_OSPEED0<<(Pin * 2), 0x02 << (Pin * 2));
  // Set Alternate function 1 to port
  MODIFY_REG(Port->AFR[Pin / 8], 0x0F << (Pin % 8 * 4), 0x01 << (Pin % 8 * 4));
}

//-----------------------------------------------------------------------------
void Lcd_WriteCOMDataInRam(uint8_t Com, uint32_t Data)
{
  lcdRam[Com] = Data;
}

//-----------------------------------------------------------------------------
void Lcd_WriteSegmentInRam(uint8_t Com, uint8_t Seg, bool enabled)
{
  if(enabled)lcdRam[Com]  |= (uint32_t)1 << Seg;
  else       lcdRam[Com]  &= ~((uint32_t)1 << Seg);
}

//-----------------------------------------------------------------------------
static void WriteRam2Lcd(void)
{
  uint8_t i;
  while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS)); // Wait for Display updated
  for(i = 0; i < 8; i++)
    LCD->RAM[i * 2] = lcdRam[i];
}
