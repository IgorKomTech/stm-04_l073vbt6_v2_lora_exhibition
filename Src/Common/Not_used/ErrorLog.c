//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    ErrorLog.c
/// \author  RFU
/// \date    12-Feb-2016
/// \brief   Module for error logging
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "ErrorLog.h"
#include "Nvm.h"

//-----------------------------------------------------------------------------
Error ErrorLog_ResetErrorCounters(void)
{
  Error  error = Error_None();
  int    index;
  
  for(index = 0; index < 8; index++) {
    if(Error_IsNone(error)) {
      error = Nvm_WriteErrorCounter(0, index);
    }
  }
  
  return error;  
}

//-----------------------------------------------------------------------------
Error ErrorLog_IncrementErrorCounter(ErrorCounter errorCounter)
{
  Error    error;
  uint8_t  index;
  uint32_t value;
  
  index = (uint8_t)errorCounter;
  
  error = Nvm_ReadErrorCounter(&value, index);
  
  if(Error_IsNone(error)) {
    error = Nvm_WriteErrorCounter(++value, index);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error ErrorLog_ReadErrorCounter(ErrorCounter errorCounter, uint32_t* value)
{
  Error    error;
  uint8_t  index;
  
  index = (uint8_t)errorCounter;
  
  error = Nvm_ReadErrorCounter(value, index);
  
  return error;
}
