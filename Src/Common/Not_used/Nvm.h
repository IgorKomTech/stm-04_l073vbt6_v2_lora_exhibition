
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Nvm.h
/// \author  RFU
/// \date    07-Jan-2016
/// \brief   Module for the non-volatile memory
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef NVM_H
#define NVM_H

#include "Typedef.h"
#include "FlowCorrectionLut.h"
#include "Error.h"

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_NVM_READ               (ERROR_MODULE_NVM | 0x00)
#define ERROR_NVM_WRITE              (ERROR_MODULE_NVM | 0x01)
#define ERROR_NVM_INDEX_OUT_OF_RANGE (ERROR_MODULE_NVM | 0x02)
#define ERROR_NVM_LUT_IS_TOO_LARGE   (ERROR_MODULE_NVM | 0x03)
#define ERROR_NVM_NOT_IMPLEMENTED    (ERROR_MODULE_NVM | 0x1F)

//-----------------------------------------------------------------------------
/// \brief Union to serialize the backup block structure.
typedef union {
  struct {
    double   Volume;
    uint32_t Timestamp;
    uint32_t Crc32;
  } Itmes;
  uint8_t Data[16];
}BackupBlock;

//-----------------------------------------------------------------------------
/// \brief Addresses in the non-volatile memory.
typedef enum {
  NVM_ADDRESS_BOOT_COUNTER             = 0x0000, // 0x0000-0x0003, size: 4
  NVM_ADDRESS_CRC32_PROGRAM            = 0x0004, // 0x0004-0x0007, size: 4
  NVM_ADDRESS_CRC32_PARAMETERS         = 0x0008, // 0x0008-0x000B, size: 4
  NVM_ADDRESS_TEST_MODE_PASSWORD       = 0x000C, // 0x000C-0x000F, size: 4
  NVM_ADDRESS_LAST_SUCCESSFUL_BOOT     = 0x0010, // 0x0010-0x0013, size: 4
  NVM_ADDRESS_RESERVE                  = 0x0014, // 0x0014-0x005F, size: 76
  NVM_ADDRESS_ERROR_COUNTERS           = 0x0060, // 0x0060-0x007F, size: 32
  NVM_ADDRESS_BACKUP_BLOCKS            = 0x0080, // 0x0080-0x00FF, size: 128
  NVM_ADDRESS_LAST_BATTERY_REPLACEMENT = 0x0100, // 0x0100-0x01FF, size: 4
  NVM_ADDRESS_PARAMETERS_RESERVE       = 0x0104, // 0x0104-0x01FF, size: 252
  NVM_ADDRESS_CORRECTION_LUTS          = 0x0200, // 0x0200-0x03FF, size: 512
  NVM_ADDRESS_ERROR_LOGS               = 0x0400, // 0x0400-0x07FF, size: 1024
} Nvm_Addresses;                 

//-----------------------------------------------------------------------------
/// \brief Block sizes for different contents in the non-volatile memory.                                 
typedef enum {                   
  NVM_BLOCK_SIZE_BACKUP_BLOCK    = 0x0010, //  16 (8 blocks)
  NVM_BLOCK_SIZE_CORRECTION_LUT  = 0x0080, // 128 (4 blocks)
  NVM_BLOCK_SIZE_ERROR_LOG       = 0x0100, // 256 (4 blocks)
} Nvm_BlockSizes;

//-----------------------------------------------------------------------------
/// \brief Reads the number of system boots from the non-volatile memory.
Error Nvm_ReadBootCounter(uint32_t* value);

//-----------------------------------------------------------------------------
/// \brief Writes the number of system boots to the non-volatile memory.
Error Nvm_WriteBootCounter(uint32_t value);

//-----------------------------------------------------------------------------
/// \brief Reads the CRC-32 reference value for the programm memory from the
///        non-volatile memory.
Error Nvm_ReadCrc32Program(uint32_t* value);

//-----------------------------------------------------------------------------
/// \brief Writes the CRC-32 reference value for the programm memory to the
///        non-volatile memory.
Error Nvm_WriteCrc32Program(uint32_t value);

//-----------------------------------------------------------------------------
/// \brief Reads the CRC-32 reference value for the parameter memory from the
///        non-volatile memory.
Error Nvm_ReadCrc32Parameter(uint32_t* value);

//-----------------------------------------------------------------------------
/// \brief Writes the CRC-32 reference value for the parameter memory to the
///        non-volatile memory.
Error Nvm_WriteCrc32Parameter(uint32_t value);

//-----------------------------------------------------------------------------
/// \brief Reads the last sucessful system boot number from the non-volatile
///        memory.
Error Nvm_ReadLastSuccessfulBoot(uint32_t* value);

//-----------------------------------------------------------------------------
/// \brief Writes the last sucessful system boot number to the non-volatile
///        memory.
Error Nvm_WriteLastSuccessfulBoot(uint32_t value);

//-----------------------------------------------------------------------------
/// \brief Reads the test mode password from the non-volatile memory. 
Error Nvm_ReadTestModePassword(uint32_t* value);

//-----------------------------------------------------------------------------
/// \brief Writes the test mode password to the non-volatile memory. 
Error Nvm_WriteTestModePassword(uint32_t value);

//-----------------------------------------------------------------------------
/// \brief Reads an error counter from the non-volatile memory. 
Error Nvm_ReadErrorCounter(uint32_t* value, uint8_t index);

//-----------------------------------------------------------------------------
/// \brief Writes an error counter to the non-volatile memory. 
Error Nvm_WriteErrorCounter(uint32_t value, uint8_t index);

//-----------------------------------------------------------------------------
/// \brief Reads a backup block from the non-volatile memory.
Error Nvm_ReadBackupBlock(BackupBlock* backup, uint8_t index);

//-----------------------------------------------------------------------------
/// \brief Writes a backup block to the non-volatile memory.
Error Nvm_WriteBackupBlock(BackupBlock backup, uint8_t index);

//-----------------------------------------------------------------------------
/// \brief Reads the timestamp of last battery replacement from the non-
///        volatile memory.
Error Nvm_ReadLastBatteryReplacement(uint32_t* timestamp);

//-----------------------------------------------------------------------------
/// \brief Writes the timestamp of last battery replacement to the non-volatile
///        memory.
Error Nvm_WriteLastBatteryReplacement(uint32_t timestamp);

//-----------------------------------------------------------------------------
/// \brief Reads a FlowCorrectionLut from the non-volatile memory.
/// \param value        Pointer to return the read LUT from the non-volatile
///                     memory.
/// \param index        LUT index [0...3]
Error Nvm_ReadCorrectionLut(FlowCorrectionLut* value, uint8_t index);

//-----------------------------------------------------------------------------
/// \brief Writes a FlowCorrectionLut to the non-volatile memory.
/// \param value        LUT to write to the non-volatile memory.
/// \param index        LUT index [0...3]
Error Nvm_WriteCorrectionLut(FlowCorrectionLut value, uint8_t index);

//-----------------------------------------------------------------------------
/// \brief Reads an error log entry from the non-volatile memory.
///        --> not yet implemented! <--
Error Nvm_ReadErrorLogEntry(Error* value, uint8_t logBlockIndex,
      uint8_t entryIndex);
      
//-----------------------------------------------------------------------------
/// \brief Writes an error log entry to the non-volatile memory.
///        --> not yet implemented! <--
Error Nvm_WriteErrorLogEntry(Error value, uint8_t logBlockIndex,
      uint8_t entryIndex);
      
#endif /* NVM_H */
