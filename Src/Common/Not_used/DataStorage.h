//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    DataStorage.h
/// \author  RFU
/// \date    09-Feb-2016
/// \brief   Functions to store and load data for the gasmeter.
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef DATASTORAGE_H
#define DATASTORAGE_H

#include "Typedef.h"
#include "Error.h"

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_DATASTORAGE_NO_BACKUP_BLOCK_FOUND (ERROR_MODULE_DATASTORAGE | 0x00)

//-----------------------------------------------------------------------------
/// \brief  Clears all backup blocks (timestamp, total volume) and resets the 
///         battery replacement time to 0.
/// \return Error information, => Error_None() if no error occurred.
Error DataStorage_ClearAllData(void);

//-----------------------------------------------------------------------------
/// \brief  Loads the last stored total volume with the timestamp (operation time)
///         and the battery replacement time.
/// \param  volume             Pointer to return the total volume.
/// \param  operationTime      Pointer to return the operation time.
/// \param  batteryReplaceTime Pointer to return the battery replacement time.
/// \return Error information, => Error_None() if no error occurred.
Error DataStorage_LoadData(double* volume, uint32_t* operationTime,
                           uint32_t* batteryReplaceTime);

//-----------------------------------------------------------------------------
/// \brief  Stores the total volume with the timestamp (operation time).
/// \param  volume             The total measured volume by the gasmeter.
/// \param  operationTime      The current timestamp (operation time).
/// \return Error information, => Error_None() if no error occurred.
Error DataStorage_StoreVolumeAndOperationTime(double volume,
                                              uint32_t operationTime);

//-----------------------------------------------------------------------------
/// \brief  Stores the battery replacement time.
/// \param  batteryReplaceTime  Timestamp of last battery replacement.
/// \return Error information, => Error_None() if no error occurred.
Error DataStorage_StoreBatteryReplaceTime(uint32_t batteryReplaceTime);

#endif /* DATASTORAGE_H */
