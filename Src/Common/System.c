//=============================================================================
//   
//=============================================================================
/// \file    System.h
/// \author  
/// \date    
/// \brief   Watchdog timer, which restarts the device on unexpected hang up.
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#include "System.h"
#include "stm32l0xx.h"
#include "stm32l073xx.h"
#include "GasMeter.h"
#include "Eeprom.h"

#define CONFIG_USE_CATEGORY_WELMEC
#include "Configuration.h"

#define IWDG_START         (uint32_t)0x0000CCCC
#define IWDG_WRITE_ACCESS  (uint32_t)0x00005555
#define IWDG_REFRESH       (uint32_t)0x0000AAAA

static bool isWatchdogResetOccurred;
static bool isSoftwareResetOccurred;
static void CheckResetFlags(void);
static void StartWatchdog(void);

#pragma section = ".eeprom"
const double VolumeBeforeReset @ ".eeprom" =0.0;
//static const  

//-----------------------------------------------------------------------------
void System_Init(void)
{
  CheckResetFlags();
  
//  if(CONFIG_WELMEC_USE_WATCHDOG)
//      StartWatchdog();
//  if(System_IsWatchdogResetOccurred() || System_IsSoftwareResetOccurred())
  
      GasMeter.VE_Lm3 = VolumeBeforeReset;
}

//-----------------------------------------------------------------------------
void System_WatchdogRefresh(void)
{
  SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPHEN);  
  CLEAR_BIT(GPIOH->MODER,GPIO_MODER_MODE1);  //  ���� WAKE
  MODIFY_REG(GPIOH->MODER, GPIO_MODER_MODE0, GPIO_MODER_MODE0_0); // ����� Done
  SET_BIT(GPIOH->ODR, GPIO_ODR_OD0);
  for (int i=0; i<50; i++)
   __no_operation();
  CLEAR_BIT(GPIOH->ODR, GPIO_ODR_OD0);
  CLEAR_BIT(RCC->IOPENR,RCC_IOPENR_IOPHEN);  

//  IWDG->KR = IWDG_REFRESH;
}

//-----------------------------------------------------------------------------
void System_Reset(void)
{
  //TODO: add save to eeprom current data-time, volume 
 // System_SaweBeforeReset();
  NVIC_SystemReset();
}

//-----------------------------------------------------------------------------
bool System_IsWatchdogResetOccurred(void)
{
  return isWatchdogResetOccurred;
}

//-----------------------------------------------------------------------------
bool System_IsSoftwareResetOccurred(void)
{
  return isSoftwareResetOccurred;
}

//-----------------------------------------------------------------------------
static void CheckResetFlags(void)
{
  // check if last reset was a watchdog reset
  isWatchdogResetOccurred = (RCC->CSR & RCC_CSR_IWDGRSTF) == RCC_CSR_IWDGRSTF;
  
  // check if last reset was a software reset
  isSoftwareResetOccurred = (RCC->CSR & RCC_CSR_IWDGRSTF) == RCC_CSR_SFTRSTF;

  // clear reset flags
//  RCC->CSR |= RCC_CSR_RMVF;
}

//-----------------------------------------------------------------------------
/*static void StartWatchdog(void)
{
  // Activate IWDG (not needed if done in option bytes)
  IWDG->KR  = IWDG_START;
  // Enable write access to IWDG registers
  IWDG->KR  = IWDG_WRITE_ACCESS;
  // Set prescaler by 256
  IWDG->PR  = 6;
  // Set reload value to have a rollover each 5s (37kHz / 256 * 5s)
  IWDG->RLR = 723;
  // Check if flags are reset
  while(IWDG->SR);
  // Refresh counter
  System_WatchdogRefresh();
}
*/

//------------------------------------------------------------------------------
void System_SaweBeforeReset(void)
{
  Eeprom_WriteDouble((uint32_t)&VolumeBeforeReset, GasMeter.VE_Lm3);
}

