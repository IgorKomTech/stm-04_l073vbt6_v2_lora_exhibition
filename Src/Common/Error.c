//=============================================================================
/// \file    Error.c
/// \author  RFU
/// \date    26-Jan-2016
/// \brief   Functions for error handling
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "Error.h"

// ������ �������� ������� ����� 3� ������ - ���������� �����
//#pragma section = ".eeprom"
//const uint16_t DisableArcRecord @".eeprom" = 0;  


//-----------------------------------------------------------------------------
Error Error_None(void)
{
  Error error;
  error.Raw = 0;
  return error;
}

//-----------------------------------------------------------------------------
Error Error_Add(Error* error, uint16_t modulError)
{
  if(0 != modulError) 
  {
    if(0 == error->Items.PrimaryError) 
    {
      error->Items.PrimaryError = modulError;
    }
    else 
    {
      if(0 != error->Items.InnerError)
      {
        error->Items.Overflow = 1;
      }
      error->Items.InnerError = error->Items.LastError;
    }
    error->Items.LastError = modulError;
  }
  return *error;
}

//-----------------------------------------------------------------------------
Error Error_AppendIfError(Error* error, uint16_t modulError)
{
  if(Error_IsError(*error)) {
    Error_Add(error, modulError);
  }
  
  return *error;
}

//-----------------------------------------------------------------------------
Error Error_SetFatal(Error* error)
{
  error->Items.FatalError = 1;
  return *error;
}

//-----------------------------------------------------------------------------
bool Error_IsNone(Error error)
{
  return (0 == error.Raw);
}

//-----------------------------------------------------------------------------
bool Error_IsError(Error error)
{
  return (0 != error.Raw);
}

//-----------------------------------------------------------------------------
bool Error_IsFatal(Error error)
{
  return (0 != error.Items.FatalError);
}
