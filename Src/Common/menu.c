//=============================================================================
// 
//=============================================================================
/// \file    Menu.c
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for MenuSwith 
//=============================================================================

//=============================================================================

#include "io.h"
#include "lcd_8s.h"
#include "menu.h"
#include "Configuration.h"
#include "GasMeter.h"
#include "SensorTypeParameters.h"
#include "Time_g.h"
#include <stdio.h>
#include "GSMSIM800C.h"
#include "main_function.h"
#include "string.h"
#include "ValveTask.h"


 uint8_t Global_Menu = 0;


//-----------------------------------------------------------------------------
void Switch_Next_Menu(void)
{
  uint8_t temp_MAX_MAIN_MENU = MAX_MAIN_MENU;
  if(!valve_presence)
    --temp_MAX_MAIN_MENU;
 if(Global_Menu > MAX_MAIN_MENU)       // ������� ������� � ��������������� ����
 {
   if(++Global_Menu > MAX_TEH_MENU)
     Global_Menu = TEH_MENU_WARNINGS;
 }
 else
 {
   if(++Global_Menu > temp_MAX_MAIN_MENU)
     Global_Menu = MAIN_MENU_VOLUME;
 } 
 LCD8_Menu(0,0);
 KeyPressed = KEY_PRESSED_NOT;
}    

//-----------------------------------------------------------------------------
void Switch_Sub_Menu(void)
{
 if(Global_Menu > 5)       // ������� ������� � ��������������� ����
    Global_Menu = MAIN_MENU_VOLUME;
 else
    Global_Menu = TEH_MENU_WARNINGS;
 
 LCD8_Menu(0,0);
 KeyPressed = KEY_PRESSED_NOT;
} 

//-----------------------------------------------------------------------------
void LCD8_Menu(uint8_t pMenu, uint8_t pParam)
{
  uint8_t pLCD_menu_point = 0, loMenu;
  uint8_t lo_blink_point = 0;
//  uint8_t errorKfaktor = _FALSE;
  if (pMenu)
    loMenu = pMenu;
  else
    loMenu = Global_Menu;

  switch(loMenu)
  {
  case MAIN_MENU_VOLUME:  
  default:  
         sprintf(LCD_string,"%9.3f", GasMeter.VE_Lm3);
         if(type_device_int == 0)
           sprintf(LCD_string,"E %d", 1);
       //  if(StatusDevice.reverse_flow)
       //    sprintf(LCD_string,"E%d", 6);
         if(StatusDevice.check_sn_sgm_false)
           sprintf(LCD_string,"A%d", 8);
         if(StatusWarning.body_cover_open)
           sprintf(LCD_string,"A%d", 4);         
         if(StatusDevice.error_in_SGM_module)
           sprintf(LCD_string,"A%d", 1);  
         
         pLCD_menu_point = 0x80; 
      break;
  case MAIN_MENU_FLOW:   
         sprintf(LCD_string,"%9.3f", GasMeter.QgL_Disp);
         pLCD_menu_point = 0x40; 
      break;
  case MAIN_MENU_DATE: 
         GetDate_Time(&SystDate,&SystTime);  
         sprintf(LCD_string,"%0.2u-%0.2u-%0.2u", SystDate.Day,\
                                  SystDate.Month,SystDate.Year);
         pLCD_menu_point = 0x20;
      break;      
  case MAIN_MENU_DATE_VERIFICATION: 
       sprintf(LCD_string,"%0.2u-%0.2u-%0.2u", 
                 DateVerification.Day, DateVerification.Month, DateVerification.Year);
  //       pLCD_menu_point = 0x20;
      break;            
  case MAIN_MENU_DATE_NEXT_VERIFICATION: 
       sprintf(LCD_string,"%0.2u-%0.2u-%0.2u", 
                 DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year);
  //       pLCD_menu_point = 0x20;
      break;          
  case MAIN_MENU_VALVE:
       {
         switch(valve_status)
         {
         default:
         case VALVE_CLOSE:
         case VALVE_CLOSE_REVERSE_FLOW:  
         case VALVE_CLOSE_Q_MAX:  
         case VALVE_CLOSE_NON_NULL: 
            sprintf(LCD_string,"CL05E");
            break;
         case VALVE_OPEN: 
            sprintf(LCD_string,"0PE�"); 
            break;
         case VALVE_UNKNOWN:   
         case VALVE_NON_POWER: 
            sprintf(LCD_string,"--------"); 
            break;   
         }
         pLCD_menu_point =0x10;
       }
       break;  
       
  case MAIN_MENU_TEMPER:  
         sprintf(LCD_string,"%9.2f", GasMeter.var_T);    
         pLCD_menu_point = 0x46;
      break;
  case MENU_RESIDUAL_CAPACITY_BATTERY:  
         sprintf(LCD_string,"%   3.2f", residual_capacity_battery_percent);    
       //  pLCD_menu_point = 0x46;
      break;
  case MAIN_MENU_GASTYPE: // if(Global_Menu == MAIN_MENU_GASTYPE)
         sprintf(LCD_string,"L %6.5u", Sensor.KL);    
         pLCD_menu_point = 0x02;
      break;  

  case MAIN_MENU_TIME:    
         GetDate_Time(&SystDate,&SystTime);  
         sprintf(LCD_string,"  %0.2u.%0.2u.%0.2u", SystTime.Hours,\
                       SystTime.Minutes,SystTime.Seconds);
         pLCD_menu_point = 0x47;
      break; 

  case TEH_MENU_ERROR:   
    {
      uint8_t err[8];
      uint8_t err_ind=0;
      memset(err, 0, 8);
      /*
      if(StatusDevice.error_in_SGM_module)
        err[err_ind++] = 1;
  //    if(StatusDevice.unauthorized_intervention)
  //      err[1] = 2;
      
      if(StatusDevice.going_beyond_Q)
        err[err_ind++] = 3;      
      if(StatusDevice.going_beyond_T)
        err[err_ind++] = 4;        
      if(StatusDevice.battery_not_connected)
        err[err_ind++] = 5;  
      if(event_int_story & EVENT_INT_OPEN_CASE)
        err[err_ind++] = 7;        
      if(StatusDevice.check_crc_dQf_false)
        err[err_ind++] = 8; 
      if(StatusDevice.calibration_lock_open)
        err[err_ind++] = 9;       
      */
      if(StatusDevice.reverse_flow)
        err[err_ind++] = 1;
      if(StatusDevice.going_beyond_T)
        err[err_ind++] = 2;
      if(StatusDevice.going_beyond_Q)
        err[err_ind++] = 4;
      if(StatusDevice.unauthorized_intervention)
        err[err_ind++] = 5;
      sprintf(LCD_string,"E%d.%d.%d.%d.%d.%d.%d", err[0], err[1], err[2], err[3], err[4], err[5], err[6]);

      pLCD_menu_point = 0x02;
      break;  
    } 
  case TEH_MENU_CRASH:   
    {
      uint8_t crash[8];
      uint8_t crash_ind=0;
      memset(crash, 0, 8);
      if(StatusDevice.error_in_SGM_module)
        crash[crash_ind++] = 1;
      if(StatusWarning.body_cover_open)
        crash[crash_ind++] = 4;
      if(StatusDevice.calibration_lock_open)
        crash[crash_ind++] = 5;
/*
      if(StatusDevice.reverse_flow)
        crash[crash_ind++] = 1;
      if(StatusDevice.going_beyond_T)
        crash[crash_ind++] = 2;
      if(StatusDevice.going_beyond_Q)
        crash[crash_ind++] = 4;
      if(StatusDevice.unauthorized_intervention)
        crash[crash_ind++] = 5;*/
      sprintf(LCD_string,"A%d.%d.%d.%d.%d.%d.%d", crash[0], crash[1], crash[2], crash[3], crash[4], crash[5], crash[6]);

      pLCD_menu_point = 0x02;
      break;  
    } 
  case TEH_MENU_WARNINGS:   
    {
      uint8_t war[8];
      uint8_t war_ind=0;
      memset(war, 0, 8);
    //  if(StatusWarning.SIM_card_non) // 1
    //    war[war_ind++] = 1;       
      if(StatusWarning.SIM_card_non) // ????? StatusDevice.unauthorized_intervention
        war[war_ind++] = 2;  
   //   if(StatusWarning.Qmax_warning)
   //     war[war_ind++] = 3; 
      if(StatusWarning.battery_not_connected) // StatusWarning.Tmax_min_warning
        war[war_ind++] = 3;      
      if(StatusDevice.residual_capacity_battery_min10)
        war[war_ind++] = 6;
//      if(StatusDevice.reverse_flow)
 //       war[war_ind++] = 6;    
      if(StatusWarning.battery_cover_open)
        war[war_ind++] = 8;
      
      sprintf(LCD_string,"�%d.%d.%d.%d.%d.%d", war[0], war[1], war[2], war[3], war[4], war[5]);

   //   pLCD_menu_point = 0x02;
      break;  
    } 
    
  case MAIN_MENU_VOLUME_FLOAT:
       {
       uint32_t temp;
       temp = (uint32_t)(GasMeter.VE_Lm3 * 10000);
       temp %= 10000;
       sprintf(LCD_string,"    .%04d", temp);
    //   LCD_string[3] = ' ';
       pLCD_menu_point = 0x45;    
       }
       break;
  case TEH_MENU_SW:  
         sprintf(LCD_string,"C0 %d.%d%.02u",FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_GSM_VERSION);
        // pLCD_menu_point = 0x46; 
         pLCD_menu_point = 1;
       break; 
  case TEH_MENU_CHKSUMM:  
         sprintf(LCD_string,"C1  6314");
       //  pLCD_menu_point =0x45;  
          pLCD_menu_point = 0x02;
      break;
  case TEH_MENU_MDMTST:
         sprintf(LCD_string,"�.�. %d %d", flag_event, Get_confirm_transmit_LORA());
  //  sprintf(LCD_string,"%s", "�.�.�.�.�.�.�.�.");
     //    pLCD_menu_point =0x44;
         pLCD_menu_point = 1;
      break;
  case TEH_SERIAL_DEVICE:  
    { char *ptr;
      char len;
      ptr  = (char*)Device_STM.SN_Device;
      len = strlen((char*)Device_STM.SN_Device);
      if(len > 8)
        ptr += len - 8;
       sprintf(LCD_string,"%s", ptr);
         pLCD_menu_point =0x44;
    }
      break;
  case TEH_ALL_SEGMENT:
      sprintf(LCD_string,"%s", "8.8.8.8.8.8.8.8.");
      pLCD_menu_point =0xFF;
      break;
 
  }  
 if(READ_BIT(GPIOC->IDR, GPIO_IDR_ID12)==0)
     pLCD_menu_point |=0x01;
  
 if(GSM_Get_flag_power() == GSM_MUST_ENABLE)
       lo_blink_point |= 0x01;  
 
  if (loMenu < MAIN_MENU_VALVE)  
  {
    if (valve_status != VALVE_OPEN && valve_presence == 1)
       lo_blink_point |= 0x10; 
  }
  
  if(CLB_lock == 0x55)
    lo_blink_point |= 0x02; 
  display_buffer(pLCD_menu_point, lo_blink_point );
  
  
}
