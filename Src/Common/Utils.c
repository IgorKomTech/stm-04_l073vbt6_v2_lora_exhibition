//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Utils.c
/// \author  SUL
/// \date    11-Dec-2014
/// \brief   Module with miscellaneous utility functions
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "Utils.h"
//#include "Nvm.h"

#define CONFIG_USE_CATEGORY_UTILS
#include "Configuration.h"

/*/-----------------------------------------------------------------------------
Error Utils_ProgramMemoryCheck(uint32_t* crc)
{
  Error    error;
  uint32_t crcRef;
  
  *crc = Utils_CalculateCrc32(Config_FlashProgramBaseAddress, Config_FlashProgramSize, Config_UtilsCrc32Polynominal);
  error = Nvm_ReadCrc32Program(&crcRef);
  if(Error_IsNone(error)) {
      error = Utils_Crc32Check(*crc, crcRef);
  }

  return error;
}

//-----------------------------------------------------------------------------
Error Utils_ParameterMemoryCheck(uint32_t* crc)
{
  Error    error;
  uint32_t crcRef;
  
  *crc  = Utils_CalculateCrc32(Config_EepromParameterBaseAddress, Config_EepromParameterSize, Config_UtilsCrc32Polynominal);
  error = Nvm_ReadCrc32Parameter(&crcRef);
  if(Error_IsNone(error)) {
      error = Utils_Crc32Check(*crc, crcRef);
  }
  
  return error;
}
*/
//-----------------------------------------------------------------------------
uint8_t Utils_CalculateCrc(const uint8_t* data, uint8_t length,
                           uint8_t polynomial, uint8_t startVector)
{
  uint8_t crc = startVector;
  int     byteIndex;
  uint8_t bit;
  
  //calculates 8-Bit checksum with given polynomial
  for (byteIndex = 0; byteIndex < length; ++byteIndex) { 
    crc ^= data[byteIndex];
    for (bit = 8; bit > 0; --bit) {
      if (crc & 0x80) {
        crc <<= 1;
        crc ^= polynomial;
      } else {
        crc <<= 1;
      }
    }
  }
  return crc;
}

//-----------------------------------------------------------------------------
uint32_t Utils_CalculateCrc32(const uint8_t* data, uint32_t length, uint32_t polynomial)
{
  uint32_t crc = 0xFFFFFFFF;
  int      byteIndex;
  uint8_t  bit;
  
  //calculates 32-Bit checksum with given polynomial
  for (byteIndex = 0; byteIndex < length; ++byteIndex) {
    crc ^= data[byteIndex] << 24;
    for (bit = 8; bit > 0; --bit) {
      if (crc & 0x80000000) {
        crc <<= 1;
        crc ^= polynomial;
      } else {
        crc <<= 1;
      }
    }
  }
  return crc;
}

//-----------------------------------------------------------------------------
Error Utils_Crc32Check(uint32_t crc1, uint32_t crc2)
{
  Error error = Error_None();

  if(crc1 != crc2) {
    Error_Add(&error, ERROR_UTILS_CRC);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
uint16_t Utils_MakeWord(uint8_t highByte, uint8_t lowByte)
{
  return ((uint16_t) (((uint16_t)(highByte) << 8) | (lowByte)));
}

//-----------------------------------------------------------------------------
uint8_t Utils_GetHighByte(uint16_t word)
{
  return ((uint8_t)((word & 0xFF00) >> 8));
}
