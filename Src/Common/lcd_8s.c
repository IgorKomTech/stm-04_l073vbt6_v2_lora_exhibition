/**
  ******************************************************************************
  * File Name          : LCD.c
  * Description        : This file provides code for the configuration
  *                      of the LCD instances.
  ******************************************************************************
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "lcd_8s.h"
#include "stm32l073xx.h"
#include "stm32l0xx.h"
#include "Configuration.h"
#include "main_function.h" 
#include <stdio.h>

static void InitLcdPin(GPIO_TypeDef* Port, uint8_t Pin);
void LCD8_Disable(void);
void LCD8_Enable(void);
void display_buffer(uint8_t pLCD_menu_point, uint8_t blink_point);
void LCD8_clear(void);
void LCD8_clearRAM(void);



char LCD_string[16];


static const uint32_t LCD_COM[8][3][29] = {
// ��� ��� 0 ���������� COM00,COM10,COM20
//  0            1              2               3               4               5               6               7               8               9               A-10               b               C               d               E               F               -               _               ,               ������          H	        L         	o	        P-23	        U-24	        �-25	        �-26	        �-27            � - 28
0x00020000,	0x00000000,	0x00020000,	0x00020000,	0x00000000,	0x00020000,	0x00020000,	0x00000000,	0x00020000,	0x00020000,	0x00000000,	0x00020000,	0x00020000,	0x00020000,	0x00020000,	0x00000000,	0x00000000,	0x00000080,	0x00008000,	0x00000000,	0x00000000,	0x00020000,	0x00020000,	0x00000000,	0x00020000,	0x00000000,	0x00020000,	0x00000000,     0x00020000,  
0x00008080,	0x00008000,	0x00020080,	0x00028000,	0x00028000,	0x00028000,	0x00028080,	0x00008000,	0x00028080,	0x00028000,	0x00028080,	0x00028080,	0x00000080,	0x00028080,	0x00020080,	0x00020080,	0x00020000,	0x00000000,	0x00000000,	0x00000000,	0x00028080,	0x00000080,	0x00028080,	0x00020080,	0x00008080,	0x00008080,	0x00028000,	0x00000080,     0x00028080, 
0x00028080,	0x00008000,	0x00028000,	0x00028000,	0x00008080,	0x00020080,	0x00020080,	0x00028000,	0x00028080,	0x00028080,	0x00028080,	0x00000080,	0x00020080,	0x00008000,	0x00020080,	0x00020080,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00008080,	0x00000080,	0x00000000,	0x00028080,	0x00008080,	0x00028080,	0x00008080,	0x00020080,     0x00028000, 
// ��� ��� 1 ���������� COM00,COM10,COM20
0x00002000,	0x00000000,	0x00002000,	0x00002000,	0x00000000,	0x00002000,	0x00002000,	0x00000000,	0x00002000,	0x00002000,	0x00000000,	0x00002000,	0x00002000,	0x00002000,	0x00002000,	0x00000000,	0x00000000,	0x00004000,	0x00001000,	0x00000000,	0x00000000,	0x00002000,	0x00002000,	0x00000000,	0x00002000,	0x00000000,	0x00002000,	0x00000000,     0x00002000, 
0x00005000,	0x00001000,	0x00006000,	0x00003000,	0x00003000,	0x00003000,	0x00007000,	0x00001000,	0x00007000,	0x00003000,	0x00007000,	0x00007000,	0x00004000,	0x00007000,	0x00006000,	0x00006000,	0x00002000,	0x00000000,	0x00000000,	0x00000000,	0x00007000,	0x00004000,	0x00007000,	0x00006000,	0x00005000,	0x00005000,	0x00003000,	0x00004000,     0x00007000, 
0x00007000,	0x00001000,	0x00003000,	0x00003000,	0x00005000,	0x00006000,	0x00006000,	0x00003000,	0x00007000,	0x00007000,	0x00007000,	0x00004000,	0x00006000,	0x00001000,	0x00006000,	0x00006000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00005000,	0x00004000,	0x00000000,	0x00007000,	0x00005000,	0x00007000,	0x00005000,	0x00006000,     0x00003000,
// ��� ��� 2 ���������� COM00,COM10,COM20
0x00000400,	0x00000000,	0x00000400,	0x00000400,	0x00000000,	0x00000400,	0x00000400,	0x00000000,	0x00000400,	0x00000400,	0x00000000,	0x00000400,	0x00000400,	0x00000400,	0x00000400,	0x00000000,	0x00000000,	0x00000800,	0x00000040,	0x00000000,	0x00000000,	0x00000400,	0x00000400,	0x00000000,	0x00000400,	0x00000000,	0x00000400,	0x00000000,     0x00000400, 
0x00000840,	0x00000040,	0x00000C00,	0x00000440,	0x00000440,	0x00000440,	0x00000C40,	0x00000040,	0x00000C40,	0x00000440,	0x00000C40,	0x00000C40,	0x00000800,	0x00000C40,	0x00000C00,	0x00000C00,	0x00000400,	0x00000000,	0x00000000,	0x00000000,	0x00000C40,	0x00000800,	0x00000C40,	0x00000C00,	0x00000840,	0x00000840,	0x00000440,	0x00000800,     0x00000C40, 
0x00000C40,	0x00000040,	0x00000440,	0x00000440,	0x00000840,	0x00000C00,	0x00000C00,	0x00000440,	0x00000C40,	0x00000C40,	0x00000C40,	0x00000800,	0x00000C00,	0x00000040,	0x00000C00,	0x00000C00,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000840,	0x00000800,	0x00000000,	0x00000C40,	0x00000840,	0x00000C40,	0x00000840,	0x00000C00,     0x00000440, 
 // ��� ��� 3 ���������� COM00,COM10,COM20
0x00800000,	0x00000000,	0x00800000,	0x00800000,	0x00000000,	0x00800000,	0x00800000,	0x00000000,	0x00800000,	0x00800000,	0x00000000,	0x00800000,	0x00800000,	0x00800000,	0x00800000,	0x00000000,	0x00000000,	0x00000020,	0x00400000,	0x00000000,	0x00000000,	0x00800000,	0x00800000,	0x00000000,	0x00800000,	0x00000000,	0x00800000,	0x00000000,     0x00800000, 
0x00400020,	0x00400000,	0x00800020,	0x00C00000,	0x00C00000,	0x00C00000,	0x00C00020,	0x00400000,	0x00C00020,	0x00C00000,	0x00C00020,	0x00C00020,	0x00000020,	0x00C00020,	0x00800020,	0x00800020,	0x00800000,	0x00000000,	0x00000000,	0x00000000,	0x00C00020,	0x00000020,	0x00C00020,	0x00800020,	0x00400020,	0x00400020,	0x00C00000,	0x00000020,     0x00C00020, 
0x00C00020,	0x00400000,	0x00C00000,	0x00C00000,	0x00400020,	0x00800020,	0x00800020,	0x00C00000,	0x00C00020,	0x00C00020,	0x00C00020,	0x00000020,	0x00800020,	0x00400000,	0x00800020,	0x00800020,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00400020,	0x00000020,	0x00000000,	0x00C00020,	0x00400020,	0x00C00020,	0x00400020,	0x00800020,     0x00C00000, 
// ��� ��� 4 ���������� COM00,COM10,COM20
0x00000008,	0x00000000,	0x00000008,	0x00000008,	0x00000000,	0x00000008,	0x00000008,	0x00000000,	0x00000008,	0x00000008,	0x00000000,	0x00000008,	0x00000008,	0x00000008,	0x00000008,	0x00000000,	0x00000000,	0x00000010,	0x00000004,	0x00000000,	0x00000000,	0x00000008,	0x00000008,	0x00000000,	0x00000008,	0x00000000,	0x00000008,	0x00000000,     0x00000008, 
0x00000014,	0x00000004,	0x00000018,	0x0000000C,	0x0000000C,	0x0000000C,	0x0000001C,	0x00000004,	0x0000001C,	0x0000000C,	0x0000001C,	0x0000001C,	0x00000010,	0x0000001C,	0x00000018,	0x00000018,	0x00000008,	0x00000000,	0x00000000,	0x00000000,	0x0000001C,	0x00000010,	0x0000001C,	0x00000018,	0x00000014,	0x00000014,	0x0000000C,	0x00000010,     0x0000001C, 
0x0000001C,	0x00000004,	0x0000000C,	0x0000000C,	0x00000014,	0x00000018,	0x00000018,	0x0000000C,	0x0000001C,	0x0000001C,	0x0000001C,	0x00000010,	0x00000018,	0x00000004,	0x00000018,	0x00000018,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00000014,	0x00000010,	0x00000000,	0x0000001C,	0x00000014,	0x0000001C,	0x00000014,	0x00000018,     0x0000000C, 
// ��� ��� 5 ���������� COM00,COM10,COM20
0x00000001,	0x00000000,	0x00000001,	0x00000001,	0x00000000,	0x00000001,	0x00000001,	0x00000000,	0x00000001,	0x00000001,	0x00000000,	0x00000001,	0x00000001,	0x00000001,	0x00000001,	0x00000000,	0x00000000,	0x00000002,	0x00200000,	0x00000000,	0x00000000,	0x00000001,	0x00000001,	0x00000000,	0x00000001,	0x00000000,	0x00000001,	0x00000000,     0x00000001, 
0x00200002,	0x00200000,	0x00000003,	0x00200001,	0x00200001,	0x00200001,	0x00200003,	0x00200000,	0x00200003,	0x00200001,	0x00200003,	0x00200003,	0x00000002,	0x00200003,	0x00000003,	0x00000003,	0x00000001,	0x00000000,	0x00000000,	0x00000000,	0x00200003,	0x00000002,	0x00200003,	0x00000003,	0x00200002,	0x00200002,	0x00200001,	0x00000002,     0x00200003, 
0x00200003,	0x00200000,	0x00200001,	0x00200001,	0x00200002,	0x00000003,	0x00000003,	0x00200001,	0x00200003,	0x00200003,	0x00200003,	0x00000002,	0x00000003,	0x00200000,	0x00000003,	0x00000003,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00200002,	0x00000002,	0x00000000,	0x00200003,	0x00200002,	0x00200003,	0x00200002,	0x00000003,     0x00200001, 
// ��� ��� 6 ���������� COM00,COM10,COM20  
0x00080000,	0x00000000,	0x00080000,	0x00080000,	0x00000000,	0x00080000,	0x00080000,	0x00000000,	0x00080000,	0x00080000,	0x00000000,	0x00080000,	0x00080000,	0x00080000,	0x00080000,	0x00000000,	0x00000000,	0x00100000,	0x00040000,	0x00000000,	0x00000000,	0x00080000,	0x00080000,	0x00000000,	0x00080000,	0x00000000,	0x00080000,	0x00000000,     0x00080000, 
0x00140000,	0x00040000,	0x00180000,	0x000C0000,	0x000C0000,	0x000C0000,	0x001C0000,	0x00040000,	0x001C0000,	0x000C0000,	0x001C0000,	0x001C0000,	0x00100000,	0x001C0000,	0x00180000,	0x00180000,	0x00080000,	0x00000000,	0x00000000,	0x00000000,	0x001C0000,	0x00100000,	0x001C0000,	0x00180000,	0x00140000,	0x00140000,	0x000C0000,	0x00100000,     0x001C0000, 
0x001C0000,	0x00040000,	0x000C0000,	0x000C0000,	0x00140000,	0x00180000,	0x00180000,	0x000C0000,	0x001C0000,	0x001C0000,	0x001C0000,	0x00100000,	0x00180000,	0x00040000,	0x00180000,	0x00180000,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00140000,	0x00100000,	0x00000000,	0x001C0000,	0x00140000,	0x001C0000,	0x00140000,	0x00180000,     0x000C0000, 
// ��� ��� 7 ���������� COM00,COM10,COM20  
0x00000200,	0x00000000,	0x00000200,	0x00000200,	0x00000000,	0x00000200,	0x00000200,	0x00000000,	0x00000200,	0x00000200,	0x00000000,	0x00000200,	0x00000200,	0x00000200,	0x00000200,	0x00000000,	0x00000000,	0x00010000,	0x00000100,	0x00000000,	0x00000000,	0x00000200,	0x00000200,	0x00000000,	0x00000200,	0x00000000,	0x00000200,	0x00000000,     0x00000200, 
0x00010100,	0x00000100,	0x00010200,	0x00000300,	0x00000300,	0x00000300,	0x00010300,	0x00000100,	0x00010300,	0x00000300,	0x00010300,	0x00010300,	0x00010000,	0x00010300,	0x00010200,	0x00010200,	0x00000200,	0x00000000,	0x00000000,	0x00000000,	0x00010300,	0x00010000,	0x00010300,	0x00010200,	0x00010100,	0x00010100,	0x00000300,	0x00010000,     0x00010300, 
0x00010300,	0x00000100,	0x00000300,	0x00000300,	0x00010100,	0x00010200,	0x00010200,	0x00000300,	0x00010300,	0x00010300,	0x00010300,	0x00010000,	0x00010200,	0x00000100,	0x00010200,	0x00010200,	0x00000000,	0x00000000,	0x00000000,	0x00000000,	0x00010100,	0x00010000,	0x00000000,	0x00010300,	0x00010100,	0x00010300,	0x00010100,	0x00010200,     0x00000300 
};                                                                                                                                                                                                 
                                                                                                                                                                                                 

//static const uint32_t LCD_statusMask = 0x001148b2;
//static const uint32_t LCD_inv_stMask = 0xFFEEB74D;

void LCD8_Init(void)
{  
  // enable clock for Port A,B,C,D
  SET_BIT(RCC->IOPENR, RCC_IOPENR_IOPAEN | RCC_IOPENR_IOPBEN | RCC_IOPENR_IOPCEN /*| RCC_AHBENR_GPIODEN*/);
  // enable LCD clk 
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_LCDEN); 

  // Set Pin for Port A
  InitLcdPin(GPIOA, 1);
  InitLcdPin(GPIOA, 2);
  InitLcdPin(GPIOA, 3);
  InitLcdPin(GPIOA, 6);
  InitLcdPin(GPIOA, 7);
  InitLcdPin(GPIOA, 15);
  InitLcdPin(GPIOA, 8);
  InitLcdPin(GPIOA, 9);
  InitLcdPin(GPIOA, 10);
  // Set Pin for Port B
  InitLcdPin(GPIOB, 0);
  InitLcdPin(GPIOB, 1);
  InitLcdPin(GPIOB, 3);
  InitLcdPin(GPIOB, 4);
  InitLcdPin(GPIOB, 5);
  InitLcdPin(GPIOB, 8);
  InitLcdPin(GPIOB, 10);
  InitLcdPin(GPIOB, 11);
  InitLcdPin(GPIOB, 12);
  InitLcdPin(GPIOB, 13);
  InitLcdPin(GPIOB, 14);
  InitLcdPin(GPIOB, 15);
  // Set Pin for Port C
  InitLcdPin(GPIOC, 0);
  InitLcdPin(GPIOC, 1);
  InitLcdPin(GPIOC, 2);
  InitLcdPin(GPIOC, 3);
  InitLcdPin(GPIOC, 4);
  InitLcdPin(GPIOC, 5);
  // Set Pin for Port D
//  InitLcdPin(GPIOD, 2);

  // Disable LCD
  LCD8_Disable();
  // LCD control: 1/4 Bias, 1/8 Duty, VSel internal,
  MODIFY_REG(LCD->CR, LCD_CR_DUTY|LCD_CR_BIAS|LCD_CR_VSEL, LCD_CR_DUTY_1|LCD_CR_BIAS_1/*|LCD_CR_VSEL*/);
    // Configure LCD Frame register for 40Hz Frame frequency
//  MODIFY_REG(LCD->FCR, LCD_FCR_HD, LCD_FCR_HD );    // HD=1
  MODIFY_REG(LCD->FCR, LCD_FCR_DIV, 9 << 18);   // DIV = 9
  MODIFY_REG(LCD->FCR, LCD_FCR_BLINK, 0 << 16); // 0: Blink disabled
  MODIFY_REG(LCD->FCR, LCD_FCR_CC, optic_kontrast << 10);    // Contrast Control = 7
   SET_BIT(LCD->FCR,LCD_FCR_PON_0);
  LCD8_Enable();
 
}

//-----------------------------------------------------------------------------
void LCD8_Enable(void)
{
  SET_BIT(LCD->CR, LCD_CR_LCDEN);
}

//-----------------------------------------------------------------------------
void LCD8_Disable(void)
{
  CLEAR_BIT(LCD->CR, LCD_CR_LCDEN);
}

//-----------------------------------------------------------------------------
static void InitLcdPin(GPIO_TypeDef* Port, uint8_t Pin)
{
  // Set alternative function mode for given port
  MODIFY_REG(Port->MODER, GPIO_MODER_MODE0<<(Pin * 2), 0x02 << (Pin * 2));
  // Set Port to medium Speed
  MODIFY_REG(Port->OSPEEDR, GPIO_OSPEEDER_OSPEED0<<(Pin * 2), 0x02 << (Pin * 2));
  // Set Alternate function 1 to port
  MODIFY_REG(Port->AFR[Pin / 8], 0x0F << (Pin % 8 * 4), 0x01 << (Pin % 8 * 4));
}

//-----------------------------------------------------------------------------
void LCD8_Update(void)
{
  while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS))
    ; // Wait for Display updated
  SET_BIT(LCD->SR, LCD_SR_UDR); // Update Display request
}

//-----------------------------------------------------------------------------
void LCD8_testSym(void)
{
  for (char i =0; i<0xA; i++)
  {
    for (char k =0; k <0x8; k++)
      LCD_string[k] = i+0x30;
      display_buffer(0,0);
  }  
} 

//-----------------------------------------------------------------------------
void display_buffer(uint8_t pLCD_menu_point, uint8_t blink_point)
{
  char otersymb, pos_sym;
  uint32_t blink_data = 0;
  int i=0;
   
  LCD8_clearRAM();
    pos_sym = 0;

   while(LCD_string[i]!=0)
   {
     if ((LCD_string[i] >= 0x30) && (LCD_string[i] <= 0x39))
       otersymb = LCD_string[i]-0x30;
    else
     switch (LCD_string[i])
     {
     case 'A':
     case 'a':
         otersymb = 10;
         break;
     case 'B':
     case 'b':
         otersymb = 11;
         break;
     case 'C':
     case 'c':
         otersymb = 12;
         break;
     case 'D':
     case 'd':
         otersymb = 13;
         break;
     case 'E':
     case 'e':
         otersymb = 14;
         break;
     case 'F':
     case 'f':
         otersymb = 15;
         break;
     case '-':
         otersymb = 16;
         break;
     case '.':
         otersymb = 18;
         pos_sym--;
         break;
     case 'H':
         otersymb = 20;
         break;
     case 'L':
         otersymb = 21;
         break;
     case 'o':
         otersymb = 22;
         break;
     case 'P':
         otersymb = 23;
         break;
     case 'U':
         otersymb = 24;
         break;
     case '�':
         otersymb = 25;
         break;
     case '�':
         otersymb = 26;
         break;
     case '�':
         otersymb = 27;
         break;
     case '�':
     case '�':
         otersymb = 28;
         break;
     default:
         otersymb = 19; 
         break; 
     }
    while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS));

    LCD->RAM[0] |= LCD_COM[pos_sym][0][otersymb];
    LCD->RAM[2] |= LCD_COM[pos_sym][1][otersymb];
    LCD->RAM[4] |= LCD_COM[pos_sym][2][otersymb];
    i++;
    if(i > 15)
      break;
    if(++pos_sym > 7)
    {
       if(LCD_string[i] == '.')
          __NOP();
       else
          break;
    }
  }


// ��������� ����
  /*
  if(pLCD_menu_point & 0x40 && !(pLCD_menu_point & blink_point))
  {
    while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS));
       LCD->RAM[0] |= LCD_COM[1][0][17];
  }
  if(pLCD_menu_point & 0x80 && !(pLCD_menu_point & blink_point))
  {
    while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS));
       LCD->RAM[0] |= LCD_COM[0][0][17];
  }
  */
  /*
  if(pLCD_menu_point == 0xFF)
  {
    uint32_t buf = 0;
    for(uint8_t i=0; i < 8; i++)
      buf |= LCD_COM[i][0][17];
    while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS));
       LCD->RAM[0] |= buf; // 
  }
  */
 //   while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS));
 //      LCD->RAM[0] |= LCD_COM[(pLCD_menu_point & 0x07)][0][17];
       
  if (blink_point || pLCD_menu_point)
  {
    uint8_t temp_bit=0;
    uint8_t temp_bit_point = pLCD_menu_point | blink_point;
    for(uint8_t i = 0; i < 8; i++)
    {
      temp_bit = 1 << i;
   //   if(blink_point & temp_bit)
      if(temp_bit_point & temp_bit)
      {
         if(blink_point & temp_bit)
         {
            if(timeSystemUnix % 2)
               blink_data = LCD_COM[i][0][17]; 
            else
              blink_data = 0;
         }
         else
            blink_data = LCD_COM[i][0][17];
         while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS));
         LCD->RAM[0] |= blink_data;
      }
    }
  //  blink_data ^= LCD_COM[(blink_point & 0x0F)][0][17]; 
   // LCD->RAM[0] |= blink_data;
  } 
  LCD8_Update();
 }        


//-----------------------------------------------------------------------------
// �������� ������� 
void LCD8_clearRAM(void)
{
   while((LCD->SR & LCD_SR_UDR) && (LCD->SR & LCD_SR_ENS));

   LCD->RAM[0] = 0;// &= LCD_statusMask;
   LCD->RAM[2] = 0;
   LCD->RAM[4] = 0;
}




  
  