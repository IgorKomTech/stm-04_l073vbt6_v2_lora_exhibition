//=============================================================================
// 
//=============================================================================
/// \file    Io.c
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Io (jumpers and switch on Gasmeter Reference Design)
//=============================================================================

//=============================================================================

#include "Io.h"
#include "Time_g.h"
#include "Menu.h"
#include "UART_152.h"
#include "stm32l073xx.h"

//#include "stm32l1xx_tim.h"

void startOutputTimer(uint16_t pPeriod);
void stopOutputTimer(void);


uint16_t Pulse;
uint8_t OptoPort = OPTO_PORT_DISABLE;
uint32_t PressKeyDelay = 0, LockPressKeyDelay =0;
TKeyPressed KeyPressed = KEY_PRESSED_NOT, LockKeyState = KEY_PRESSED_NOT;

// ������������� ����� - ������ ������, �������� �������� �� 55 - ������  
uint8_t CLB_lock; 


// ----------------------------------------------------------------------------
// Init OptoSwitch and Key 
void Io_InitHardware(void)
{
  //Power off for Extern Flash and sensor
  // PORT C
  SET_BIT(RCC->IOPENR, RCC_IOPENR_IOPCEN); 
  SET_BIT(RCC->IOPENR, RCC_IOPENR_IOPBEN); 
  SET_BIT(RCC->IOPENR, RCC_IOPENR_IOPEEN); 
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE6, GPIO_MODER_MODE6_0); // FLASH ON   
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE9, GPIO_MODER_MODE9_0); // SENS_PWR_ON
//  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE7, GPIO_MODER_MODE7_0); // PWR_BAT2 
//  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE8, GPIO_MODER_MODE8_0); // PWR_BAT1 
 
  SET_BIT(GPIOC->ODR,GPIO_ODR_OD9);  // Sensor Power OFF
  SET_BIT(GPIOC->ODR,GPIO_ODR_OD6);  // Power Flash OFF

//  CLEAR_BIT(GPIOA->MODER,GPIO_MODER_MODE5);
     
  //SET_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPD7_1); // pull down
  //SET_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPD8_1); // pull down
  
  // Key1 is GPIO PC13, configure as input with pullup
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE13);
  CLEAR_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPD13_0);
  
    // Key1 is GPIO PC13, configure as input with pullup
  CLEAR_BIT(GPIOB->MODER, GPIO_MODER_MODE9);
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE7);
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE8);
  CLEAR_BIT(GPIOE->MODER, GPIO_MODER_MODE6);
  
  // Redcontact (OPTO _ON) is GPIO PC12, configure as input with pullup
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE12);
  CLEAR_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPD12_0);  
  
  // CalibrLock - PB2
  CLEAR_BIT(GPIOB->MODER, GPIO_MODER_MODE2);
  SET_BIT(GPIOB->PUPDR, GPIO_PUPDR_PUPD2_0);
 
   // PD2 - input SGM_PWR_PG
  SET_BIT(RCC->IOPENR, RCC_IOPENR_IOPDEN); 
  CLEAR_BIT(GPIOD->MODER, GPIO_MODER_MODE2);  
  CLEAR_BIT(GPIOD->PUPDR, GPIO_PUPDR_PUPD2);    
   // PD1 SGM_PWR_EN
  MODIFY_REG(GPIOD->MODER, GPIO_MODER_MODE1,GPIO_MODER_MODE1_0);  
  MODIFY_REG(GPIOD->PUPDR, GPIO_PUPDR_PUPD1,GPIO_PUPDR_PUPD1_1);
  CLEAR_BIT(GPIOD->ODR, GPIO_ODR_OD1);
  
     //PD0 MCU_GSM_PWR
  MODIFY_REG(GPIOD->MODER, GPIO_MODER_MODE0,GPIO_MODER_MODE0_0); 
  CLEAR_BIT(GPIOD->ODR, GPIO_ODR_OD0);  
  
    //PD8,PD9  Valve ON/OFF   
  MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE4,GPIO_MODER_MODE4_0);
  MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE5,GPIO_MODER_MODE5_0);
  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_OD4);  
  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_OD5);  

  CLEAR_BIT(RCC->IOPENR, RCC_IOPENR_IOPDEN); 
  
  //External Watchdog  
  SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPHEN);  
  CLEAR_BIT(GPIOH->MODER,GPIO_MODER_MODE1);  //  ���� WAKE
  MODIFY_REG(GPIOH->MODER, GPIO_MODER_MODE0, GPIO_MODER_MODE0_0); // ����� Done
  CLEAR_BIT(GPIOH->ODR, GPIO_ODR_OD0);
  CLEAR_BIT(RCC->IOPENR,RCC_IOPENR_IOPHEN);  
  //End External Watchdog;  
  
  
  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
  
  // � �������� ��������� ���������� 12,13 ���� C
  SYSCFG->EXTICR[3] |= (uint16_t)SYSCFG_EXTICR4_EXTI13_PC;  // Key1
  SYSCFG->EXTICR[3] |= (uint16_t)SYSCFG_EXTICR4_EXTI12_PC;  // Enable OPTO_PORT
  SYSCFG->EXTICR[0] |= (uint16_t)SYSCFG_EXTICR1_EXTI2_PB;   // Kalibr LOCK
  
  
 // �������� ������������ ���������� �� �����
  SET_BIT(EXTI->FTSR, EXTI_FTSR_TR12|EXTI_FTSR_TR13|EXTI_FTSR_TR2);
  // �������� ������������ ���������� �� ������������ ������
  SET_BIT(EXTI->RTSR, EXTI_RTSR_TR12|EXTI_RTSR_TR13|EXTI_RTSR_TR2);

  // ��������� �� ����
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE8); 
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE7);
  
  // ������� CMSIS ����������� ���������� � NVIC
  NVIC_SetPriority(EXTI4_15_IRQn,3);
  NVIC_EnableIRQ(EXTI4_15_IRQn );
  
  NVIC_SetPriority(EXTI2_3_IRQn,3);
  NVIC_EnableIRQ(EXTI2_3_IRQn );
  
   
  //��������� ����������
  EXTI->IMR |= EXTI_IMR_IM13|EXTI_IMR_IM12|EXTI_IMR_IM2;
  if(READ_BIT(GPIOC->IDR, GPIO_IDR_ID12)==0) 
      OptoPort = OPTO_PORT_ENABLE;     
  
 /* 
  RCC->IOPSMENR =0;
  SET_BIT(RCC->IOPSMENR, RCC_IOPSMENR_IOPASMEN | RCC_IOPSMENR_IOPBSMEN\
     | RCC_IOPSMENR_IOPCSMEN); 
  RCC->AHBSMENR = 0;
  SET_BIT(RCC->AHBSMENR,RCC_AHBSMENR_DMASMEN | RCC_AHBSMENR_SRAMSMEN);
  RCC->APB2SMENR =0;
  SET_BIT(RCC->APB2SMENR, RCC_APB2SMENR_SYSCFGSMEN);
  RCC->APB1SMENR =0;
  SET_BIT(RCC->APB1SMENR,RCC_APB1SMENR_LCDSMEN |RCC_APB1SMENR_WWDGSMEN\
    |RCC_APB1SMENR_LPUART1SMEN);
*/
  
}

// ----------------------------------------------------------------------------
/*
void EXTI4_15_IRQHandler(void)
{
  static uint32_t PressKeyTime, LockPressTime ;
  
  
  if (EXTI->PR & EXTI_PR_PR13) // Key request?
  {
    SET_BIT(EXTI->PR, EXTI_PR_PR13);
     if(READ_BIT(GPIOC->IDR, GPIO_IDR_ID13)==0) 
     {
       PressKeyTime = Time_GetSystemUpTimeSecond();
       KeyPressed = KEY_PRESSED_DOWN;
     }  
     else
     {
        PressKeyDelay = Time_GetSystemUpTimeSecond() - PressKeyTime;  
//      if(PressKeyDelay)
         if(PressKeyDelay < 2)
           KeyPressed = KEY_PRESSED_SHORT;
          else
           KeyPressed = KEY_PRESSED_LONG;
        PressKeyDelay =0;   
     } 
    return;  // End key request handler 
    } 

  if (EXTI->PR & EXTI_PR_PR12) // Red Cont. request?
  {
    EXTI->PR |= EXTI_PR_PR12;
     if(READ_BIT(GPIOC->IDR, GPIO_IDR_ID12)==0) 
     { 
       OptoPort = OPTO_PORT_ENABLE;      /// ���� ���������� ���������
       Opto_Uart_Init();
     } 
        
     else
     {
       OptoPort = OPTO_PORT_DISABLE;      /// ����� ����� ���������� ���������
       Opto_Uart_deInit();
     } 
  }
 if (EXTI->PR & EXTI_PR_PR15) // LockKey request?
  {
    SET_BIT(EXTI->PR,EXTI_PR_PR15);
   if(READ_BIT(GPIOD->IDR, GPIO_IDR_ID15)==0) 
     {
       LockPressTime = Time_GetSystemUpTimeSecond();
       LockKeyState = KEY_PRESSED_DOWN;
     }  
     else
     {
       LockPressKeyDelay = Time_GetSystemUpTimeSecond() - LockPressTime;  
         if(LockPressKeyDelay > 1)
         { 
           LockKeyState = KEY_PRESSED_NOT;
           if (CLB_lock == 0x55) 
             CLB_lock = 0;
           else
             CLB_lock = 0x55;
         LockPressKeyDelay =0;   
         } 
    return;  // End key request handler 
    }
 }
}
*/  
// ----------------------------------------------------------------------------
void InitOtputTimer(void)
{
  SET_BIT(RCC->APB1ENR,RCC_APB1ENR_TIM6EN);
  TIM6->PSC = 16000-1;  
  TIM6->ARR = 0xFFFF;
  SET_BIT(TIM6->DIER,TIM_DIER_UIE);
  NVIC_SetPriority(TIM6_IRQn,3);
  NVIC_EnableIRQ(TIM6_IRQn);
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODE10,GPIO_MODER_MODE10_0); 
}
// ----------------------------------------------------------------------------
void DeInitOtputTimer(void)
{
  CLEAR_BIT(RCC->APB1ENR,RCC_APB1ENR_TIM6EN);
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODE10,GPIO_MODER_MODE10_1); 
}  
// ----------------------------------------------------------------------------
void stopOutputTimer(void)
{
  CLEAR_BIT(TIM6->CR1,TIM_CR1_CEN);       // ��������� �������
  CLEAR_BIT(GPIOC->ODR,GPIO_ODR_OD10);  // ����� � 0
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODE10,GPIO_MODER_MODE10_1);
 // SET_BIT(GPIOC->BRR,GPIO_BRR_BR_7);
}

// ----------------------------------------------------------------------------
void TIM6_IRQHandler(void)
{
 if(TIM6->SR & TIM_SR_UIF)
 {
  CLEAR_BIT(TIM6->SR,TIM_SR_UIF);
    if (Pulse == 0)
    {
      stopOutputTimer();
      return;
    } 
    if(READ_BIT(GPIOC->IDR,GPIO_IDR_ID10))
       CLEAR_BIT(GPIOC->ODR,GPIO_ODR_OD10); // ����� � 0
    else
    {
      SET_BIT(GPIOC->ODR,GPIO_ODR_OD10);
      Pulse--;
    }

 } 
} 
   

// ----------------------------------------------------------------------------
void startOutputTimer(uint16_t pPeriod)
{
  TIM6->ARR = pPeriod;
  TIM6->CNT = 0;
  MODIFY_REG(GPIOC->MODER,GPIO_MODER_MODE10,GPIO_MODER_MODE10_0);
  SET_BIT(GPIOC->ODR,GPIO_ODR_OD10);
  SET_BIT(TIM6->CR1,TIM_CR1_CEN);
  
}



  


