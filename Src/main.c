// ******************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

#include "FreeRTOS.h"
#include "task.h"

#include "Typedef.h"
#include "Error.h"
#include "Time_g.h"
#include "Io.h"
#include "I2c.h"
#include "lcd_8s.h"
#include "Utils.h"
#include "System.h"
#include "Sgm.h"
#include "Gasmeter.h"
#include "FlowMeasurement.h"
#include "Menu.h"
#include "UART_152.h"
#include "ExtFlash.h"
#include "Archiv.h"
#include <string.h>
#include "OptoComand.h"
#include "USART_Modem.h"
#include "Configuration.h"
#include <stdlib.h>
#include <stdio.h>
#include "Eeprom.h"
#include "stm32l0xx_ll_rcc.h"
#include "ValveTask.h"

#include "GSMSIM800C.h"
#include "Global.h"
//#include "main_function.h"
#include "main_function.c"
#define CONFIG_USE_CATEGORY_MAIN
#define CONFIG_USE_CATEGORY_WELMEC

/* Variables -----------------------------------------------------------------*/
uint8_t flag_end_task=0;
uint8_t error_in_SGM_module;

extern uint8_t successful_transfer_data_to_server;
extern LL_RTC_TimeTypeDef SystTime;
extern LL_RTC_DateTypeDef SystDate;
//extern uint8_t counter_failed_transmission_LORA;
uint8_t rez_LORA_ATY;
uint8_t flag_opto_reconnect = 0;
uint32_t additional_time_message = 0;
//uint8_t cnt_repetition_connect_TCP;

osThreadId GasmeterTaskHandle;
osThreadId OPTO_ReadTaskHandle;
osThreadId ReadWriteARCTaskHandle;

osThreadId GSMTaskHandle;
osThreadId GSM_RX_UARTTaskHandle;

SemaphoreHandle_t xRTCSemaphore;// = NULL;
SemaphoreHandle_t xRxSemaphore;// = NULL;

osMessageQId QueueGSM_RX_UARTHandle;

SemaphoreHandle_t xMdmArcRecReadySemaphore;
SemaphoreHandle_t xOptoArcRecReadySemaphore;
SemaphoreHandle_t xArcRecReadyWriteSemaphore;

xQueueHandle xARCQueue;

Error Gl_Error;

/* Private function prototypes -----------------------------------------------*/
void GasmeterTask(void const * argument);
static Error InitModules(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */
//static void LogError(Error error);
void StartLORATask(void const * argument);
void StartGSM_RX_UARTTask(void const * argument);

const struct _Mode_Transfer Mode_Transfer;

//extern const char MdmPort_Server[6];


struct _FlagMessageTMR FlagMessageTMR;

/*---------------------------------------------------------------------------*/
int main(void)
{

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

   // memcpy(SDa4uaryb7Ketting.number_balans, "*100#", 5);
//  strcpy((char*)Setting.ipserver, "089.109.032.058");
 // strcpy((char*)Setting.portserver, "40050");
//  Eeprom_WriteString((uint8_t*)&MdmNumberBalans, "*100#", sizeof("*100#"));
 // Eeprom_WriteString((uint8_t*)&MdmURL_Server, "89.109.23.203", sizeof("89.109.23.203"));
 // Eeprom_WriteString((uint8_t*)&MdmNumberBalans, "*100#", sizeof("*100#"));
 // Eeprom_WriteString((uint8_t*)&MdmURL_Server, "89.109.23.203", sizeof("89.109.23.203")); // history
 // Eeprom_WriteString((uint8_t*)&MdmURL_Server, "79.126.15.220", sizeof("79.126.15.220")); // new  
 // Eeprom_WriteString((uint8_t*)&MdmPort_Server, "28002", sizeof("28002"));
//  Eeprom_WriteString((uint8_t*)&MdmURL_Server, "089.109.032.058", sizeof("089.109.032.058"));
//  Eeprom_WriteString((uint8_t*)&MdmPort_Server, "40050", sizeof("40050"));  
 // reserved_int = 600;
  successful_transfer_data_to_server = _OK;
  GetDate_Time(&SystDate,&SystTime);
  timeSystemUnix = Get_IndexTime(SystDate, SystTime);
  timeGSMUnixStart = timeSystemUnix;
  time_record_Volume = timeSystemUnix;
  timeUnixGSMNextConnect = timeSystemUnix + 600;
  error_in_SGM_module  = 0;
  CLB_lock = CLB_lock_eeprom;
  MotoHoursToMils = MotoHoursToMilsEEPROM;
  /* Start scheduler */
  osKernelStart();
  
  while (1)
  {

  }
}

/*---------------------------------------------------------------------------*/
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
  Error error;
  System_Init();
  Time_InitHardware();
  LCD8_Init();
  Io_InitHardware();  //// Init OptoSwitch and Key 
  I2c_InitHardware();
  Sgm_InitHardware();
  Opto_Uart_Init();
  CRC_Init();
  MX_LPTIM1_Init();

  LCD8_Menu(TEH_MENU_SW,0);
  error = InitModules();
   
  if(Error_IsError(error))
    Error_SetFatal(&error);
  
  number_communication_gsm_fail = number_communication_gsm_fail_eeprom; 
  current_communication_gsm_number = current_communication_gsm_number_eeprom; 
  //Restart programm!!!
 //  ErrorLog_IncrementErrorCounter(ERRORLOG_BOOT_FAILURE);
  

/* USER CODE BEGIN RTOS_MUTEX */
  
/* USER CODE END RTOS_MUTEX */

/* USER CODE BEGIN RTOS_SEMAPHORES */
  	xRTCSemaphore =  xSemaphoreCreateBinary();
    xRxSemaphore  =  xSemaphoreCreateBinary();
    xMdmArcRecReadySemaphore = xSemaphoreCreateBinary();
    xOptoArcRecReadySemaphore = xSemaphoreCreateBinary();
    xArcRecReadyWriteSemaphore = xSemaphoreCreateBinary();
/* USER CODE END RTOS_SEMAPHORES */

/* USER CODE BEGIN RTOS_TIMERS */

/* USER CODE END RTOS_TIMERS */

/* USER CODE BEGIN RTOS_THREADS */
  osThreadDef(GasmeterTask, GasmeterTask, osPriorityNormal, 0, 220);
  GasmeterTaskHandle = osThreadCreate(osThread(GasmeterTask), NULL);
  
  osThreadDef(OPTO_ReadTask, OPTO_ReadTask, osPriorityNormal, 0, 250);
  OPTO_ReadTaskHandle = osThreadCreate(osThread(OPTO_ReadTask), NULL);
  
  osThreadDef(ReadWriteARC, ReadWriteARC, osPriorityNormal, 0, 180);
  ReadWriteARCTaskHandle = osThreadCreate(osThread(ReadWriteARC), NULL);

    /* definition and creation of GSMTask */
  osThreadDef(LORATask, StartLORATask, osPriorityIdle, 0, 256);
  GSMTaskHandle = osThreadCreate(osThread(LORATask), NULL);

  /* definition and creation of GSM_RX_UARTTask */
  osThreadDef(GSM_RX_UARTTask, StartGSM_RX_UARTTask, osPriorityIdle, 0, 200);
  GSM_RX_UARTTaskHandle = osThreadCreate(osThread(GSM_RX_UARTTask), NULL);
  
//  vTaskSuspend(OPTO_ReadTaskHandle);
 /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  xARCQueue = xQueueCreate( 5, sizeof(TARC_Queue)); 
  vQueueAddToRegistry( xARCQueue, "ARCQueue" );
  
    osMessageQDef(QueueGSM_RX_UART, 5, uint8_t);
  QueueGSM_RX_UARTHandle = osMessageCreate(osMessageQ(QueueGSM_RX_UART), NULL);
  
  /* USER CODE END RTOS_QUEUES */
}

  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
/* StartGSMTask function */
void StartLORATask(void const * argument)
{
  
  for(;;)
  {
    flag_end_task |= 0x01;
    //--- ������� ������ �������� � ����� � ���� ������ ��������� ����� ������ ��� ��������� ������� �������� ������ ��� ������ 
    stack_task_GSM = uxTaskGetStackHighWaterMark(NULL); 
    //--------------------------------------------------------------------------
    
    //--- ������������� ���� ������ �������� �������������� ����� (������� ������ �� �������� ������� �����, ��� ��������� ���� ~4 ������)
     if(TestModeActive != TestModeActive_previous)
     {
          sprintf((char*)OldValue,"%d", TestModeActive_previous);  
          sprintf((char*)NewValue, "%d", TestModeActive); 
          WriteChangeArcRecord(CHANGE_TEST_MODE, CHANGE_PROGRAM); 
          TestModeActive_previous = TestModeActive;
     }
     //--------------------------------------------------------------------------------------------      
     if(!READ_BIT(GPIOC->IDR, GPIO_IDR_ID7))
     {// body cover open
        if(!StatusWarning.body_cover_open)
        {
          //StatusDevice.body_cover_open = 1;
          StatusWarning.body_cover_open = 1;
          LoraStatus.body_cover_open = 1;
          WriteIntEventArcRecord(EVENT_INT_OPEN_CASE, EVENT_INT_BEGIN);
          sprintf((char*)OldValue,"%d", 0);  
          sprintf((char*)NewValue, "%d", 1); 
          WriteChangeArcRecord(CHANGE_BODY_COVER_OPEN, CHANGE_PROGRAM); 
        }
     }
     else
     {
        if(StatusWarning.body_cover_open)
        {
          //StatusDevice.body_cover_open = 0;
          StatusWarning.body_cover_open = 0;
          WriteIntEventArcRecord(EVENT_INT_OPEN_CASE, EVENT_INT_END);
          sprintf((char*)OldValue,"%d", 1);  
          sprintf((char*)NewValue, "%d", 0); 
          WriteChangeArcRecord(CHANGE_BODY_COVER_OPEN, CHANGE_PROGRAM); 
        }
     }     
     //--------------------------------------------------------------------------------------------      
     if(!READ_BIT(GPIOC->IDR, GPIO_IDR_ID8))
     {// battery_cover_open
        if(!StatusWarning.battery_cover_open)
        {
          StatusWarning.battery_cover_open = 1;
          LoraStatus.battery_cover_open = 1;
          WriteIntEventArcRecord(EVENT_INT_BATTERY_COVER_OPEN, EVENT_INT_BEGIN);
          sprintf((char*)OldValue,"%d", 0);  
          sprintf((char*)NewValue, "%d", 1); 
          WriteChangeArcRecord(CHANGE_BATTERY_COVER_OPEN, CHANGE_PROGRAM); 
        }
     }
     else
     {
        if(StatusWarning.battery_cover_open)
        {
          StatusWarning.battery_cover_open = 0;
          LoraStatus.SIM_card_non = 1;
          WriteIntEventArcRecord(EVENT_INT_BATTERY_COVER_OPEN, EVENT_INT_END);
          sprintf((char*)OldValue,"%d", 1);  
          sprintf((char*)NewValue, "%d", 0); 
          WriteChangeArcRecord(CHANGE_BATTERY_COVER_OPEN, CHANGE_PROGRAM); 
        }
     }          
     //--------------------------------------------------------------------------------------------      
     if(READ_BIT(GPIOB->IDR, GPIO_IDR_ID9))
     {// sim card not connected 
        if(!StatusWarning.SIM_card_non)
        {
          StatusWarning.SIM_card_non = 1;
          LoraStatus.SIM_card_non = 1;
          WriteIntEventArcRecord(EVENT_INT_SIM_CARD_NON, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.SIM_card_non)
        {
          StatusWarning.SIM_card_non = 0;
          WriteIntEventArcRecord(EVENT_INT_SIM_CARD_NON, EVENT_INT_END);
        }
     }
     //--------------------------------------------------------------------------------------------      
     if(READ_BIT(GPIOE->IDR, GPIO_IDR_ID6))
     {// battery not connected 
        if(!StatusWarning.battery_not_connected)
        {
          //StatusDevice.battery_not_connected = 1;
          StatusWarning.battery_not_connected = 1;
          LoraStatus.battery_not_connected = 1;
          WriteIntEventArcRecord(EVENT_INT_BATARY_TELEMETRY_NON, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.battery_not_connected)
        {
          //StatusDevice.battery_not_connected = 0;
          StatusWarning.battery_not_connected = 0;
          WriteIntEventArcRecord(EVENT_INT_BATARY_TELEMETRY_NON, EVENT_INT_END);
        }
     }
     //--------------------------------------------------------------------------------------------  
     if(flag_check_crc_dQf)
     {
       flag_check_crc_dQf = 0;
       uint32_t crc;
       crc = CalculateCRC32((uint8_t*)Qf_param, Qf_sizeof, CRC_CTART_STOP);
       if(crc != crc_dQf_eeprom)
       {
          if(!StatusDevice.check_crc_dQf_false)
          {
             StatusDevice.check_crc_dQf_false = 1;
             WriteIntEventArcRecord(EVENT_INT_ERROR_CS_DQF, EVENT_INT_BEGIN);
          }
       }
       else
       {
          if(StatusDevice.check_crc_dQf_false)
          {
             StatusDevice.check_crc_dQf_false = 0;
             WriteIntEventArcRecord(EVENT_INT_ERROR_CS_DQF, EVENT_INT_END);
          }
       }
     }
     //--------------------------------------------------------------------------------------------

    if(CLB_lock == 0x55) 
     {
        if(!StatusDevice.calibration_lock_open) // ���� ���� �� ����������, ������������� ���, ���������� �������� CLB_lock � ������ eeprom
        {
           StatusDevice.calibration_lock_open = 1;  
           Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, CLB_lock);
        }
     }
     else
     {
        if(StatusDevice.calibration_lock_open) // ���� ���� ����������, ���������� ���,���������� �������� CLB_lock � ������ eeprom
        {
           StatusDevice.calibration_lock_open = 0;
           Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, CLB_lock);
        }
     }
    //--------------------------------------------------------------------------
         if(CLB_lock == 0x55)
     {
         if(!StatusDevice.calibration_lock_open)
         {
            StatusDevice.calibration_lock_open = 1;
            Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, 0x55);
            WriteIntEventArcRecord(EVENT_INT_OPEN_CALIB_SWITCH, EVENT_INT_BEGIN);
            
            sprintf((char*)OldValue,"%d", 0);  
            sprintf((char*)NewValue, "%d", CLB_lock); 
            WriteChangeArcRecord(CHANGE_CLB_LOCK, CHANGE_PROGRAM); 
         }
     }
     else
     {
         if(StatusDevice.calibration_lock_open)
         {
            StatusDevice.calibration_lock_open = 0;
            Eeprom_WriteChar((uint32_t)&CLB_lock_eeprom, 0);
            WriteIntEventArcRecord(EVENT_INT_OPEN_CALIB_SWITCH, EVENT_INT_END);
            sprintf((char*)OldValue,"%d", 0x55);  
            sprintf((char*)NewValue, "%d", CLB_lock);
            WriteChangeArcRecord(CHANGE_CLB_LOCK, source_change_CLB_LOCK); 
            source_change_CLB_LOCK = CHANGE_PROGRAM;
         }
     }
     //--------------------------------------------------------------------------------------------
     if(OptoPort == OPTO_PORT_ENABLE)
     {
        if(!StatusDevice.optical_port_communication_session)
        {
           CalculateMotoHourceOPTIC(MOTO_HOURCE_START);
           StatusDevice.optical_port_communication_session = 1;
           WriteTelemetry(TELEM_EVENT_OPTIC_UP, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
           WriteTelemetry(TELEM_EVENT_OPTIC_UP, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
        }
     }
     else
     {
        if(StatusDevice.optical_port_communication_session)
        {
           CalculateMotoHourceOPTIC(MOTO_HOURCE_STOP);
           StatusDevice.optical_port_communication_session = 0;
           WriteTelemetry(TELEM_EVENT_OPTIC_DOWN, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
           WriteTelemetry(TELEM_EVENT_OPTIC_DOWN, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
        }
     }
     //--------------------------------------------------------------------------------------------
     if(error_in_SGM_module)
     {
       if(!StatusDevice.error_in_SGM_module)
       {
          StatusDevice.error_in_SGM_module = 1;
          WriteIntEventArcRecord(EVENT_INT_ERROR_SGM_MODULE, EVENT_INT_BEGIN);
       }
     }
     else
     {
       if(StatusDevice.error_in_SGM_module)
       {
          StatusDevice.error_in_SGM_module = 0;
          WriteIntEventArcRecord(EVENT_INT_ERROR_SGM_MODULE, EVENT_INT_END);
       }
     }
     //-----------------------------------------------------------------------
     if(going_beyond_T)
     {
        if(!StatusDevice.going_beyond_T)
        {
          StatusDevice.going_beyond_T = 1;
          WriteIntEventArcRecord(EVENT_INT_MAX_T, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusDevice.going_beyond_T)
        {
          StatusDevice.going_beyond_T = 0;
          WriteIntEventArcRecord(EVENT_INT_MAX_T, EVENT_INT_END);
        }
     }
     //-----------------------------------------------------------------------
     if(going_beyond_T_warning)
     {
        if(!StatusWarning.Tmax_min_warning)
        {
          StatusWarning.Tmax_min_warning = 1;
          WriteIntEventArcRecord(EVENT_INT_ABAUT_T_MAX, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.Tmax_min_warning)
        {
           StatusWarning.Tmax_min_warning = 0;
           WriteIntEventArcRecord(EVENT_INT_ABAUT_T_MAX, EVENT_INT_END);
        }
     }     
     //-----------------------------------------------------------------------
     if(going_beyond_Q)
     {
        if(!StatusDevice.going_beyond_Q)
        {
          StatusDevice.going_beyond_Q = 1;
          WriteIntEventArcRecord(EVENT_INT_MAX_Q, EVENT_INT_BEGIN);
       //   WriteEventArcRecord(EVENT_GOING_BEYOND_Q_BEGIN, GSM_Get_current_communication_gsm_number());
        }
     }
     else
     {
        if(StatusDevice.going_beyond_Q)
        {
          StatusDevice.going_beyond_Q = 0;
          WriteIntEventArcRecord(EVENT_INT_MAX_Q, EVENT_INT_END);
        //  WriteEventArcRecord(EVENT_GOING_BEYOND_Q_END, GSM_Get_current_communication_gsm_number());
        }
     }
     //-----------------------------------------------------------------------
     if(Qmax_warning_temp)
     {
        if(!StatusWarning.Qmax_warning)
        {
          StatusWarning.Qmax_warning = 1;
          WriteIntEventArcRecord(EVENT_INT_ABAUT_Q_MAX, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusWarning.Qmax_warning)
        {
          StatusWarning.Qmax_warning = 0;
          WriteIntEventArcRecord(EVENT_INT_ABAUT_Q_MAX, EVENT_INT_END);
        }
     }     
     //-----------------------------------------------------------------------
     if(reverse_flow)
     {
        if(!StatusDevice.reverse_flow)
        {
          StatusDevice.reverse_flow = 1;
        //  WriteEventArcRecord(EVENT_REVERSE_FLOW_BEGIN, GSM_Get_current_communication_gsm_number());
          WriteIntEventArcRecord(EVENT_INT_REVERSE_FLOW, EVENT_INT_BEGIN);
        }
     }
     else
     {
        if(StatusDevice.reverse_flow)
        {
          StatusDevice.reverse_flow = 0;
       //   WriteEventArcRecord(EVENT_REVERSE_FLOW_END, GSM_Get_current_communication_gsm_number());
          WriteIntEventArcRecord(EVENT_INT_REVERSE_FLOW, EVENT_INT_END);
        }
     }     
     //-----------------------------------------------------------------------     
    //--------------------------------------------------------------------------
      //-------------------------------------------------------
     if(OptoPort == OPTO_PORT_ENABLE && ((xTaskGetTickCount() - Get_tick_count_message_from_OPTIC_msec()) > WAITING_TIME_CONNECTING_OPTIC_MSEC)) 
     {
        OptoPort = OPTO_PORT_DISABLE;
        Opto_Uart_deInit();
     }

    LORAPower();
    
    if(GSM_Get_flag_power() == GSM_MUST_ENABLE ) // ����� ����� ������ ���� �������
    {
      if(FindSpeedUARTGSMModule() == SPEED_FOUND_ERROR)
      {
        GSM_ExtremalPowerDown();             // ������������� ���������� ������� ������, ������� ���� �� ��������� �������� ��� ������
        Eeprom_WriteDword((uint32_t)&cnt_fail_speed_gsm_modem_eeprom, cnt_fail_speed_gsm_modem_eeprom + 1);
        successful_transfer_data_to_server = _FALSE;
        ++flagPowerONGSMOneTime;
      }  
    }      
    rez_LORA_ATY = RequestRegistration();
    if ((rez_LORA_ATY == LORA_ATY_JOINED)&&(flag_opto_reconnect == 0))
    {
      TransmitDataLORA();                         // �������� ������ �� ����
    } 
    else
      if ((rez_LORA_ATY == LORA_ATY_NOTJOINED)||(flag_opto_reconnect))
    {
      RegistrationModemLORA();                    // ������������ �����
    }       
    
    //------------------------------------------------------------------------
   // uint8_t rez_GSM_TCPConnect;
    /*rez_GSM_TCPConnect = GSM_TCPConnect((uint8_t*)MdmURL_Server, (uint8_t*)MdmPort_Server, (uint8_t*)MdmAPN_Adress, (uint8_t*)MdmAPN_Login, (uint8_t*)MdmAPN_Password);
    if(rez_GSM_TCPConnect == TCP_YES_CONNECT)
    {
       //  tick_count_message_from_server_msec = xTaskGetTickCount(); //###
       //if(DifferenceTickCount(xTaskGetTickCount(), tick_count_message_from_server_msec) > WAITING_TIME_MESSAGE_FROM_SERVER_MSEC)
       if(((xTaskGetTickCount() - tick_count_message_from_server_msec) > WAITING_TIME_MESSAGE_FROM_SERVER_MSEC) 
          && GSM_Get_flag_power() == GSM_MUST_ENABLE)  
       {
          ++number_communication_gsm_fail;
          Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
          ++flagPowerONGSMOneTime;
          memset((char*)&FlagMessageTMR.command, 0, sizeof(FlagMessageTMR));
          GSM_ExtremalPowerDown();
       }

       ParsingMessageTMR();
    }
    else
    {// нет TCP соединени
        if(rez_GSM_TCPConnect == TCP_CONNECTING)
           successful_transfer_data_to_server = _FALSE;
        tick_count_message_from_server_msec = xTaskGetTickCount();
    }
    if(rez_GSM_TCPConnect == TCP_ERROR_CONNECT_INTERNET)
    {
          ++number_communication_gsm_fail;
          Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
          ++flagPowerONGSMOneTime;
          GSM_ExtremalPowerDown();
          rez_GSM_TCPConnect = TCP_NON_CONNECT;
    }*/
    //--- ���� � ��������� �������� ������ � ������ ������ ������ ������� ��� WAITING_TIME_MESSAGE_FROM_GSM_MSEC
    if(((xTaskGetTickCount() - LORA_Get_tick_count_message_from_LORA_msec()) > (WAITING_TIME_MESSAGE_FROM_GSM_MSEC + additional_time_message)) 
        && GSM_Get_flag_power() == GSM_MUST_ENABLE
        && GSM_Get_Status_power() == GSM_POWER_ON)
    {
         ++number_communication_gsm_fail;
         Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
         ++flagPowerONGSMOneTime;
         memset((char*)&FlagMessageTMR.command, 0, sizeof(FlagMessageTMR));
         TransmitDebugMessageOptic("������� ����� �������� ���������");
         GSM_ExtremalPowerDown();   
         
         successful_transfer_data_to_server = _FALSE;
    }    
    //--- ���� �� ������� ������ ������ ������ ������ ������� ��� WAITING_TIME_CONNECTING_SERVER_GSM_MSEC(5 ���)
    if(((xTaskGetTickCount() - LORA_Get_tick_count_powerON_LORA_msec()) > WAITING_TIME_CONNECTING_SERVER_GSM_MSEC) 
        && GSM_Get_flag_power() == GSM_MUST_ENABLE
        && GSM_Get_Status_power() == GSM_POWER_ON)
    {
       ++flagPowerONGSMOneTime;
       ++number_communication_gsm_fail;
       Eeprom_WriteDword((uint32_t)&number_communication_gsm_fail_eeprom,  number_communication_gsm_fail);
       TransmitDebugMessageOptic("������� ����� ������");
       GSM_ExtremalPowerDown();
       successful_transfer_data_to_server = _FALSE;
    }
      
    //-------------------------------------------------------------------------------------------
    if(successful_transfer_data_to_server != _NULL2)
    {

       CalculationNextCommunicationSession();
       successful_transfer_data_to_server = _NULL2;
    }
    //-------------------------------------------------------------------------------------------
    //ParsingMessageTMR();
    Valve_Parsing();
    //--------------------------------------------------------------------------------------------
          ///////////////////////////
      if(*((uint32_t*)&StatusDevice_buf) != *((uint32_t*)&StatusDevice))
      {
         // WriteChangeArcStatus(EVENT_BEGIN);
         StatusDevice_buf = StatusDevice;
         WriteSystemArcRecord(SYSTEM_CHANGE_STATUS, GSM_Get_current_communication_gsm_number());
         
         StatusDevice.start_program = 0;
         StatusDevice.RCC_CSR = 0;
         if(StatusDevice.hard_fault)
         {
            StatusDevice.hard_fault = 0;
          //  WriteSystemArcRecord(EVENT_RESTART, GSM_Get_current_communication_gsm_number());
            Eeprom_WriteChar((uint32_t)&hard_fault,  0); 
         }
         // WriteChangeArcStatus(EVENT_END);
      }
      ///////////////////////////
      if(((timeSystemUnix - time_record_Volume) > 600)
         &&  GSM_Get_Status_power() == POWER_NON 
         && GSM_Get_flag_power() == GSM_MUST_DISABLE)
      {
         time_record_Volume = timeSystemUnix;
         System_SaweBeforeReset();
      }
      //--------------------------------------------------------------------------------------------
    flag_end_task &= ~0x01;
    osDelay(2);
  }
}
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  
// GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
// GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
// GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
/* StartGSM_RX_UARTTask function */
void StartGSM_RX_UARTTask(void const * argument)
{
  for(;;)
  {
     stack_GSM_RX_UARTTask = uxTaskGetStackHighWaterMark(NULL);
     fGSM_RX_UARTTask(); // ������ �� �������
     flag_end_task &= ~0x02;
     osDelay(1);
  }
}
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 

//-----------------------------------------------------------------------------
/* Start GasmeterTask function */
void GasmeterTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
 // IntArcFlag = 2;
   
   StatusDevice.start_program = 1;
   StatusDevice.RCC_CSR = RCC->CSR >> 24;
   LL_RCC_ClearResetFlags();
   StatusDevice.hard_fault = hard_fault;
  /* Infinite loop */
 //  while(!Error_IsFatal(Gl_Error)) 
   while(1)
   {
      stack_GasmeterTask = uxTaskGetStackHighWaterMark(NULL);
      xSemaphoreTake(xRTCSemaphore, portMAX_DELAY);
      System_WatchdogRefresh();
      Gl_Error = Gasmeter_Process();
    //  pressure_abs_gas = pressure_abs;//###

      
      if(Error_IsError(Gl_Error))
      {
         ++cnt_error_SGM;
         if(cnt_error_SGM > cnt_error_SGM_eeprom)
         { 
            //LCD8_Menu(TEH_MENU_ERROR,0);
            error_in_SGM_module = 1;
         }
         InitModules();
      } 
      else
      {
         if(cnt_error_SGM)
         {
            cnt_error_SGM = 0;
            InitModules();
         }
         if(   Global_Menu != MAIN_MENU_VOLUME 
            && (Time_GetSystemUpTimeSecond() - PressKeyTime) > 180)
               Global_Menu = MAIN_MENU_VOLUME;
     //    Global_Menu = TEH_MENU_MDMTST;
         //-------------------------------------
         if(GasMeter.VE_Lm3 > 99999.9999)
           GasMeter.VE_Lm3 = 0;
         //-----------------------------------------
    //     LCD8_Menu(0,0);     
           error_in_SGM_module = 0;
      }
      LCD8_Menu(0,0);
      ///////////////////////////
      if(timeSystemUnix >= timeUnixGSMNextConnect && GSM_Get_flag_power() == GSM_MUST_DISABLE)
      {
          timeGSMUnixStart = timeSystemUnix;
          timeUnixGSMNextConnect = timeSystemUnix + 3600*6; // do not interfere with shutdown
          if(hard_fault)
          {
             successful_transfer_data_to_server = _OK;
          }
          else
          {
             GSM_Set_TCPFlag(TCP_MUST_ENABLE);   // ��������� TCP ����������
             GSM_Set_flag_power(GSM_MUST_ENABLE); // ����������� �����     
             
             if(StatusDevice.GSM_Modem_Power_optic != _OK && StatusDevice.GSM_Modem_Power_switch != _OK)
               StatusDevice.GSM_Modem_Power_mode = _OK;
             if(StatusDevice.GSM_Modem_Power_optic == _OK)
             {
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_OPTIC, 0, 0, EVENT_BEGIN);
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_OPTIC, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
             }
             if(StatusDevice.GSM_Modem_Power_switch == _OK)
             {
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_BUTTON, 0, 0, EVENT_BEGIN);
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_BUTTON, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
             }            
             if(StatusDevice.GSM_Modem_Power_mode == _OK)
             {
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_TIME, 0, 0, EVENT_BEGIN);
               WriteTelemetry(TELEM_EVENT_START_GSM_FOR_TIME, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);
             }   
          }
       }      
      ///////////////////////////
      if(IntArcFlag) // ������� ����, ������������ ������ ��� � 00 ���.
      {
          //GSM_ExtremalPowerDown(); // ����� ��������� ������ ���
          WriteIntEventArcRecord(EVENT_INT_CURRENT_FLOW, EVENT_INT_BEGIN);
          Eeprom_WriteString((uint8_t*)&MotoHoursToMilsEEPROM.time_workingGSM, (uint8_t*)&MotoHoursToMils.time_workingGSM, sizeof(MotoHoursToMils));
          IntArcFlag =0;
          Calculation_residual_capacity_battery_percent();
      }       
      if(!flag_end_task)
	 EnterSTOPMode();
      osDelay(1);
  }
 // LogError(Gl_Error);
 // System_Reset();
 }

//-----------------------------------------------------------------------------
static Error InitModules(void)
{
  Error error;
 
  error = Sgm_InitModule(); // power on
  
  if(Error_IsNone(error))
    error = FlowMeas_InitModule();
  
  if(Error_IsNone(error)) 
    error = Gasmeter_InitModule();
  
  error = Sgm_DeinitModule();
  if(Error_IsNone(error) && strlen(EEPROM_SN_SGM) !=0)
  {
     if(!strcmp(EEPROM_SN_SGM, (char*)Sensor_Const.SN_SGM))
       StatusDevice.check_sn_sgm_false = 0;
     else
       StatusDevice.check_sn_sgm_false = 1;
  }
  return error;
}

//-----------------------------------------------------------------------------
/*
static void LogError(Error error)
{
  if(Gl_Error.Raw != error.Raw)
  {
    Gl_Error.Raw = error.Raw;
    Eeprom_WriteWord((uint32_t)&DisableArcRecord, 0xAA);   /// Log if Error
  }  
  // Add Error in EventArc
}
*/
//-----------------------------------------------------------------------------
void vApplicationIdleHook( void )
{
  stack_IDLE = uxTaskGetStackHighWaterMark(NULL);
  if (KeyPressed == KEY_PRESSED_SHORT)
      Switch_Next_Menu();
  
//  if ((OptoPort==OPTO_PORT_DISABLE) || (READ_BIT(GPIOC->IDR, GPIO_IDR_ID12))) 
 //  __WFI();
//   EnterSTOPMode();
} 


/**
  * @brief System Clock Configuration
  * @retval None
  */
