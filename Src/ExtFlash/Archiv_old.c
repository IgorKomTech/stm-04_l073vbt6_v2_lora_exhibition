//=============================================================================
/// \file    Archiv.c
/// \author  MVA
/// \date    13-Mar-2018
/// \brief   Functions for archive
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "Archiv.h"
#include "Time_g.h"
#include "ExtFlash.h"
#include "System.h"
#include "Eeprom.h"
#include "GasMeter.h"
#include "stm32l073xx.h"
#include <string.h>
#include <time.h>

IntArcTypeDef    IntArc;
IntArcTypeDef    DayliArc;
EventArcTypeDef  EvArc;
ChangeArcTypeDef ChangeArc;

/* ������ ������� �������� ������� */
#pragma section = ".eeprom"
const uint32_t IntArcRecNo @".eeprom" = 0;
const uint32_t DayArcRecNo @".eeprom" = 0;
const uint32_t EvArcRecNo @".eeprom"  = 0;

__root const uint32_t ChangeArcRecNo @".eeprom";

const uint16_t  GasDayBegin @".eeprom" = 0;


const uint16_t IntArcLen = sizeof(IntArcTypeDef);
const uint16_t ChangeArcLen = sizeof(ChangeArcTypeDef);
const uint16_t EventArcLen = sizeof(EventArcTypeDef); 


const uint32_t IntArcMem_Size = IntArcMemBorder_End - IntArcMemBorder_Start; 
const uint16_t IntArcRecCount = (IntArcMemBorder_End - IntArcMemBorder_Start)/sizeof(IntArcTypeDef);

const uint32_t DayArcMem_Size = DayArcMemBorder_End - DayArcMemBorder_Start; 
const uint16_t DayArcRecCount = (DayArcMemBorder_End - DayArcMemBorder_Start)/ sizeof(IntArcTypeDef);

const uint32_t EvArcMem_Size = EventArcMemBorder_End - EventArcMemBorder_Start; 
const uint16_t EvArcRecCount =(EventArcMemBorder_End - EventArcMemBorder_Start)/sizeof(EventArcTypeDef);

const uint32_t ChangeArcMem_Size = ChangeArcMemBorder_End - ChangeArcMemBorder_Start; 
const uint16_t ChangeArcRecCount =(ChangeArcMemBorder_End - ChangeArcMemBorder_Start)/sizeof(ChangeArcTypeDef);



uint8_t IntArcRecBlok = 0;
uint16_t Day_Temp_Count = 0;

float Day_Temp_Midl =0.0; 
float Day_Flow_Max =0.0;
float Day_Temp_Max =0.0;
float Day_Temp_Min =0.0;  




#define SECTOR_LEN 0x1000

//-----------------------------------------------------------------------------
static uint32_t CalcCRC(uint8_t* pRecord, uint16_t pSize)
{
  uint32_t retValue;
  
  SET_BIT(RCC->AHBENR,RCC_AHBENR_CRCEN);
  SET_BIT(CRC->CR,CRC_CR_POLYSIZE|CRC_CR_RESET);
  
  for(uint16_t i =0; i< pSize;i++)
    CRC->DR = pRecord[i] ;
  retValue = CRC->DR;
  CLEAR_BIT(RCC->AHBENR,RCC_AHBENR_CRCEN);
  return retValue;
}  

//-----------------------------------------------------------------------------
uint64_t Get_IndexTime(LL_RTC_DateTypeDef p_ArcDate, LL_RTC_TimeTypeDef p_ArcTime)
{
  struct tm lo_tm;  
  uint64_t ret;
  
  lo_tm.tm_hour = p_ArcTime.Hours;
  lo_tm.tm_min = p_ArcTime.Minutes;
  lo_tm.tm_sec = p_ArcTime.Seconds;  

  lo_tm.tm_year = p_ArcDate.Year +100;
  lo_tm.tm_mon =  p_ArcDate.Month;
  lo_tm.tm_mday = p_ArcDate.Day;
 
  ret =  mktime(&lo_tm);
 return ret;
} 

//-----------------------------------------------------------------------------
uint32_t CalkArcIntAdress(uint32_t p_IntArcRecNo)
{
 uint32_t lo_CurAdress;
 
  lo_CurAdress = IntArcMemBorder_Start + p_IntArcRecNo *IntArcLen;
  if (lo_CurAdress >= IntArcMemBorder_End)  // ����� �� ������� 
      lo_CurAdress -= IntArcMemBorder_End;
 return lo_CurAdress;
} 

//-----------------------------------------------------------------------------
uint32_t CalkArcDayAdress(uint32_t p_DayArcRecNo)
{
 uint32_t lo_CurAdress;
 
  lo_CurAdress = DayArcMemBorder_Start + p_DayArcRecNo *IntArcLen;
  if (lo_CurAdress >= DayArcMemBorder_End)  // ����� �� ������� 
      lo_CurAdress -= DayArcMemBorder_End;
 return lo_CurAdress;
} 

//-----------------------------------------------------------------------------
uint8_t WriteIntArcRecord(uint8_t  pArcFlag)
{
  uint16_t ret =0;
  uint32_t CurAdress;
  LL_RTC_DateTypeDef lo_ArcDate;   
  LL_RTC_TimeTypeDef lo_ArcTime;  
  
   
  SPI_InitHardware();
  GetDate_Time(&lo_ArcDate,&lo_ArcTime);
  
  IntArc.Index_Time =  Get_IndexTime(lo_ArcDate,lo_ArcTime);
  IntArc.ArcDay = lo_ArcDate.Day;
  IntArc.ArcMon = lo_ArcDate.Month;
  IntArc.ArcYear = lo_ArcDate.Year +2000;
  IntArc.ArcHour = lo_ArcTime.Hours;
  IntArc.ArcMinute = lo_ArcTime.Minutes;
  IntArc.ArcSecond = lo_ArcTime.Seconds;

  IntArc.ArcVolume = GasMeter.VE_Lm3;
  IntArc.ArcRecNo = IntArcRecNo;
  IntArc.ArcFlag = pArcFlag;
  IntArc.ArcTemp = Int_Temp_Midl; 
  IntArc.ArcMaxQ = Int_Flow_Max;
  IntArc.ArcMaxT = Int_Temp_Max;
  IntArc.ArcMinT = Int_Temp_Min;  
  IntArc.ArcK = Sensor.KL;
  IntArc.ArcError = Gl_Error.Raw;
  IntArc.ArcStatus = 1;
  IntArc.Reserv = 0;
 
  IntArc.ArcCRC  = CalcCRC((uint8_t*)&IntArc,IntArcLen-8); 

  
  // Calk adress
  CurAdress = CalkArcIntAdress(IntArcRecNo);

  ret = SPI_Write(CurAdress,(uint8_t*)&IntArc,IntArcLen);

  
 CurAdress = IntArcRecNo+1;
 Eeprom_WriteDword((uint32_t)&IntArcRecNo,CurAdress);
 

 Int_Temp_Count = 0;

 // ������� ����������� �� �����
 Day_Temp_Midl += Int_Temp_Midl; 
 Day_Temp_Count++;
 
// ���-��� �� �����
 if(Day_Flow_Max < Int_Flow_Max)
   Day_Flow_Max = Int_Flow_Max;
 if(Day_Temp_Max < Int_Temp_Max)
   Day_Temp_Max = Int_Temp_Max;
 if(Day_Temp_Min  > Int_Temp_Min)
    Day_Temp_Min = Int_Temp_Min;  

// if(IntArc.ArcHour == GasDayBegin)  // ����� �������� ���
 if((IntArc.ArcMinute % 5)==0)   // ����!!!! ����� ������ 5 �����!!!!!
 {
   // ������� ����������� �� �����
   Day_Temp_Midl = Day_Temp_Midl/Day_Temp_Count; 
   
   Day_Temp_Count = 0;
   
   DayliArc.Index_Time =  Get_IndexTime(lo_ArcDate,lo_ArcTime);
   DayliArc.ArcDay = lo_ArcDate.Day;
   DayliArc.ArcMon = lo_ArcDate.Month;
   DayliArc.ArcYear = lo_ArcDate.Year +2000;
   DayliArc.ArcHour = lo_ArcTime.Hours;
   DayliArc.ArcMinute = lo_ArcTime.Minutes;
   DayliArc.ArcSecond = lo_ArcTime.Seconds;

   DayliArc.ArcVolume = GasMeter.VE_Lm3;
   DayliArc.ArcRecNo = DayArcRecNo;
   DayliArc.ArcFlag = pArcFlag;
   DayliArc.ArcTemp = Day_Temp_Midl; 
   DayliArc.ArcMaxQ = Day_Flow_Max;
   DayliArc.ArcMaxT = Day_Temp_Max;
   DayliArc.ArcMinT = Day_Temp_Min;  
   DayliArc.ArcK = Sensor.KL;
   DayliArc.ArcError = Gl_Error.Raw;
   DayliArc.ArcStatus = 1;
   DayliArc.Reserv = 0;
 
   DayliArc.ArcCRC  = CalcCRC((uint8_t*)& DayliArc,IntArcLen-8); 
 
  // Calk adress
  CurAdress = CalkArcDayAdress(DayArcRecNo);
  ret = SPI_Write(CurAdress,(uint8_t*)&DayliArc,IntArcLen);
  
  CurAdress = DayArcRecNo+1;
  Eeprom_WriteDword((uint32_t)&DayArcRecNo,CurAdress);
 
  Day_Temp_Count = 0;
   
 }
 SPI_DeInitHardware();
 System_SaweBeforeReset();

 return ret;
}  

//-----------------------------------------------------------------------------
// �������� ������ �������������/���������  ������ � ���������� IntArc
//-----------------------------------------------------------------------------
uint8_t ReadIntArc(uint32_t p_ArcRecNo, uint8_t p_arcType)
{
  uint32_t lo_CRC, CurAdress;
  

  if(p_arcType) 
     CurAdress = CalkArcDayAdress(p_ArcRecNo); //  �������� �����
  else      
     CurAdress = CalkArcIntAdress(p_ArcRecNo);  // ������������ �����
  if( SPI_Read_Data(CurAdress,64))
   return 0xFF;
  else
  {
      memcpy((char*)&IntArc,(char*)_buf,IntArcLen);
      lo_CRC = CalcCRC((uint8_t*)&IntArc,IntArcLen-8);
      if (lo_CRC != IntArc.ArcCRC)
         return 5;   // Bad CRC
       else
         return 0;
  }
}  

