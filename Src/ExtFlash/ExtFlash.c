//=============================================================================
//  
//=============================================================================
/// \file    ExtFlash.c
/// \author 
/// \date   
/// \brief   HW communication with exeternal Flash
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

#include "stm32l0xx.h"
#include "stm32l0xx_ll_gpio.h"
#include "stm32l0xx_ll_spi.h"
#include "time_g.h"
#include "ExtFlash.h"
#include "Archiv.h"
#include "main_function.h"
#include <string.h>
#include <typedef.h>


#define RDY_BSY       0x01
#define WEL           0x02
#define EPE           0x20
#define SPRL          0x80

#define JEDEC_ID      0x9F

#define STATUS_REG1   0x05
#define STATUS_REG2   0x35
#define STATUS_WRITE  0x01

#define CMD_WRITE_ENABLE  0x06
#define CMD_WRITE_DISABLE 0x04
#define CMD_READ_DATA     0x03
#define CMD_FASTREAD_DATA 0x0B
#define CMD_PAGE_PROGRAM  0x02

#define CMD_SECTOR_ERASE     0x20  // 4�
#define CMD_BLOCK_ERASE      0x52  //32K
#define CMD_BIG_BLOCK_ERASE  0xD8  //64K 

#define DUMMY_CHAR    0xFF

#define FLASH_ON()  SET_BIT(GPIOC->BSRR,GPIO_BSRR_BR_6) 
#define FLASH_OFF() SET_BIT(GPIOC->BSRR,GPIO_BSRR_BS_6) 


//uint8_t  _buf[_buf_SIZE];
uint8_t  *_buf;
uint8_t  _buf_get_registr;

uint16_t _spi_buf_cnt;

static LL_GPIO_InitTypeDef _port;
static LL_SPI_InitTypeDef _spi_port;

extern xQueueHandle xARCQueue;
extern SemaphoreHandle_t xOptoArcRecReadySemaphore;
extern SemaphoreHandle_t xArcRecReadyWriteSemaphore;
extern SemaphoreHandle_t xMdmArcRecReadySemaphore;

extern uint8_t flag_end_task;

//------------------------------------------------------------------------------
bool IS_FLASH_ON (void)
{
  return !(READ_BIT(GPIOC->IDR, LL_GPIO_PIN_6) & (LL_GPIO_PIN_6));
}   

//-----------------------------------------------------------------------------
static inline void CS_ON(void) 
{
  SET_BIT(GPIOE->BSRR,GPIO_BSRR_BR_12);
  for(uint16_t i=0;i<0x2FF;i++);
} 

//-----------------------------------------------------------------------------
static inline void CS_OFF(void)
{
  SET_BIT(GPIOE->BSRR,GPIO_BSRR_BS_12);
   for(uint16_t i=0;i<0x1FF;i++);;
} 

//------------------------------------------------------------------------------
void SPI_InitHardware(void)
{
  SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPEEN);
  
  // PORT SPI
  _port.Pin = LL_GPIO_PIN_13 | LL_GPIO_PIN_14| LL_GPIO_PIN_15;
  _port.Mode = LL_GPIO_MODE_ALTERNATE;
  _port.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  _port.OutputType =  LL_GPIO_OUTPUT_PUSHPULL;
  _port.Pull = LL_GPIO_PULL_NO;
  _port.Alternate = LL_GPIO_AF_2;
  LL_GPIO_Init(GPIOE, &_port);

  // SPI_SC
  _port.Pin =  LL_GPIO_PIN_12;
  _port.Mode = LL_GPIO_MODE_OUTPUT;
  _port.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  _port.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  _port.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOE, &_port);
  
  // Power ext FLASH
  _port.Pin =  LL_GPIO_PIN_6;
  _port.Mode = LL_GPIO_MODE_OUTPUT;
  _port.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  _port.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  _port.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOC, &_port);
  
  
   SET_BIT(RCC->APB2ENR,RCC_APB2ENR_SPI1EN);   //������������ ������ SPI1 
   
  _spi_port.TransferDirection = LL_SPI_FULL_DUPLEX;
   _spi_port.Mode = LL_SPI_MODE_MASTER;
  _spi_port.DataWidth = LL_SPI_DATAWIDTH_8BIT;
   _spi_port.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV2;
  _spi_port.BitOrder = LL_SPI_MSB_FIRST;
  _spi_port.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE;
  _spi_port.NSS = LL_SPI_NSS_SOFT;   
  _spi_port.ClockPolarity = LL_SPI_POLARITY_LOW;
  _spi_port.ClockPhase = LL_SPI_PHASE_1EDGE;
  _spi_port.CRCPoly = 0;
  LL_SPI_Init(SPI1,&_spi_port);
  LL_SPI_DisableIT_RXNE(SPI1);
 
  LL_SPI_SetStandard(SPI1, LL_SPI_PROTOCOL_MOTOROLA);
  LL_SPI_Disable(SPI1); 
  
   NVIC_EnableIRQ(SPI1_IRQn);
   NVIC_SetPriority(SPI1_IRQn,3);

   LL_SPI_Enable(SPI1);
   CS_OFF();
   FLASH_ON();

   osDelay(5);
}

//------------------------------------------------------------------------------
void SPI_DeInitHardware(void)
{
 CS_OFF(); 
 FLASH_OFF();
 //CLEAR_BIT(RCC->IOPENR,RCC_IOPENR_IOPEEN); 
 LL_SPI_Disable(SPI1);
 LL_SPI_DeInit(SPI1);
 CLEAR_BIT(RCC->APB2ENR,RCC_APB2ENR_SPI1EN);   //������������ ������ SPI1 
} 

//------------------------------------------------------------------------------
static inline void SPI_Transmit(uint8_t p_data)
{
   SPI1->DR = p_data;
    while(!(READ_BIT(SPI1->SR, SPI_SR_TXE) == (SPI_SR_TXE)));
}

//------------------------------------------------------------------------------
// Return 0 - OK
//        0xFF - not OK 
//------------------------------------------------------------------------------
uint8_t SPI_Read_Data(uint32_t spi_adress, uint16_t p_size, uint8_t* p_Buffer)
{
 uint8_t adr_hi;
  uint8_t adr_hl;
  uint8_t adr_lo;
 
  if(IS_FLASH_ON())
    ;
  else
    return 0xA;
  
  do  // Read status register
  {
    SPI_GET_Register(STATUS_REG1,1);
  } while (SPI1->DR & RDY_BSY);
    
   adr_lo = spi_adress & 0xFF;                  // calculate low byte of address
   adr_hl = (spi_adress >> 8) & 0xFF;           // calculate mid byte
   adr_hi = (spi_adress >> 16) & 0xFF;          // calculate high byte

   // Move CS to Asserted state - LOW
    CS_ON();
    // Clear spi buffer
 //  memset(_buf, 0, _buf_SIZE);
   
   SPI_Transmit(CMD_READ_DATA);
   SPI_Transmit(adr_hi);
   SPI_Transmit(adr_hl);
   SPI_Transmit(adr_lo);
   LL_SPI_EnableIT_RXNE(SPI1);
   _spi_buf_cnt = 0;
   _buf = p_Buffer;
   for (uint16_t i=0; i < p_size; i++)
   {
     SPI_Transmit(DUMMY_CHAR);   
   }  
   CS_OFF();
   _buf = 0;
 // memcpy((char*)p_Buffer, (char*)_buf, p_size);
  return 0;  
}

//------------------------------------------------------------------------------
uint8_t SPI_GET_Register(uint8_t p_register, uint8_t answer_cnt)
{
   // Move CS to Asserted state - LOW
    CS_ON();
    SPI_Transmit(p_register);
    LL_SPI_EnableIT_RXNE(SPI1);
    _spi_buf_cnt =0;
    _buf = 0;
   for (uint8_t i=0; i < answer_cnt; i++)
      SPI_Transmit(DUMMY_CHAR);

   CS_OFF();
   return _buf_get_registr;
}   

//------------------------------------------------------------------------------
uint8_t SPI_Erase_4K(uint32_t p_adress)
{
  uint8_t adr_hi;
  uint8_t adr_hl;
  uint8_t adr_lo;
  uint8_t ret = 0 ;
 
  adr_lo = p_adress & 0xFF;                  // calculate low byte of address
  adr_hl = (p_adress >> 8) & 0xFF;           // calculate mid byte
  adr_hi = (p_adress >> 16) & 0xFF;          // calculate high byte

 _spi_buf_cnt =0;
 do  // Read status register
  {
    SPI_GET_Register(STATUS_REG1,1);
  } while (SPI1->DR & RDY_BSY);

   CS_ON();  // Move CS to Asserted state - LOW
   SPI_Transmit(CMD_WRITE_ENABLE);
   CS_OFF();
   do  // Read status register
    {
      SPI_GET_Register(STATUS_REG1,1);
     } while (SPI1->DR & WEL != WEL );

   CS_ON();  // Move CS to Asserted state - LOW

   SPI_Transmit(CMD_SECTOR_ERASE);
   SPI_Transmit(adr_hi);
   SPI_Transmit(adr_hl);
   SPI_Transmit(adr_lo);
   CS_OFF();
   
   for (uint16_t i=0x1FFF; i>0; i--);

   do  // Read status register
  {
    SPI_GET_Register(STATUS_REG1,1);
  } while (SPI1->DR & (RDY_BSY|WEL));
   CS_ON();
   
   SPI_Transmit(CMD_READ_DATA);
   SPI_Transmit(adr_hi);
   SPI_Transmit(adr_hl);
   SPI_Transmit(adr_lo);
   LL_SPI_DisableIT_RXNE(SPI1);
   _spi_buf_cnt =0;
   for (uint32_t i=0; i<0x1000; i++)
   {
     SPI_Transmit(DUMMY_CHAR);
     if(SPI1->DR != 0xFF)
     {
       ret=0xFF;
       break;
     }  
   }  
  CS_OFF();
return ret;  
//  SPI_DeInitHardware();
}

//------------------------------------------------------------------------------
uint8_t SPI_Write(uint32_t p_adress, uint8_t* p_data, uint16_t p_bytes_to_write)
{
  uint8_t adr_hi;
  uint8_t adr_hl;
  uint8_t adr_lo;
  
  if ((p_adress & 0x0FFF)==0)
    if(SPI_Erase_4K(p_adress))
       return (0xFF);

  if(p_bytes_to_write > 256)
    return (1);                                // ERROR Owerfov page length
  
   adr_lo = p_adress & 0xFF;                  // calculate low byte of address
   adr_hl = (p_adress >> 8) & 0xFF;           // calculate mid byte
   adr_hi = (p_adress >> 16) & 0xFF;          // calculate high byte
  _spi_buf_cnt =0;

  do  // Read status register
  {
    SPI_GET_Register(STATUS_REG1,1);
  } while (SPI1->DR & RDY_BSY);

   CS_ON();
    SPI_Transmit(CMD_WRITE_ENABLE);
   CS_OFF();

   do  // Read status register
    {
      SPI_GET_Register(STATUS_REG1,1);
     } while (SPI1->DR & WEL != WEL );
 
   CS_ON();
   SPI_Transmit(CMD_PAGE_PROGRAM);
   for(uint16_t i=0;i<0xFF;i++);
   SPI_Transmit(adr_hi);
   SPI_Transmit(adr_hl);
   SPI_Transmit(adr_lo);
   for(uint16_t i=0; i< p_bytes_to_write; i++)
     SPI_Transmit(p_data[i]);
   CS_OFF();
  
   do  // Read status register
    {
      SPI_GET_Register(STATUS_REG1,1);
     } while (SPI1->DR & (RDY_BSY| WEL));
   
  return 0;
}  

//-----------------------------------------------------------------------------
void FLASH_Reset(void)
{
  SPI_InitHardware();
  CS_ON(); 
  SPI_Transmit(0x66);
   CS_OFF();
   CS_ON();
    SPI_Transmit(0x99);
   CS_OFF();
  SPI_DeInitHardware(); 
  for(uint32_t i=0;i<100000;i++);
}  

//-----------------------------------------------------------------------------
uint32_t CalkArcIntAdress(uint32_t p_IntArcRecNo)
{
 uint32_t lo_CurAdress;
  
  p_IntArcRecNo -= 1; 
  lo_CurAdress = p_IntArcRecNo % IntArcRecCount;
  lo_CurAdress = lo_CurAdress * 64;  
  lo_CurAdress += IntArcMemBorder_Start; 

 return lo_CurAdress;
} 

//-----------------------------------------------------------------------------
uint32_t CalkArcDayAdress(uint32_t p_DayArcRecNo)
{
 uint32_t lo_CurAdress;

  p_DayArcRecNo -= 1; 
  lo_CurAdress = p_DayArcRecNo % DayArcRecCount ;
  lo_CurAdress = lo_CurAdress * 64;  
  lo_CurAdress += DayArcMemBorder_Start; 

 return lo_CurAdress;
} 

//-----------------------------------------------------------------------------
uint32_t CalkArcEventAdress(uint32_t p_EvArcRecNo)
{
 uint32_t lo_CurAdress;
 
  p_EvArcRecNo -= 1; 
  lo_CurAdress = p_EvArcRecNo % EvArcRecCount ;
  // lo_CurAdress = lo_CurAdress * EventArcLen;  
  lo_CurAdress = lo_CurAdress * 64; 
  lo_CurAdress += EventArcMemBorder_Start; 

 return lo_CurAdress;
} 
//-----------------------------------------------------------------------------
uint32_t CalkArcChangeAdress(uint32_t p_ChArcRecNo)
{
  uint32_t lo_CurAdress;
 
  p_ChArcRecNo -= 1; 
  lo_CurAdress = p_ChArcRecNo % ChangeArcRecCount;
//  lo_CurAdress = lo_CurAdress * ChangeArcLen; 
  lo_CurAdress = lo_CurAdress * 128;   
  lo_CurAdress += ChangeArcMemBorder_Start; 

  return lo_CurAdress;
} 
//-----------------------------------------------------------------------------
uint32_t CalkArcSystemAdress(uint32_t p_ChArcRecNo)
{
  uint32_t lo_CurAdress;
 
  p_ChArcRecNo -= 1; 
  lo_CurAdress = p_ChArcRecNo % SystemArcRecCount;
 // lo_CurAdress = lo_CurAdress * SystemArcLen; 
  lo_CurAdress = lo_CurAdress * 64; 
  
  lo_CurAdress += SystemArcMemBorder_Start; 

  return lo_CurAdress;
} 
//-----------------------------------------------------------------------------
uint32_t CalkArcTelemetryAdress(uint32_t p_ChArcRecNo)
{
  uint32_t lo_CurAdress;
 
  p_ChArcRecNo -= 1; 
  lo_CurAdress = p_ChArcRecNo % TelemetryArcRecCount;
 // lo_CurAdress = lo_CurAdress * SystemArcLen; 
  lo_CurAdress = lo_CurAdress * 64; 
  
  lo_CurAdress += TelemetryArcMemBorder_Start; 

  return lo_CurAdress;
} 

//-----------------------------------------------------------------------------
void ReadWriteARC(void const * argument)
{
// portBASE_TYPE xStatus;
 TARC_Queue lo_ArcRec_Queue;   // ������� �������
 uint32_t lo_FlashAddres;
 uint16_t lo_RecSize;
 //uint8_t  lo_Error;
  
 while(1) 
 {
//   uxQueueMessagesWaiting(xARCQueue );
   stack_ReadWriteARC = uxTaskGetStackHighWaterMark(NULL);
   xQueueReceive(xARCQueue, &lo_ArcRec_Queue, portMAX_DELAY); 
   flag_end_task |= 0x08;
   if(!IS_FLASH_ON())
     SPI_InitHardware();
   switch(lo_ArcRec_Queue.Arc_Type)
   {
   case TYPE_ARCHIVE_HOURLY_TIME:
   case TYPE_ARCHIVE_HOURLY_NUMBER:     
       lo_FlashAddres = CalkArcIntAdress(lo_ArcRec_Queue.Arc_RecNo);
       lo_RecSize = IntArcLen;
       break;
   case TYPE_ARCHIVE_DAILY_TIME:
   case TYPE_ARCHIVE_DAILY_NUMBER:     
       lo_FlashAddres = CalkArcDayAdress(lo_ArcRec_Queue.Arc_RecNo);
       lo_RecSize = IntArcLen;
       break;
 //  case TYPE_ARCHIVE_EVENT_TIME:
 //  case TYPE_ARCHIVE_EVENT_NUMBER:     
//       lo_FlashAddres = CalkArcEventAdress(lo_ArcRec_Queue.Arc_RecNo);
//       lo_RecSize = EventArcLen;
//       break;
   case TYPE_ARCHIVE_CHANG_TIME:
   case TYPE_ARCHIVE_CHANG_NUMBER:     
       lo_FlashAddres = CalkArcChangeAdress(lo_ArcRec_Queue.Arc_RecNo);
       lo_RecSize = ChangeArcLen;
       break; 
   case TYPE_ARCHIVE_SYSTEM_TIME:
   case TYPE_ARCHIVE_SYSTEM_NUMBER:     
       lo_FlashAddres = CalkArcSystemAdress(lo_ArcRec_Queue.Arc_RecNo);
       lo_RecSize = SystemArcLen;
       break;
   case TYPE_ARCHIVE_TELEMETRY_TIME:
   case TYPE_ARCHIVE_TELEMETRY_NUMBER:     
       lo_FlashAddres = CalkArcTelemetryAdress(lo_ArcRec_Queue.Arc_RecNo);
       lo_RecSize = TelemetryArcLen;
       break;       
   default: continue; break;
   }  // END SWITCH

   if(lo_ArcRec_Queue.Arc_RW == ARC_WRITE)
   {   // ����� ����� ������
      SPI_Write(lo_FlashAddres, (uint8_t*)&lo_ArcRec_Queue.Arc_Buffer.beginArc, lo_RecSize); 
    // if (lo_ArcRec_Queue.Arc_Type == ARC_INT)
    //  
      // ��������� ���������� ������� ��� �������� ������
   }
   if(lo_ArcRec_Queue.Arc_RW == ARC_READ)
   { // ������ ������     
      SPI_Read_Data(lo_FlashAddres, lo_RecSize, lo_ArcRec_Queue.Arc_Buffer.ptr_read);
      /*
      if (lo_ArcRec_Queue.Arc_Source == SOURCE_OPTIC)
         xSemaphoreGive(xOptoArcRecReadySemaphore); 
      if (lo_ArcRec_Queue.Arc_Source == SOURCE_MDM)
         xSemaphoreGive(xMdmArcRecReadySemaphore); 
      */
      xSemaphoreGive(xArcRecReadyWriteSemaphore); // ����� ������, ��� ��������/�������
   }
   // Add ERROR in systen Error!!!
   if(uxQueueMessagesWaiting(xARCQueue )==0) 
      SPI_DeInitHardware();
   
   flag_end_task &= ~0x08;
 }  // END WHILE  
}   


