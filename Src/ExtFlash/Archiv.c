//=============================================================================
/// \file    Archiv.c
/// \author  MVA
/// \date    13-Mar-2018
/// \brief   Functions for archive
//=============================================================================
// COPYRIGHT
//=============================================================================
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "Archiv.h"
#include "Time_g.h"
#include "ExtFlash.h"
#include "System.h"
#include "Eeprom.h"
#include "GasMeter.h"
#include "main_function.h"
#include "GSMSIM800C.h"
#include "stm32l073xx.h"
#include "stdio.h"
#include "Io.h"
#include "Global.h"

#include <string.h>
#include <time.h>

//IntArcTypeDef*    IntArc;
//EventArcTypeDef*  EvArc;
//ChangeArcTypeDef ChArc;

//IntArcTypeDef frameArchiveInt;
//EventArcTypeDef frameArchiveEvent;
//ChangeArcTypeDef frameArchiveChange;

//ChangeArcTypeDef* ChangeArc;
//uint8_t W_Arc_buffer[65];
//uint8_t W_Event_buffer[65];

char OldValue[64];
char NewValue[64];
uint64_t EventTimeBegin;
uint64_t EventTimeEnd;

//TARC_Queue ArcRec_Queue;   // ������� �������

/* ������ ������� �������� ������� */
#pragma section = ".eeprom"
const uint32_t IntArcRecNo @".eeprom" = 1;//768
const uint32_t DayArcRecNo @".eeprom" = 1;//192
//const uint32_t EvArcRecNo @".eeprom"  = 1;
const uint32_t ChangeArcRecNo @".eeprom" = 1;
const uint32_t SystemArcRecNo @".eeprom" = 1;
const uint32_t TelemetryArcRecNo @".eeprom" = 1;

const uint16_t  GasDayBorder @".eeprom" = 10;

uint8_t day_arc_buf = 0;
const uint8_t IntArcLen = sizeof(IntArcTypeDef);
//const uint16_t ChangeArcLen = sizeof(ChangeArcTypeDef);
const uint8_t ChangeArcLen = ChangeArcTypeDefSIZE;
const uint8_t EventArcLen = sizeof(EventSystemArcTypeDef); 
const uint8_t SystemArcLen = sizeof(EventSystemArcTypeDef); 
const uint8_t TelemetryArcLen = sizeof(EventSystemArcTypeDef); 

// const uint32_t IntArcMem_Size = IntArcMemBorder_End - IntArcMemBorder_Start; 
const uint16_t IntArcRecCount = (IntArcMemBorder_End - IntArcMemBorder_Start)/64;

// const uint32_t DayArcMem_Size = DayArcMemBorder_End - DayArcMemBorder_Start; 
const uint16_t DayArcRecCount = (DayArcMemBorder_End - DayArcMemBorder_Start)/64;

// const uint32_t EvArcMem_Size = EventArcMemBorder_End - EventArcMemBorder_Start; 
const uint16_t EvArcRecCount =(EventArcMemBorder_End - EventArcMemBorder_Start)/64;

// const uint32_t ChangeArcMem_Size = ChangeArcMemBorder_End - ChangeArcMemBorder_Start; 
//const uint16_t ChangeArcRecCount =(ChangeArcMemBorder_End - ChangeArcMemBorder_Start)/sizeof(ChangeArcTypeDef);
const uint16_t ChangeArcRecCount =(ChangeArcMemBorder_End - ChangeArcMemBorder_Start)/128;

const uint16_t SystemArcRecCount = (SystemArcMemBorder_End - SystemArcMemBorder_Start)/64;

const uint16_t TelemetryArcRecCount = (TelemetryArcMemBorder_End - TelemetryArcMemBorder_Start)/64;

#define SECTOR_LEN 0x1000

const uint16_t IntArcRecordPerSector = SECTOR_LEN / sizeof(IntArcTypeDef);
const uint16_t EventArcRecordPerSector = SECTOR_LEN / sizeof(EventSystemArcTypeDef);
//const uint16_t ChangeRecordPerSector = SECTOR_LEN / sizeof(ChangeArcTypeDef);
const uint16_t ChangeRecordPerSector = SECTOR_LEN / 128;

uint8_t IntArcRecBlok = 0;
uint16_t Day_Temp_Count = 0;

float Day_Temp_Midl =0.0; 
float Day_Flow_Max =0.0;
float Day_Temp_Max =0.0;
float Day_Temp_Min =0.0;  
double Int_QgL_m3_midl = 0.0;

extern struct _DataArchiveTransmitLORA DataArchiveTransmitLORA; // ��������� ��� �������� ������ ���������� ��� ������������ ��������� ������, ��� �������� �� � ���������� �� LORE
extern xQueueHandle xARCQueue;
extern SemaphoreHandle_t xArcRecReadyWriteSemaphore;
extern TypeStatus StatusDevice;
extern uint64_t   timeSystemUnix;
extern double VolumeBeforeReset;

//-----------------------------------------------------------------------------
uint32_t CalcCRC(uint8_t* pRecord, uint16_t pSize)
{
  uint32_t retValue;
  
  SET_BIT(RCC->AHBENR,RCC_AHBENR_CRCEN);
  SET_BIT(CRC->CR,CRC_CR_POLYSIZE|CRC_CR_RESET);
  
  for(uint16_t i =0; i< pSize;i++)
    CRC->DR = pRecord[i] ;
  retValue = CRC->DR;
  CLEAR_BIT(RCC->AHBENR,RCC_AHBENR_CRCEN);
  return retValue;
}  

//-----------------------------------------------------------------------------
uint64_t Get_IndexTime(LL_RTC_DateTypeDef p_ArcDate, LL_RTC_TimeTypeDef p_ArcTime)
{
  struct tm lo_tm;  
  uint64_t ret;
  
  memset(&lo_tm, 0, sizeof(lo_tm));
  lo_tm.tm_hour = p_ArcTime.Hours;
  lo_tm.tm_min = p_ArcTime.Minutes;
  lo_tm.tm_sec = p_ArcTime.Seconds;  

  lo_tm.tm_year = p_ArcDate.Year + 100; // since the time has gone since 1900
  lo_tm.tm_mon =  p_ArcDate.Month - 1; // from 0
  lo_tm.tm_mday = p_ArcDate.Day;
 
  ret =  mktime(&lo_tm);
 return ret;
} 

//-----------------------------------------------------------------------------
uint8_t WriteIntEventArcRecord(uint32_t  ArcFlag, uint32_t flag_begin_end)
{
  uint16_t ret =0;
  double diff_HourVolume;
  
  //---------------------------------------------------------------------------
  DataArchiveTransmitLORA.temperature = GasMeter.var_T;
  DataArchiveTransmitLORA.volume      = (uint32_t)(GasMeter.VE_Lm3 * 1000);
  DataArchiveTransmitLORA.k_factor    = Sensor.KL;
  DataArchiveTransmitLORA.hour        = SystTime.Hours;
  //---------------------------------------------------------------------------
  LL_RTC_DateTypeDef lo_ArcDate;   
  LL_RTC_TimeTypeDef lo_ArcTime;  
  uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  TARC_Queue Queue_enumIntArc;
 // IntArc = (IntArcTypeDef*)W_Arc_buffer;
 // IntArc = &frameArchiveInt;

  diff_HourVolume = GasMeter.VE_Lm3 - VolumeBeforeReset;
  if (diff_HourVolume < 0.000099) // MVA
    GasMeter.VE_Lm3 -= diff_HourVolume;  // ����� ������� ���� 0,099 �/���

  event_int_story |= ArcFlag;
  GetDate_Time(&lo_ArcDate,&lo_ArcTime);
  
  timeSystemUnix = Get_IndexTime(lo_ArcDate,lo_ArcTime);
  Queue_enumIntArc.Arc_Buffer.IntArc.Index_Time =  timeSystemUnix;
 // Queue_enumIntArc.Arc_Buffer.IntArc.QgL_m3_midl = Int_QgL_m3_midl;
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcVolume = GasMeter.VE_Lm3;
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcRecNo = IntArcRecNo;
  Queue_enumIntArc.Arc_Buffer.IntArc.event = ArcFlag | flag_begin_end;
  Queue_enumIntArc.Arc_Buffer.IntArc.event_story = event_int_story;  
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcTemp = Int_Temp_Midl; 
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcK = Sensor.KL;   
 // Queue_enumIntArc.Arc_Buffer.IntArc.ArcStatus = *((uint32_t*)&StatusDevice);  
  
  
  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxQ = Int_Flow_Max;
  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxT = Int_Temp_Max;
  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcMinT = Int_Temp_Min;  

  //Queue_enumIntArc.Arc_Buffer.IntArc.ArcError = Gl_Error.Raw;

  //Queue_enumIntArc.Arc_Buffer.IntArc.Reserv = 0;
  //Queue_enumIntArc.Arc_Buffer.IntArc.Reserv2 = 0;  
 
  Queue_enumIntArc.Arc_Buffer.IntArc.ArcCRC  = CalculateCRC32((uint8_t*)&Queue_enumIntArc.Arc_Buffer, IntArcLen - 4, CRC_CTART_STOP); 


  if(ArcFlag == EVENT_INT_CURRENT_FLOW)
  {
     Int_Temp_Count = 0;

     // ������� ����������� �� �����
     Day_Temp_Midl += Int_Temp_Midl; 
     Day_Temp_Count++;
   }
  // ���-��� �� �����
  if(Day_Flow_Max < Int_Flow_Max)
     Day_Flow_Max = Int_Flow_Max;
  if(Day_Temp_Max < Int_Temp_Max)
     Day_Temp_Max = Int_Temp_Max;
  if(Day_Temp_Min  > Int_Temp_Min)
      Day_Temp_Min = Int_Temp_Min;  
    
  //  ��������� � �������� � ������� �������
   // ArcRec_Queue.Arc_Buffer = (uint8_t*)IntArc;
    Queue_enumIntArc.Arc_RW = ARC_WRITE;
    Queue_enumIntArc.Arc_Type = TYPE_ARCHIVE_HOURLY_TIME;
    Queue_enumIntArc.Arc_RecNo = IntArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumIntArc, 1000);  
    if( xStatus != pdPASS )
    {
      //### ������ �������� � �������! 
    }  
    
    lo_CurAdress = IntArcRecNo+1;
    Eeprom_WriteDword((uint32_t)&IntArcRecNo,lo_CurAdress);
    /*
    // ����-� �������, ��� ����������� ����� �������.
    xStatus = xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
    if( xStatus != pdPASS )
    {
      //### ������  
    } 
    */
   if(!lo_ArcTime.Hours)
     flag_check_crc_dQf = 1;
    
 if(lo_ArcTime.Hours == GasDayBorder && lo_ArcDate.Day != day_arc_buf)  // ����� �������� ���
 {
   day_arc_buf = lo_ArcDate.Day;
   // ������� ����������� �� �����
   Day_Temp_Midl = Day_Temp_Midl/Day_Temp_Count; 
   
   Day_Temp_Count = 0;
   
   Queue_enumIntArc.Arc_Buffer.IntArc.ArcRecNo = DayArcRecNo;
   Queue_enumIntArc.Arc_Buffer.IntArc.ArcTemp = Day_Temp_Midl;    
   Queue_enumIntArc.Arc_Buffer.IntArc.num_int = IntArcRecNo - 1;
   Queue_enumIntArc.Arc_Buffer.IntArc.num_change = ChangeArcRecNo - 1;
   Queue_enumIntArc.Arc_Buffer.IntArc.num_system = SystemArcRecNo - 1;
   Queue_enumIntArc.Arc_Buffer.IntArc.num_tel = TelemetryArcRecNo - 1;
   // For SMS
   frameArchiveInt.ArcRecNo = DayArcRecNo;
   frameArchiveInt.Index_Time =  timeSystemUnix;
   frameArchiveInt.ArcVolume = GasMeter.VE_Lm3;
   frameArchiveInt.ArcTemp = Day_Temp_Midl;    
   frameArchiveInt.ArcK = Sensor.KL;   
   frameArchiveInt.num_int = IntArcRecNo - 1;
   frameArchiveInt.num_change = ChangeArcRecNo - 1;
   frameArchiveInt.num_system = SystemArcRecNo - 1;
   frameArchiveInt.num_tel = TelemetryArcRecNo - 1;
   frameArchiveInt.event_story = event_int_story;
   
   
//   IntArc->ArcFlag = p_ArcFlag;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxQ = Day_Flow_Max;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.ArcMaxT = Day_Temp_Max;
 //  Queue_enumIntArc.Arc_Buffer.IntArc.ArcMinT = Day_Temp_Min;  
 
    Queue_enumIntArc.Arc_Buffer.IntArc.ArcCRC  = CalculateCRC32((uint8_t*)&Queue_enumIntArc.Arc_Buffer, IntArcLen - 4, CRC_CTART_STOP); 




   //  ��������� � �������� � ������� �������
 //   ArcRec_Queue.Arc_Buffer = (uint8_t*)IntArc;
    Queue_enumIntArc.Arc_RW = ARC_WRITE;
    Queue_enumIntArc.Arc_Type = TYPE_ARCHIVE_DAILY_TIME;
    Queue_enumIntArc.Arc_RecNo = DayArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumIntArc, 1000);  
    if( xStatus != pdPASS )
    {
      //### ������ �������� � �������! 
    }  
  lo_CurAdress = DayArcRecNo+1;
  Eeprom_WriteDword((uint32_t)&DayArcRecNo,lo_CurAdress);
  /*
  // ����-� �������, ��� ����������� ����� �������.
    xStatus = xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
    if( xStatus != pdPASS )
    {
      //### ������ �������� � �������! 
    } 
    */
  Day_Temp_Count = 0;
   
 }
// SPI_DeInitHardware();
 System_SaweBeforeReset();
 return ret;
}  

//-----------------------------------------------------------------------------
uint8_t WriteTelemetryArcRecord(uint16_t p_Event_Code, int8_t p_Event_Result, uint16_t p_Sesion_Number)
{
  uint16_t ret =0;
 // LL_RTC_DateTypeDef lo_ArcDate;   
 // LL_RTC_TimeTypeDef lo_ArcTime;  
  uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  TARC_Queue Queue_enumSystemArc;
  //EvArc = (EventArcTypeDef*)W_Event_buffer;
 // EvArc = &frameArchiveEvent;  

 // GetDate_Time(&lo_ArcDate,&lo_ArcTime);

  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.ArcRecNo  = TelemetryArcRecNo; 
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.TimeBegine = EventTimeBegin;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.TimeEnd    = EventTimeEnd;  
  
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Event_Code   = p_Event_Code;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Event_Result = p_Event_Result;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Sesion_Number = p_Sesion_Number;
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.Status = *((uint32_t*)&StatusDevice);  
  
  Queue_enumSystemArc.Arc_Buffer.EventSystemArc.ArcCRC =  CalculateCRC32((uint8_t*)&Queue_enumSystemArc.Arc_Buffer, TelemetryArcLen - 4, CRC_CTART_STOP);
  
   //  ��������� � �������� � ������� �������
//  ArcRec_Queue.Arc_Buffer = (uint8_t*)EvArc;
  Queue_enumSystemArc.Arc_RW = ARC_WRITE;
  Queue_enumSystemArc.Arc_Type = TYPE_ARCHIVE_TELEMETRY_NUMBER;
  Queue_enumSystemArc.Arc_RecNo = TelemetryArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumSystemArc, 5000);  
    if( xStatus != pdPASS )
    {
     // ������ �������� � �������! 
    }  
  lo_CurAdress = TelemetryArcRecNo + 1;
  Eeprom_WriteDword((uint32_t)&TelemetryArcRecNo, lo_CurAdress);
  // ����-� �������, ��� ����������� ����� �������.
//    xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
  return ret;
}
//-----------------------------------------------------------------------------
uint8_t WriteSystemArcRecord(uint16_t p_Event_Code, uint16_t p_Sesion_Number)
{
  uint16_t ret =0;
  LL_RTC_DateTypeDef lo_ArcDate;   
  LL_RTC_TimeTypeDef lo_ArcTime;  
  uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  TARC_Queue Queue_enumEventArc;

  GetDate_Time(&lo_ArcDate,&lo_ArcTime);
  
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.ArcRecNo  = SystemArcRecNo; 
  
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.TimeBegine = timeSystemUnix;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.TimeEnd    = timeSystemUnix;  
  
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Event_Code   = p_Event_Code;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Event_Result = 0;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Sesion_Number = p_Sesion_Number;
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.Status = *((uint32_t*)&StatusDevice);  
  
  Queue_enumEventArc.Arc_Buffer.EventSystemArc.ArcCRC =  CalculateCRC32((uint8_t*)&Queue_enumEventArc.Arc_Buffer, EventArcLen - 4, CRC_CTART_STOP);
  
   //  ��������� � �������� � ������� �������
//  ArcRec_Queue.Arc_Buffer = (uint8_t*)EvArc;
  Queue_enumEventArc.Arc_RW = ARC_WRITE;
  Queue_enumEventArc.Arc_Type = TYPE_ARCHIVE_SYSTEM_NUMBER;
  Queue_enumEventArc.Arc_RecNo = SystemArcRecNo;

    xStatus = xQueueSendToBack(xARCQueue, &Queue_enumEventArc, 5000);  
    if( xStatus != pdPASS )
    {
     // ������ �������� � �������! 
    }  
  lo_CurAdress = SystemArcRecNo + 1;
  Eeprom_WriteDword((uint32_t)&SystemArcRecNo, lo_CurAdress);
  // ����-� �������, ��� ����������� ����� �������.
//    xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
  return ret;
}

//-----------------------------------------------------------------------------
uint8_t WriteChangeArcRecord(TCHANG_Type p_Parametr, uint8_t _ID_Change)   
{
  uint16_t ret =0;
  TARC_Queue lo_ArcRec_Queue;
  uint32_t lo_CurAdress;
  portBASE_TYPE xStatus;
  
  LL_RTC_DateTypeDef lo_ArcDate;   
  LL_RTC_TimeTypeDef lo_ArcTime;    
  
  GetDate_Time(&lo_ArcDate, &lo_ArcTime);  
  
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.Index_Time = timeSystemUnix;
  
  if(CLB_lock == 0x55)
     lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcCLB_Lock = 1;  
  else
     lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcCLB_Lock = 0;
  
  
  strcpy((char*)lo_ArcRec_Queue.Arc_Buffer.ChangeArc.OldValue, OldValue);
  strcpy((char*)lo_ArcRec_Queue.Arc_Buffer.ChangeArc.NewValue, NewValue);
  
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.status_cover = StatusWarning.body_cover_open;
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcRecNo = ChangeArcRecNo;
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ID_Param = (uint8_t) p_Parametr;
  lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ID_Change = _ID_Change;
   lo_ArcRec_Queue.Arc_Buffer.ChangeArc.ArcCRC =  CalculateCRC32((uint8_t*)&lo_ArcRec_Queue.Arc_Buffer, ChangeArcLen - 4, CRC_CTART_STOP);
  
  
  // ��������� � �������� � ������� �������

  lo_ArcRec_Queue.Arc_RW = ARC_WRITE;
  lo_ArcRec_Queue.Arc_Type = TYPE_ARCHIVE_CHANG_NUMBER;
  lo_ArcRec_Queue.Arc_RecNo = ChangeArcRecNo;

  xStatus = xQueueSendToBack(xARCQueue, &lo_ArcRec_Queue, 1000);  
  if( xStatus != pdPASS )
  {
   // ������ �������� � �������! 
    __NOP();
  }  
  lo_CurAdress = ChangeArcRecNo+1;
  Eeprom_WriteDword((uint32_t)&ChangeArcRecNo, lo_CurAdress);

  return ret;
}

/*!
Read record without compare through queue
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/
uint8_t ReadArchiveQueue(TArchiv_type archiv_type, 
                    uint32_t     n_record,
                    uint64_t     label_begin,
                    uint64_t     label_end,
                    uint8_t      *ptr_out)
{
    TARC_Queue lo_ArcRec_Queue;
    portBASE_TYPE xStatus;
    uint8_t      type_label = LABEL_DATE;
    
    if(label_begin == 0 && label_end == 0)
    {
       type_label = LABEL_NUMBER;
    }
    
    lo_ArcRec_Queue.Arc_RW = ARC_READ;
    lo_ArcRec_Queue.Arc_Type = archiv_type;
    lo_ArcRec_Queue.Arc_RecNo = n_record;
    lo_ArcRec_Queue.Arc_Buffer.ptr_read = (uint8_t*)&lo_ArcRec_Queue.Arc_Buffer;
    xStatus = xQueueSendToBack(xARCQueue, &lo_ArcRec_Queue, 5000);  
    if( xStatus != pdPASS )
    {
      // ������ �������� � �������! 
      return ARC_READ_STATUS_TIME_FAIL_ERROR;
    }
    
    // ����-� �������, ��� ����������� ����� ��������.
    xStatus = xSemaphoreTake(xArcRecReadyWriteSemaphore, 5000);
    if( xStatus != pdPASS )
    {
      // ������ 
       return ARC_READ_STATUS_TIME_FAIL_ERROR;
    }
    //-------------------------------------------------------
    if(type_label == LABEL_DATE)
    {
       if(lo_ArcRec_Queue.Arc_Buffer.NoAndTimeArcType.Index_Time < label_begin
          || lo_ArcRec_Queue.Arc_Buffer.NoAndTimeArcType.Index_Time > label_end)
          return ARC_READ_STATUS_NOT_RANGE_ERROR;
    }

    //-------------------------------------------------------
    ReadArchiveParsing(&lo_ArcRec_Queue.Arc_Buffer, archiv_type, ptr_out);
    //### ����� ��������� �� CRC
    
    return ARC_READ_STATUS_IN_RANGE_OK;
}
/*!
Read record without compare through queue
\param[in] archiv_type ��� ������
\param[in] n_record ����� �������� ������
\param[out] *ptr_out ��������� �� ����� ���� ����������
\return the result of the operation
*/
void ReadArchiveParsing(enumArcTypeDef *ptr_Arc_Buffer, TArchiv_type archiv_type, uint8_t      *ptr_out)
{
   struct tm lo_tm, lo_tm_end;
   memcpy(&lo_tm, gmtime((time_t*)&ptr_Arc_Buffer->NoAndTimeArcType.Index_Time), sizeof(lo_tm));
   memcpy(&lo_tm_end, gmtime((time_t*)&ptr_Arc_Buffer->NoAndTimeArcType.Index_Time_end), sizeof(lo_tm_end));
   
   switch(archiv_type)
   {
   case TYPE_ARCHIVE_HOURLY_TIME:
   case TYPE_ARCHIVE_HOURLY_NUMBER:     
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%d,%02d:%02d:%02d;%d;%0.2f;%d;%08X;%08X\r\n", 
                ptr_Arc_Buffer->IntArc.ArcRecNo,    
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,
                (int)(ptr_Arc_Buffer->IntArc.ArcVolume * 1000),
                ptr_Arc_Buffer->IntArc.ArcTemp,   
                ptr_Arc_Buffer->IntArc.ArcK,                
                ptr_Arc_Buffer->IntArc.event, 
                ptr_Arc_Buffer->IntArc.event_story 
                );
        break;
   case TYPE_ARCHIVE_DAILY_TIME:
   case TYPE_ARCHIVE_DAILY_NUMBER: 
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%d,%02d:00:00;%d;%0.2f;%d;%d;%d;%d;%d;%08X\r\n", 
                ptr_Arc_Buffer->IntArc.ArcRecNo,    
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour,
                (int)(ptr_Arc_Buffer->IntArc.ArcVolume * 1000),
                ptr_Arc_Buffer->IntArc.ArcTemp,   
                ptr_Arc_Buffer->IntArc.ArcK,                
                ptr_Arc_Buffer->IntArc.num_int,
                ptr_Arc_Buffer->IntArc.num_change,
                ptr_Arc_Buffer->IntArc.num_tel,                
                ptr_Arc_Buffer->IntArc.num_system, 
           //     ptr_Arc_Buffer->IntArc.event, 
                ptr_Arc_Buffer->IntArc.event_story                 
                );
        break;
  // case TYPE_ARCHIVE_EVENT_NUMBER:
   case TYPE_ARCHIVE_SYSTEM_NUMBER:     
   case TYPE_ARCHIVE_SYSTEM_TIME:
 //  case TYPE_ARCHIVE_EVENT_TIME:       
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%04d,%02d:%02d:%02d;%02d.%02d.%04d,%02d:%02d:%02d;%d;%d;%d;%08X\r\n", 
                ptr_Arc_Buffer->EventSystemArc.ArcRecNo,                 
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,   
                lo_tm_end.tm_mday, lo_tm_end.tm_mon, lo_tm_end.tm_year + 1900, 
                lo_tm_end.tm_hour, lo_tm_end.tm_min, lo_tm_end.tm_sec,                 
                ptr_Arc_Buffer->EventSystemArc.Event_Code,
                ptr_Arc_Buffer->EventSystemArc.Event_Result,
                ptr_Arc_Buffer->EventSystemArc.Sesion_Number, //
                ptr_Arc_Buffer->EventSystemArc.Status
                );
        break;
   case TYPE_ARCHIVE_TELEMETRY_TIME:
   case TYPE_ARCHIVE_TELEMETRY_NUMBER:      
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%04d,%02d:%02d:%02d;%02d.%02d.%04d,%02d:%02d:%02d;%d;%d;%d;%08X\r\n", 
                ptr_Arc_Buffer->EventSystemArc.ArcRecNo,                 
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,   
                lo_tm_end.tm_mday, lo_tm_end.tm_mon, lo_tm_end.tm_year + 1900, 
                lo_tm_end.tm_hour, lo_tm_end.tm_min, lo_tm_end.tm_sec,                 
                ptr_Arc_Buffer->EventSystemArc.Event_Code,
                ptr_Arc_Buffer->EventSystemArc.Event_Result,
                ptr_Arc_Buffer->EventSystemArc.Sesion_Number, //
                ptr_Arc_Buffer->EventSystemArc.Status
                );
        break;    
        
   case TYPE_ARCHIVE_CHANG_NUMBER:
   case TYPE_ARCHIVE_CHANG_TIME:     
        sprintf((char *)ptr_out,
                "%d;%02d.%02d.%04d,%02d:%02d:%02d;%d;%d;%d;%s;%s;%d\r\n", 
                ptr_Arc_Buffer->ChangeArc.ArcRecNo,                 
                lo_tm.tm_mday, lo_tm.tm_mon + 1, lo_tm.tm_year + 1900, 
                lo_tm.tm_hour, lo_tm.tm_min, lo_tm.tm_sec,
                ptr_Arc_Buffer->ChangeArc.ArcCLB_Lock,
                ptr_Arc_Buffer->ChangeArc.ID_Change,
                ptr_Arc_Buffer->ChangeArc.ID_Param, //
                ptr_Arc_Buffer->ChangeArc.OldValue,
                ptr_Arc_Buffer->ChangeArc.NewValue,
                ptr_Arc_Buffer->ChangeArc.status_cover
                );       
        break;
   default:break;
   }
}