//=============================================================================
/// \file    Sgm.c
/// \author  RFU
/// \date    13-Apr-2016
/// \brief   Module for Sensirion Gasmeter Sensor
//=============================================================================
// COPYRIGHT
//=============================================================================
#include "FreeRTOS.h"
#include "task.h"

#include "Sgm.h"
#include "I2c.h"
#include "stm32l073xx.h"
#include "Time_g.h"
#include "Utils.h"
#include "main_function.h"

#define CONFIG_USE_CATEGORY_SGM
#include "Configuration.h"

typedef enum {
  MACRO_READID                 = 0x3624,
  MACRO_MEAS_FLOW              = 0x3603,
  MACRO_RECOGGAS               = 0x3608,
  MACRO_MEASFLOWLUTN           = 0x361E,
  MACRO_GETSFOFFSET            = 0x3624,
  MACRO_GETPRODUCTSERIALNUMBER = 0x362F,
  MACRO_GETARTICLECODE         = 0x3639,
  MACRO_SHORT_FLOW             = 0x3615,  
  MACRO_GET_VDD                = 0x3646
} Macro_Code;

/*
typedef enum {
  REGISTER_GPBUFFER            = 0xE102,
  REGISTER_STATUS_REG          = 0xEFFD
} Register_Address;
*/
typedef enum {
  REGISTER_DATABUFFER = 0xE000,
  REGISTER_GPBUFFER0  = 0xE102,
  REGISTER_GPBUFFER1  = 0xE109,
  REGISTER_GPBUFFER2  = 0xE114,
  REGISTER_GPBUFFER3  = 0xE11F,
  REGISTER_GPBUFFER4  = 0xE125,
  REGISTER_GPBUFFER5  = 0xE12E,
  REGISTER_GPBUFFER6  = 0xE133,
  REGISTER_GPBUFFER7  = 0xE138,
  REGISTER_GPBUFFER8  = 0xE147,
  REGISTER_GPBUFFER9  = 0xE14C,
  REGISTER_GPBUFFERA  = 0xE151,
  REGISTER_GPBUFFERB  = 0xE15A,
  REGISTER_GPBUFFERC  = 0xE160,
  REGISTER_GPBUFFERD  = 0xE16B,
  REGISTER_GPBUFFERE  = 0xE176,
  REGISTER_GPBUFFERF  = 0xE17D,
  REGISTER_PRAMSTATUS = 0xEFFD,
} Register_Address;

void SGM_PowerOn(void);
void SGM_PowerOff(void);
static void InitPowerGpio(void);


static Error RunMacro(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
static Error WakeUp(void);
static Error ReadGPBuffer(uint16_t* buffer, uint8_t size);
static Error SetPointerToAddress(uint16_t value);
static Error ReadMacroReturnValues(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2);
static Error ReadWordsWithCrcCheck(uint16_t values[], uint8_t size);
static Error WriteWord(uint16_t value);
static Error WriteData(uint8_t data[], uint8_t size);
static Error ReadWord(uint16_t* value, bool finalizeWithNack);
static void ReadData(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack);
static Error CheckCrc(uint8_t* data, uint8_t length, uint8_t checksum);
static Error ReadRegister1(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size);

Sgm_Type _SgmType = SGM_TYPE_UNDEFINED; // TODO


//-----------------------------------------------------------------------------
void Sgm_InitHardware(void)
{
  InitPowerGpio();
}

//-----------------------------------------------------------------------------
Error Sgm_InitModule(void)
{
  SGM_PowerOff();
  //osDelay(100);
  Time_DelayMilliSeconds(100);
  SGM_PowerOn();
  Time_DelayMilliSeconds(100);
  //osDelay(100);
  
  return Error_None();
}


//-----------------------------------------------------------------------------
Error Sgm_DeinitModule(void)
{
  SGM_PowerOff();
 // osDelay(100);
 // Time_DelayMilliSeconds(100);
  return Error_None();
}

//-----------------------------------------------------------------------------
Error Sgm_GetId(uint16_t* id)
{
  return RunMacro(MACRO_READID, 0, id, 0, 0);
}

//-----------------------------------------------------------------------------
Error Sgm_MeasFlow(TSensor* pSensor) // Guide 631
{
//  taskENTER_CRITICAL();

	
  // Wake-up
  Error error = WakeUp();
  
  // Start condition
  if(Error_IsNone(error)) 
    error = I2c_StartCondition();
  
  // Send write header
  if(Error_IsNone(error)) 
    error = I2c_WriteByte(I2c_MakeWriteHeader(Config_SgmAddress));
  
  // Write the macro code (Meas Flow)
  if(Error_IsNone(error)) 
    error = SetPointerToAddress(MACRO_MEAS_FLOW);

  // Write the macro argument (k value)
  if(Error_IsNone(error)) 
  {
    uint16_t lo_KL;

    lo_KL = pSensor->KL; 
     error = WriteWord(lo_KL);
     I2c_StopCondition();
  }
  
  // Wait for data in buffer
  if(Error_IsNone(error)) 
  {
    Time_DelayMilliSeconds(15);//17
    // Poll the Databuffer status register
    for(int i = 8; i > 0; i--) {
      uint16_t pramStatus;
      error = ReadRegister1(REGISTER_PRAMSTATUS, &pramStatus, 1);
      if(Error_IsNone(error) && (pramStatus != 0x0000)) break;
      I2c_StopCondition();
      Time_DelayMilliSeconds(1);
    }
    Error_AppendIfError(&error, ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT);
  }

/*  
  // Read GP Buffer 8 and 9
  if(Error_IsNone(error))
  {
    error = ReadRegister1(REGISTER_GPBUFFER1, &pSensor->Traw, 1); 
		error = ReadRegister1(REGISTER_GPBUFFER8, &pSensor->Qfs, 1);  //
    error = ReadRegister1(REGISTER_GPBUFFER9, &pSensor->Qfs_FA, 1);
  }
*/  
  
  // 1 Read macro return values
  if(Error_IsNone(error)) 
  {
    uint16_t buffer[3];
    error = ReadRegister1(REGISTER_DATABUFFER, buffer, 3);
    pSensor->Qfl = buffer[0];    //Qf long
    pSensor->KS  = buffer[1];    //K for short meassure
    pSensor->Qfl_FA = buffer[2]; //QF  long fine adjustement
  }

  // 2 Read macro return values  
  if(Error_IsNone(error)) 
  {
    uint16_t buffer[3];
    error = ReadRegister1(REGISTER_DATABUFFER, buffer, 3);
    pSensor->Qfs     = buffer[0];    //Qf short
    pSensor->Qfs_FA  = buffer[1];    //Short fine adj 
    pSensor->Traw    = buffer[2];    //Temper
  }

//taskEXIT_CRITICAL();
  
  I2c_StopCondition();
  
  return error;
}

//-----------------------------------------------------------------------------
//Error Sgm_MeasFlowShort(uint16_t k8T8, uint16_t* qShort, uint16_t* qFA_Short)
Error Sgm_MeasFlowShort(TSensor* pSensor)
{
  Error error;
    error = RunMacro(MACRO_SHORT_FLOW, &pSensor->KS, &pSensor->Qfs, &pSensor->Qfs_FA, 0);
  return error;
}

//-----------------------------------------------------------------------------
Error Sgm_GasRecognition(uint16_t* pGasRec)
{
  return RunMacro(MACRO_RECOGGAS, 0, pGasRec, 0, 0);
}

//-----------------------------------------------------------------------------
Error Sgm_GetVDD(short* p_dQVDD, uint16_t* p_VDD)
{
  return RunMacro(MACRO_GET_VDD, 0, (uint16_t*)p_dQVDD, 0, p_VDD);
}

//-----------------------------------------------------------------------------
Error Sgm_GetSf_Offset(uint16_t* scaleFactor, uint16_t* flowUnit, uint16_t* offset)
{
  Error error;
 // uint16_t localBuffer[10];
  
  error = RunMacro(MACRO_GETSFOFFSET, 0, offset, scaleFactor, flowUnit);
  
/*  if(Error_IsNone(error)) 
    error = ReadGPBuffer(localBuffer, 2);
*/
  return error;
}

//-----------------------------------------------------------------------------
Error Sgm_GetProductSerialNumber(uint64_t* serialNumber)
{
  Error error; 
  uint16_t buffer[4];
    int i;
  
  error = RunMacro(MACRO_GETPRODUCTSERIALNUMBER, 0, 0, 0, 0);  
  if(Error_IsNone(error)) {
    error = ReadGPBuffer((uint16_t*)buffer, 4);
  }
  
  if(Error_IsNone(error))
  {
    *serialNumber = 0;
    for(i = 1; i < 3; i++) 
    {
      *serialNumber |= buffer[i];
      *serialNumber <<= 16;
    }
    *serialNumber |= buffer[i];
  }
  
  return error;
}

//-----------------------------------------------------------------------------
Error Sgm_GetArticleCode(uint8_t* buffer)
{
  uint16_t localBuffer[10];
  Error error = RunMacro(MACRO_GETARTICLECODE, 0, 0, 0, 0);
  
  if(Error_IsNone(error)) {
    error = ReadGPBuffer(localBuffer, 10);
  }
  
  if(Error_IsNone(error)) {
    for(int i = 0; i < 10; i++) {
      buffer[i * 2]     = localBuffer[i] >> 8;
      buffer[i * 2 + 1] = localBuffer[i];
    }
  }

  return error;
}

//-----------------------------------------------------------------------------
/*
SensorTypeParameters* Sgm_GetParameters(void)
{
  return &sensorTypeParameters[_SgmType];
}
*/
//-----------------------------------------------------------------------------
void SGM_PowerOn(void)
{
  CalculateMotoHourceSGM(MOTO_HOURCE_START);
  I2c_EnableLines();
  CLEAR_BIT(GPIOC->ODR, GPIO_ODR_OD9);  //New HW
  for(int i=0; i<0x500; i++);
}

//-----------------------------------------------------------------------------
void SGM_PowerOff(void)
{
  CalculateMotoHourceSGM(MOTO_HOURCE_STOP);
  I2c_DisableLines();
  SET_BIT(GPIOC->ODR, GPIO_ODR_OD9);  //New HW
//  I2c_EnableLines();
}

//-----------------------------------------------------------------------------
static void InitPowerGpio(void)
{
  // power switch is GPIO PC9
  // configure as output with push-pull
  // set output to 0
// New HW 
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE9, GPIO_MODER_MODE9_0);
  SET_BIT(GPIOC->ODR, GPIO_ODR_OD9);
}

//-----------------------------------------------------------------------------
static Error RunMacro(Macro_Code macro, uint16_t* argument,
                      uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{ 
  // Wake-up
  Error error = WakeUp();
  
  // Start condition
  if(Error_IsNone(error))
      error = I2c_StartCondition();
  
  
  // Send write header
  if(Error_IsNone(error)) 
      error = I2c_WriteByte(I2c_MakeWriteHeader(Config_SgmAddress));
    
  // Write the macro code
  if(Error_IsNone(error))
    error = SetPointerToAddress(macro);
  

  // Write the macro argument
  if(Error_IsNone(error))
  {
    if(argument)
      error = WriteWord(*argument);
  }
  // Read return values
  if(Error_IsNone(error)) {
    for(int i = 8; i > 0; i--) 
    {
      Time_DelayMilliSeconds(5);
      error = ReadMacroReturnValues(ret0, ret1, ret2);
      if(Error_IsNone(error)) 
        break;
    }
  }
  
  I2c_StopCondition();
  
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadRegister1(Register_Address registerAddress, uint16_t* buffer,
                          uint8_t size)
{
  // Start condition
  Error error = I2c_StartCondition();

  // Send write header
  if(Error_IsNone(error))
    error = I2c_WriteByte(I2c_MakeWriteHeader(Config_SgmAddress));

  // Set pointer to Address
  if(Error_IsNone(error)) 
    error = SetPointerToAddress(registerAddress);

  // Start condition
  error = I2c_StartCondition();

  // Send read header
  if(Error_IsNone(error)) 
    error = I2c_WriteByte(I2c_MakeReadHeader(Config_SgmAddress));

  if(Error_IsNone(error))
      error = ReadWordsWithCrcCheck(buffer, size);
  
  I2c_StopCondition();
  return error;
}

//-----------------------------------------------------------------------------
static Error WakeUp(void)
{
  Error error = I2c_StartCondition();
  
  if(Error_IsNone(error)) {
    I2c_WriteByte(I2c_MakeWriteHeader(Config_SgmAddress));
  }
  
  I2c_StopCondition();
  
  if(Error_IsNone(error)) {
    Time_DelayMicroSeconds(500);
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadGPBuffer(uint16_t* buffer, uint8_t size)
{
  // Start condition
  Error error = I2c_StartCondition();
  
  // Send write header
  if(Error_IsNone(error)) {
    error = I2c_WriteByte(I2c_MakeWriteHeader(Config_SgmAddress));
  }
  
  // Set pointer to GPBuffer
  if(Error_IsNone(error)) {
    error = SetPointerToAddress(REGISTER_GPBUFFER0);
  }
  
  // Start condition
  error = I2c_StartCondition();
  
  // Send read header
  if(Error_IsNone(error)) {
    error = I2c_WriteByte(I2c_MakeReadHeader(Config_SgmAddress));
  }
  
  if(Error_IsNone(error)) {
    error = ReadWordsWithCrcCheck(buffer, size);
  }
  
  I2c_StopCondition();
  
  return error;
}

//-----------------------------------------------------------------------------
static Error SetPointerToAddress(uint16_t address)
{
  Error error;
  uint8_t data[2];
  
  data[0] = address >> 8;
  data[1] = address & 0xFF;
  error = WriteData(data, sizeof(data));
  
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadMacroReturnValues(uint16_t* ret0, uint16_t* ret1, uint16_t* ret2)
{
  Error error = Error_None();
  
  uint8_t numOfReturnValues = ret2 ? 3 : (ret1 ? 2 : (ret0 ? 1 : 0));
  uint16_t values[3];

  // Start to read the macro return values, if any expected.
  if(numOfReturnValues > 0) {
    // Start condition
    error = I2c_StartCondition();
    
    // Send read header
    if(Error_IsNone(error)) {
      error = I2c_WriteByte(I2c_MakeReadHeader(Config_SgmAddress));
    }

    // Read the macro return values
    if(Error_IsNone(error)) {
      error = ReadWordsWithCrcCheck(values, numOfReturnValues);
    }
    
    if(Error_IsNone(error)) {
      *ret0 = values[0];
      *ret1 = values[1];
      *ret2 = values[2];
    }
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadWordsWithCrcCheck(uint16_t values[], uint8_t size)
{
  Error error;
  uint8_t i;
  bool finalizeRead;

  for(i = 0; i < size; i++) {
    finalizeRead = (i == (size - 1));
    error = ReadWord(&values[i], finalizeRead);
    if(Error_IsError(error)) {
      // If the reading has not been finalized with a NACK, do that now.
      if(finalizeRead) I2c_ReadByte(NO_ACK);
      break;
    }
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error WriteWord(uint16_t value)
{
  Error error;
  uint8_t data[3];
  
  data[0] = value >> 8;
  data[1] = value & 0xFF;
  data[2] = Utils_CalculateCrc(data, 2, Config_SgmCrcPolynomial, Config_SgmCrcStartVector);
  error = WriteData(data, sizeof(data));
  
  return error;
}

//-----------------------------------------------------------------------------
static Error WriteData(uint8_t data[], uint8_t size)
{
  Error error = Error_None();
  uint8_t i;
  
  for(i = 0; i < size; i++) {
    error = I2c_WriteByte(data[i]);
    if(Error_IsError(error)) break;
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static Error ReadWord(uint16_t* value, bool finalizeWithNack)
{
  Error error;
  uint8_t data[3];
  
  ReadData(data, 3, finalizeWithNack);
  error = CheckCrc(data, 2 , data[2]);
  
  if(Error_IsNone(error)) {
    *value = data[0] << 8 | data[1];
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static void ReadData(uint8_t data[], uint8_t numberOfBytes, bool finalizeWithNack)
{
  uint8_t i;
  I2c_Ack ack = ACK;
  
  for(i = 0; i < numberOfBytes; i++) {
    if((i == (numberOfBytes - 1)) && finalizeWithNack) {
      ack = NO_ACK;
    }
    data[i] = I2c_ReadByte(ack);
  }
}

//-----------------------------------------------------------------------------
static Error CheckCrc(uint8_t* data, uint8_t length, uint8_t checksum)
{
  Error   error = Error_None();
  uint8_t crc   = 0;

  crc = Utils_CalculateCrc(data, length, Config_SgmCrcPolynomial, Config_SgmCrcStartVector);
  if (crc != checksum) {
    Error_Add(&error, ERROR_SGM_CRC);
  }
  
  return error;
}
