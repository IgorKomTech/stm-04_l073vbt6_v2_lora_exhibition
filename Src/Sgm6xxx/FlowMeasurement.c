//=============================================================================
/// \file    FlowMeasurement.c
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Flow-Measurement
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "Time_g.h"
#include "Sgm.h"
#include "GasMeter.h"
#include "FlowMeasurement.h"
#include "Eeprom.h"
#include "main_function.h"
#include "stdlib.h"

#define NUMBER_OF_FLOW_CORRECTION_LUTS 1

static Error ReadFlowMeasurementConstants(void);
// static float ZeroingTinyFlow(float flow);  ---MVA 01.11.18

uint32_t cnt_reverse_flow=0;
uint8_t reverse_flow=0;
//extern bool EEprom_Lock;
uint16_t Sensor_Qfl;
TSensor Sensor;
TSens_Const Sensor_Const;

float _Short2LongFactor = 1.0f;

float percent_QgL_Disp_min = 0.05;
float percent_QgL_Disp_max = 0.15;
uint16_t Qfl_limit_QgL_Disp = 20000;

#pragma section = ".eeprom"

const TQf_param Qf_param[] @".eeprom" ={
//  min    max     dQf      ctg 
{0,0,65535,0.0,0.0f},
{1,0,65535,0.0,0.0f},
{2,0,65535,0.0,0.0f},
{3,0,65535,0.0,0.0f},
{4,0,65535,0.0,0.0f},
{5,0,65535,0.0,0.0f},
{6,0,65535,0.0,0.0f},
{7,0,65535,0.0,0.0f},
{8,0,65535,0.0,0.0f},
{9,0,65535,0.0,0.0f},
{10,0,65535,0.0,0.0f},
{11,0,65535,0.0,0.0f},
{12,0,65535,0.0,0.0f},
{13,0,65535,0.0,0.0f},
{14,0,65535,0.0,0.0f},
{15,0,65535,0.0,0.0f},
{16,0,65535,0.0,0.0f},
{17,0,65535,0.0,0.0f},
{18,0,65535,0.0,0.0f},
{19,0,65535,0.0,0.0f},
{20,0,65535,0.0,0.0f},
{21,0,65535,0.0,0.0f},
{22,0,65535,0.0,0.0f},
{23,0,65535,0.0,0.0f},
{24,0,65535,0.0,0.0f},
{25,0,65535,0.0,0.0f},
{26,0,65535,0.0,0.0f},
{27,0,65535,0.0,0.0f},
{28,0,65535,0.0,0.0f},
{29,0,65535,0.0,0.0f},
};

const uint16_t Qf_count = sizeof(Qf_param)/sizeof(Qf_param[0]);
const uint16_t Qf_sizeof = sizeof(Qf_param);

const uint16_t QposLimit @".eeprom" = 10100;
const uint16_t QnegLimit @".eeprom" = 9800;

const uint16_t KalmanBorder @".eeprom" = 10220;
const uint8_t KalmanCnt @".eeprom" = 2;
const float KalmanK1 @".eeprom" = 0.05f;
const float KalmanK2 @".eeprom" = 0.95f;

// -- Normal Mode (default) ---------------------------------------------------
/** \name Normal Mode (default) */
/// \brief FUllFlow measurement interval in seconds.
const uint16_t Config_GasmeterMeasurementIntervalNormalModeFM @".eeprom" = 2;
/// \brief SHORTFlow measurement interval in seconds.
const uint16_t Config_GasmeterMeasurementIntervalNormalModeSM @".eeprom" = 2;
/// \brief VDD_ticks recognition interval in seconds.
//const Config_GasmeterVDDRecognitionIntervalNormalMode @".eeprom" = 86400  ;
/// \brief Gas recognition interval in seconds.
const uint16_t Config_GasmeterGasRecognitionIntervalNormalMode @".eeprom" =  21600; // 
const uint16_t Config_GasmeterGasRecognitionIntervalMeasureMode @".eeprom" =  2; // 
const uint16_t Config_GasmeterGasRecognitionIntervalErrorMode @".eeprom" =  10800; // 
/// \brief Gas recognition retry interval on failer in seconds.
const uint16_t Config_GasmeterGasRecognitionRetryIntervalNormalMode @".eeprom" = 600; // 10 �����
/// \brief Time until low battery symbol appears in seconds.
//#define Config_GasmeterTimeUntilLowBatterySymbolNormalMode  141912000 // 4.5y
/// \brief Buckup interval in seconds.
//#define Config_GasmeterBackupIntervalNormalMode 1800 // 30min

extern uint32_t _MeasurementIntervalFM;
// -- Test Mode ---------------------------------------------------------------
/** \name Test Mode */
/// \brief Flow measurement fast interval in seconds.
const uint16_t Config_GasmeterMeasurementIntervalTestModeFM @".eeprom" = 1;
/// \brief Gas recognition interval in seconds.
const uint16_t Config_GasmeterGasRecognitionIntervalTestMode @".eeprom" = 300;

// ----------------------------------------------------------------------------
Error FlowMeas_InitModule(void)
{
  Error error = Error_None();
 
    error = ReadFlowMeasurementConstants();  // ��������� �� ������ !!!
  return error;
}

//----------------------------------------------------------------------------
double CalcSensOffset(uint16_t Qfa)
{
  double dQf = 0;

//  while(EEprom_Lock); // Check EEPROM status
  
 if (Qfa >= 10000)
  for( unsigned int count_ckl = 0; count_ckl < Qf_count; count_ckl++)
  { 
    if ((Qfa > Qf_param[count_ckl].min_Border_Qf) && (Qfa <= Qf_param[count_ckl].max_Border_Qf))
    { 
      if(count_ckl == 0)
      {
        dQf = (double)(Qf_param[count_ckl].max_Border_Qf - Qfa);
        dQf  *= Qf_param[count_ckl].ctg_dQf;
        dQf  = Qf_param[count_ckl].offset_dQf - dQf;
        break;
      }         
      else  
      { 
        dQf = (double)(Qfa - Qf_param[count_ckl].min_Border_Qf);
        dQf *= Qf_param[count_ckl].ctg_dQf;
        dQf += Qf_param[count_ckl].offset_dQf;
        break;
      } 
    }  
       
  }
 return dQf;
 } 

// ----------------------------------------------------------------------------


/* ----------------------------------------------------------------------------
Error FlowMeas_MeasureFlowShort(void) 
{
  double dQf;
  Error error = Sgm_MeasFlowShort(&Sensor);
  
   dQf = CalcSensOffset(Sensor.Qfs_FA);
  if(Error_IsNone(error)) 
  {
      //	_DQF_Short = GetFlowCorrection(qFA_Short);
   if(Sensor.Qfl > QposLimit)
    {
     GasMeter.QgS = ((double)Sensor.Qfs - (double)Device_STM.Lut +(double)Sensor.dQVDD + (double)dQf)/ (double)Device_STM.SF;
     GasMeter.QgS *= _Short2LongFactor;
//     GasMeter.QgS = ZeroingTinyFlow(GasMeter.QgS);
    }
   else
    GasMeter.QgS = 0.0f ;
   
     GasMeter.QgS *= Device_STM.CalkValue;
     GasMeter.QgS_m3 =GasMeter.QgS * 0.06f;
     //  * _Short2LongFactor;
    //???? *value = qShort * _Short2LongFactor;
  }
  
  Error_AppendIfError(&error, ERROR_FLOWMEAS_MEASURE_FLOW_CHECK);
  return error;
} */

// ----------------------------------------------------------------------------
/*FlowMeas_Unit FlowMeas_GetFlowUnit(void)
{
  return Device_STM.FlowUnit;
}
*/

// ----------------------------------------------------------------------------
/*
Error ReadDeviceVDD(void)
{
  short int lo_dQVDD;  
  uint16_t lo_VDD_ticks;
  Error error;
  
   error = Sgm_GetVDD((short*)&lo_dQVDD,&lo_VDD_ticks);
   if(Error_IsNone(error))
   { 
     Sensor.dQVDD = lo_dQVDD;
     GasMeter.VDD_power = (float)lo_VDD_ticks/9840.0f;
   } 
  return error; 
} 
*/
// ----------------------------------------------------------------------------
static Error ReadFlowMeasurementConstants(void)
{
  Error error ;
  
  uint16_t scaleFactor;
  uint16_t unit;
  uint16_t offset;
  FlowMeas_Unit _Unit = FLOWMEAS_UNIT_UNKOWN; // TODO
    
  error = Sgm_GetSf_Offset(&scaleFactor, &unit, &offset);
  if(Error_IsNone(error))
    {
      if (Device_STM.SF != scaleFactor)
        Eeprom_WriteWord((uint32_t)&Device_STM.SF,scaleFactor);
      if (Device_STM.Lut != offset) 
        Eeprom_WriteWord((uint32_t)&Device_STM.Lut,offset);
    }
  
  if(Error_IsNone(error))
  {
    float CalkValue = 1.0;
    switch(unit) 
    {
      case 0x0048:
        _Unit = FLOWMEAS_UNIT_NORM_LITER_PER_MINUTE;
        CalkValue = 293.15f / 273.15f;
        break;
      case 0x0248:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_15_PER_MINUTE;
        CalkValue = 293.15f / 288.15f;
        break;
      case 0x0148:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_20_PER_MINUTE;
        CalkValue = 293.15f / 293.15f;
        break;
      case 0x0348:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_25_PER_MINUTE;
        CalkValue =  293.15f / 298.15f;
        break;
      case 0x005B:
        _Unit = FLOWMEAS_UNIT_NORM_LITER_PER_HOUR;
        break;
      case 0x025B:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_15_PER_HOUR;
        break;
      case 0x015B:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_20_PER_HOUR;
        break;
      case 0x035B:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_25_PER_HOUR;
        break;
      default:
        _Unit = FLOWMEAS_UNIT_UNKOWN;
        Error_Add(&error, ERROR_FLOWMEAS_UNKOWN_FLOW_UNIT);
    }
    if (Device_STM.CalkValue != CalkValue)
     Eeprom_WriteFloat((uint32_t)&Device_STM.CalkValue,CalkValue);
    
      if(Error_IsNone(error))
        Sensor_Const.FlowUnit = _Unit;
  }
  
  return error;
}

 //----------------------------------------------------------------------------
Error FlowMeas_MeasureFlow(void)
{

  static double lo_QgLm3;
  static uint32_t lo_mesCount=0; 
  double dQf_l; // dQf_s; ---MVA 1.10.18
  static float lo_LastSensQf = 10000.0f;
  static float lo_KalmanK = 0.1;
  static uint8_t lo_KalmanFlag = 0, lo_AddToFlow = 0;
  static uint32_t lo_LastMeas = 0;
  static double lo_GasmeterDisp = 0.0;  
  Error error = Error_None();  
  
  // ��������� �������
  error = Sgm_MeasFlow(&Sensor);
  if(Error_IsError(error))  // ������ ��������� 
  {
    Error_AppendIfError(&error, ERROR_FLOWMEAS_MEASURE_FLOW);
    return error;
  }
//++++++++++++++++++++++++++++++++++++++++++++++  
  Sensor_Qfl = Sensor.Qfl;
//++++++++++++++++++++++++++++++++++++++++++++++    
  if(EnableKalmanFilter)
   {
     
     if(Sensor.Qfl > KalmanBorder)  // KalmanBorder =10220
     {
//       if(lo_mesCount > 5)
//         KalmanK = KalmanK1;
//       else
       if(lo_KalmanK != KalmanK2)         // ��������� �� ������ �
        {
           if(lo_KalmanFlag >= KalmanCnt) // ��������� � ����������� ���� n ��� 
           {
             lo_KalmanK = KalmanK2;
             lo_LastSensQf = 10000; 
             lo_AddToFlow =1;
           } 
           else                       // ������ ������� 
           {
            lo_LastMeas += Sensor.Qfl;  // ��������� ��������
            Sensor.Qfl = 10000;         // ���������� ���������
            lo_KalmanFlag++;           // ��������� �����
           } 
        }  
     } 
     else                               // ���� �������
     { 
       if((lo_KalmanK != KalmanK1) || lo_KalmanFlag)           
       {
         lo_KalmanK = KalmanK1;
         lo_LastSensQf = 10000;
         if (lo_KalmanFlag)
         {
           lo_KalmanFlag = 0;
           lo_LastMeas = 0;
         } 
       } 
//       if(lo_LastSensQf > KalmanBorder) 
//         lo_LastSensQf = 10000;
     }
     float lo_SensQf;
     lo_SensQf =(float)Sensor.Qfl;

     lo_SensQf = (lo_KalmanK * lo_SensQf +(1-lo_KalmanK)* lo_LastSensQf);
     lo_LastSensQf = lo_SensQf;
     Sensor.Qfl = (int)lo_SensQf; 
 
     if(lo_AddToFlow)
     {
       double lo_QgL;
       if (lo_LastMeas > QposLimit)
       {
         dQf_l = CalcSensOffset(lo_LastMeas);
         lo_QgL = ((double)lo_LastMeas - (double)Device_STM.Lut + (double)dQf_l)/(double)Device_STM.SF;
         lo_QgL *= Device_STM.CalkValue; /// ������� �� 20 ��������
         lo_QgL *= 0.06;                 // �������� �3 � ��� 
         GasMeter.VE_Lm3 += (lo_QgL * (float)_MeasurementIntervalFM/3600.0f);
       } 
       lo_LastMeas = 0;
       lo_AddToFlow = 0;
     }  
   }

  //������ ������ --------------------------------------------------------------------------------------
  if(Sensor.Qfl > QposLimit)
   {
      dQf_l = CalcSensOffset(Sensor.Qfl);
      GasMeter.dQf = dQf_l;
  
      GasMeter.QgL = ((double)Sensor.Qfl - (double)Device_STM.Lut + (double)dQf_l)/(double)Device_STM.SF;
      GasMeter.QgL *= Device_STM.CalkValue; /// ������� �� 20 ��������
  
#ifdef PABS_OK 
      double kPabs;
      kPabs = 1 + (pressure_abs_gas - 96.325) / 100;
      GasMeter.QgL *= kPabs;
#endif
    }
    else
      GasMeter.QgL = 0.0;
  
// ������ �������  ------------------------------------------------
    if (Sensor_Qfl >=  QposLimit)
    {
      dQf_l = CalcSensOffset(Sensor_Qfl);
      lo_GasmeterDisp = ((double)Sensor_Qfl - (double)Device_STM.Lut + (double)dQf_l)/(double)Device_STM.SF;
      lo_GasmeterDisp *= Device_STM.CalkValue; /// ������� �� 20 ��������
      lo_GasmeterDisp *= 0.06;
    }
    else 
     lo_GasmeterDisp = 0.0;  
  
    //------------------------------
    if(Sensor_Qfl < QnegLimit)
      { 
         if(cnt_reverse_flow > 10)
           StatusDevice.reverse_flow = 1;
         ++cnt_reverse_flow;
      }
      else
      {
         StatusDevice.reverse_flow = 0;
         cnt_reverse_flow = 0;
      }
    //------------------------------
   GasMeter.QgL_m3 = GasMeter.QgL * 0.06;

 // ����������� �����
//    if (GasMeter.QgL_m3 !=0.0)
    if (lo_GasmeterDisp !=0.0)
       {
          lo_QgLm3 =(lo_GasmeterDisp - GasMeter.QgL_Disp)/lo_GasmeterDisp;    
          if((lo_QgLm3 < 0.15) && (lo_QgLm3 > -0.15))
          {
            GasMeter.QgL_Disp = ((GasMeter.QgL_Disp*lo_mesCount)+ lo_GasmeterDisp )/(lo_mesCount+1);
            lo_mesCount++;
          }
          else
          {
            GasMeter.QgL_Disp = lo_GasmeterDisp ;
            lo_mesCount =0;
          } 
       }        
      else
        {
          GasMeter.QgL_Disp = lo_GasmeterDisp ;
          lo_mesCount =0;
        } 
  
  return error;
}