//=============================================================================
/// \file    GasMeter.c
/// \author  RFU
/// \date    09-Feb-2016
/// \brief   Implementing the Gasmeter-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "task.h"
#include "Sgm.h"
#include "GasMeter.h"
#include "FlowMeasurement.h"
#include "SensorTypeParameters.h"
#include "Time_g.h"
#include "Io.h"
#include "Error.h"
#include "menu.h"
#include "archiv.h"
#include "Uart_152.h"
#include "System.h"
#include "main_function.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CONFIG_USE_CATEGORY_GASMETER
#define CONFIG_USE_CATEGORY_WELMEC
#define CONST_DELTA_Q 0.01
#include "Configuration.h"
uint8_t TransmitOpticTest=0;
static Error PerformInitalMeasurements(void);
static bool AnalyzeWhichTaskIsRequered(void);
static Error PerformFMMeasurements(void);
//static Error PerformSMMeasurements(void);
static void PerformGasRecognition(void);

static uint32_t GetTimeSinceLastMeasurementFM(void);
static uint32_t GetTimeSinceLastMeasurementSM(void);
static uint32_t GetTimeSinceLastGasRecognition(void);
static Error GetProductSerialNumber(uint8_t* buffer, uint8_t* size);
static Error GetArticleCode(uint8_t* buffer, uint8_t* size);
static Error Perform_TEST_FMMeasurements(void);
//static uint32_t GetTimeSinceLastVDDMeasurement(void);
//static Error Perform_VDDeasurements(void);

static void Uint64ToString(uint64_t number, uint8_t* buffer, uint8_t* size);

static Error Gasmeter_TEST_Process(void);

static uint16_t TestMeassureInterval;
uint32_t Qmax_warning_cnt_measurement_temp=0;
uint32_t Qmax_cnt_measurement_temp=0;
uint32_t cnt_measurement_temper=0;
uint32_t cnt_measurement_temper_warning=0;

uint16_t TestInterval=100;
uint16_t Kfactor_measure_cnt=0;

uint8_t Kfactor_measure_mode=KFACTOR_MODE_NORM;

//-----------------------------------------------------------------------------
// \brief Measurement interval.
static uint32_t _MeasurementIntervalFM;
uint32_t _MeasurementIntervalFM;
static uint32_t _MeasurementIntervalSM;
//static uint32_t _VDDMeasurementInterval;
/// \brief Gas recognition interval.
static uint32_t _GasRecognitionInterval;
/// \brief Time of last measurement.
static uint32_t _LastMeasurementTimeFM = 0;
/// \brief Time of last measurement.
static uint32_t _LastMeasurementTimeSM = 0;
/// \brief Time of last gas recognition.
static uint32_t _LastGasRecognitionTime = 0;
/// \brief Time of last VDD_ticks measurement..
//static uint32_t _LastVDDMeasurementTime = 0;

///// \brief Is set to true if a new measurement is required.
static bool _IsFMMeasurementRequered = false;
static bool _IsSMMeasurementRequered = false;
static bool _IsGasRecognitionRequered = false;
//static bool _IsVDDMeasurementRequered = false;
static bool _IsGasRecognitionValid = false;

/// \brief Counts successive errors.
static uint32_t _ErrorCounter = 0;
double GasSelfBorder;

uint8_t index_Meas_interval_mas=0;

T_TestMode TestModeActive = TestModeOFF;
T_TestMode TestModeActive_previous = TestModeOFF;
TGasMeter GasMeter;

#pragma section = ".eeprom"
const TDevice Device_STM @".eeprom" = {400,
                                       10000,
                                       1.0173521044f,
                                       FW_VERSION_MAJOR,
                                       FW_VERSION_MINOR,
                                       HW_VERSION_MAJOR,
                                       HW_VERSION_MINOR,
                                       {0,2,25,18},
                                       "123456",
                                     //  20.0f,
                                     //  0.01f,
                                       55,
                                       -25
                                             };

// If NoShortMeassure = 0 in NormalMode used ShortMeassure procedure 
const uint16_t NoShortMeassure @".eeprom" = 0xFF;


// ����� ���������� ��� ������ 
/// \brief The current flow measurement-value.
float _FlowValue = 0.0f;
/// \brief The current flow measurement-value short.
float _ShortFlowValue = 0.0f;
/// \brief The current total volume measured by the gasmeter.
double _VPulse = 0;

uint16_t _GasTemperatureTraw;
uint16_t _GasRecognitionK;
uint16_t _GasRecognitionK_T; 

double TEST_VE_L = 0.0;
double TEST_VE_Pulse = 0.0;   //�����!!!!       
double TEST_VE_Lm3 = 0.0;
uint32_t TEST_QF_summ = 0;
uint16_t TEST_QF_Count = 0;
float Test_QF_midle = 0.0;

float Int_Flow_Max =0.0;
uint16_t Int_Temp_Count = 0;
float Int_Temp_Max = 0;
float Int_Temp_Min = 0;
double Int_Temp_Midl =0;
double Int_cnt_Temp_Midl =0;

extern TypeStatus StatusDevice;

//-----------------------------------------------------------------------------
Error Gasmeter_InitModule(void)
{
  uint8_t size; 
  Error error = Error_None();
  
  error = GetProductSerialNumber(Sensor_Const.SN_SGM, &size);
  if(Error_IsNone(error))   
   error =  GetArticleCode(Sensor_Const.Article,&size);
  if(Error_IsNone(error))   
  {
    /*
    uint8_t lo_type;
    lo_type =Sensor_Const.Article[6];
    if ((lo_type == 0x34 || Device_STM.SF == 400) && (type_device == SGM_TYPE_SGM4 || type_device == SGM_TYPE_SGM10)) 
    {
      _SgmType = (Sgm_Type)type_device;
      if(type_device == SGM_TYPE_SGM4)
        type_device_int = 4;
      else
        type_device_int = 10;
    }
    else
    {
      if ((lo_type == 0x36 || Device_STM.SF == 250) && (type_device == SGM_TYPE_SGM6 || type_device == SGM_TYPE_SGM10)) 
      {
        _SgmType = (Sgm_Type)type_device;
       // Sensor_Const.Article[6] = '6';
        if(type_device == SGM_TYPE_SGM6)
           type_device_int = 6;
        else
           type_device_int = 10;
      }
      else
      {
         _SgmType = SGM_TYPE_UNDEFINED;
         Error_Add(&error, ERROR_SGM_CRC);
         type_device_int = 0;
      }
    }
    */
    _SgmType = (Sgm_Type)type_device;
    switch(type_device)
    {
    case SGM_TYPE_SGM4:
         type_device_int = 4;
         break;
    case SGM_TYPE_SGM6:
         type_device_int = 6;
         break;    
    case SGM_TYPE_SGM10:
         type_device_int = 10;
         break;  
    default:
         type_device_int = 0;
         _SgmType = SGM_TYPE_UNDEFINED;
         Error_Add(&error, ERROR_SGM_CRC);
         break;
    }
  }
  if(Error_IsNone(error)) 
    error = PerformInitalMeasurements();
  
 ActivateTestMode(TestModeOFF); 
  
  return error;
}

//-----------------------------------------------------------------------------
Error Gasmeter_Process(void)
{
     
  Error error = Error_None();  
 
  if(TestModeActive)
  {
    error = Gasmeter_TEST_Process();
    return error;
  } 
  
   if (AnalyzeWhichTaskIsRequered())             // ����� ������� ������
       SGM_PowerOn();
   else 
     return error;
  
   if(_IsGasRecognitionRequered || Global_Menu == MAIN_MENU_GASTYPE)               //���������� �������� � ������
     PerformGasRecognition();
//   else

 /*
   if(_IsVDDMeasurementRequered)
    error = Perform_VDDeasurements();
   */
   if(_IsFMMeasurementRequered)                // Long ���������
        error = PerformFMMeasurements();
 /*   else
    {
      if(_IsSMMeasurementRequered ) // Short ���������  
          error = PerformSMMeasurements();
    } */
  SGM_PowerOff();
   
  if(Error_IsError(error)) 
      _ErrorCounter++;
  else 
  {
    float lo_Temper;
/*
    if(Int_Temp_Count)
    {
      if(abs((int)(GasMeter.var_T - Int_Temp_Midl)) > 4)
        GasMeter.var_T = Int_Temp_Midl;
    }
    */
    lo_Temper = GasMeter.var_T;
    _ErrorCounter = 0;
    if (Int_Temp_Count==0)
    {
      Int_Temp_Max = lo_Temper;
      Int_Temp_Min = Int_Temp_Max;
      Int_Temp_Midl = lo_Temper;
      Int_Flow_Max = GasMeter.QgL_m3;
      Int_QgL_m3_midl = GasMeter.QgL_m3;
    }
    else
    {
      if(lo_Temper >= Int_Temp_Max)
        Int_Temp_Max = lo_Temper;
      if(lo_Temper <= Int_Temp_Min)
        Int_Temp_Min = lo_Temper;
      if(GasMeter.QgL_m3 > Int_Flow_Max)
        Int_Flow_Max = GasMeter.QgL_m3;
     
       Int_Temp_Midl =(Int_Temp_Midl * Int_Temp_Count + lo_Temper) / (Int_Temp_Count  + 1);
       Int_QgL_m3_midl = (Int_QgL_m3_midl * Int_Temp_Count + GasMeter.QgL_m3) / (Int_Temp_Count + 1);
    }

      //--------------------------------------------------------------------------------------
      if(GasMeter.QgL_Disp > sensorTypeParameters[_SgmType].Q_max * Qmax_min_warning)
      {
        if(Qmax_warning_cnt_measurement_temp > Qmax_warning_cnt_measurement)
          Qmax_warning_temp = 1;
        ++Qmax_warning_cnt_measurement_temp;
      }
      else
      {
        Qmax_warning_cnt_measurement_temp = 0;
        Qmax_warning_temp = 0;
      }
      //--------------------------------------------------------------------------------------
      if(GasMeter.QgL_Disp > sensorTypeParameters[_SgmType].Q_max * Qmax_warning)
      {
        if(Qmax_cnt_measurement_temp > Qmax_cnt_measurement)
          going_beyond_Q = 1;
        ++Qmax_cnt_measurement_temp;
      }
      else
      {
        Qmax_cnt_measurement_temp = 0;
        going_beyond_Q = 0;
      }      
      //--------------------------------------------------------------------------------------
      if(GasMeter.var_T > Device_STM.Max_T || GasMeter.var_T < Device_STM.Min_T)
      {
        if(cnt_measurement_temper > cnt_measurement_temper_eeprom)
          going_beyond_T = 1;
        ++cnt_measurement_temper;
      }
      else
      {
         going_beyond_T = 0;
         cnt_measurement_temper = 0;
      }
      //--------------------------------------------------------------------------------------
      if((GasMeter.var_T > Tmax_warning && GasMeter.var_T <= Device_STM.Max_T)
         || (GasMeter.var_T < Tmin_warning && GasMeter.var_T <= Device_STM.Min_T))
      {
        if(cnt_measurement_temper_warning > cnt_measurement_temper_warning_eeprom)
          going_beyond_T_warning = 1;
        ++cnt_measurement_temper_warning;
      }
      else
      {
         going_beyond_T_warning = 0;
         cnt_measurement_temper_warning = 0;
      }    
      //--------------------------------------------------------------------------------------
      if(!(Int_Temp_Count % 10) && Int_Temp_Count)
      {
         Int_cnt_Temp_Midl /=10;
         Int_cnt_Temp_Midl = GasMeter.var_T;
      }
      else
      {
        if(!Int_Temp_Count)
           Int_cnt_Temp_Midl = GasMeter.var_T;
        else
           Int_cnt_Temp_Midl += GasMeter.var_T; 
      }

   Int_Temp_Count++; 
  }
    if(TransmitEnable == TRANSMIT_ENABLE && TransmitOpticTest)
      {
        char *ptr;
        GetDate_Time(&SystDate,&SystTime); 
        sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u\t%u\t%u\t%0.4f\t%u\t%0.2f\t%0.6f", // dQf = %0.4f
        SystTime.Hours,SystTime.Minutes,SystTime.Seconds,Sensor.Qfl,Sensor_Qfl, GasMeter.QgL_m3,
        Sensor.KL, GasMeter.var_T, GasMeter.VE_Lm3);
        ptr = (char*)TransmitBuffer + strlen(TransmitBuffer);
        sprintf(ptr,"\tK=%d T=%d KK1=%0.3f KK2=%0.3f KB=%d P=%d\r\n", // 
                                       EnableKalmanFilter, 0,
                                       KalmanK1,KalmanK2, KalmanBorder, KalmanCnt);
        Opto_Uart_Transmit(); 
      }  
  
  if(_ErrorCounter >= 3)
    Error_SetFatal(&error);
 return error;   
}

//-----------------------------------------------------------------------------
static Error Gasmeter_TEST_Process(void)
{
  uint8_t lo_Flag = 0;
  uint16_t lo_pulsePeriod;
  uint16_t lo_sysdelay;
    
  Error error = Error_None();  
  
  startPeriodicTimer();
   
  if(AnalyzeWhichTaskIsRequered())       // ����� ������� ������
     SGM_PowerOn();
    
  if(_IsGasRecognitionRequered)               //���������� �������� � ������
    PerformGasRecognition();
 /*
  if(_IsVDDMeasurementRequered)
   error = Perform_VDDeasurements();
  */
  if(_IsFMMeasurementRequered)                // Long ���������
    {
      error = Perform_TEST_FMMeasurements();
      lo_Flag = 0xff;
    }
  else
    {
/*      if((_IsSMMeasurementRequered ) && (TestModeActive== TestMode_4))// Short ���������  
      { 
        error = PerformSMMeasurements();
        lo_Flag = 0xff;  
      }  */
    }
  SGM_PowerOff(); 

  lo_sysdelay = stopPeriodicTimer();
  
  if(Error_IsError(error)) 
    _ErrorCounter++;
  else 
    _ErrorCounter = 0;
  
  if(_ErrorCounter >= 3)
      Error_SetFatal(&error);
    
  if(lo_Flag)
  {
    switch(TestModeActive)
          { 
/*            case TestMode_4:  
                   GetDate_Time(&SystDate,&SystTime); 
                   sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u\t%0.5f\t%0.5f\t%0.5f\r\n",\
                          SystTime.Hours,SystTime.Minutes,SystTime.Seconds,GasMeter.QgL_m3,\
                          GasMeter.QgS_m3, TEST_VE_Lm3);

                   Opto_Uart_Transmit(); 
                  break;   */
            case TestMode_3:
                   if(TestMeassureInterval)
                     {
                       TestMeassureInterval -= _MeasurementIntervalFM;
                       TEST_QF_summ += Sensor.Qfl;
                       TEST_QF_Count++;
                     }
                    if(TestMeassureInterval==0)
                      {
                        Test_QF_midle = (float)TEST_QF_summ/(float)TEST_QF_Count;
                         if(TransmitEnable == TRANSMIT_ENABLE)
                           {
                              sprintf((char*)TransmitBuffer,"STOP: %0.2u sek.\t%0.4f\t%0.4f\t%0.4f\t%u\r\n",\
                              TestInterval, TEST_VE_L, TEST_VE_Lm3,Test_QF_midle,TEST_QF_Count );  
                              Opto_Uart_Transmit(); 
                           }
                         GasMeter.VE_Lm3 += TEST_VE_Lm3; 
                        ActivateTestMode(TestModeOFF);  
                      }              
                   break;
          case TestMode_2:
                _VPulse += TEST_VE_Pulse * 10.0f; 
                Pulse = (int)_VPulse;          // ����� ����� ������ � ������
                _VPulse -= (float)Pulse;
      
                if(Pulse) 
                  {
                    lo_pulsePeriod = (950-lo_sysdelay)/(Pulse*2);
                    lo_pulsePeriod *= _MeasurementIntervalFM;
                    startOutputTimer(lo_pulsePeriod);
                  }  
                break;  
          case TestMode_1: 
                if(TransmitEnable == TRANSMIT_ENABLE)
                  {
                    GetDate_Time(&SystDate,&SystTime); 
                    sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u\t%u\t%0.4f\t%u\t%u\t%0.2f\t%0.8f\t%0.4f\r\n", // dQf = %0.4f
                    SystTime.Hours,SystTime.Minutes,SystTime.Seconds,Sensor.Qfl,GasMeter.QgL_m3,
                    Sensor.KL, Sensor.KS, GasMeter.var_T, TEST_VE_Lm3, GasMeter.dQf/*, GasMeter.VDD_power*/);

                    Opto_Uart_Transmit(); 
                  }
                break;  
          case TestMode_5: 
                if(TransmitEnable == TRANSMIT_ENABLE)
                  {
                    GetDate_Time(&SystDate,&SystTime); 
                    uint32_t sizefree = xPortGetFreeHeapSize();
                    sprintf((char*)TransmitBuffer,"%0.2u:%0.2u:%0.2u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\r\n", 
                    SystTime.Hours,SystTime.Minutes,SystTime.Seconds,
                    stack_GasmeterTask, stack_OPTO_ReadTask, stack_ReadWriteARC, stack_task_GSM, stack_GSM_RX_UARTTask, stack_IDLE, sizefree);

                    Opto_Uart_Transmit(); 
                  }
                break;                  
          }  // end switch  
    lo_Flag =0;
   }
  return error;
}

//-----------------------------------------------------------------------------
static Error PerformInitalMeasurements(void)
{
  Error error = Error_None();
  
  for(uint8_t i = 4; i > 0; i--) 
  {
    PerformGasRecognition(); 
    if(_IsGasRecognitionValid)
      break;
  }
  
  if(!_IsGasRecognitionValid) 
    Error_Add(&error, ERROR_GASMETER_INIT_GASREC);
 /*  if(Error_IsNone(error)) 
   error = Perform_VDDeasurements();
   */ 
  if(Error_IsNone(error)) 
    error = PerformFMMeasurements();
  
  return error;
}

//-----------------------------------------------------------------------------
static bool AnalyzeWhichTaskIsRequered(void)
{
  uint32_t gasRegInterval;
                                   
  switch(Kfactor_measure_mode)
  {
  default:
  case KFACTOR_MODE_NORM:
       gasRegInterval = _IsGasRecognitionValid ? _GasRecognitionInterval : Config_GasmeterGasRecognitionRetryIntervalNormalMode;       
       break;
  case KFACTOR_MODE_MEASURE:
       gasRegInterval = _IsGasRecognitionValid ? Config_GasmeterGasRecognitionIntervalMeasureMode : Config_GasmeterGasRecognitionRetryIntervalNormalMode;       
       break;
  case KFACTOR_MODE_ERROR:
       gasRegInterval = _IsGasRecognitionValid ? Config_GasmeterGasRecognitionIntervalErrorMode : Config_GasmeterGasRecognitionRetryIntervalNormalMode;       
       break;
  }                                          
                                          

  //_IsVDDMeasurementRequered = GetTimeSinceLastVDDMeasurement() >= _VDDMeasurementInterval;                                          

  if(TestModeActive) 
  {   
     _IsGasRecognitionRequered = GetTimeSinceLastGasRecognition() >= 
                                   gasRegInterval;
     
      _IsFMMeasurementRequered = GetTimeSinceLastMeasurementFM() >= _MeasurementIntervalFM;

//      if (TestModeActive == TestMode_4)
//       _IsSMMeasurementRequered = GetTimeSinceLastMeasurementSM() >= _MeasurementIntervalSM;
//      else
//        _IsSMMeasurementRequered = false;
  }
  else
  { 
      _IsGasRecognitionRequered = GetTimeSinceLastGasRecognition() >= gasRegInterval;
      _IsFMMeasurementRequered = GetTimeSinceLastMeasurementFM() >= _MeasurementIntervalFM;
      
     if(NoShortMeassure) 
          _IsSMMeasurementRequered = false;
     else
     {
       if (_IsFMMeasurementRequered)
         _IsSMMeasurementRequered = false;
        else
       _IsSMMeasurementRequered = GetTimeSinceLastMeasurementSM() >= _MeasurementIntervalSM;
     }  
  }
  if(_IsGasRecognitionRequered || _IsSMMeasurementRequered || _IsFMMeasurementRequered /*|| _IsVDDMeasurementRequered*/ )
    return true;
  else
    return false;
}  

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void CalcSMVolume(uint8_t p_LScalc)
{
  double WE_IntervalValue;
  uint32_t Mess_interval;
  
  if(NoShortMeassure)  
     Mess_interval = GetTimeSinceLastMeasurementFM();
  else
     Mess_interval = GetTimeSinceLastMeasurementSM();

  // Short �����
  GasMeter.VE_S += (GasMeter.QgS * Mess_interval);
  WE_IntervalValue = (GasMeter.QgS_m3 * (float)Mess_interval/3600.0f);
  GasMeter.VE_Sm3 += WE_IntervalValue;
  // ������� � Long ������
  if (p_LScalc)
  {
    GasMeter.VE_L += (GasMeter.QgS * Mess_interval);
    WE_IntervalValue = (GasMeter.QgL_m3 * (float)Mess_interval/3600.0f);
    GasMeter.VE_Lm3 += WE_IntervalValue;
  }
  _LastMeasurementTimeSM = Time_GetSystemUpTimeSecond();
 } 

//------------------------------------------------------------------------------
static Error PerformFMMeasurements(void) // 19.07.19
{
 // #define MAX_PERIOD 5  // ������������ ������ "�������" - N ������ ���������



 
    
   
  double WE_IntervalValue;
  uint32_t Meas_interval;
  
  
  Error error = Error_None();
  
   error = FlowMeas_MeasureFlow();
   if(Error_IsNone(error)) 
   { 

      Meas_interval = GetTimeSinceLastMeasurementFM();

               WE_IntervalValue = (GasMeter.QgL * (float)Meas_interval/60.0f ); //�����/������
            //   GasMeter.VE_L += WE_IntervalValue;
               GasMeter.VE_Pulse = WE_IntervalValue;   //�����!!!!       
               GasMeter.VE_Lm3 += (GasMeter.QgL_m3 * (float)Meas_interval/3600.0f);



      
     
    GasMeter.var_T = (float)Sensor.Traw/595.0f -35.0f;
      // �������� �����
    _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond();

  }
  return error;
}

//------------------------------------------------------------------------------
/*
static Error Perform_VDDeasurements(void)
{
  Error error;
   error = ReadDeviceVDD();
   _LastVDDMeasurementTime = Time_GetSystemUpTimeSecond();
   return error;
} 
*/
//------------------------------------------------------------------------------
static Error Perform_TEST_FMMeasurements(void)
{
  
  double WE_IntervalValue;
  uint16_t Meas_interval;
  
  Error error = Error_None();
  
   error = FlowMeas_MeasureFlow();
   if(Error_IsNone(error)) 
   { 
     Meas_interval = GetTimeSinceLastMeasurementFM();
     // ��������� ����� � ������ "��������" �������" 
     WE_IntervalValue = (GasMeter.QgL * (float)Meas_interval/60.0f ); //�����/������
     TEST_VE_L += WE_IntervalValue;
     TEST_VE_Pulse = WE_IntervalValue;   //�����!!!!       
     TEST_VE_Lm3 += (GasMeter.QgL_m3 * (float)Meas_interval/3600.0f);

    GasMeter.var_T = (float)Sensor.Traw/595.0f -35.0f;
      // �������� �����
    _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond();
  }
  return error;
}

/*------------------------------------------------------------------------------
static Error PerformSMMeasurements(void)
{
  Error error;
  double lo_Qg;
          
  error = FlowMeas_MeasureFlowShort();
  if(Error_IsNone(error))
  { 
    if((GasMeter.QgS ==0.0f) && (GasMeter.QgL ==0.0f))
    {
       _LastMeasurementTimeSM = Time_GetSystemUpTimeSecond(); 
       return error;
    } 
    if(GasMeter.QgS !=0.0f)
       lo_Qg = GasMeter.QgS_m3; 
    if(GasMeter.QgL !=0.0f)      
       lo_Qg = GasMeter.QgL_m3; 
    {
      if((fabs(GasMeter.QgS_m3 - GasMeter.QgL_m3)/lo_Qg) <= CONST_DELTA_Q)
         CalcSMVolume(0x1F); 
       else
       {
         if(TestModeActive == TestMode_4)
           error = Perform_TEST_FMMeasurements(); 
         else
           error = PerformFMMeasurements(); 
       }  
    }
    
    _LastMeasurementTimeSM = Time_GetSystemUpTimeSecond();
  }
     
  return error;
}
*/

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static void PerformGasRecognition(void)
{
  uint16_t recGas[5];
  uint32_t SumRec = 0;
  Error error;
  
  for (uint8_t i=0; i<5; i++)
  {
     error =Sgm_GasRecognition(&recGas[i]);
     if (Error_IsNone(error))
       SumRec += recGas[i];
     else
       break;
  } 
   Sensor.KL = SumRec/5;

  if(Sensor.KL == 40960)
     pressure_abs_gas = 96.325;
  else
     pressure_abs_gas = pressure_abs;
  
  //--------------------------------------------------------
  if((Sensor.KL > Kfactor_average && Sensor.KL < Kfactor_max) || (Sensor.KL < Kfactor_min))
  {
     if(Kfactor_measure_mode == KFACTOR_MODE_NORM)
     {
       Kfactor_measure_mode = KFACTOR_MODE_MEASURE;
     }
     if(Kfactor_measure_mode != KFACTOR_MODE_ERROR && Kfactor_measure_cnt > Kfactor_measure_cnt_eeprom)
     {
        Kfactor_measure_mode = KFACTOR_MODE_ERROR;
     }
     ++Kfactor_measure_cnt;
  }
  else
  {
    Kfactor_measure_mode = KFACTOR_MODE_NORM;
    Kfactor_measure_cnt = 0;
  }
  //--------------------------------------------------------
  _IsGasRecognitionValid = Error_IsNone(error);
 _LastGasRecognitionTime = Time_GetSystemUpTimeSecond();
}

//-----------------------------------------------------------------------------
static uint32_t GetTimeSinceLastMeasurementFM(void)
{
  return Time_GetSystemUpTimeSecond() - _LastMeasurementTimeFM;
}

//-----------------------------------------------------------------------------
static uint32_t GetTimeSinceLastMeasurementSM(void)
{
  return Time_GetSystemUpTimeSecond() - _LastMeasurementTimeSM;
}

//-----------------------------------------------------------------------------
static uint32_t GetTimeSinceLastGasRecognition(void)
{
  return Time_GetSystemUpTimeSecond() - _LastGasRecognitionTime;
}

//-----------------------------------------------------------------------------
/*
static uint32_t GetTimeSinceLastVDDMeasurement(void)
{
  return Time_GetSystemUpTimeSecond() - _LastVDDMeasurementTime;
}
*/

//-----------------------------------------------------------------------------
static void Uint64ToString(uint64_t number, uint8_t* buffer, uint8_t* size)
{
  uint64_t divisor = 10000000000000000000ul;
  uint8_t  digit;
  *size = 0;
  while((number < divisor) && (divisor > 0)) {
    divisor /= 10;
  }
  while(divisor) {
    digit = number / divisor;
    number -= digit * divisor;
    buffer[(*size)++] = '0' + digit;
    divisor /= 10;
  }
  if(*size == 0) {
    buffer[0] = '0';
    *size = 1;
  }
}

//-----------------------------------------------------------------------------
static Error GetProductSerialNumber(uint8_t* buffer, uint8_t* size)
{
  Error error;
  
  uint64_t serialNumber;
  error = Sgm_GetProductSerialNumber(&serialNumber);
  
  if(Error_IsNone(error))
      Uint64ToString(serialNumber, buffer, size);
  return error;
}

//-----------------------------------------------------------------------------
static Error GetArticleCode(uint8_t* buffer, uint8_t* size)
{
  *size = 20;
  return Sgm_GetArticleCode(buffer);
}

//-----------------------------------------------------------------------------
void ActivateTestMode(T_TestMode pTestMode)
{
     if(TestModeActive != pTestMode)
     {
        TestModeActive = pTestMode;
     }
     if (TestModeActive)
     {
       TEST_VE_L = 0.0;
       TEST_VE_Pulse = 0.0;
       TEST_VE_Lm3 = 0.0;
       Kfactor_measure_mode = KFACTOR_MODE_NORM;
       _MeasurementIntervalFM = Config_GasmeterMeasurementIntervalTestModeFM;
       _GasRecognitionInterval = Config_GasmeterGasRecognitionIntervalTestMode;
       //_VDDMeasurementInterval = Config_GasmeterVDDRecognitionIntervalTestMode;
       
       _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond();
//       if(pTestMode == TestMode_4)
//         if(_MeasurementIntervalSM >= _MeasurementIntervalFM)
//           _MeasurementIntervalFM = _MeasurementIntervalSM * 3; 
       if(pTestMode == TestMode_2)
          InitOtputTimer();
       else
          DeInitOtputTimer();
       if(pTestMode == TestMode_3)
        {
          TestMeassureInterval = TestInterval;
          TEST_QF_summ = 0;
          TEST_QF_Count = 0;
        } 
      } 
   else
     {
       DeInitOtputTimer();
      _MeasurementIntervalFM = Config_GasmeterMeasurementIntervalNormalModeFM;
      _MeasurementIntervalSM = Config_GasmeterMeasurementIntervalNormalModeSM;
      _GasRecognitionInterval = Config_GasmeterGasRecognitionIntervalNormalMode;
      //_VDDMeasurementInterval = Config_GasmeterVDDRecognitionIntervalNormalMode;
     }  
}

//-----------------------------------------------------------------------------
