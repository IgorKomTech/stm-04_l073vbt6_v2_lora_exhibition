//=============================================================================
/// \file    SensorTypeParameters.c
/// \author  RFU
/// \date    29-Oct-2015
/// \brief   Module for sensor specific parameters
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "SensorTypeParameters.h"

// Indexes must match with Sgm_Type definition in Sgm.h!
SensorTypeParameters sensorTypeParameters[] = { 
//   Q_neg | Q_neg | Q_pos | Q_min |   Q_t  |  Q_max |  Q_pos |
//    Sat  | Limit | Limit |       |        |        |   Sat  |        Sensor
  {  -24.0f, -0.13f,  0.13f, 0.040f, 10.000f, 6.00f,130.0f}, // SGM6004  
  {  -39.0f, -0.20f,  0.20f, 0.060f, 16.670f, 10.00f,215.0f}, // SGM6006
  {  -39.0f, -0.20f,  0.33f, 0.100f, 16.670f, 16.00f,215.0f}, // SGM6010  
}; // indexes must match SensorType-enum in Sgm.h!!!
