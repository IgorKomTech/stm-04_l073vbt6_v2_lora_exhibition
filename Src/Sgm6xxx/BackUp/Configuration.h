//=============================================================================
//   
//=============================================================================
/// \file    Configuration.h
/// \author 
/// \date   
/// \brief   Configuration for Gasmeter Reference Design.
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

//-----------------------------------------------------------------------------
// Firmware Version
//-----------------------------------------------------------------------------
/** \name Firmware Version */
/// \{
#define FW_VERSION_MAJOR  3
#define FW_VERSION_MINOR  1
#define FW_VERSION_DEBUG  0
/// \}

//-----------------------------------------------------------------------------
/// Device Information, Please do not change!
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_DEVICE_INFORMATION

/// Sgm7xxx: HwType = 0\n
/// Sgm6xxx: HwType = 1
const uint8_t Config_DeviceInformation_HwType = 1;

#endif /* CONFIG_USE_CATEGORY_DEVICE_INFORMATION */

//-----------------------------------------------------------------------------
/// Main
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_MAIN

/// The time that is waited bevor the device performs a repeated reboot in case
/// of an fatal error.
const uint32_t Config_Main_WaitingTimeUntilNextReboot  = 3600; // 1h

#endif /* CONFIG_USE_CATEGORY_MAIN */

//-----------------------------------------------------------------------------
/// WELMEC Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_WELMEC

/// Set this define to true to enable the auto backup and restore feature.
#define CONFIG_WELMEC_AUTO_BACKUP_AND_RESTORE_DATA false
/// Set this define to true to enable the watchdog timer.
#define CONFIG_WELMEC_USE_WATCHDOG                 false
/// Set this define to true to perform a CRC check of the flash and the paramter memory.
#define CONFIG_WELMEC_PERFORM_MEMORY_CRC_CHECK     false
/// Set this define to true to enable the boot counter.
#define CONFIG_WELMEC_USE_BOOT_COUNTER             false

#endif /* CONFIG_USE_CATEGORY_WELMEC */

//-----------------------------------------------------------------------------
/// I2c Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_I2C

/// \brief I2c-speed type
typedef enum {
  CONFIG_I2C_SPEED_40_KHZ  = 10, ///< I2c-speed  40 kHz
  CONFIG_I2C_SPEED_65_KHZ  =  5, ///< I2c-speed  65 kHz
  CONFIG_I2C_SPEED_90_KHZ  =  3, ///< I2c-speed  90 kHz
  CONFIG_I2C_SPEED_110_KHZ =  2, ///< I2c-speed 110 kHz
  CONFIG_I2C_SPEED_140_KHZ =  1, ///< I2c-speed 140 kHz
  CONFIG_I2C_SPEED_180_KHZ =  0  ///< I2c-speed 180 kHz (max. cable length: 30cm)
} Config_I2cSpeedType;

/// \brief Configuration of I2c-speed. Use on of Config_I2cSpeedType.
const Config_I2cSpeedType Config_I2cSpeed = CONFIG_I2C_SPEED_40_KHZ;

#endif /* CONFIG_USE_CATEGORY_I2C */

//-----------------------------------------------------------------------------
/// Sgm Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_SGM

/// \brief I2c-address of the Sgm-Sensor
const uint8_t  Config_SgmAddress                             = 0x28;
/// \brief Sgm-reset-time. The time to wait to reset the sensor.
const uint32_t Config_SgmResetTimeMilliseconds               = 500;
/// \brief Sgm-startup-time. The time needed for startup of the sensor.
const uint32_t Config_SgmStartupTimeMilliseconds             = 200;
/// \brief The CRC-Polynomial used by the sensor.
///        P(x)=x^8+x^5+x^4+1 = 100110001 => 0x0131
///        The MSB of the CRC generator polynomials is omitted,
///        it is allwas 1 and will not be used.
const uint8_t  Config_SgmCrcPolynomial                       = 0x31;
/// \brief The CRC start vector used by the sensor.
const uint8_t  Config_SgmCrcStartVector                      = 0xFF;

#endif /* CONFIG_USE_CATEGORY_SGM */

//-----------------------------------------------------------------------------
/// FlowMeasurement Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_FLOW_MEAS


/// \brief The interval for polling the sensor.
const uint32_t Config_FlowMeasMeasurementPollingTimeMilliseconds  = 5;
/// \brief The number of tries used to poll the sensor.
const uint32_t Config_FlowMeasMeasurementNumberOfTriesRead        = 10;

#endif /* CONFIG_USE_CATEGORY_FLOW_MEAS */

//-----------------------------------------------------------------------------
/// Gasmeter Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_GASMETER

/// \brief The time to wait before a new flow-measurement.
const uint32_t Config_GasmeterTimeBeforeFlowMeasurementMilliseconds = 300;
/// \brief Test mode password
const uint32_t Config_GasmeterTestModePassword = 0x1494E26A;

// -- Normal Mode (default) ---------------------------------------------------
/** \name Normal Mode (default) */
/// \{
/// \brief FUllFlow measurement interval in seconds.
#define Config_GasmeterMeasurementIntervalNormalModeFM  30
/// \brief SHORTFlow measurement interval in seconds.
#define Config_GasmeterMeasurementIntervalNormalModeSM  2
/// \brief VDD recognition interval in seconds.
#define Config_GasmeterVDDRecognitionIntervalNormalMode 86400  ;
/// \brief Gas recognition interval in seconds.
#define Config_GasmeterGasRecognitionIntervalNormalMode  21600 // 6h
/// \brief Gas recognition retry interval on failer in seconds.
#define Config_GasmeterGasRecognitionRetryIntervalNormalMode 3600 // 1h
/// \brief Time until low battery symbol appears in seconds.
#define Config_GasmeterTimeUntilLowBatterySymbolNormalMode  141912000 // 4.5y
/// \brief Buckup interval in seconds.
#define Config_GasmeterBackupIntervalNormalMode 1800 // 30min
/// \}

// -- Test Mode ---------------------------------------------------------------
/** \name Test Mode */
/// \{
/// \brief Flow measurement fast interval in seconds.
const uint32_t Config_GasmeterMeasurementIntervalTestModeFM = 1;
/// \brief Gas recognition interval in seconds.
const uint32_t Config_GasmeterGasRecognitionIntervalTestMode = 20;

/// \brief VDD recognition interval in seconds.
const uint32_t Config_GasmeterVDDRecognitionIntervalTestMode = 60;

/// \brief Time until low battery symbol appears in seconds.
const uint32_t Config_GasmeterTimeUntilLowBatterySymbolTestMode = 60;
/// \brief Buckup interval in seconds.
const uint32_t Config_GasmeterBackupIntervalTestMode = 10;
/// \}

#endif /* CONFIG_USE_CATEGORY_GASMETER */

//-----------------------------------------------------------------------------
/// Utils Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_UTILS

/// \brief Defines the base address of the flash memory.
const uint8_t* Config_FlashProgramBaseAddress    = (uint8_t*)0x08000000;
/// \brief Defines the size of the flash memory.
const uint32_t Config_FlashProgramSize           = 0x10000;
/// \brief Defines the base address of the EEPROM memory.
const uint8_t* Config_EepromParameterBaseAddress = (uint8_t*)0x08080100;
/// \brief Defines the size of the EEPROM memory.
const uint16_t Config_EepromParameterSize        = 0x300;
/// \brief Defines the polynominal for the CRC32 calculation.
const uint32_t Config_UtilsCrc32Polynominal      = 0x04C11DB7;

#endif /* CONFIG_USE_CATEGORY_UTILS */

//-----------------------------------------------------------------------------
/// Datastorage Configuration
//-----------------------------------------------------------------------------
#ifdef CONFIG_USE_CATEGORY_DATASTORAGE

/// \brief Defines the polynominal for the CRC32 calculation.
const uint32_t Config_DataStorageCrc32Polynominal = 0x04C11DB7;

#endif /* CONFIG_USE_CATEGORY_DATASTORAGE */

#endif /* CONFIGURATION_H */

