//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Sgm.c
/// \author  RFU
/// \date    13-Apr-2016
/// \brief   Module for Sensirion Gasmeter Sensor
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef SGM_H
#define SGM_H

#ifdef __cplusplus
extern "C" {
#endif
  
#include "Typedef.h"
#include "Error.h"
#include "SensorTypeParameters.h"

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_SGM_CRC                   (ERROR_MODULE_SGM | 0x00)
#define ERROR_SGM_SENSOR_EEPROM_READ    (ERROR_MODULE_SGM | 0x01)
#define ERROR_SGM_UNKOWN_SGM_TYPE       (ERROR_MODULE_SGM | 0x02)
#define ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT (ERROR_MODULE_SGM | 0x03)  

//-----------------------------------------------------------------------------
/// \brief Sensor types indexes.
/// Must match with SensorTypeParameters in SensorTypeParameters.c!
typedef enum {
  SGM_TYPE_UNDEFINED = -1,
  SGM_TYPE_DEFAULT   =  0,
} Sgm_Type;

//-----------------------------------------------------------------------------
/// \brief Initialize the sensor power pin.
void Sgm_InitHardware(void);

//-----------------------------------------------------------------------------
/// \brief Restart sensor and read the sensor type.
///
/// Performs a power-cycle to reset the sensor and waits until the sensor is
/// ready for communication through I2c.
/// \return Error flags
Error Sgm_InitModule(void);

//-----------------------------------------------------------------------------
/// \brief Gets the sensor id number.
Error Sgm_GetId(uint16_t* id);

//-----------------------------------------------------------------------------
/// \brief Performs a flow measurement.
//Error Sgm_MeasFlow(uint16_t k, uint16_t* rawFlow, uint16_t* rawQfa);
Error Sgm_MeasFlow(uint16_t k, // ��� ������
					uint16_t* qLutShort, // ��������� ��� ��������� Short 
					uint16_t* qLutLong,  // ��������� ��� ��������� Long
					uint16_t* k8T8,      // R ������ ��� Sort ��������� 
					uint16_t* qFA_long,  // ������ Long
					uint16_t* qFA_short, // ������ Short
					uint16_t* Traw);       // �����������

//-----------------------------------------------------------------------------
Error Sgm_MeasFlowShort(uint16_t k8T8, uint16_t* qShort, uint16_t* qFA_Short);

//-----------------------------------------------------------------------------
/// \brief Performs a gas recognition and returns the k value.
Error Sgm_GasRecognition(uint16_t* k);


//-----------------------------------------------------------------------------
/// \brief Gets the scale factor, flow unit and offset.
Error Sgm_GetSf_Offset(uint16_t* scaleFactor, uint16_t* flowUnit, uint16_t* offset);

//-----------------------------------------------------------------------------
/// \brief Gets the product serial number from the sensor.
Error Sgm_GetProductSerialNumber(uint64_t* serialNumber);

//-----------------------------------------------------------------------------
/// \brief Gets the article code from the sensor.
Error Sgm_GetArticleCode(uint8_t* buffer);

//-----------------------------------------------------------------------------
/// \brief Gets the sensor specific parameters.
/// \return sensor parameters
SensorTypeParameters* Sgm_GetParameters(void);

#endif /* SGM_H */
