//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    FlowMeasurement.c
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Flow-Measurement
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "Time.h"
#include "FlowMeasurement.h"
#include "Sgm.h"
#include "GasMeter.h"


#define NUMBER_OF_FLOW_CORRECTION_LUTS 1

static Error ReadFlowMeasurementConstants(void);


float _InverseScaleFactor;
float _LutOffset;
float _Short2LongFactor = 1.0f;
FlowMeas_Unit _Unit = FLOWMEAS_UNIT_UNKOWN; // TODO
uint16_t qFA_Long;
uint16_t qFA_Short;


// ----------------------------------------------------------------------------
Error FlowMeas_InitModule(void)
{
  Error error = Error_None();
 
  if(Error_IsNone(error)) 
    error = ReadFlowMeasurementConstants();
  
  
  return error;
}

// ----------------------------------------------------------------------------
Error FlowMeas_MeasureFlow(float* value)
{
  Error    error = Error_None();
//  uint16_t rawFlow;
//  uint16_t rawQfa;
//  int16_t  rawFlowCorrection;
  uint16_t qLutShort;
  uint16_t qLutLong;
  uint16_t lo_temp;
  uint16_t _K8T8;
  
  error = Sgm_MeasFlow(_GasRecognitionK, &qLutShort, &qLutLong, &_GasRecognitionK_T, &qFA_Long, &qFA_Short, &_GasTemperatureTraw);
                       
/*  
    if(Error_IsNone(error)) 
    error = GetFlowCorrection(&rawFlowCorrection, 0, rawQfa);
*/  
  if(Error_IsNone(error)) 
    *value = (qFA_Long  - _LutOffset) * _InverseScaleFactor;
   
  Error_AppendIfError(&error, ERROR_FLOWMEAS_MEASURE_FLOW);
  
  return error;
}

// ----------------------------------------------------------------------------
Error FlowMeas_MeasureFlowShort(float* value) 
{
  uint16_t qLutShort;
	uint16_t qFA_Short;
	
  Error error = Sgm_MeasFlowShort(_GasRecognitionK_T, &qLutShort, &qFA_Short);
  
  if(Error_IsNone(error)) {
//		_DQF_Short = GetFlowCorrection(qFA_Short);
    float qShort = (float)(qLutShort - _LutOffset /*+ _DQVdd + _DQF_Short*/) * _InverseScaleFactor;
    *value = qShort * _Short2LongFactor;
  }
  
  Error_AppendIfError(&error, ERROR_FLOWMEAS_MEASURE_FLOW_CHECK);
  
  return error;
}
// ----------------------------------------------------------------------------
FlowMeas_Unit FlowMeas_GetFlowUnit(void)
{
  return _Unit;
}

// ----------------------------------------------------------------------------
static Error ReadFlowMeasurementConstants(void)
{
  Error error;
  
  uint16_t scaleFactor;
  uint16_t unit;
  uint16_t offset;
  
  error = Sgm_GetSf_Offset(&scaleFactor, &unit, &offset);
  
  if(Error_IsNone(error)) {
    _InverseScaleFactor = 1.0f / (float)scaleFactor;
    _LutOffset = (float)offset;
    switch(unit) {
      case 0x0048:
        _Unit = FLOWMEAS_UNIT_NORM_LITER_PER_MINUTE;
        break;
      case 0x0248:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_15_PER_MINUTE;
        break;
      case 0x0148:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_20_PER_MINUTE;
        break;
      case 0x0348:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_25_PER_MINUTE;
        break;
      case 0x005B:
        _Unit = FLOWMEAS_UNIT_NORM_LITER_PER_HOUR;
        break;
      case 0x025B:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_15_PER_HOUR;
        break;
      case 0x015B:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_20_PER_HOUR;
        break;
      case 0x035B:
        _Unit = FLOWMEAS_UNIT_STANDARD_LITER_25_PER_HOUR;
        break;
      default:
        _Unit = FLOWMEAS_UNIT_UNKOWN;
        Error_Add(&error, ERROR_FLOWMEAS_UNKOWN_FLOW_UNIT);
    }
  }

  return error;
}

/*
static void UpdateShort2LongFactor(float qShort, float qLong)
{
  if((qShort >= Config_Short2LongFactor_MinQ_For_Update) &&  
     (qLong >= Config_Short2LongFactor_MinQ_For_Update)) 
	{
    float short2LongFactor = qLong / qShort;
		float weight = 1.0;
		if(fabsf(short2LongFactor - _Short2LongFactor) >  Config_Short2LongFactor_MaxDeviation)
		{
			weight = Config_Short2LongFactor_MaxDeviation / fabs(short2LongFactor - _Short2LongFactor);
		}
		_Short2LongFactorUpdateTimeMs = Time_GetSystemUpTimeMs();
    _Short2LongFactor += (short2LongFactor - _Short2LongFactor)
                              * Config_Short2LongFactor_SmoothingFactor * weight;
			
  }
}
*/
