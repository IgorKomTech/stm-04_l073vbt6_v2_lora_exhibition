//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    GasMeter.c
/// \author  RFU
/// \date    09-Feb-2016
/// \brief   Implementing the Gasmeter-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "GasMeter.h"
#include "FlowMeasurement.h"
#include "Time.h"
#include "Io.h"
#include "Display.h"
#include "Sgm.h"
#include "Error.h"
#include "Uart.h"
#include <math.h>
#include <stdio.h>

#define CONFIG_USE_CATEGORY_GASMETER
#define CONFIG_USE_CATEGORY_WELMEC
#include "Configuration.h"

static Error PerformInitalMeasurements(void);
static void ReadInputs(void);
static void AnalyzeWhichTaskIsRequered(void);
static Error PerformFMMeasurements(void);
static Error PerformSMMeasurements(void);
static Error FlowMeasurement(void);
static Error FlowMeasurementShort(void);
static Error CalculateValues(void);
static void PerformGasRecognition(void);
static void UpdateDisplay(Error error);
static void ActivateTestMode(void);
static void ActivateNormalMode(void);

static uint32_t GetTimeSinceLastMeasurementFM(void);
static uint32_t GetTimeSinceLastMeasurementSM(void);
static uint32_t GetTimeSinceLastGasRecognition(void);
static uint32_t GetTimeSinceLastBatteryReplacement(void);
static uint32_t GetTimeSinceTestModeActivation(void);

static void CutTotalVolumeOverflow(void);
static float ZeroingTinyFlow(float flow);
static void Uint64ToString(uint64_t number, uint8_t* buffer, uint8_t* size);



//-----------------------------------------------------------------------------

/// \brief Contains the timestamp of the latest backup on startup.
static uint32_t _TimestampOnStartup;

/// \brief Measurement interval.
static uint32_t _MeasurementIntervalFM = Config_GasmeterMeasurementIntervalNormalModeFM;
static uint32_t _MeasurementIntervalSM = Config_GasmeterMeasurementIntervalNormalModeSM;
/// \brief Gas recognition interval.
static uint32_t _GasRecognitionInterval = Config_GasmeterGasRecognitionIntervalNormalMode;
/// \brief Time of last measurement.
static uint32_t _LastMeasurementTimeFM = 0;
/// \brief Time of last measurement.
static uint32_t _LastMeasurementTimeSM = 0;
/// \brief Time of last gas recognition.
static uint32_t _LastGasRecognitionTime = 0;
/// \brief Time of last battery replacement.
static uint32_t _LastBatteryReplacementTime = 0;
/// \brief Time of last switch read.
static uint32_t _LastSwitchReadTime = 0;
///
static uint32_t _TestModeActivationTime = 0;
/// \brief Duration of switch press. 
static uint32_t _SwitchPressDuration = 0;
///// \brief The current flow measured by the gasmeter.
static float _CurrentFlowNormLiterPerMinute = 0.0f;
///// \brief The current flow measured by the gasmeter after zeroing tiny flow.
static float _CurrentFlowNormLiterPerMinuteWithZeroing = 0.0f;
/// \brief Is set to true if battery is low.
static bool _IsBatteryLow = false;
///// \brief Is set to true if a new measurement is required.
static bool _IsFMMeasurementRequered = false;
static bool _IsSMMeasurementRequered = false;
static bool _IsGasRecognitionRequered = false;
/// \brief Show FirmwareVersion flag.
static bool _IsShowVersionActive = false;
/// \brief Is set to true if test mode password is set.
static bool _IsTestModePasswordSet;
/// \brief Test mode active flag => true if device is in test mode.
static bool _IsTestModeActive = false;
static bool _IsTotalVolumeBlinking = false;
static bool _IsGasRecognitionValid = false;
static bool _BlinkToggleFlag = false;
/// \brief Counts successive errors.
static uint32_t _ErrorCounter = 0;


// ����� ���������� ��� ������ 
/// \brief The current flow measurement-value.
float _FlowValue = 0.0f;
/// \brief The current flow measurement-value short.
float _ShortFlowValue = 0.0f;
/// \brief The current total volume measured by the gasmeter.
double _TotalVolume = 0.0f;
/// \brief The current gas temperature.
uint16_t _GasTemperatureTraw;
uint16_t _GasRecognitionK;
uint16_t _GasRecognitionK_T; 


//-----------------------------------------------------------------------------
Error Gasmeter_InitModule(void)
{
  Error error = Error_None();

    _TotalVolume = 0.0;
    _TimestampOnStartup = 0;
    _IsTotalVolumeBlinking = false;
  
  if(Error_IsNone(error)) 
    error = PerformInitalMeasurements();
  
  return error;
}

//-----------------------------------------------------------------------------
Error Gasmeter_Process(void)
{
  Error error = Error_None();  
  
  ReadInputs();                              // ��������� ��������� ��������� 
  AnalyzeWhichTaskIsRequered();             // ����� ������� ������
  
  if(_IsGasRecognitionRequered)               //���������� �������� � ������
     PerformGasRecognition();
    
  if(_IsFMMeasurementRequered)                // Long ���������
    error = PerformFMMeasurements();

  if(_IsSMMeasurementRequered)       // Short ���������  
    error = PerformSMMeasurements();
  
  if(Error_IsError(error)) 
  {
    _ErrorCounter++;
    UpdateDisplay(error);
  }
  else 
    _ErrorCounter = 0;
  
  
  if(_ErrorCounter >= 3)
  {
    Error_SetFatal(&error);
  }

  UpdateDisplay(error);  // �������� �������  
  
  sprintf((char*)TransmitBuffer,"%u\t%u\t%u\t%u\t%u\r\n",\
  qFA_Long,qFA_Short,_GasRecognitionK,_GasRecognitionK_T,_GasTemperatureTraw);

  Uart_Transmit();
  
  return error;
}

//-----------------------------------------------------------------------------
static Error PerformInitalMeasurements(void)
{
  Error error = Error_None();
  
  for(uint8_t i = 4; i > 0; i--) 
  {
    PerformGasRecognition();
    if(_IsGasRecognitionValid) break;
  }
  
  if(!_IsGasRecognitionValid) {
    Error_Add(&error, ERROR_GASMETER_INIT_GASREC);
  }
  
  if(Error_IsNone(error)) {
    error = PerformFMMeasurements();
  }
  
  return error;
}

//-----------------------------------------------------------------------------
static void ReadInputs(void)
{
  uint32_t systemUpTime;
  
  systemUpTime = Time_GetSystemUpTimeSecond();
  
  if(Io_IsSwitch1Closed()) {
    _SwitchPressDuration += systemUpTime - _LastSwitchReadTime;
  } else {
    _SwitchPressDuration = 0;
  }
  _LastSwitchReadTime = systemUpTime;
  
  // Is switch press duration reached to show version?
  _IsShowVersionActive = (_SwitchPressDuration >= 2);
  
  // Is switch press duration reached to activate test mode?
  if(_SwitchPressDuration >= 10) {
    ActivateTestMode();
  } 
  
  // Is Jumber1 set -> activate test mode.
  if(Io_IsSetJumper1()) 
    ActivateTestMode();
  else 
    ActivateNormalMode();
  
}

//-----------------------------------------------------------------------------
static void AnalyzeWhichTaskIsRequered(void)
{
  uint32_t gasRegInterval;
  
  gasRegInterval = _IsGasRecognitionValid ? Config_GasmeterGasRecognitionIntervalNormalMode
                                          : Config_GasmeterGasRecognitionRetryIntervalNormalMode;

  if(_IsTestModeActive) 
  {   
     _IsGasRecognitionRequered = GetTimeSinceLastGasRecognition() >= 
                                   gasRegInterval;
      _IsFMMeasurementRequered = GetTimeSinceLastMeasurementFM() >= _MeasurementIntervalFM;
      _IsSMMeasurementRequered = false;
       // GetTimeSinceLastMeasurementSM() >= _MeasurementIntervalSM;
  }
  else
  {
     _IsGasRecognitionRequered = GetTimeSinceLastGasRecognition() >= 
                                   gasRegInterval;
      _IsFMMeasurementRequered = GetTimeSinceLastMeasurementFM() >= _MeasurementIntervalFM;
      if (_IsFMMeasurementRequered)
        _IsSMMeasurementRequered = false;
      else
      _IsSMMeasurementRequered = GetTimeSinceLastMeasurementSM() >= _MeasurementIntervalSM;
  }

}  

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static Error PerformFMMeasurements(void)
{
   Error error = Error_None();
  
   Time_DelayMilliSeconds(Config_GasmeterTimeBeforeFlowMeasurementMilliseconds);
   error = FlowMeas_MeasureFlow(&_FlowValue);
  
  if(Error_IsNone(error)) {
    error = CalculateValues();
  }
  
  if(Error_IsNone(error)) {
    _LastMeasurementTimeFM = Time_GetSystemUpTimeSecond();
  }
      
  return error;
}
//------------------------------------------------------------------------------
static Error PerformSMMeasurements(void)
{
  Error error;
  
  Time_DelayMilliSeconds(Config_GasmeterTimeBeforeFlowMeasurementMilliseconds);
  error = FlowMeas_MeasureFlowShort(&_FlowValue);

  if(Error_IsNone(error))
    error = CalculateValues();   //TODO: �� 633 ����������
    
  if(Error_IsNone(error)) {
    _LastMeasurementTimeSM = Time_GetSystemUpTimeSecond();
  }
      
  return error;
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

static Error CalculateValues(void)
{
  Error error = Error_None();
  
  switch(FlowMeas_GetFlowUnit()) {
    case FLOWMEAS_UNIT_NORM_LITER_PER_MINUTE:
      _CurrentFlowNormLiterPerMinute = _FlowValue;
      break;
    case FLOWMEAS_UNIT_STANDARD_LITER_20_PER_MINUTE:
      _CurrentFlowNormLiterPerMinute = 273.15f / 293.15f * _FlowValue;
      break;
    case FLOWMEAS_UNIT_STANDARD_LITER_15_PER_MINUTE:
      _CurrentFlowNormLiterPerMinute = 273.15f / 288.15f * _FlowValue;
      break;
    case FLOWMEAS_UNIT_STANDARD_LITER_25_PER_MINUTE:
      _CurrentFlowNormLiterPerMinute = 273.15f / 298.15f * _FlowValue;
      break;
    case FLOWMEAS_UNIT_NORM_LITER_PER_HOUR:
    case FLOWMEAS_UNIT_STANDARD_LITER_20_PER_HOUR:
    case FLOWMEAS_UNIT_STANDARD_LITER_15_PER_HOUR:
    case FLOWMEAS_UNIT_STANDARD_LITER_25_PER_HOUR:
    case FLOWMEAS_UNIT_UNKOWN:
    default:
      Error_Add(&error, ERROR_GASMETER_INVALID_FLOW_UNIT);
      break;
  }
  
  _CurrentFlowNormLiterPerMinuteWithZeroing = ZeroingTinyFlow(
                                              _CurrentFlowNormLiterPerMinute);
  
  _TotalVolume += (_CurrentFlowNormLiterPerMinuteWithZeroing
                  * GetTimeSinceLastMeasurementFM())*(0.06f);
  
  CutTotalVolumeOverflow();
  
  return error;
}

//-----------------------------------------------------------------------------
static void PerformGasRecognition(void)
{
  _IsGasRecognitionValid = (Error_IsNone(Sgm_GasRecognition(&_GasRecognitionK)));
  _LastGasRecognitionTime = Time_GetSystemUpTimeSecond();
}


//-----------------------------------------------------------------------------
static void UpdateDisplay(Error error)
{
  uint8_t decimalPlaces;
  Display_ClearRam();
  Display_EnableLogo();
  
  if(_IsShowVersionActive) 
      Display_WriteFwVersion(FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_DEBUG);
   else 
   {
    if(Error_IsError(error)) 
    {
      Display_WriteDigitInRam(7, DIGIT_E);
      Display_WriteDigitInRam(8, DIGIT_CHAR_r);
      Display_WriteDigitInRam(9, DIGIT_CHAR_r);
    }
    else 
    { 
      if (_IsTestModeActive)
      {
        float lo_temp;
        lo_temp = ((float)_GasTemperatureTraw/595.0f -35.0f);
        Display_WriteCurrentFlow(lo_temp);  
        Display_WriteLine2InRam(false, 0, (uint32_t) qFA_Long,  DIGIT_CHAR_P );
      } 
      else
      {
        Display_WriteCurrentFlow(_CurrentFlowNormLiterPerMinuteWithZeroing);
        _BlinkToggleFlag = _IsTotalVolumeBlinking ? !_BlinkToggleFlag : true;
        if(_BlinkToggleFlag) 
          {
            decimalPlaces = sensorTypeParameters->decimalPlaces;
            if(_IsTestModeActive) decimalPlaces++;
            Display_WriteTotalVolume(_TotalVolume, decimalPlaces);
          }
      }
    
      if(true) // if(GasRec_IsCurrentGasAir()) // TODO
       Display_WriteSymbolInRam(SYMBOL_AIR, true);
      else 
      {
        Display_WriteSymbolInRam(SYMBOL_GAS, true);
        Display_WriteSymbolInRam(SYMBOL_NAT, true);
      }
    }
   }
  Display_WriteSymbolInRam(SYMBOL_LOWBATT, _IsBatteryLow);
  Display_WriteSymbolInRam(SYMBOL_CYCLE, _IsTestModePasswordSet);
  Display_WriteSymbolInRam(SYMBOL_DOT, _IsTestModeActive);
  Display_Update();
  
  //printf(TransmitBuffer,qFA_Long,qFA_Short,
}

//-----------------------------------------------------------------------------
static uint32_t GetTimeSinceLastMeasurementFM(void)
{
  return Time_GetSystemUpTimeSecond() - _LastMeasurementTimeFM;
}

static uint32_t GetTimeSinceLastMeasurementSM(void)
{
  return Time_GetSystemUpTimeSecond() - _LastMeasurementTimeSM;
}


//-----------------------------------------------------------------------------
static uint32_t GetTimeSinceLastGasRecognition(void)
{
  return Time_GetSystemUpTimeSecond() - _LastGasRecognitionTime;
}

// ----------------------------------------------------------------------------
static void CutTotalVolumeOverflow(void)
{
  uint8_t decimalPlaces;
  double  volumeLimit = 1.0;
  int     i;
  
  decimalPlaces = sensorTypeParameters->decimalPlaces;
  if(_IsTestModeActive) decimalPlaces++;
  
  for(i = decimalPlaces; i < 7; i++) {
    volumeLimit *= 10;
  }
  
  while(_TotalVolume >= volumeLimit) {
    _TotalVolume -= volumeLimit;
  }
}

// ----------------------------------------------------------------------------
static float ZeroingTinyFlow(float flow)
{
  SensorTypeParameters* params = Sgm_GetParameters();
  
  if((params->Q_negLimit < flow) && (flow < 0.0f)) {
    return 0.0f;
  }
  
  if((0.0f < flow) && (flow < params->Q_posLimit)) {
    return 0.0f;
  }
  
  return flow;
}

//-----------------------------------------------------------------------------
static void Uint64ToString(uint64_t number, uint8_t* buffer, uint8_t* size)
{
  uint64_t divisor = 10000000000000000000ul;
  uint8_t  digit;
  *size = 0;
  while((number < divisor) && (divisor > 0)) {
    divisor /= 10;
  }
  while(divisor) {
    digit = number / divisor;
    number -= digit * divisor;
    buffer[(*size)++] = '0' + digit;
    divisor /= 10;
  }
  if(*size == 0) {
    buffer[0] = '0';
    *size = 1;
  }
}

//-----------------------------------------------------------------------------
static Error GetProductSerialNumber(uint8_t* buffer, uint8_t* size)
{
  Error error;
  
  uint64_t serialNumber;
  error = Sgm_GetProductSerialNumber(&serialNumber);
  
  if(Error_IsNone(error)) {
    Uint64ToString(serialNumber, buffer, size);
  }

  return error;
}

//-----------------------------------------------------------------------------
static Error GetArticleCode(uint8_t* buffer, uint8_t* size)
{
  *size = 20;
  return Sgm_GetArticleCode(buffer);
}

//-----------------------------------------------------------------------------
static void ActivateTestMode(void)
{
//  if(_IsTestModePasswordSet) 
  {
    _IsTestModeActive = true;
    _TestModeActivationTime = Time_GetSystemUpTimeSecond();
    _MeasurementIntervalFM = Config_GasmeterMeasurementIntervalTestModeFM;
    _GasRecognitionInterval = Config_GasmeterGasRecognitionIntervalTestMode;
    }
}

//-----------------------------------------------------------------------------
static void ActivateNormalMode(void)
{
  _IsTestModeActive = false;
  _TestModeActivationTime = 0;
  _MeasurementIntervalFM = Config_GasmeterMeasurementIntervalNormalModeFM;
  _MeasurementIntervalSM = Config_GasmeterMeasurementIntervalNormalModeSM;
  _GasRecognitionInterval = Config_GasmeterGasRecognitionIntervalNormalMode;
}
