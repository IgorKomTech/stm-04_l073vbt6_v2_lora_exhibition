//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    SensorTypeParameters.c
/// \author  RFU
/// \date    29-Oct-2015
/// \brief   Module for sensor specific parameters
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#include "SensorTypeParameters.h"

// Indexes must match with Sgm_Type definition in Sgm.h!
SensorTypeParameters sensorTypeParameters[] = { 
//   Q_neg | Q_neg | Q_pos | Q_min |   Q_t  |  Q_max |  Q_pos |  |decimal|
//    Sat  | Limit | Limit |       |        |        |   Sat  |  |places |  Sensor
//  {   -9.0f, -0.05f,  0.05f, 0.267f,  4.167f,  41.67f,53.0f, 2     }, // Default
//  {  -15.0f, -0.08f,  0.08f, 0.417f,  6.670f,  66.67f,85.0f, 2     }, // SGM6002
    {  -24.0f, -0.13f,  0.13f, 0.667f, 10.000f, 100.00f,130.0f,2     }, // SGM6004
//  {  -39.0f, -0.20f,  0.20f, 1.000f, 16.670f, 166.67f,215.0f,2     }, // SGM6006
//  { -124.0f, -0.53f,  0.53f, 2.660f, 41.600f, 417.00f,615.0f,1     }, // SGM7016
//  { -199.0f, -0.83f,  0.83f, 4.160f, 66.660f, 667.00f,1000.0f, 1     }, // SGM7025
}; // indexes must match SensorType-enum in Sgm.h!!!
