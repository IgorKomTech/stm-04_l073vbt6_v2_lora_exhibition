//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    FlowMeasurement.h
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Flow-Measurement
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef FLOW_MEASUREMENT_H
#define FLOW_MEASUREMENT_H

#include "Typedef.h"
#include "Error.h"
#include "Sgm.h"



//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_FLOWMEAS_LUT_NUMBER_OUT_OF_RANGE (ERROR_MODULE_FLOWMEAS | 0x00)
#define ERROR_FLOWMEAS_TOO_FEW_SAMPLINGPOINTS  (ERROR_MODULE_FLOWMEAS | 0x01)
#define ERROR_FLOWMEAS_TOO_MANY_SAMPLINGPOINTS (ERROR_MODULE_FLOWMEAS | 0x02)
#define ERROR_FLOWMEAS_LUT_VALUES_INCORRECT    (ERROR_MODULE_FLOWMEAS | 0x03)
#define ERROR_FLOWMEAS_UNKOWN_FLOW_UNIT        (ERROR_MODULE_FLOWMEAS | 0x04)
#define ERROR_FLOWMEAS_SENSOR_CONFIGURATION    (ERROR_MODULE_FLOWMEAS | 0x05)
#define ERROR_FLOWMEAS_TIMEOUT                 (ERROR_MODULE_FLOWMEAS | 0x06)
#define ERROR_FLOWMEAS_MEASURE_FLOW            (ERROR_MODULE_FLOWMEAS | 0x09)
#define ERROR_FLOWMEAS_MEASURE_FLOW_CHECK      (ERROR_MODULE_FLOWMEAS | 0x0A) 

//-----------------------------------------------------------------------------
/// \brief flow unit
typedef enum {
  FLOWMEAS_UNIT_UNKOWN,
  FLOWMEAS_UNIT_NORM_LITER_PER_MINUTE,        ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_20_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_15_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_25_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_NORM_LITER_PER_HOUR,          ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_20_PER_HOUR,   ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_15_PER_HOUR,   ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_25_PER_HOUR,   ///< 
} FlowMeas_Unit;


extern uint16_t qFA_Long;
extern uint16_t qFA_Short;

// ----------------------------------------------------------------------------
/// \brief Initialize Flow-Measurement-Module.
Error FlowMeas_InitModule(void);

// ----------------------------------------------------------------------------
/// \brief Perform a flow measurement.
/// \param value       Pointer to return the flow measurment value
/// \param k           TODO
/// \return Error flags
Error FlowMeas_MeasureFlow(float* value);

Error FlowMeas_MeasureFlowShort(float* value); 

// ----------------------------------------------------------------------------
/// \brief Get the unit of the flow-measurement.
///
/// Unit is only valid for flow measurements!
/// \return The unit of the flow-measurement
FlowMeas_Unit FlowMeas_GetFlowUnit(void);

#endif /* FLOW_MEASUREMENT_H */
