//=============================================================================
//  
//=============================================================================
/// \file    I2c.c
/// \author  SUL
/// \date    09-Dec-2014
/// \brief   Module for I2c-communication
//=============================================================================
// COPYRIGHT
//=============================================================================
//=============================================================================

#include "I2c.h"
#include "Time_g.h"
#include "stm32l073xx.h"

static void EnableGpios(void);
static void InitSclLine(void);
static void InitSdaLine(void);
static int ReadSdaLine(void);
static int ReadSclLine(void);
static void SetSdaLineLow(void);
static void SetSdaLineHigh(void);
static void SetSclLineLow(void);
static void SetSclLineHigh(void);
static Error CheckDataLineHigh(void);

#define CONFIG_USE_CATEGORY_I2C
#include "Configuration.h"

// This constant is used to correctly shape I2c-signals
static const uint32_t _i2cShapeCorrection = 1;

//----------------------------------------------------------------------------
void I2c_InitHardware(void)
{
  EnableGpios();
  InitSclLine();
  InitSdaLine();
  I2c_DisableLines();
}

//----------------------------------------------------------------------------
inline void I2c_EnableLines()
{
  SetSclLineHigh();
  SetSdaLineHigh();
}

//----------------------------------------------------------------------------
inline void I2c_DisableLines()
{
  SetSclLineLow();
  SetSdaLineLow();
}

//----------------------------------------------------------------------------
Error I2c_StartCondition(void)
{
  Error error;
  
  SetSdaLineHigh();
  SetSclLineHigh();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  error = CheckDataLineHigh();
  if(Error_IsNone(error)) {
    SetSdaLineLow();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLineLow();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  }
  
  return error;
}

//----------------------------------------------------------------------------
void I2c_StopCondition(void)
{
  SetSdaLineLow();
  SetSclLineLow();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLineHigh();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSdaLineHigh();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
}

//----------------------------------------------------------------------------
Error I2c_WriteByte(uint8_t data)
{
  Error   error = Error_None();
  int     ack    = 0;
  uint8_t mask; // Shift-mask for data-bits
//while(ack == 0)  
{
  for(mask = 0x80; mask > 0; mask >>= 1) 
  {
    if(0 == (data & mask)) 
      SetSdaLineLow();
     else 
      SetSdaLineHigh();
   
    Time_DelayMicroSeconds(Config_I2cSpeed);
    SetSclLineHigh();
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLineLow();
  }
  SetSdaLineHigh();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLineHigh();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  ack = ReadSdaLine();
  if(ack == 1) {
    Error_Add(&error, ERROR_I2C_ACK);
  }
  SetSclLineLow();
  Time_DelayMicroSeconds(Config_I2cSpeed);
}
  return error;
}

//----------------------------------------------------------------------------
uint8_t I2c_ReadByte(I2c_Ack ack)
{
  uint8_t data = 0;
  uint8_t mask; // Shift-mask for data-bits

  SetSdaLineHigh();
  for (mask = 0x80; mask > 0; mask >>= 1) {
    Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
    SetSclLineHigh();
    
    while(ReadSclLine() == 0);
    
    Time_DelayMicroSeconds(Config_I2cSpeed);
    if(ReadSdaLine() == 1) {
      data = (data | mask);
    }
    SetSclLineLow();
  }
  if(ack == ACK) {
    SetSdaLineLow();
  } else {
    SetSdaLineHigh();
  }
  Time_DelayMicroSeconds(Config_I2cSpeed);
  SetSclLineHigh();
  Time_DelayMicroSeconds(Config_I2cSpeed + _i2cShapeCorrection);
  SetSclLineLow();
  SetSdaLineHigh();
  Time_DelayMicroSeconds(Config_I2cSpeed);
  
  return data;
}

//----------------------------------------------------------------------------
static void EnableGpios(void)
{
  SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPBEN);
}

//----------------------------------------------------------------------------
static void InitSclLine(void)
{
  // Scl-line is GPIO PB6
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE6, GPIO_MODER_MODE6_0);
  MODIFY_REG(GPIOB->OTYPER, GPIO_OTYPER_OT_6, GPIO_OTYPER_OT_6);
  MODIFY_REG(GPIOB->OSPEEDR, GPIO_OSPEEDER_OSPEED6,
      GPIO_OSPEEDER_OSPEED6_0 | GPIO_OSPEEDER_OSPEED6_1);
 // SET_BIT(GPIOB->PUPDR, GPIO_PUPDR_PUPD6_0);
  SET_BIT(GPIOB->ODR, GPIO_ODR_OD6);
}

//----------------------------------------------------------------------------
void InitSdaLine(void)
{
  // Scl-line is GPIO PB7
  // Configure as output with open drain, high speed, pull-up,
  // set output to 0
  MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE7, GPIO_MODER_MODE7_0);
  MODIFY_REG(GPIOB->OTYPER, GPIO_OTYPER_OT_7, GPIO_OTYPER_OT_7);
  MODIFY_REG(GPIOB->OSPEEDR, GPIO_OSPEEDER_OSPEED7,
      GPIO_OSPEEDER_OSPEED7_0 | GPIO_OSPEEDER_OSPEED7_1);
 // SET_BIT(GPIOB->PUPDR, GPIO_PUPDR_PUPD7_0);
  SET_BIT(GPIOB->ODR, GPIO_ODR_OD7);
}

//----------------------------------------------------------------------------
inline int ReadSdaLine(void)
{
  // Return bit 7 of GPIOB
  return READ_BIT(GPIOB->IDR, GPIO_IDR_ID7) ? 1 : 0;
}

//----------------------------------------------------------------------------
inline int ReadSclLine(void)
{
  // Return bit 6 of GPIOB
  return READ_BIT(GPIOB->IDR, GPIO_IDR_ID6) ? 1 : 0;
}

//----------------------------------------------------------------------------
inline void SetSdaLineLow(void)
{
  // Reset bit 7 of GPIOB
  CLEAR_BIT(GPIOB->ODR, GPIO_ODR_OD7);
}

//----------------------------------------------------------------------------
inline void SetSdaLineHigh(void)
{
  // Set bit 7 of GPIOB
  SET_BIT(GPIOB->ODR, GPIO_ODR_OD7);
}

//----------------------------------------------------------------------------
inline void SetSclLineLow(void)
{
  // Reset bit 6 of GPIOB
  CLEAR_BIT(GPIOB->ODR, GPIO_ODR_OD6);
}

//----------------------------------------------------------------------------
inline void SetSclLineHigh(void)
{
  // Set bit 6 of GPIOB
  SET_BIT(GPIOB->ODR, GPIO_ODR_OD6);
}

//----------------------------------------------------------------------------
Error CheckDataLineHigh(void)
{
  Error error = Error_None();
  
  if(1 != ReadSdaLine()) {
    Error_Add(&error, ERROR_I2C_DATA_LINE_LOW);
  }
  
  return error;
}

//----------------------------------------------------------------------------
uint8_t I2c_MakeWriteHeader(uint8_t address)
{
  return (uint8_t)((address << 1 & ~I2C_RW_MASK) | I2C_WRITE);
}

//----------------------------------------------------------------------------
uint8_t I2c_MakeReadHeader(uint8_t address)
{
  return (uint8_t)((address << 1 & ~I2C_RW_MASK) | I2C_READ);
}
