//=============================================================================
//    
//=============================================================================
/// \file    Eeprom.c
/// \author  RFU
/// \date    07-Jan-2016
/// \brief   Module for the uC internal EEPROM
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#include "Eeprom.h"
#include "stm32l0xx.h"
#include "FlowMeasurement.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

#include <typedef.h>


//static Error Eeprom_CheckAddress(uint32_t address, uint8_t size);
static void Unlock(void);
static void Lock(void);

//-----------------------------------------------------------------------------
static inline void Unlock(void)
{
  while ((FLASH->SR & FLASH_SR_BSY) != 0);
  if ((FLASH->PECR & FLASH_PECR_PELOCK) != 0)
  {
    FLASH->PEKEYR = FLASH_PEKEY1;
    FLASH->PEKEYR = FLASH_PEKEY2;
  }
  taskENTER_CRITICAL();
}

//-----------------------------------------------------------------------------
static inline void Lock(void)
{
  while ((FLASH->SR & FLASH_SR_BSY) != 0);
  FLASH->PECR |= FLASH_PECR_PELOCK;
  taskEXIT_CRITICAL();
}


//-----------------------------------------------------------------------------
int Eeprom_WriteWord(uint32_t p_address, uint16_t p_data)
{
    Unlock();
    *(uint16_t *)(p_address) = p_data;
    Lock();

  return 0;
}

//-----------------------------------------------------------------------------
int Eeprom_WriteSWord(uint32_t p_address, int p_data)
{
    Unlock();
    *(int *)(p_address) = p_data;
    Lock();
 return 0;
}

//-----------------------------------------------------------------------------
int Eeprom_WriteDword(uint32_t p_address, uint32_t p_data)
{
    Unlock();
    *(uint32_t *)(p_address) = p_data;
    Lock();
  return 0;
}

//-----------------------------------------------------------------------------
int Eeprom_WriteQword(uint32_t p_address, uint64_t p_data)
{
    Unlock();
    *(uint64_t *)(p_address) = p_data;
    Lock();
   return 0;
}

//-----------------------------------------------------------------------------
int Eeprom_WriteDouble(uint32_t p_address, double p_data)
{
   Unlock();
    *(double *)(p_address) = p_data;
    Lock();
  return 0;
}

//-----------------------------------------------------------------------------
int Eeprom_WriteFloat(uint32_t p_address, float p_data)
{
    Unlock();
    *(float *)(p_address) = p_data;
    Lock();
  return 0;
}


//-----------------------------------------------------------------------------
int Eeprom_WriteQf(uint16_t p_num,uint16_t p_min,uint16_t p_max,float p_diff, float p_tan)
{
  void* lo_RecordAdress;
   
   lo_RecordAdress = (void*)&Qf_param[p_num].num_param;
   Eeprom_WriteWord((uint32_t)lo_RecordAdress, p_num); 
   lo_RecordAdress = (void*)&Qf_param[p_num].min_Border_Qf;
   Eeprom_WriteWord((uint32_t)lo_RecordAdress, p_min); 
   lo_RecordAdress = (void*)&Qf_param[p_num].max_Border_Qf;
   Eeprom_WriteWord((uint32_t)lo_RecordAdress, p_max); 
   lo_RecordAdress = (void*)&Qf_param[p_num].offset_dQf;
   Eeprom_WriteFloat((uint32_t)lo_RecordAdress, p_diff); 
   lo_RecordAdress = (void*)&Qf_param[p_num].ctg_dQf;
   Eeprom_WriteFloat((uint32_t)lo_RecordAdress,p_tan); 
   
 return 0;     
}   

//-----------------------------------------------------------------------------
int Eeprom_WriteString(uint8_t* p_StrData, uint8_t* p_DataToCopy, uint16_t p_maxData)
{
  uint8_t* lo_RecordAdress;
  uint8_t lo_data;
  
  lo_RecordAdress = p_StrData;
  Unlock();
  for(uint16_t charCnt =0; charCnt < p_maxData; charCnt++)
  {
    lo_data = *(p_DataToCopy + charCnt);
    *lo_RecordAdress = lo_data;
    lo_RecordAdress ++;
  } 
  *lo_RecordAdress = 0;
  Lock();
 return 0;
} 


//-----------------------------------------------------------------------------
int Eeprom_WriteChar(uint32_t p_address, uint8_t p_data)
{
    Unlock();
    *(uint8_t*)(p_address) = p_data;
    Lock();
 
  return 0;
}


