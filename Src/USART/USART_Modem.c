//=============================================================================
/// \file    USART_Modem.c
/// \author  MNA
/// \date    23-Apr-2018
/// \brief   Module for SGM modem
//=============================================================================
// COPYRIGHT
//=============================================================================
#include "stm32l0xx.h"
#include "Typedef.h"
#include "time_g.h"
#include "stm32l0xx_ll_usart.h"
#include "stm32l0xx_ll_gpio.h"
#include "eeprom.h"
#include "USART_Modem.h"
#include "GSMSIM800C.h"
#include "Uart_152.h"

#include <string.h>
#include <stdio.h>
#include <ctype.h>

static LL_GPIO_InitTypeDef _port;

#define Mdm_RX_BUFFER_MAX_SIZE 512
#define Mdm_TX_BUFFER_MAX_SIZE 128

//char MdmTXBuffer[Mdm_TX_BUFFER_MAX_SIZE];
char MdmRXBuffer[Mdm_RX_BUFFER_MAX_SIZE];

// Power key
//+++ #define MCU_GSM_ON()  SET_BIT(GPIOE->BSRR,GPIO_BSRR_BR_3) 
//+++ #define MCU_GSM_OFF() SET_BIT(GPIOE->BSRR,GPIO_BSRR_BS_3) 



#define GSM_PWR_PG()   READ_BIT(GPIOE->DI, GPIO_DI_6)

#define TRANSMIT_ENABLE 0xFF
#define TRANSMIT_DISABLE 0x0


uint16_t ModemTransmitEnable = TRANSMIT_DISABLE;
uint16_t MdmReceivedCounter = 0;
uint16_t MdmFlagDataReceived = 0;

#pragma section = ".eeprom"
//const char MdmImei[25] @".eeprom";
//const char MdmSimID [25] @".eeprom";



/*
const char* mdm_cmd[]  = { "AT\r",
                           "AT+IPR=%u\n\r",
}; 

*/
uint8_t Mdm_IsRegistred(void);

//TMdm_Flags Mdm_Flags;

uint8_t GPRS_flag = 1;
//-----------------------------------------------------------------------------
void Mdm_SaweID (char* pBuffer, uint8_t* p_adress)
{
  uint16_t digit_cnt =0, start_str=0;
  
  while(isdigit(pBuffer[start_str++])==0);
  pBuffer+=(start_str-1);
  while(isdigit(pBuffer[digit_cnt++]));
  digit_cnt--;
  Eeprom_WriteString(p_adress,(uint8_t*)pBuffer,digit_cnt);
}  

//-----------------------------------------------------------------------------
void Mdm_RXBufferClear(void)
{
  memset(MdmRXBuffer,0,Mdm_RX_BUFFER_MAX_SIZE);
  MdmReceivedCounter = 0;
}

//-----------------------------------------------------------------------------
/*void Mdm_Transmit(void)
{
  uint16_t dataLen;
    
  while(ModemTransmitEnable == TRANSMIT_DISABLE);

  dataLen = strlen((char*)MdmTXBuffer);
  if(dataLen)
  {
    CLEAR_BIT(DMA1_Channel4->CCR,DMA_CCR_EN);
    SET_BIT(DMA1->IFCR,DMA_IFCR_CTCIF4);  // ������ ���� �������� ������
    DMA1_Channel4->CNDTR = dataLen; // ����� �������� � ������� ���
    SET_BIT(DMA1_Channel4->CCR ,DMA_CCR_EN);
    ModemTransmitEnable =  TRANSMIT_DISABLE;
  }  
}  
*/
void Mdm_Transmit2(uint8_t const *ptr_str)
{
  uint16_t dataLen;
  uint16_t i=0;
    
  TransmitDebugMessageOptic(ptr_str);
  LORA_Update_tick_count_message_from_LORA_msec();
  while(ModemTransmitEnable == TRANSMIT_DISABLE)
  {
    ++i;
    if(i == 0xFFF)
      ModemTransmitEnable = TRANSMIT_ENABLE; // freeze protection
  }
  
//  strcpy((char*)MdmTXBuffer, (char*)ptr_str);
    
 // dataLen = strlen((char*)MdmTXBuffer);
  dataLen = strlen((char*)ptr_str);
  if(dataLen)
  {
    CLEAR_BIT(DMA1_Channel4->CCR,DMA_CCR_EN);
    SET_BIT(DMA1->IFCR,DMA_IFCR_CTCIF4);  // ������ ���� �������� ������
    DMA1_Channel4->CNDTR = dataLen; // ����� �������� � ������� ���
    DMA1_Channel4->CMAR = (uint32_t)ptr_str;  //����� ������ �����������
    SET_BIT(DMA1_Channel4->CCR ,DMA_CCR_EN);
    ModemTransmitEnable =  TRANSMIT_DISABLE;
  }  
}  
//-----------------------------------------------------------------------------
uint8_t Mdm_WaitAnswer(char* Response,uint16_t wait_Time)
{
  uint8_t  ret =0;
  uint32_t  Delay_counter = Time_GetSystemUpTimeSecond();
  do
  {
   if ((MdmReceivedCounter) && ( MdmFlagDataReceived ))
   {
     if(strstr(MdmRXBuffer,Response))
     {
       ret = 0xFF; // Ok
       break;
     }
     if(strstr(MdmRXBuffer,"ERROR"))
     {
       ret = 0; // Error
       break;
     }
   }
  } while ((Time_GetSystemUpTimeSecond()-Delay_counter)< wait_Time);
  if(strstr(MdmRXBuffer,"RDY"))
    ret = 0xFF;
  return ret;
}

//-----------------------------------------------------------------------------
// ������� ��������� � RX ������ ������ 
uint8_t Mdm_CheckAnswer(char* Response)
{
  uint8_t  ret =0;

  if ((MdmReceivedCounter) && ( MdmFlagDataReceived ))
   {
     if(strstr(MdmRXBuffer,Response))
       ret = 0xFF; // Ok
     if(strstr(MdmRXBuffer,"ERROR"))
       ret = 0; // Error
   }
  if(strstr(MdmRXBuffer,"READY"))
    ret = 0xFF;
  return ret;
} 
//-----------------------------------------------------------------------------
/*
uint8_t Mdm_Test(uint8_t pNumSpeed)
{
  static uint8_t lo_StateMashin = 0;
  static uint32_t lo_Delay_counter; 
  static uint8_t lo_Test_counter =0;
  
   //uint8_t modem_test_cnt =0;
   uint8_t mdmAnswer = 0;
 
//   if(Mdm_Flags.Items.Mdm_TestOk)      // ����� ��������������
//    return 0; 
   
 switch (lo_StateMashin) 
 {
   case 0: 
           strcpy((char*)MdmTXBuffer,mdm_cmd[0]);  // At?
           Mdm_RXBufferClear(); 
           Mdm_Transmit();
           lo_Delay_counter = Time_GetSystemUpTimeSecond();
           lo_StateMashin = 1;  
           return 1;
   case 1: 
            mdmAnswer = Mdm_CheckAnswer("OK"); // ����� �� ��
            if (mdmAnswer)    // ����� ����������
            { 
              Mdm_RXBufferClear(); 
              sprintf((char*)MdmTXBuffer,mdm_cmd[1],mas_speed_uart[pNumSpeed]);
              Mdm_Transmit();
              lo_StateMashin = 2;  // ��������� ���  
              lo_Delay_counter = Time_GetSystemUpTimeSecond();
              return 2;
            }  
            else  
            {  // ��� ������ ��� �� ����������
             if((Time_GetSystemUpTimeSecond() - lo_Delay_counter) < 5 )  
               return 1; // ���� �������
             else
             {
               lo_Test_counter++;
               lo_StateMashin = 0;  
               if(lo_Test_counter > 3)
                 return 0xFF;           // Error?  
             }        
            }
   case 2:                    // ��������� ���      
          mdmAnswer = Mdm_CheckAnswer("OK");
          if(mdmAnswer)
            {  
              Mdm_RXBufferClear(); 
              strcpy((char*)MdmTXBuffer,"ATE0\r");;
              Mdm_Transmit();
              lo_StateMashin = 3;  // ��������� ���  
              lo_Delay_counter = Time_GetSystemUpTimeSecond();
              return 3;
            } 
           else  
            {  // ��� ������ ��� �� ����������
              if((Time_GetSystemUpTimeSecond() - lo_Delay_counter) < 5 )  
                return 2; // ���� �������
              else
                return 0xFF;           // Error?  
            } 
   case 3:                    // �������� � ������
          mdmAnswer = Mdm_CheckAnswer("OK");
          if(mdmAnswer)
            {  
              Mdm_RXBufferClear(); 
              strcpy((char*)MdmTXBuffer,"AT&W\r");
              Mdm_Transmit();
              lo_StateMashin = 4;  // ��������� ���  
              lo_Delay_counter = Time_GetSystemUpTimeSecond();
              return 4;
            } 
           else  
            {  // ��� ������ ��� �� ����������
              if((Time_GetSystemUpTimeSecond() - lo_Delay_counter) < 5 )  
                return 3; // ���� �������
              else
                return 0xFF;           // Error?  
            } 
   case 4:                  // �������� IMEI
          mdmAnswer = Mdm_CheckAnswer("OK");
          if(mdmAnswer)
            {  
              Mdm_RXBufferClear(); 
              strcpy((char*)MdmTXBuffer,"AT+CGSN\r");
              Mdm_Transmit();
              lo_StateMashin = 5;  // ��������� ���  
              lo_Delay_counter = Time_GetSystemUpTimeSecond();
              return 5;
            } 
           else  
            {  // ��� ������ ��� �� ����������
              if((Time_GetSystemUpTimeSecond() - lo_Delay_counter) < 5 )  
                return 5; // ���� �������
              else
                return 0xFF;           // Error?  
            } 
   case 5:                  // ��������� SIM ID
          mdmAnswer = Mdm_CheckAnswer("OK");  // IMEI ������� ���������
          if(mdmAnswer)
            {  
              Mdm_SaweID(MdmRXBuffer,(uint8_t*)&MdmImei);  
              Mdm_RXBufferClear(); 
              if (Mdm_Flags.Items.Mdm_SimON)  // ����� � ���������
              {
                strcpy((char*)MdmTXBuffer,"AT+CCID\r");
                Mdm_Transmit();
                lo_StateMashin = 6;  // ��������� ���  
                lo_Delay_counter = Time_GetSystemUpTimeSecond();
                return 6;
              } 
               else // ��� ��� ����� - ��������� ������������
               {
                  Mdm_RXBufferClear();
                 return 0;
               }  
            } 
           else  
            {  // ��� ������ ��� �� ����������
              if((Time_GetSystemUpTimeSecond() - lo_Delay_counter) < 5 )  
                return 5; // ���� �������
              else
                return 0xFF;           // Error?  
            } 
   case 6:                          // ��������� ������������
          mdmAnswer = Mdm_CheckAnswer("OK");  // ID ����� ������� ���������
          if(mdmAnswer)
            {  
              Mdm_SaweID(MdmRXBuffer,(uint8_t*)&MdmSimID);  
              Mdm_RXBufferClear(); 
              Mdm_Flags.Items.Mdm_SimON = 1;  // ����� � ���������
              lo_StateMashin = 7;
              return 0;
            } 
           else  
            {  // ��� ������ ��� �� ����������
              if((Time_GetSystemUpTimeSecond() - lo_Delay_counter) < 5 )  
                return 7; // ���� �������
              else
                return 0xFF;           // Error?  
            } 
 case 7:   
       Mdm_IsRegistred();
       lo_StateMashin = 0;
   }    // END SWITCH
 return 0;
 }  
*/
/*
   if(mdmAnswer)
        { // 
          Mdm_RXBufferClear();
          strcpy((char*)MdmTXBuffer,"AT+CGATT=1\r");
          Mdm_Transmit();
          mdmAnswer = Mdm_WaitAnswer("OK",10);
        }
        Mdm_IsRegistred();
       return(mdmAnswer);   
      } 
    else   
    {
      modem_test_cnt++;
      Mdm_RXBufferClear();
    } 
    
    }  
  return 0x0A;       // ����� �� �������
  
}          
*/
//-----------------------------------------------------------------------------
void  Mdm_HardwareInit(void)
{
 //+++ static uint8_t lo_StateMashin = 0;
//+++  static uint32_t lo_Delay_counter; 
 
//  if(lo_StateMashin==0)   //  
//  {  
  //  LL_USART_InitTypeDef usart;

     // Enable the peripheral clock of GPIOC
  
    if (READ_BIT(RCC->IOPENR, RCC_IOPENR_GPIODEN)==0) //���� �� ���������, ��������� ������������ ����� D
      SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIODEN);
 //+++  if (READ_BIT(RCC->IOPENR, RCC_IOPENR_GPIOEEN)==0)
 //+++     SET_BIT(RCC->IOPENR, RCC_IOPENR_GPIOEEN);

        // GPIO configuration for UART signals 
        // Select AF mode (10) on PD3,4,5,6
    // RX TX
    MODIFY_REG(GPIOD->MODER,/*GPIO_MODER_MODE3|GPIO_MODER_MODE4|*/GPIO_MODER_MODE5      // ����� ��������� ��� ����������� D5 � D6
        |GPIO_MODER_MODE6, /*GPIO_MODER_MODE3_1|GPIO_MODER_MODE4_1|*/GPIO_MODER_MODE5_1 //+++
        |GPIO_MODER_MODE6_1);
    
     //  Power_EN, PWRKY
    _port.Pin = LL_GPIO_PIN_0 | LL_GPIO_PIN_1 | LL_GPIO_PIN_7;
    _port.Mode = LL_GPIO_MODE_OUTPUT;
    _port.Speed = LL_GPIO_SPEED_FREQ_LOW; //+++ LL_GPIO_SPEED_FREQ_HIGH
    _port.OutputType =  LL_GPIO_OUTPUT_PUSHPULL;
    _port.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOD, &_port);
    
    // Power Good
    _port.Pin = LL_GPIO_PIN_2;
    _port.Mode = LL_GPIO_MODE_INPUT;
    _port.Speed = LL_GPIO_SPEED_FREQ_LOW;//+++ LL_GPIO_SPEED_FREQ_HIGH
    _port.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    _port.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOD, &_port);
  
    // Enable the peripheral clock USART2
    SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART2EN);
  /*
    LL_USART_StructInit(&usart);
    usart.BaudRate = SPEED_UART_DEFAULT; // 57600
    usart.DataWidth = LL_USART_DATAWIDTH_8B;
    usart.HardwareFlowControl = LL_USART_HWCONTROL_NONE;// LL_USART_HWCONTROL_RTS_CTS;
    usart.TransferDirection = LL_USART_DIRECTION_TX_RX;
    usart.Parity = LL_USART_PARITY_NONE;
    usart.StopBits = LL_USART_STOPBITS_1;
    LL_USART_Init(USART2, &usart);
    */
    Mdm_USARTInit(SPEED_UART_DEFAULT);
    LL_USART_Disable(USART2);
    LL_USART_ClearFlag_TC (USART2); // Clear Transmit comp. interrapt flag
    
    // Enable DMA1 clock
   //+++ if ((RCC->AHBENR & RCC_AHBENR_DMA1EN) != RCC_AHBENR_DMA1EN)
    // RCC->AHBENR |=RCC_AHBENR_DMA1EN;
    EnableDisableDMA1(DMA1_ENABLE);
    
    // USART2_DMA_config
    DMA1_Channel4->CPAR = (uint32_t) &USART2->TDR;      //����� �������� ������ �����������
 //   DMA1_Channel4->CMAR = (uint32_t)MdmTXBuffer;         //����� ������ �����������
   //����������� ����������� ��������:
   //������ �� ������, ��������� ��������� � ������
    DMA1_Channel4->CCR = DMA_CCR_DIR | DMA_CCR_MINC;
   //��������� ���������� �� ���������� ������:
   //����� 4
    DMA1_Channel4->CCR |= DMA_CCR_TCIE;
/*
//  DMA1_Channel5->CPAR = (uint32_t) &USART2->RDR;      //����� �������� ������ ���������
//   DMA1_Channel5->CMAR = (uint32_t)ModemResiveBuffer;  //����� ������ ���������
//   DMA1_Channel5->CNDTR = Mdm_RX_BUFFER_MAX_SIZE;
   //����������� ����������� ��������:
   //������ �� ������, ��������� ��������� � ������
//   DMA1_Channel5->CCR =  DMA_CCR_MINC;
   //��������� ���������� �� ���������� �������
//   DMA1_Channel4->CCR |= DMA_CCR_TCIE;
   */
   //DMA remaped: Chanel4 - UART2_TX, Chanel5 - UART2_RX  
    DMA1_CSELR->CSELR |= 0x44000U;
   
    NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
    NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn,1);

    //������������� USART2 ��� ������ ����� DMA:
    //��������� �������� USART2 ����� DMA
    LL_USART_EnableDMAReq_TX(USART2);
    //   LL_USART_EnableDMAReq_RX(USART2);

    LL_USART_EnableIT_RXNE(USART2);
   // �������� ���������� � NVIC
    NVIC_EnableIRQ(USART2_IRQn);
    NVIC_SetPriority(USART2_IRQn,2);
    LL_USART_Enable(USART2);
    
    ModemTransmitEnable = TRANSMIT_ENABLE;
 //+++   MCU_GSM_ON();
 //+++   GSM_PWR_EN();
//+++    lo_Delay_counter = Time_GetSystemUpTimeSecond();
 //+++   lo_StateMashin = 1;
  //+++  return 1;                  
  //  }   // if p_s
  /*
 if (lo_StateMashin == 1)
  {
    if ((Time_GetSystemUpTimeSecond() - lo_Delay_counter)<2)
      return 1;
    else
    {
      USART2->ICR = 0xFFFFFFFFU;
      MCU_GSM_OFF();
      lo_Delay_counter = Time_GetSystemUpTimeSecond();
      lo_StateMashin = 2;
      return 2;
    }
  }  
 if (lo_StateMashin == 2)  
 {
   if ((Time_GetSystemUpTimeSecond() - lo_Delay_counter)<2)
     return 2;
    else
    {
      MCU_GSM_ON();
      lo_Delay_counter = Time_GetSystemUpTimeSecond();
      lo_StateMashin = 3;
      return 3;
    }
  }  
  if (lo_StateMashin == 3)    
   {
     if((Mdm_CheckAnswer("+CPIN")) || (Time_GetSystemUpTimeSecond() - lo_Delay_counter) > 8) 
     {      
       if(Mdm_CheckAnswer("NOT INSERTED"))
         Mdm_Flags.Items.Mdm_SimON =0;
       ModemTransmitEnable = TRANSMIT_ENABLE;
       Mdm_Flags.Items.Mdm_PowerON = 1;
       lo_StateMashin = 0;
       return 0;           // ������������� ��!
     }
    else
      return 4;
   }
 return 0xFF; // ���������� �������� 
  */
} 

void Mdm_USARTInit(uint32_t BaudRate)
{
    LL_USART_InitTypeDef usart;
    
    LL_USART_Disable(USART2);    

    LL_USART_StructInit(&usart);
    usart.BaudRate = BaudRate; // 38400
    usart.DataWidth = LL_USART_DATAWIDTH_8B;
    usart.HardwareFlowControl = LL_USART_HWCONTROL_NONE;// LL_USART_HWCONTROL_RTS_CTS;
    usart.TransferDirection = LL_USART_DIRECTION_TX_RX;
    usart.Parity = LL_USART_PARITY_NONE;
    usart.StopBits = LL_USART_STOPBITS_1;
    LL_USART_Init(USART2, &usart);
    
    LL_USART_Enable(USART2);
}

//-----------------------------------------------------------------------------
void USART2_IRQHandler(void)
{
  uint8_t lo_RChar;

  if(USART2->ISR & USART_ISR_RXNE)
  {
      lo_RChar = USART2->RDR; // ������ ���� ����� ��������� 
      GSM_RX_IT(lo_RChar);  // �������������� ���� ��������� 
      
      /*
      if((lo_RChar) && (lo_RChar != 0x0A))
      {
        if (lo_RChar == 0x0D)
           MdmFlagDataReceived = 0xFF;
         
       MdmRXBuffer[MdmReceivedCounter++]= lo_RChar;
      } 
//   if(READ_BIT(LPUART1->CR1,LPUART1_CR1_TE)==0)
//    DataReceived = 0x0F;
      if(MdmReceivedCounter > Mdm_RX_BUFFER_MAX_SIZE)
         Mdm_RXBufferClear();
      */
  }
  if(USART2->ISR & USART_ISR_ORE) //   Overrun error
  {
     SET_BIT(USART2->CR3,USART_CR3_OVRDIS); 
     SET_BIT(USART2->ICR,USART_ICR_ORECF); 
     __no_operation();
  } 
  if(USART2->ISR & USART_ISR_CMF)  
  {
     __no_operation();
  } 
  USART2->ICR = 0xFFFFFFFFU; 
}        

//-----------------------------------------------------------------------------

void Mdm_HardwareDeInit(void)
{
 // MCU_GSM_OFF();
 //  Mdm_WaitAnswer("POWER DOWN",5);
 // GSM_PWR_DIS();
  LL_USART_Disable(USART2);
  LL_USART_DeInit(USART2);
    // Disable the peripheral clock USART2
  CLEAR_BIT(RCC->APB1ENR, RCC_APB1ENR_USART2EN);
  //Mdm_Flags.Items.Mdm_PowerON = 0;
  CLEAR_BIT(GPIOE->ODR,GPIO_ODR_OD3); 
  
  EnableDisableDMA1(DMA1_DISABLE);
} 

void Waiting_transfer_GSM(uint16_t limit)
{
  uint16_t i=0;
  while(ModemTransmitEnable == TRANSMIT_DISABLE)
  {
    ++i;
    if(i == limit)
      ModemTransmitEnable = TRANSMIT_ENABLE; // freeze protection
  }
}
//-----------------------------------------------------------------------------
/*
uint8_t Mdm_IsRegistred(void)
{
  uint8_t mdmAnswer;
  char* delim_point;
  
  if(Mdm_Flags.Items.Mdm_PowerON)
  {  
   if(Mdm_Flags.Items.Mdm_SimON) 
   {
     Mdm_RXBufferClear();
     strcpy((char*)MdmTXBuffer,"AT+CSPN?\r");
      Mdm_Transmit();
    
      mdmAnswer = Mdm_WaitAnswer("OK",2);
   if(mdmAnswer)    
   {
      Mdm_RXBufferClear();
      strcpy((char*)MdmTXBuffer,"AT+CREG?\r");
      Mdm_Transmit();
      mdmAnswer = Mdm_WaitAnswer("OK",2);
   }
   if(mdmAnswer)
   {
     delim_point = strchr(MdmRXBuffer,',');
     mdmAnswer = atoi(delim_point+1);
     if((mdmAnswer)==1 || (mdmAnswer)== 5) 
     {
       Mdm_Flags.Items.Mdm_Ready=1;
      // return 0xff;
     } 
   }
  }
 } 
  return 0;
} 
*/