//=============================================================================
//  
//=============================================================================
/// \file    OptoComand.c
/// \author 
/// \date   
/// \brief   Comand optical interface
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "Uart_152.h"
#include "Typedef.h"
#include "Time_g.h"
#include "Configuration.h"
#include "GasMeter.h"
#include "EEPROM.h"
#include "io.h"
#include "time_g.h"
#include "SensorTypeParameters.h"
#include "OptoComand.h"
#include "FlowMeasurement.h"
#include "Archiv.h"
#include "ExtFlash.h"
#include "USART_Modem.h"
#include "main_function.h"
#include "GSMSIM800C.h"
#include "System.h"
#include "Global.h"
#include "menu.h"
#include "ValveTask.h"


const TParser PFunc[] = {{(TParsFunc)PFR_DateTime,      "DATETIME"},      //+
                //   {(TParsFunc)PFR_DevInfo,           "SW_VER"},        //+  ???
                   {(TParsFunc)PFW_TestMode,            "TestMode"},      // CLB 
                   {(TParsFunc)PFW_Qdf,                 "QdF"},           // CLB 
                   {(TParsFunc)PFR_DevInfo,             "DevInfo"},       // + 
                   {(TParsFunc)PFW_SerialNum,           "DEVICE_SN"},     //CLB +
                   {(TParsFunc)PFW_MeasTest,            "START"},         //CLB
                //   {(TParsFunc)PFR_DevInfo,           "HW_VER"},        // +   ???
                //   {(TParsFunc)PFR_DevInfo,           "DevName"},       // + ????
                   {(TParsFunc)PFR_CurVol,              "Volume"},         //+
                   {(TParsFunc)PFW_GasRTime,            "N_GAS_REC_TIME"},  //CLB
                   {(TParsFunc)PFW_N_GasRTime,          "N_GAS_REC"},  //CLB
                   {(TParsFunc)PFW_GasRange,            "GAS_RANGE"},  //CLB
                   
//                   {(TParsFunc)PFW_GasSTime,          "N_GasSMesTime"},  //CLB
                   {(TParsFunc)PFW_GasLTime,            "N_GasLMesTime"},   //CLB
                   {(TParsFunc)PFW_TestRTime,         "Test_GasRecTime"}, //CLB
                   {(TParsFunc)PFW_TestLTime,         "Test_GasLMesTime"}, //CLB
//                   {(TParsFunc)PFW_ShortMeassure,     "N_ShortMeasUsed"}, //CLB
                   {(TParsFunc)PFR_ReadArc,             "ARCHIVE"},        //
                   {(TParsFunc)PFW_ServerURL2,          "SERVER_URL2"},          //+ 
                   {(TParsFunc)PFW_ServerURL3,          "SERVER_URL3"},          //+                        
                   {(TParsFunc)PFW_ServerURL1,          "SERVER_URL"},          //+
                   {(TParsFunc)PFW_APNAdress,           "APN_ADDRESS"},         //+
                   {(TParsFunc)PFW_APNLogin,            "APN_LOGIN"},           //+
                   {(TParsFunc)PFW_APNPassword,         "APN_PASSWORD"},       //+
                   {(TParsFunc)PFW_MaxFlowRate,         "Max_FlowRate"},   //CLB  +
                   {(TParsFunc)PFW_MaxTemp,             "Max_Temp"},       //CLB  +
                   {(TParsFunc)PFW_MinTemp,             "Min_Temp"},       //CLB  +
                   {(TParsFunc)PFR_GetLockState,        "LOCK STATE"},    //
                   {(TParsFunc)PFW_MdmStart,            "Modem_START"},  //
//                   {(TParsFunc)PFW_ManualK,           "MANUAL_K"},       //CLB  
                   {(TParsFunc)PFW_GasDayBorter,        "GAS_DAY"},       //  
                   {(TParsFunc)PFW_ModemModeTransfer,   "MODE_TRANSFER"},  //���,���:���,�� +
                   {(TParsFunc)PFW_ModemBalansePhohe,   "BALANCE_PHONE"},  //*100# +
                   {(TParsFunc)PFR_ArcNumRecords,       "ArcNumRecords"},                     
                   {(TParsFunc)PFW_ModemReservedInterval, "RESERVED_INT"},   
/*                 {(TParsFunc)PFW_ModemAutoSwitchMode, "AUTO_SWITCH_MODE"}, */
                   {(TParsFunc)PFW_MdmToOpto,           "Modem_LOG"},         //
                   {(TParsFunc)PFR_GroupInfo,           "GET_GROUP_INFO"},
                   {(TParsFunc)PFW_ClearArc,            "CLEAR_ARHIVE"},
                   {(TParsFunc)PFW_ClearCount,            "CLEAR_COUNT"},
                   {(TParsFunc)PFW_ClearMdmCnt,         "CLEAR_MDM_CNT"},
                   
                   {(TParsFunc)PFW_type_device,         "TYPE_DEVICE"},
                   {(TParsFunc)PFW_QposLimit,           "Q_PosLimit"},
                //   {(TParsFunc)PFW_QnegLimit,           "Q_NegLimit"},
                   {(TParsFunc)PFW_Gl_Error,            "GL_ERROR"},
                   {(TParsFunc)PFW_hw_version,          "HW_VERSION"},
                   {(TParsFunc)PFW_pressure_abs,        "PABS"},
                   {(TParsFunc)PFW_all_segment,         "ALL_SEGMENT"},
                   {(TParsFunc)PFW_valve_time,          "VALVE_TIME"},
                   {(TParsFunc)PFW_valve_check_open_time, "CHECK_OPEN_TIME"},
                   {(TParsFunc)PFW_Valve_min,           "VALVE_MIN"},
                   {(TParsFunc)PFW_Valve_set,           "VALVE_SET"},
                   {(TParsFunc)PFW_valve,               "VALVE"},      
                   {(TParsFunc)PFW_SN_SGM_E,            "SN_SGM_E"},
                   {(TParsFunc)PFW_QGL_MIN,             "QGL_MIN"},
                   {(TParsFunc)PFW_QGL_MAX,             "QGL_MAX"},       
                   {(TParsFunc)PFW_QFL_LIMIT,           "QFL_LIMIT"},            
                   {(TParsFunc)PFW_KONTRAST,            "KONTRAST"},
                   {(TParsFunc)PFW_VOLUME_INST,         "VOLUME_INST"},
                   {(TParsFunc)PFW_MOTO_INFO,           "MOTO_INFO"},    
                   {(TParsFunc)PFW_DATE_VERIFIC_NEXT,   "DATE_VERIFIC_NEXT"},                   
                   {(TParsFunc)PFW_MOTO_DATE_VERIFIC,   "DATE_VERIFIC"},  
                   {(TParsFunc)PFW_Qmax_warning,        "QMAX_WARNING"},      
                   {(TParsFunc)PFW_Qmax_min_warning,    "QMAX_MIN_WARNING"},
                   {(TParsFunc)PFW_Qmax_warning_cnt_measurement,        "QMAX_WAR_CNT_MEAS"},      
                   {(TParsFunc)PFW_Qmax_cnt_measurement,"QMAX_CNT_MEAS"},     
                   {(TParsFunc)PFW_DQF_END,             "DQF_CRC"},  
                   {(TParsFunc)PFW_cnt_measurement_temper_eeprom, "CNT_MEAS_TEMP"},
                   {(TParsFunc)PFW_Tmax_warning,        "TMAX_WARNING"},
                   {(TParsFunc)PFW_Tmin_warning,        "TMIN_WARNING"}, 
                   {(TParsFunc)PFW_cnt_measurement_temper_warning_eeprom,        "T_WARN_CNT"}, 
                   {(TParsFunc)PFW_cnt_reverse_flow_eeprom,        "REV_FL_CNT"},
                   {(TParsFunc)PFW_Qfl_reverse_flow,    "REV_FL"},               
                   {(TParsFunc)PFW_serial_number_board, "NUMBER_BOARD"},     
                   {(TParsFunc)PFW_cnt_error_SGM_eeprom, "CNT_ERR_SGM"},   
                   {(TParsFunc)PFW_status,              "STATUS"},
                   {(TParsFunc)PFW_LORA_power,          "LORA_power"},
                   {(TParsFunc)PFW_LORA_reconnect,      "LORA_reconnect"},
                   {(TParsFunc)PFR_LORA_control,        "LORA_control"},
                   
                   {(TParsFunc)PFW_Self_GasBorder,      "SELF_BORDER"},
                   {(TParsFunc)PFW_Set_KalmanFilter,    "EN_KALMAN_FILTER"},
                   {(TParsFunc)PFW_KalmanK,             "KALMAN_K"}, 
                   {(TParsFunc)PFW_KalmanBorder,        "KALMAN_BORDER"},
                   {(TParsFunc)PFW_KalmanCounter,       "KALMAN_COUNTER"}
                   
};

const uint16_t MAX_FUNC = sizeof(PFunc)/sizeof(PFunc[0]);

uint32_t tick_count_message_from_OPTIC_msec = 0;

extern xQueueHandle xARCQueue;
extern SemaphoreHandle_t xOptoArcRecReadySemaphore;
extern struct _Mode_Transfer Mode_Transfer;
//extern uint64_t timeUnixGSMNextConnect; 
extern uint64_t timeSystemUnix;
extern uint32_t number_communication_gsm_fail;

extern TypeStatus StatusDevice_old_connect_TCP;
extern uint8_t cnt_seans_TCP_mode_2;
//uint8_t R_ARC_Buf[ChangeArcTypeDefSIZE];

extern uint32_t current_communication_gsm_number;
extern struct _GSMStatus GSMStatus;
extern uint8_t flag_opto_reconnect;

extern char num;
extern char BufferPause[128][64];
extern char count_ExtremalPowerDown;

const char* lo_CRC_OK = "CRC OK";
const char* lo_CRC_ERR = "CRC ERROR";


//------------------------------------------------------------------------------
uint8_t Str2DataTime(LL_RTC_DateTypeDef* p_Date,LL_RTC_TimeTypeDef* p_Time, char*parString)
{
  char* index;
  char delimiter = '.';
  uint8_t temp_value;
  uint8_t ret=0;
   
    // ������ ���
    index = parString;
    uint8_t i=0;
    while((ret==0) && (i<6))
    {
       switch (i)
       {
        case 0:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 32)
             p_Date->Day = temp_value;
           else 
             ret = 2;
           break;
        case 1:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 13)
             p_Date->Month = temp_value;
           else 
             ret = 2;
           break;
        case 2:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 99)
             p_Date->Year = temp_value;
           else 
             ret = 2;
           delimiter = ',';
           break;
        case 3:
           temp_value = atoi(index);
           if (temp_value < 25)
           {
             p_Time->Hours = temp_value;
              delimiter = ':';
           }  
           else 
             ret = 0x2;
           break;
        case 4:
           temp_value = atoi(index);
           if (temp_value < 60)
             p_Time->Minutes = temp_value;
           else 
             ret = 0x2;
           break;
        case 5:
           temp_value = atoi(index);
           if (temp_value < 60)
             p_Time->Seconds = temp_value;
           else 
             ret = 0x2;
           break;
       }           
     index = strchr(index,delimiter);
     index++;
     i++;
    }
  return ret;
}



//-----------------------------------------------------------------------------
uint8_t PFR_DateTime(char* parString)
{
  LL_RTC_DateTypeDef loDate, lo_ArcDate;
  LL_RTC_TimeTypeDef loTime, lo_ArcTime;
  uint8_t ret =0;
  
 if(parString ==0)
  {
    GetDate_Time(&loDate,&loTime);
    sprintf((char*)TransmitBuffer,"DATETIME =%0.2u.%0.2u.%0.2u,%0.2u:%0.2u:%0.2u\r\n",\
      loDate.Day,loDate.Month,loDate.Year,\
      loTime.Hours,loTime.Minutes,loTime.Seconds);
    Opto_Uart_Transmit(); 
    return(0); 
  }
 else
 {
    ret = Str2DataTime(&loDate,&loTime,parString);
    if(ret == 0)
    {
       GetDate_Time(&lo_ArcDate,&lo_ArcTime);
       sprintf((char*)OldValue,"%02d.%02d.%02d,%02d:%02d:%02d",
                                      lo_ArcDate.Day, lo_ArcDate.Month, lo_ArcDate.Year, 
                                      lo_ArcTime.Hours, lo_ArcTime.Minutes, lo_ArcTime.Seconds);  
       SetDate_Time(&loDate,&loTime);
       sprintf((char*)NewValue,"%02d.%02d.%02d,%02d:%02d:%02d",
                                      loDate.Day, loDate.Month, loDate.Year, 
                                      loTime.Hours, loTime.Minutes, loTime.Seconds);  
       WriteChangeArcRecord(CHANGE_DATE_TIME, CHANGE_OPTIC);

       timeSystemUnix = Get_IndexTime(loDate,loTime);
       time_record_Volume = timeSystemUnix;
       timeGSMUnixStart = timeSystemUnix;
       timeUnixGSMNextConnect = timeSystemUnix + 3600; // do not interfere with shutdown
       successful_transfer_data_to_server = _OK;
      return 0;
    }  
   else
     return ret;
 } 
}

//-----------------------------------------------------------------------------
uint8_t PFR_SWVers(char* parString)
{
 if(parString ==0)
  {
    sprintf((char*)TransmitBuffer,"SW_VER=%0.2u.%0.2u\r\n",\
       Device_STM.FW_major,Device_STM.FW_minor);
    Opto_Uart_Transmit();
   return 0; 
  }
  else
    return 4;
} 

//-----------------------------------------------------------------------------
uint8_t PFR_DevInfo(char* parString)
{
  uint8_t temp_major, temp_minor;
  temp_major = hw_version / 100;
  temp_minor = hw_version % 100;
 if(parString ==0)
  {
      char type_smt[4]; // 25K
      if(valve_presence)
         sprintf((char*)type_smt,"%dK", type_device_int);
      else
         sprintf((char*)type_smt,"%d", type_device_int);
              
    sprintf((char*)TransmitBuffer,
            "\r\nDevName= SMT Smart-G%s\r\nDEVICE_SN= %s\r\nSW_VER= %0.2d.%0.2d%0.2d%0.2d\r\nHW_VER= %0.2d.%0.2d",
            type_smt, Device_STM.SN_Device,  
            Device_STM.FW_major,Device_STM.FW_minor,FW_GSM_VERSION,FW_TEST_VERSION,
            /*Device_STM.HW_major,Device_STM.HW_minor*/temp_major, temp_minor);
    Opto_Uart_Transmit();          
//   while(TransmitEnable == TRANSMIT_DISABLE); 
   if(while_TransmitEnable() == _FALSE) return 1;
   sprintf((char*)TransmitBuffer,
           "\r\nCHK_SUMM= 6314\r\nArticleCode= %s\r\nSGM_SN= %s\r\nSGM_SF = %d\r\nSGM_LUT = %d\r\n",
            Sensor_Const.Article,
            Sensor_Const.SN_SGM,  
             Device_STM.SF,Device_STM.Lut);
    Opto_Uart_Transmit();
  return 0; 
  }
  else
    return 4;
}

//-----------------------------------------------------------------------------
uint8_t PFW_SerialNum(char* parString)
{
  if(parString ==0)
  {
      sprintf((char*)TransmitBuffer,"DEVICE_SN= %s\r\n", Device_STM.SN_Device);
      Opto_Uart_Transmit();
      return 0;
  }
  else
  {
    uint16_t len_buf =0;
    if (CLB_lock != 0x55)
      return 3;

    len_buf = strlen(parString)-2;
    if((len_buf > 20) || (len_buf == 0))  //GasMeter.h  TDevice
      return 2;
    else
    {
      for(uint8_t i=0; i< len_buf;i++)
        if(isdigit(parString[i]))
           ;
         else
          return 0xFF;
         
         sprintf((char*)OldValue, "%s", Device_STM.SN_Device);  

         Eeprom_WriteString((uint8_t*)Device_STM.SN_Device,(uint8_t*)parString,len_buf); 
       
         sprintf((char*)NewValue, "%s", Device_STM.SN_Device); 
         WriteChangeArcRecord(CHANGE_SERIAL_NUMBER, CHANGE_OPTIC);        
    }
  }           
 return 0;         
}
//-----------------------------------------------------------------------------
uint8_t PFW_TestMode(char* parString)
{
  uint8_t loTestMode;
  
  if (CLB_lock != 0x55)
    return 3;
  loTestMode = atoi(parString);
  if (loTestMode < 6)
  {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"\r\nTestMode=%0.1u\r\n",loTestMode); 
     Opto_Uart_Transmit();
    // while(TransmitEnable == TRANSMIT_DISABLE);  
     if(while_TransmitEnable() == _FALSE) return 1;
 //    SetUsartTE();
     switch(loTestMode)
          {
            case 0: ActivateTestMode(TestModeOFF);
                    break;
            case 1: ActivateTestMode(TestMode_1);
                   break;
            case 2: ActivateTestMode(TestMode_2);
                    break;
//            case 4: ActivateTestMode(TestMode_4);
//                    break;
            case 5: ActivateTestMode(TestMode_5);
                    break;                    
           default: ActivateTestMode(TestModeOFF);         
             return 0xFF;         
           }
     return 0; 
  }
  else 
    return 2;  
} 

//-----------------------------------------------------------------------------
 static void OutDQFToOptik(uint8_t value)
 {
   //  while(TransmitEnable == TRANSMIT_DISABLE);
      if(while_TransmitEnable() == _FALSE) return;
      sprintf((char*)TransmitBuffer,"%0.2u\t%0.5u\t%0.5u\t%0.2f\t%0.6f\r\n",\
        Qf_param[value].num_param,Qf_param[value].min_Border_Qf,Qf_param[value].max_Border_Qf,\
        Qf_param[value].offset_dQf,Qf_param[value].ctg_dQf);
      Opto_Uart_Transmit();
 }
//-----------------------------------------------------------------------------
uint8_t PFW_Qdf(char* parString)
{
  uint16_t ret=OPTIC_OK, str_len=0,paramIndex =0;
  char* index = parString;
  char* newData; 

  if (CLB_lock != 0x55)
    return OPTIC_ACCESS_DENIED;
  
  if(parString==0)
  {
    newData = strchr((char*)ReceivedBuffer,'[');  
    if (newData !=NULL)
    {
      newData++;
      paramIndex = atoi(newData);
      if(paramIndex >= Qf_count)
        return OPTIC_INCORRECT_VALUE;
      else
      {
        OutDQFToOptik(paramIndex);
        return OPTIC_OK;
      } 
    }
   for (uint8_t i=0; i< Qf_count; i++)
     OutDQFToOptik(i);
   return OPTIC_OK;
   }
  else
   {
    uint16_t lo_num,lo_min,lo_max;
    float lo_diff;
    float lo_tan;
    uint8_t i=0;    
    char delimiter = ';';
    
    str_len = strlen(parString);
    if (str_len <20) 
      return OPTIC_INCORRECT_VALUE;
 
    while((ret==OPTIC_OK) && (i<5))
    {
       switch (i)
       {
        case 0:
           lo_num = atoi(index);
           if ((lo_num)> Qf_count )
             ret = OPTIC_INCORRECT_VALUE;
           break;           
        case 1:
           lo_min = atoi(index);
           if(lo_num > 0)
             if (lo_min < Qf_param[lo_num-1].max_Border_Qf)
              ret = OPTIC_INCORRECT_VALUE;
           break;
        case 2:
           lo_max = atoi(index);
           if (lo_max < lo_min)
             ret = OPTIC_INCORRECT_VALUE;
           break;
        case 3:
            lo_diff = atof(index);
            break;
        case 4:
           lo_tan = atof(index);
           break;
        default:
           ret= OPTIC_INCORRECT_VALUE;
        }           
     index = strchr(index,delimiter);
     index++;
     i++;
    }
    if(ret == OPTIC_OK)
    {
        sprintf((char*)OldValue,"%d;%d;%d;%f;%f", lo_num, lo_min, lo_max, lo_diff, lo_tan);  

        Eeprom_WriteQf(lo_num,lo_min,lo_max,lo_diff,lo_tan);
        
        sprintf((char*)NewValue,"%d;%d;%d;%f;%f", lo_num, lo_min, lo_max, lo_diff, lo_tan); 
        WriteChangeArcRecord(CHANGE__dQF, CHANGE_OPTIC);   
        
        if(lo_num == 0)
        {
           sprintf((char*)OldValue,"%s", EEPROM_SN_SGM);  

           Eeprom_WriteString((uint8_t*)EEPROM_SN_SGM, Sensor_Const.SN_SGM, strlen((char*)Sensor_Const.SN_SGM));
        
           sprintf((char*)NewValue,"%s", EEPROM_SN_SGM); 
           WriteChangeArcRecord(CHANGE_SERIAL_NUMBER_SGM, CHANGE_TCP);  
        }
    }
  } // if-else (*parString==0)
 return ret; 
} 

//-----------------------------------------------------------------------------
uint8_t PFW_MeasTest(char* parString)
{
  //if (CLB_lock != 0x55)
   //  return 3;

  if(parString ==0)   
   {
    // while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"STOP: %0.2u sek.\t%0.6f\t%0.6f\t%0.4f\t%u\r\n",\
             TestInterval, TEST_VE_L, TEST_VE_Lm3,Test_QF_midle,TEST_QF_Count );  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
       uint16_t lo_TestInterval;
       
       lo_TestInterval = atoi(parString);
       if (lo_TestInterval % Config_GasmeterMeasurementIntervalTestModeFM)
         return 2;  
       if ((lo_TestInterval > 0) && (lo_TestInterval <3600))
       {
         TestInterval=lo_TestInterval;  
         ActivateTestMode(TestMode_3);
        return 0;
       }
       return 2;
   }         
}

//-----------------------------------------------------------------------------
uint8_t PFR_HWVers(char* parString)
{
  if(parString ==0)   
    {
      //while(TransmitEnable == TRANSMIT_DISABLE);
      if(while_TransmitEnable() == _FALSE) return 1;
      sprintf((char*)TransmitBuffer,"HW_VER=%0.2u.%0.2u\r\n",\
                Device_STM.HW_major,Device_STM.HW_minor);
      Opto_Uart_Transmit();
      return 0; 
    }
  else
   return 4;
 }

//-----------------------------------------------------------------------------
uint8_t PFR_DevName(char* parString)
{
  if(parString ==0)   
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"DevName=SMT Smart-G%d\r\n", type_device_int);
     Opto_Uart_Transmit();
     return 0; 
   }
  else
     return 4;
}

//-----------------------------------------------------------------------------
uint8_t PFR_CurVol(char* parString)
{
  if(parString ==0)   
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"Volume= %0.4f m3\r\n",\
       GasMeter.VE_Lm3);
     Opto_Uart_Transmit();
     return 0; 
   }
  else
     return 4;
}

//-----------------------------------------------------------------------------
uint8_t PFW_GasRTime(char* parString)
{
   if(parString ==0)   
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"N_GAS_REC_TIME=%d;%d;%d", 
                                            Config_GasmeterGasRecognitionIntervalNormalMode,
                                            Config_GasmeterGasRecognitionIntervalMeasureMode,
                                            Config_GasmeterGasRecognitionIntervalErrorMode);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
      if(change_GasRecognitionInterval(parString, CHANGE_OPTIC) == _OK)
        return OPTIC_OK;
      else
        return OPTIC_INCORRECT_VALUE;
   }       
}

uint8_t change_GasRecognitionInterval(char* parString, uint8_t flag_source)
{
  uint32_t temp1=65535, temp2=65535, temp3=65535;

     sscanf(parString, "%d;%d;%d", &temp1, &temp2, &temp3);
     

     if ((temp1 > 0) && (temp1 < 43200)
         && (temp2 > 0) && (temp2 < 3500)
         && (temp3 > 0) && (temp3 < 43200))   // ���� 1 ��� � �����
     {
        sprintf((char*)OldValue,"%d;%d;%d", Config_GasmeterGasRecognitionIntervalNormalMode,
                                            Config_GasmeterGasRecognitionIntervalMeasureMode,
                                            Config_GasmeterGasRecognitionIntervalErrorMode);  

        Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalNormalMode, temp1);
        Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalMeasureMode, temp2);
        Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalErrorMode, temp3);
        
        sprintf((char*)NewValue, "%d;%d;%d", Config_GasmeterGasRecognitionIntervalNormalMode,
                                            Config_GasmeterGasRecognitionIntervalMeasureMode,
                                            Config_GasmeterGasRecognitionIntervalErrorMode);  
        WriteChangeArcRecord(CHANGE_REC_TIME, flag_source);   
       
       return _OK;
     }
    return _FALSE;
}

//-----------------------------------------------------------------------------
uint8_t PFW_GasRange(char* parString)
{
  if(parString ==0)   
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"GAS_RANGE=%d;%d;%d", 
                                            Kfactor_min,
                                            Kfactor_average,
                                            Kfactor_max);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
      if(change_Kfactor_range(parString, CHANGE_OPTIC) == _OK)
        return OPTIC_OK;
      else
        return OPTIC_INCORRECT_VALUE;
   }       
}

uint8_t change_Kfactor_range(char* parString, uint8_t flag_source)
{
     uint32_t temp1=65535, temp2=65535, temp3=65535;

     sscanf(parString, "%d;%d;%d", &temp1, &temp2, &temp3);
     

     if (temp1 < temp2 
         && temp2 < temp3
         && temp3 < 40961)   // ���� 1 ��� � �����
     {
        sprintf((char*)OldValue,"%d;%d;%d", 
                                            Kfactor_min,
                                            Kfactor_average,
                                            Kfactor_max); 

        Eeprom_WriteWord((uint32_t)&Kfactor_min, temp1);
        Eeprom_WriteWord((uint32_t)&Kfactor_average, temp2);
        Eeprom_WriteWord((uint32_t)&Kfactor_max, temp3);
        
        sprintf((char*)NewValue, "%d;%d;%d", 
                                            Kfactor_min,
                                            Kfactor_average,
                                            Kfactor_max); 
        WriteChangeArcRecord(CHANGE_GAS_RANGE, flag_source);   
       
       return _OK;
     }
    return _FALSE;   
}
//-----------------------------------------------------------------------------
uint8_t PFW_N_GasRTime(char* parString)
{
   if(parString ==0)   
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"N_GAS_REC=%d", Kfactor_measure_cnt_eeprom);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
      if(change_Kfactor_measure_cnt_eeprom(parString, CHANGE_OPTIC) == _OK)
        return OPTIC_OK;
      else
        return OPTIC_INCORRECT_VALUE;
   }       
}
              
uint8_t change_Kfactor_measure_cnt_eeprom(char* parString, uint8_t flag_source)
{
     uint8_t temp;

     temp = atoi(parString);

     if(temp)   // ���� 1 ��� � �����
     {
        sprintf((char*)OldValue,"%d", Kfactor_measure_cnt_eeprom);   
        Eeprom_WriteChar((uint32_t)&Kfactor_measure_cnt_eeprom, temp);
        sprintf((char*)NewValue, "%d", Kfactor_measure_cnt_eeprom);   
        
        WriteChangeArcRecord(CHANGE_GAS_CNT_ERROR, CHANGE_OPTIC);   
       
       return _OK;
     }
     return _FALSE;  
}
//-----------------------------------------------------------------------------
/*
uint8_t PFW_GasSTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"GasShortMeasurementInterval in Normal mode= %u \r\n",\
             Config_GasmeterMeasurementIntervalNormalModeSM);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;

     if (CLB_lock != 0x55)
       return 3;

     lo_TestInterval = atoi(parString);
     if ((lo_TestInterval > Config_GasmeterMeasurementIntervalNormalModeFM)|| \
         (Config_GasmeterMeasurementIntervalNormalModeFM % lo_TestInterval !=0))
       return 2; 
     if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // ���� 1 ��� � �����
     {
       Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalNormalModeSM,lo_TestInterval);
       return 0;
     }
   }
  return 1;
}
*/
//-----------------------------------------------------------------------------
uint8_t PFW_GasLTime(char* parString)
{
  if(parString ==0)   
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"GasLongMeasurementInterval in Normal mode= %u \r\n",\
             Config_GasmeterMeasurementIntervalNormalModeFM);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;
 
     if (CLB_lock != 0x55)
        return 3;

       lo_TestInterval = atoi(parString);
      if(!NoShortMeassure) 
      {
        if ((lo_TestInterval < Config_GasmeterMeasurementIntervalNormalModeSM)|| \
           (lo_TestInterval % Config_GasmeterMeasurementIntervalNormalModeSM !=0))
         return 2; 
      }
       if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // ���� 1 ��� � �����
       {
        sprintf((char*)OldValue,"%d", Config_GasmeterMeasurementIntervalNormalModeFM);  

        Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalNormalModeFM,lo_TestInterval);
        
        sprintf((char*)NewValue, "%d", Config_GasmeterMeasurementIntervalNormalModeFM); 
        WriteChangeArcRecord(CHANGE_L_MES_TIME, CHANGE_OPTIC);  

         return 0;
       }
    return 1;
   }         
}

//-----------------------------------------------------------------------------

uint8_t PFW_TestLTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"Test_GasLMesTime=%u \r\n",\
             Config_GasmeterMeasurementIntervalTestModeFM);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
   }
   else
   {
     uint16_t lo_TestInterval;
     if (CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;

     lo_TestInterval = atoi(parString);
     if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // �� ����� 2 ��� � �����
     {
        sprintf((char*)OldValue, "%d", Config_GasmeterMeasurementIntervalTestModeFM);
        Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalTestModeFM, lo_TestInterval);
        sprintf((char*)NewValue, "%d", Config_GasmeterMeasurementIntervalTestModeFM);
        WriteChangeArcRecord(CHANGE_TEST_L_MES_TIME, CHANGE_OPTIC);
        return OPTIC_OK;
     }
    return OPTIC_ERROR;
   }         
}

//-----------------------------------------------------------------------------

uint8_t PFW_TestRTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"Test_GasRecTime=%u \r\n",\
             Config_GasmeterGasRecognitionIntervalTestMode);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;
     if (CLB_lock != 0x55)
       return 3;

     lo_TestInterval = atoi(parString);
     if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // ���� 1 ��� � �����
     {
        sprintf((char*)OldValue, "%d", Config_GasmeterGasRecognitionIntervalTestMode);
        Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalTestMode, lo_TestInterval);
        sprintf((char*)NewValue, "%d", Config_GasmeterGasRecognitionIntervalTestMode);
        WriteChangeArcRecord(CHANGE_TEST_REC_TIME, CHANGE_OPTIC);
       return 0;
     }
    return 1;
   }         
}

//-----------------------------------------------------------------------------
/*
uint8_t PFW_ShortMeassure(char* parString)
{
  const char* ans_YES = "YES";
  const char* ans_NO =  "NO";
  
  char* ansver = (char*)ans_YES;
  
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     if(NoShortMeassure)
       ansver = (char*)ans_NO;
      sprintf((char*)TransmitBuffer,"Used ShortMeassure in NormalMode= %s \r\n",\
             ansver);  
     Opto_Uart_Transmit(); 
     return 0;
   }
  else
  {
    uint16_t lo_NoShortMeassure;
    if (CLB_lock != 0x55)
      return 3;

    lo_NoShortMeassure = atoi(parString);
    if(lo_NoShortMeassure == 0)   // �� ������������ �������� ���������
       lo_NoShortMeassure = 0xFF;
    else
       lo_NoShortMeassure = 0;
    
      Eeprom_WriteWord((uint32_t)&NoShortMeassure,lo_NoShortMeassure);
     return 0;   
  }  
}
*/
//------------------------------------------------------------------------------
void Out_ArcRec(TArchiv_type arc_type)
{
  /*
  char* lo_CRC;
  uint32_t lo_ArcCRC;
  IntArcTypeDef* lo_IntArc = (IntArcTypeDef*) R_ARC_Buf;
  EventArcTypeDef* lo_EvArc = (EventArcTypeDef*)R_ARC_Buf;
  ChangeArcTypeDef* lo_ChArc = (ChangeArcTypeDef*)R_ARC_Buf;

  if(while_TransmitEnable() == _FALSE) return;
  switch (arc_type) {
  case ARC_INT:
  case ARC_DAY:
                  
                  lo_ArcCRC = CalcCRC((uint8_t*)lo_IntArc,IntArcLen - 8); 
                  if (lo_ArcCRC == lo_IntArc->ArcCRC)
                     lo_CRC=(char*)lo_CRC_OK;  
                  else
                     lo_CRC=(char*)lo_CRC_ERR;
                 sprintf((char*)TransmitBuffer,"%02u;%02u-%02u-%02u;%02u:%02u:%02u;%0.2f;%0.4f;%u;%d; %s \r\n", \
                 lo_IntArc->ArcRecNo,lo_IntArc->ArcDay,lo_IntArc->ArcMon,lo_IntArc->ArcYear,\
                 lo_IntArc->ArcHour,lo_IntArc->ArcMinute,lo_IntArc->ArcSecond,\
                 lo_IntArc->ArcTemp, lo_IntArc->ArcVolume,lo_IntArc->ArcK,lo_IntArc->ArcFlag, lo_CRC);
                 break;
  case ARC_EVENT:            
                  lo_ArcCRC = CalcCRC((uint8_t*)lo_EvArc,EventArcLen-8);                   
                  if (lo_ArcCRC == lo_EvArc->ArcCRC)
                     lo_CRC=(char*)lo_CRC_OK;  
                  else
                     lo_CRC=(char*)lo_CRC_ERR;
                  sprintf((char*)TransmitBuffer,"%02u; %02u-%02u-%02u;%02u:%02u:%02u; %02u-%02u-%02u;%02u:%02u:%02u; 0x%04X;%d;%u;0x%04X; %s\r\n", 
                  lo_EvArc->ArcRecNo,
                  lo_EvArc->TimeBegine.Day, lo_EvArc->TimeBegine.Mon, lo_EvArc->TimeBegine.Year,
                  lo_EvArc->TimeBegine.Hour,lo_EvArc->TimeBegine.Minute,lo_EvArc->TimeBegine.Second,
                  lo_EvArc->TimeEnd.Day, lo_EvArc->TimeEnd.Mon, lo_EvArc->TimeEnd.Year,
                  lo_EvArc->TimeEnd.Hour,lo_EvArc->TimeEnd.Minute,lo_EvArc->TimeEnd.Second,                  
                  lo_EvArc->Event_Code, lo_EvArc->Event_Result,lo_EvArc->Sesion_Number,lo_EvArc->Status, lo_CRC);
                break;
  case ARC_CHANG:         
                  lo_ArcCRC = CalcCRC((uint8_t*)lo_ChArc, ChangeArcLen - (sizeof(lo_ChArc->ArcCRC) + sizeof(lo_ChArc->rez)));                   
                  if (lo_ArcCRC == lo_ChArc->ArcCRC)
                     lo_CRC=(char*)lo_CRC_OK;  
                  else
                     lo_CRC=(char*)lo_CRC_ERR;
                  sprintf((char*)TransmitBuffer,"%d; %02u-%02u-%02u;%02u:%02u:%02u; %d;%d;%d;%s;%s; %s\r\n", 
                          lo_ChArc->ArcRecNo,
                          lo_ChArc->Time.Day, lo_ChArc->Time.Mon, lo_ChArc->Time.Year,
                          lo_ChArc->Time.Hour, lo_ChArc->Time.Minute, lo_ChArc->Time.Second,
                          lo_ChArc->ArcCLB_Lock,  
                          lo_ChArc->ID_Change,
                          lo_ChArc->ID_Param,
                          lo_ChArc->OldValue,
                          lo_ChArc->NewValue,

                          lo_CRC);
       break;
     }
   Opto_Uart_Transmit(); 
  */
}  

//-----------------------------------------------------------------------------
uint8_t ReadArcRecord(char* parString, TArchiv_type arc_type)
{
return 0;
}  
//-----------------------------------------------------------------------------
uint8_t PFR_ReadArc(char* parString)
{
  _ResultReply resultReplyOptic;
  if(GSM_Get_flag_power() == GSM_MUST_DISABLE ) 
  {
     cnt_str_transmit = 0;
     while(1)
     {
        if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
        
       // if(GSM_Get_flag_power() == GSM_MUST_ENABLE) 
       //    return OPTIC_ACCESS_DENIED;
       while(GSM_Get_flag_power() == GSM_MUST_ENABLE) 
       {
          sprintf((char*)TransmitBuffer, "%s", "GSM power on. Please wait\r\n");
          Opto_Uart_Transmit();
          osDelay(5000);
       }
        
        resultReplyOptic = TransmitARCHIVE((uint8_t*)parString, (uint8_t*)TransmitBuffer, TX_BUFFER_MAX_SIZE);
        Opto_Uart_Transmit(); 
        if(resultReplyOptic.end_message == _OK && resultReplyOptic.end_part_message == _OK)
           return OPTIC_OK;
     }
  }
  else
    return OPTIC_ACCESS_DENIED;
}

//-----------------------------------------------------------------------------
uint8_t PFW_ServerURL(char* parString, uint8_t number)
{
// char* lo_parString;
// uint16_t  lo_strlen;
 uint8_t rez;
 if(parString ==0)   
  {
   //while(TransmitEnable == TRANSMIT_DISABLE);
   if(while_TransmitEnable() == _FALSE) return 1;
   sprintf((char*)TransmitBuffer,"SERVER_URL%d=%s:%s\r\n", number+1,
             &MdmURL_Server[number][0], &MdmPort_Server[number][0]);  
     Opto_Uart_Transmit(); 
     rez = OPTIC_OK;
  }
  else
  {
     if(ChangeURLPortServer((uint8_t*)parString, number, CHANGE_OPTIC) == _OK)
        rez = OPTIC_OK;
     else
        rez = OPTIC_INCORRECT_VALUE;
  }
 return rez;
} 

uint8_t PFW_ServerURL1(char* parString)
{
  return PFW_ServerURL(parString, 0);
}
uint8_t PFW_ServerURL2(char* parString)
{
  return PFW_ServerURL(parString, 1);
}
uint8_t PFW_ServerURL3(char* parString)
{
  return PFW_ServerURL(parString, 2);
}
//-----------------------------------------------------------------------------
uint8_t PFW_APNAdress(char* parString)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
   //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"APN_ADDRESS= %s \r\n",\
             MdmAPN_Adress);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_strlen = strlen(parString); 
    if (lo_strlen < sizeof(MdmAPN_Adress)) 
    {
       sprintf((char*)OldValue,"%s",MdmAPN_Adress);  

       Eeprom_WriteString((uint8_t*)&MdmAPN_Adress,(uint8_t*)parString,lo_strlen-2);
       
       sprintf((char*)NewValue,"%s",MdmAPN_Adress); 
       WriteChangeArcRecord(CHANGE_APN_ADDRESS, CHANGE_OPTIC);      
      return 0;
    }
    else
     return 2;  
  }
}

//-----------------------------------------------------------------------------
uint8_t PFW_APNLogin(char* parString)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
   //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"APN_LOGIN= %s \r\n",\
             MdmAPN_Login);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_strlen = strlen(parString); 
    if (lo_strlen < sizeof(MdmAPN_Login))
    {
       sprintf((char*)OldValue,"%s", MdmAPN_Login);  

       Eeprom_WriteString((uint8_t*)&MdmAPN_Login,(uint8_t*)parString, lo_strlen-2);
       
       sprintf((char*)NewValue,"%s",MdmAPN_Login); 
       WriteChangeArcRecord(CHANGE_APN_LOGIN, CHANGE_OPTIC); 
  
      return 0;
    } 
    else
      return 2;  
  }
}

//-----------------------------------------------------------------------------
uint8_t PFW_APNPassword(char* parString)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
   //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"APN_PASSORD= %s \r\n",\
             MdmAPN_Password);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_strlen = strlen(parString); 
    if (lo_strlen < sizeof(MdmAPN_Password) )
    {
       sprintf((char*)OldValue,"%s",MdmAPN_Password);  

       Eeprom_WriteString((uint8_t*)&MdmAPN_Password,(uint8_t*)parString,lo_strlen-2);
       
       sprintf((char*)NewValue,"%s",MdmAPN_Password); 
       WriteChangeArcRecord(CHANGE_APN_PASWORD, CHANGE_OPTIC); 

      return 0;
    }   
    else
      return 2;
  }  
} 
//-----------------------------------------------------------------------------
uint8_t PFW_MaxFlowRate(char* parString)
{
 if(parString ==0)   
  {
   //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Max_FlowRate= %0.4f\r\n",\
             sensorTypeParameters[_SgmType].Q_max);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 
 
 return 2;  
}
//-----------------------------------------------------------------------------
uint8_t PFW_MaxTemp(char* parString)
{
  if(parString ==0)   
  {
   //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Max_Temp= %d \r\n",\
             Device_STM.Max_T);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     int8_t temp;
     temp = atoi(parString);
     if(temp > 30 && temp < 100)
     {
        sprintf((char*)OldValue,"%d", Device_STM.Max_T);  

        Eeprom_WriteChar((uint32_t)&Device_STM.Max_T, (uint8_t)temp);
        
        sprintf((char*)NewValue, "%d", Device_STM.Max_T); 
        WriteChangeArcRecord(CHANGE_TEMP_PAR_MAX, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  }
} 
//-----------------------------------------------------------------------------
uint8_t PFW_MinTemp(char* parString)
{
  if(parString ==0)   
  {
   //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Min_Temp= %d \r\n",\
             Device_STM.Min_T);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     int8_t temp;
     temp = atoi(parString);
     if(temp > -50 && temp < 10)
     {
        sprintf((char*)OldValue,"%d", Device_STM.Min_T);  

        Eeprom_WriteChar((uint32_t)&Device_STM.Min_T, (uint8_t)temp);
        
        sprintf((char*)NewValue, "%d", Device_STM.Min_T); 
        WriteChangeArcRecord(CHANGE_TEMP_PAR_MIN, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  } 
} 
//-----------------------------------------------------------------------------
uint8_t PFR_GetLockState(char* parString)
{
  const char* lo_Lock_Open = "Lock OPEN";
  const char* lo_Lock_Close = "Lock CLOSE";
  char* lo_Lock_State = (char*)lo_Lock_Close;
  
  if(parString ==0)   
  {
    if(CLB_lock == 0x55)
      lo_Lock_State = (char*)lo_Lock_Open;

    //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"%s \r\n",\
             lo_Lock_State);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
   //    sprintf((char*)OldValue,"%d", CLB_lock);  

        CLB_lock = 0;
        
    //    sprintf((char*)NewValue, "%d", CLB_lock); 
        source_change_CLB_LOCK = CHANGE_OPTIC;
     //   WriteChangeArcRecord(CHANGE_CLB_LOCK, CHANGE_OPTIC);  

    return 0;
  } 
}  

/*-----------------------------------------------------------------------------
uint8_t PFW_ManualK(char* parString)
{
 char const* lo_Auto_K = "Key factor auto = ";
 char const* lo_Sub_K = "Substitution Key factor = ";
 char *lo_K; 
 uint16_t lo_KF;
   
 if(parString ==0)   
  {
    if(manual_KL)
    {
      lo_K  =(char*)lo_Sub_K;
      lo_KF = manual_KL; 
    }  
    else
    {
      lo_K = (char*)lo_Auto_K;
      lo_KF = Sensor.KL;
    }  
    while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"%s %u\r\n",\
             lo_K, lo_KF);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
   if (CLB_lock != 0x55)
      return 3;
   lo_KF = atoi(parString);
   if (lo_KF > 50000)
     return 2;
   manual_KL = lo_KF;
   return 0; 
  }
} 
*/
//-----------------------------------------------------------------------------
uint8_t PFW_GasDayBorter (char* parString)
{
  uint16_t lo_GasDayBorder;
   
 if(parString ==0)   
  {
    //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"GAS_DAY= %d\r\n",\
             GasDayBorder);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 else
 {
   lo_GasDayBorder = atoi(parString);
   if (lo_GasDayBorder < 24)
   {
          sprintf((char*)OldValue, "%d", GasDayBorder);  

          Eeprom_WriteWord((uint32_t)&GasDayBorder,lo_GasDayBorder);
       
          sprintf((char*)NewValue, "%d", GasDayBorder);  
          WriteChangeArcRecord(CHANGE_GAS_DAY, CHANGE_OPTIC);    

          return 0;
   }
   else
    return 2; // Incorrect value
 }  
}  
//-----------------------------------------------------------------------------
uint8_t PFW_ModemModeTransfer (char* parString)
{
  uint8_t lo_TransferMode, lo_TransferHour,lo_TransferMinutes, lo_TransferDay;
  
  if(parString ==0)   
  {
    //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Mode= %d \r\nTime= %.2d:%.2d \r\nDay= %d \r\n",\
            Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);
//              MdmTransferMode, MdmTransferHour,MdmTransferMinutes, MdmTransferDay);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_TransferMode = atoi(parString);
    if ((lo_TransferMode < 1) && (lo_TransferMode > 5))
       return 2; 
    parString = strchr(parString,',');
    if(parString)
    {
      lo_TransferHour = atoi(parString+1);
      if(lo_TransferHour > 23)
        return 2;
    }
    else
      return 1;
    parString = strchr(parString,':');
    if(parString)
    {
      lo_TransferMinutes = atoi(parString+1);
      if(lo_TransferMinutes > 59)
        return 2;
    } 
    else
      return 1;
    parString = strchr(parString,',');
    if(parString)
    {
      lo_TransferDay = atoi(parString+1);
      if(lo_TransferDay > 31)
        return 2;
    } 
    else
      return 1;
    cnt_seans_TCP_mode_2 = 0;

    sprintf((char*)OldValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  

    Eeprom_WriteChar((uint32_t)&Mode_Transfer.mode,lo_TransferMode);
    Eeprom_WriteChar((uint32_t)&Mode_Transfer.hour,lo_TransferHour);
    Eeprom_WriteChar((uint32_t)&Mode_Transfer.minute,lo_TransferMinutes);
    Eeprom_WriteChar((uint32_t)&Mode_Transfer.day,lo_TransferDay);
    timeGSMUnixStart = timeSystemUnix;
    timeUnixGSMNextConnect = timeSystemUnix + 3600; // do not interfere with shutdown
    successful_transfer_data_to_server = _OK;   
    sprintf((char*)NewValue, "%d,%.2d:%.2d,%d", Mode_Transfer.mode, Mode_Transfer.hour, Mode_Transfer.minute, Mode_Transfer.day);  
    WriteChangeArcRecord(CHANGE_TRANSFER_MODE, CHANGE_OPTIC); 

    return 0;
  }
}  

//-----------------------------------------------------------------------------
uint8_t PFW_ModemBalansePhohe(char* parString)
{
 uint16_t lo_charCnt;
 
 if(parString ==0)   
  {
    //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Balance Phone= %s \r\n",\
             MdmNumberBalans);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 else
 {
   if((lo_charCnt=(strlen(parString)-2)) > sizeof(MdmNumberBalans)-1)
     return 2;
   else
   {
         sprintf((char*)OldValue, "%s", MdmNumberBalans);  

         Eeprom_WriteString((uint8_t*)MdmNumberBalans,(uint8_t*)parString,lo_charCnt); 
       
         sprintf((char*)NewValue, "%s", MdmNumberBalans); 
         WriteChangeArcRecord(CHANGE_BALANCE_NUMBER, CHANGE_OPTIC);       

     return 0;
   }
 }  
}

//-----------------------------------------------------------------------------
uint8_t PFW_MdmStart(char* parString)
{
 uint16_t lo_mdmInterval;
  
   if(parString ==0)   
     return OPTIC_INCORRECT_VALUE;
   else
   {
     lo_mdmInterval = atol(parString);
     if(lo_mdmInterval < 3601)
     {
        timeUnixGSMNextConnect = timeSystemUnix + lo_mdmInterval;
        StatusDevice.GSM_Modem_Power_optic = _OK;
        return OPTIC_OK;        
     } 
     else
       return OPTIC_INCORRECT_VALUE; 
   }     
}  
//-----------------------------------------------------------------------------
uint8_t PFW_ModemReservedInterval(char* parString)
{
 uint32_t lo_mdmInterval;
  
   if(parString ==0)
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"RInt= %d\r\n",\
             reserved_int);  
     Opto_Uart_Transmit();
   }
   else
   {
     lo_mdmInterval = atol(parString);
     if(lo_mdmInterval)
     {
       sprintf((char*)OldValue,"%d", reserved_int);  

       Eeprom_WriteDword((uint32_t)&reserved_int, lo_mdmInterval);
       
       sprintf((char*)NewValue,"%d", reserved_int); 
       WriteChangeArcRecord(CHANGE_RESERVED_INT, CHANGE_OPTIC); 
     }
     else
       return 2;
   }     
   return 0;   
}  
//-----------------------------------------------------------------------------
uint8_t PFW_sms_phone(char* parString)
{
   uint8_t cnt_char;
   if(parString ==0)
   {
   //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"SMS_PHONE=%s\r\n",
             telephone_number_SMS1);  
     Opto_Uart_Transmit();
   }
   else
   {
     cnt_char = strlen(parString); 
     if(cnt_char < 15)
     {
       sprintf((char*)OldValue,"%s",telephone_number_SMS1);  

       Eeprom_WriteString((uint8_t*)telephone_number_SMS1,(uint8_t*)parString, cnt_char);
       
       sprintf((char*)NewValue,"%s",telephone_number_SMS1); 
       WriteChangeArcRecord(CHANGE_SMS_NUMBBER, CHANGE_OPTIC); 
     }
     else
        return 2;
   }     
   return 0;   
}

//-----------------------------------------------------------------------------
uint8_t PFW_ClearArc(char* parString)
{
  if(CLB_lock == 0x55)
  {
     sprintf((char*)OldValue,"%d;%d;%d;%d",
             IntArcRecNo, DayArcRecNo, SystemArcRecNo, TelemetryArcRecNo );
     Eeprom_WriteDword((uint32_t)&IntArcRecNo, 1);
     Eeprom_WriteDword((uint32_t)&DayArcRecNo, 1);
     Eeprom_WriteDword((uint32_t)&SystemArcRecNo, 1);
  //   Eeprom_WriteDword((uint32_t)&ChangeArcRecNo, 1);
     Eeprom_WriteDword((uint32_t)&TelemetryArcRecNo, 1);

     sprintf((char*)NewValue,"%d;%d;%d;%d",
             IntArcRecNo, DayArcRecNo, SystemArcRecNo, TelemetryArcRecNo);
     WriteChangeArcRecord(CHANGE_CLEAR_ARHIVE, CHANGE_OPTIC);
     return OPTIC_OK;
  }
  return OPTIC_ACCESS_DENIED;
}
//-----------------------------------------------------------------------------
uint8_t PFW_ClearCount(char* parString)
{
  if(CLB_lock == 0x55)
  {
     sprintf((char*)OldValue,"%0.6f", GasMeter.VE_Lm3 );
     GasMeter.VE_L = 0.0;
     GasMeter.VE_S =0.0;
     GasMeter.VE_Sm3 = 0.0;
     GasMeter.VE_Lm3 = 0.0;
     System_SaweBeforeReset();
     sprintf((char*)NewValue, "%0.6f", GasMeter.VE_Lm3 );
     WriteChangeArcRecord(CHANGE_CLEAR_COUNTER, CHANGE_OPTIC);
     return OPTIC_OK;
  }
  return OPTIC_ACCESS_DENIED;
}
//-----------------------------------------------------------------------------
uint8_t PFR_ArcNumRecords(char* parString)
{
   if(parString ==0)   
   {
    //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Hour_Arc= %u\r\nDayli_Arc= %u\r\nSystem_Arc= %u\r\nChange_Arc= %u\r\nTelemetry_Arc= %u\r\n",\
            IntArcRecNo-1,DayArcRecNo-1,SystemArcRecNo-1, ChangeArcRecNo - 1, TelemetryArcRecNo - 1);
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
     return 2;
}

//-----------------------------------------------------------------------------
uint8_t PFW_MdmToOpto(char* parString)
{
  const char* ans_ON = "ON";
  const char* ans_OFF = "OFF";
  
  char* ansver = (char*)ans_ON;
  
  if(parString ==0)   
   {
     //while(TransmitEnable == TRANSMIT_DISABLE);
     if(while_TransmitEnable() == _FALSE) return 1;
     if(NoLogToOptik)
        ansver = (char*)ans_OFF;
      sprintf((char*)TransmitBuffer,"Log MDM to Optical= %s \r\n",\
             ansver);  
     Opto_Uart_Transmit(); 
     return 0;
   }
  else
  {
    uint16_t lo_NoLogToOptik;

    if (strncmp("ON",parString,2) == 0)
       lo_NoLogToOptik = 0;
    else   
     if (strncmp("OFF",parString,3)== 0)
       lo_NoLogToOptik = 0xFF;
    else
      return 2;
    NoLogToOptik = lo_NoLogToOptik;     
   return 0;   
  }  
}

//******************************************************************************
uint8_t PFR_GroupInfo(char* parString)
{
   for(uint8_t i = 1; i < 9; i++)
   {
      while(TransmitEnable == TRANSMIT_DISABLE);
      FormationGROUP_INFO((uint8_t*)TransmitBuffer, i); 
      Opto_Uart_Transmit(); 
   }  
  return 0;
}
//******************************************************************************
uint8_t PFW_MOTO_INFO(char* parString)
{
   uint8_t rez = OPTIC_ERROR;
   if(parString ==0) 
   {
     for(uint8_t i = 1; i < 3; i++)
     {
        while(TransmitEnable == TRANSMIT_DISABLE);
        ParsingMESSAGE_MOTO_INFO((uint8_t*)TransmitBuffer, i); 
        Opto_Uart_Transmit(); 
     }  
     rez = OPTIC_OK;
   }
   else
   {
      ParsingMESSAGE_MOTO_INFO_SET(parString, _OK, CHANGE_OPTIC);
   }
  return rez;
}

//------------------------------------------------------------------------------
uint8_t PFW_ClearMdmCnt(char* parString)
{
     ClearCntSession(CHANGE_OPTIC, 0, 0, 0, 0);
     return 0;
}

//------------------------------------------------------------------------------
uint8_t PFW_hw_version(char* parString)
{
  if(parString ==0)   
  {
     return 2; 
  }
  else
  { 
     uint16_t temp;
     temp = atol(parString);
     Eeprom_WriteWord((uint32_t)&hw_version, temp);
  }
  return 0;
}
//------------------------------------------------------------------------------
uint8_t PFW_pressure_abs(char* parString)
{
  if(parString == 0)   
  {
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"PABS=%f\r\n",
             pressure_abs);  
     Opto_Uart_Transmit(); 
  }
  else
  { 
    double pressure_abs_buf;
     sscanf((char*)parString,"%lf", &pressure_abs_buf);
     if(pressure_abs_buf >= 96.325 && pressure_abs_buf <= 106.325)
     {
           sprintf((char*)OldValue,"%f", pressure_abs);
           Eeprom_WriteDouble((uint32_t)&pressure_abs, pressure_abs_buf);
           sprintf((char*)NewValue,"%f", pressure_abs);
           WriteChangeArcRecord(CHANGE_PRESSURE_ABS, CHANGE_OPTIC); 
     }
     else
        return OPTIC_INCORRECT_VALUE;
  }
  return OPTIC_OK;
}
//------------------------------------------------------------------------------
uint8_t PFW_Self_Brr(char* parString)
{
  if(parString == 0)   
  {
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"Gas Border =%d m3/h\r\n",
             TransmitOpticTest);  
     Opto_Uart_Transmit(); 
  }
  else
  { 
    uint16_t lo_GasSelfBorder = atoi(parString);
    /*
     sscanf((char*)parString,"%lf", &lo_GasSelfBorder);
     if(lo_GasSelfBorder >= 0 && lo_GasSelfBorder <= 6.0)
     { 
       GasSelfBorder = lo_GasSelfBorder;
     }
     else
        return OPTIC_INCORRECT_VALUE;
    */
    TransmitOpticTest = lo_GasSelfBorder;
  }
  return OPTIC_OK;
}


uint8_t PFW_type_device(char* parString)
{
  if(parString ==0)   
  {
     //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"TypeDevice=%d \r\n",
             type_device_int);  
     Opto_Uart_Transmit(); 
   }
   else
   { 
     if(CLB_lock == 0x55)
     {
       uint8_t temp;
       temp = atol(parString);
       if(temp == 4 
          || temp == 6  
          || temp == 10)
       {
          sprintf((char*)OldValue,"%d", type_device);  
          switch(temp)
          {
          case 4:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM4);
               break;
          case 6:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM6);
               break;      
          case 10:
               Eeprom_WriteChar((uint32_t)&type_device, SGM_TYPE_SGM10);
               break;          
          default:break;
          }
          cnt_error_SGM = 5;
          sprintf((char*)NewValue,"%d", type_device);  
          WriteChangeArcRecord(CHANGE_TYPE_DEVICE, CHANGE_OPTIC); 
       }
       else
         return 2;
     }
     else
       return 3;
   }
   return 0;
}

//------------------------------------------------------------------------------
uint8_t PFW_QposLimit(char* parString)
{
   uint16_t lo_QposLimit;
   if(parString ==0)   
   { 
      //while(TransmitEnable == TRANSMIT_DISABLE);
      if(while_TransmitEnable() == _FALSE) return 1;
      sprintf((char*)TransmitBuffer,"Q PosLimit = %d\r\n",\
             QposLimit);  
      Opto_Uart_Transmit(); 
      return OPTIC_OK;
   }
   else
   {
      if(CLB_lock != 0x55)
         return OPTIC_ACCESS_DENIED;     
      lo_QposLimit = atoi(parString);
      if(lo_QposLimit > 10000 && lo_QposLimit < 60000)
      {
          sprintf((char*)OldValue,"%d", QposLimit);
          Eeprom_WriteWord ((uint32_t)&QposLimit,lo_QposLimit);
          sprintf((char*)NewValue,"%d", QposLimit); 
          WriteChangeArcRecord(CHANGE_QPOS_LIMIT, CHANGE_OPTIC);
          return OPTIC_OK;
      }
   }
   return 2; // Incorrect value
}
//------------------------------------------------------------------------------
uint8_t PFW_QnegLimit(char* parString)
{
 uint16_t lo_QnegLimit;

 if (CLB_lock != 0x55)
      return 3;

 if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"Q NegLimit = %d\r\n",\
             QnegLimit);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 else
 {
   lo_QnegLimit = atoi(parString);
   if (lo_QnegLimit < 10000)
        {
          Eeprom_WriteWord ((uint32_t)&QnegLimit,lo_QnegLimit);
          return 0;
        }
 }
     return 2; // Incorrect value
}
//------------------------------------------------------------------------------
uint8_t PFW_Gl_Error(char* parString)
{

     //while(TransmitEnable == TRANSMIT_DISABLE);
   if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"Gl_Error=%u \r\n",
             Gl_Error.Raw);  
     Opto_Uart_Transmit(); 
   return 0;
}
//------------------------------------------------------------------------------
uint8_t PFW_all_segment(char* parString)
{
   Global_Menu = TEH_ALL_SEGMENT;
   return 0;
}
//------------------------------------------------------------------------------
uint8_t while_TransmitEnable ()
{
   // while(TransmitEnable == TRANSMIT_DISABLE)
   for(uint32_t i = 0; (TransmitEnable == TRANSMIT_DISABLE) && (i < 0x000FFFFF); i++)
   {
     if(OptoPort==OPTO_PORT_DISABLE || i == 0x000FFFFE)
       return _FALSE;
      
   }
   
   return _OK;
}
/*!
Read serial number module SGM
\param[in] input string
\return the result of the operation OPTIC_OK
*/   
uint8_t PFW_SN_SGM_E(char* parString)
{
   if(while_TransmitEnable() == _FALSE) return 1;

   sprintf((char*)TransmitBuffer,"SN_SGM_E=%s\r\n ", EEPROM_SN_SGM);  
   Opto_Uart_Transmit(); 
   return OPTIC_OK;
}
/*!
Read and write percent_QgL_Disp_min
\param[in] input string
\return the result of the operation OPTIC_OK
*/   
uint8_t PFW_QGL_MIN(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
    sprintf((char*)TransmitBuffer,"QGL_MIN=%f\r\n",\
             percent_QgL_Disp_min);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
    percent_QgL_Disp_min = atof(parString);
    return OPTIC_OK;
   }
}
/*!
Read and write percent_QgL_Disp_max
\param[in] input string
\return the result of the operation OPTIC_OK
*/   
uint8_t PFW_QGL_MAX(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
    sprintf((char*)TransmitBuffer,"QGL_MAX=%f\r\n",\
             percent_QgL_Disp_max);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
    percent_QgL_Disp_max = atof(parString);
    return OPTIC_OK;
   }
}
/*!
Read and write percent_QgL_Disp_max
\param[in] input string
\return the result of the operation OPTIC_OK
*/   
uint8_t PFW_QFL_LIMIT(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
    sprintf((char*)TransmitBuffer,"QFL_LIMIT=%d\r\n",\
             Qfl_limit_QgL_Disp);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
    Qfl_limit_QgL_Disp = atoi(parString);
    return OPTIC_OK;
   }
}
/*!
Read and write percent_QgL_Disp_max
\param[in] input string
\return the result of the operation OPTIC_OK
*/   
uint8_t PFW_KONTRAST(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
    sprintf((char*)TransmitBuffer,"KONTRAST=%d\r\n",\
             optic_kontrast);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
    uint8_t  lo_kontrast;
    lo_kontrast = atoi(parString);
    if(lo_kontrast < 8)
    {
       sprintf((char*)OldValue,"%d", optic_kontrast);
       Eeprom_WriteWord ((uint32_t)&optic_kontrast, lo_kontrast);
       MODIFY_REG(LCD->FCR, LCD_FCR_CC, optic_kontrast << 10);    // Contrast Control = 7    
       sprintf((char*)NewValue,"%d", optic_kontrast); 
       WriteChangeArcRecord(CHANGE_KONTRAST, CHANGE_OPTIC);
       return OPTIC_OK;
    }
    else
      return OPTIC_INCORRECT_VALUE;
   }
}
/*!
Get time after connection optic
\return time to millisec
*/   
uint32_t Get_tick_count_message_from_OPTIC_msec()
{
  return tick_count_message_from_OPTIC_msec;
}

uint8_t PFW_VOLUME_INST(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
    sprintf((char*)TransmitBuffer,"VOLUME_INST=%0.4f\r\n",
                           GasMeter.QgL_m3);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
      return OPTIC_READ_ONLY;
   }
}

uint8_t PFW_DATE_VERIFIC_NEXT(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
    sprintf((char*)TransmitBuffer,"DATE_VERIFIC=%0.2u.%0.2u.%0.2u\r\n",
                           DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     
     LL_RTC_DateTypeDef DateTemp;
     uint32_t *ptr_temp;
     ptr_temp = (uint32_t*)&DateTemp;
     uint8_t cnt;
     cnt = sscanf("%u.%u.%u", (char*)&DateTemp.Day, (char*)&DateTemp.Month, (char*)&DateTemp.Year);
     if(cnt == 3
        && DateTemp.Day > 0
        && DateTemp.Day < 32
        && DateTemp.Month > 0
        && DateTemp.Month < 13
        && DateTemp.Year < 100)
     {
        sprintf((char*)OldValue,"%0.2u.%0.2u.%0.2u", DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year);
        Eeprom_WriteDword((uint32_t)&DateNextVerification, *ptr_temp);
        sprintf((char*)NewValue,"%0.2u.%0.2u.%0.2u", DateNextVerification.Day, DateNextVerification.Month, DateNextVerification.Year); 
        WriteChangeArcRecord(CHANGE_DATE_VERIFIC_NEXT, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     return OPTIC_READ_ONLY;
  }
}

uint8_t PFW_MOTO_DATE_VERIFIC(char* parString)
{
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
    sprintf((char*)TransmitBuffer,"DATE_VERIFIC=%0.2u.%0.2u.%0.2u\r\n",
                           DateVerification.Day, DateVerification.Month, DateVerification.Year);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     
     LL_RTC_DateTypeDef DateTemp;
     uint32_t *ptr_temp;
     ptr_temp = (uint32_t*)&DateTemp;
     uint8_t cnt;
     cnt = sscanf("%u.%u.%u", (char*)&DateTemp.Day,(char*)&DateTemp.Month, (char*)&DateTemp.Year);
     if(cnt == 3
        && DateTemp.Day > 0
        && DateTemp.Day < 32
        && DateTemp.Month > 0
        && DateTemp.Month < 13
        && DateTemp.Year < 100)
     {
        sprintf((char*)OldValue,"%0.2u.%0.2u.%0.2u", DateVerification.Day, DateVerification.Month, DateVerification.Year);
        Eeprom_WriteDword((uint32_t)&DateVerification, *ptr_temp);
        sprintf((char*)NewValue,"%0.2u.%0.2u.%0.2u", DateVerification.Day, DateVerification.Month, DateVerification.Year); 
        WriteChangeArcRecord(CHANGE_DATE_VERIFIC, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     return OPTIC_INCORRECT_VALUE;
   }
}

uint8_t PFW_Qmax_warning(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"QMAX_WARNING=%0.2f\r\n",
                           Qmax_warning);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     float temp;
     temp = atof(parString);
     if(temp > 0.99 && temp < 2.01)
     {
        sprintf((char*)OldValue,"%f", Qmax_warning);
        Eeprom_WriteFloat((uint32_t)&Qmax_warning, temp);
        sprintf((char*)NewValue,"%f", Qmax_warning); 
        WriteChangeArcRecord(CHANGE_QMAX_WARNING, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  }
}

uint8_t PFW_Qmax_min_warning(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"QMAX_MIN_WARNING=%0.2f\r\n",
                           Qmax_min_warning);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(change_Qmax_min_warning(parString, CHANGE_OPTIC) == _OK)
        return OPTIC_OK;
     else
       return OPTIC_INCORRECT_VALUE;
  }
}

uint8_t change_Qmax_min_warning(char* parString, uint8_t flag_source)
{
     float temp;
     temp = atof(parString);
     if(temp > 0.49 && temp < 1.0)
     {
        sprintf((char*)OldValue,"%f", Qmax_min_warning);
        Eeprom_WriteFloat((uint32_t)&Qmax_min_warning, temp);
        sprintf((char*)NewValue,"%f", Qmax_min_warning); 
        WriteChangeArcRecord(CHANGE_QMAX_MIN_WARNING, flag_source);
        return _OK;
     }
     else
       return _FALSE;
}

uint8_t PFW_Qmax_warning_cnt_measurement(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"QMAX_WAR_CNT_MEAS=%d\r\n",
                           Qmax_warning_cnt_measurement);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(change_Qmax_warning_cnt_measurement(parString, CHANGE_OPTIC) == _OK)
        return OPTIC_OK;
     else
       return OPTIC_INCORRECT_VALUE;
  }
}

uint8_t change_Qmax_warning_cnt_measurement(char* parString, uint8_t flag_source)
{
     uint8_t temp;
     temp = atoi(parString);
     if(temp > 1 && temp < 201)
     {
        sprintf((char*)OldValue,"%d", Qmax_warning_cnt_measurement);
        Eeprom_WriteWord((uint32_t)&Qmax_warning_cnt_measurement, temp);
        sprintf((char*)NewValue,"%d", Qmax_warning_cnt_measurement); 
        WriteChangeArcRecord(CHANGE_QMAX_WARNING_CNT_MEASUREMENT, flag_source);
        return _OK;
     }
     else
       return _FALSE;
}

uint8_t PFW_Qmax_cnt_measurement(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"QMAX_CNT_MEAS=%d\r\n",
                           Qmax_cnt_measurement);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(change_Qmax_cnt_measurement(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK;
     else
       return OPTIC_ACCESS_DENIED;
  }
}

uint8_t change_Qmax_cnt_measurement(char* parString, uint8_t flag_source)
{
     if(CLB_lock != 0x55)
       return _FALSE;
     uint8_t temp;
     temp = atoi(parString);
     if(temp > 1 && temp < 201)
     {
        sprintf((char*)OldValue,"%d", Qmax_cnt_measurement);
        Eeprom_WriteWord((uint32_t)&Qmax_cnt_measurement, temp);
        sprintf((char*)NewValue,"%d", Qmax_cnt_measurement); 
        WriteChangeArcRecord(CHANGE_QMAX_CNT_MEASUREMENT, flag_source);
        return _OK;
     }
     else
       return _FALSE;
}

uint8_t PFW_DQF_END(char* parString)
{
   uint32_t crc;
   if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
   crc = CalculateCRC32((uint8_t*)Qf_param, Qf_sizeof, CRC_CTART_STOP);
   Eeprom_WriteDword((uint32_t)&crc_dQf_eeprom, crc);
   return OPTIC_OK;
}

uint8_t PFW_cnt_measurement_temper_eeprom(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"CNT_MEAS_TEMP=%d\r\n",
                           cnt_measurement_temper_eeprom);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     uint16_t temp;
     temp = atoi(parString);
     if(temp > 1 && temp < 1000)
     {
        sprintf((char*)OldValue,"%d", cnt_measurement_temper_eeprom);
        Eeprom_WriteWord((uint32_t)&cnt_measurement_temper_eeprom, temp);
        sprintf((char*)NewValue,"%d", cnt_measurement_temper_eeprom); 
        WriteChangeArcRecord(CHANGE_CNT_MEASUREMENT_TEMPER_EEPROM, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  }
}

//----------------
uint8_t PFW_Tmax_warning(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"TMAX_WARNING=%d\r\n",
                           Tmax_warning);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(change_Tmax_warning(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK;
     else
       return OPTIC_ACCESS_DENIED;
  }
}

uint8_t change_Tmax_warning(char* parString, uint8_t flag_source)
{
     uint8_t temp;
     temp = atoi(parString);
     if(temp > 25 && temp < Device_STM.Max_T )
     {
        sprintf((char*)OldValue,"%d", Tmax_warning);
        Eeprom_WriteChar((uint32_t)&Tmax_warning, temp);
        sprintf((char*)NewValue,"%d", Tmax_warning); 
        WriteChangeArcRecord(CHANGE_TEMP_PAR_MAX_WARNING, flag_source);
        return _OK;
     }
     else
       return _FALSE;
}

uint8_t PFW_Tmin_warning(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"TMIN_WARNING=%d\r\n",
                           Tmin_warning);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(change_Tmin_warning(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK;
     else
       return OPTIC_ACCESS_DENIED;
  }
}

uint8_t change_Tmin_warning(char* parString, uint8_t flag_source)
{
     int8_t temp;
     temp = atoi(parString);
     if(temp > Device_STM.Min_T && temp < 15)
     {
        sprintf((char*)OldValue,"%d", Tmin_warning);
        Eeprom_WriteChar((uint32_t)&Tmin_warning, (uint8_t)temp);
        sprintf((char*)NewValue,"%d", Tmin_warning); 
        WriteChangeArcRecord(CHANGE_TEMP_PAR_MIN_WARNING, flag_source);
        return _OK;
     }
     else
       return _FALSE;
}

uint8_t PFW_cnt_measurement_temper_warning_eeprom(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"T_WARN_CNT=%d\r\n",
                           cnt_measurement_temper_warning_eeprom);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(change_cnt_measurement_temper_warning_eeprom(parString, CHANGE_OPTIC) == _OK)
       return OPTIC_OK;
     else
       return OPTIC_ACCESS_DENIED;
  }
}

uint8_t change_cnt_measurement_temper_warning_eeprom(char* parString, uint8_t flag_source)
{
     uint16_t temp;
     temp = atoi(parString);
     if(temp > 1 && temp < 1001)
     {
        sprintf((char*)OldValue,"%d", cnt_measurement_temper_warning_eeprom);
        Eeprom_WriteWord((uint32_t)&cnt_measurement_temper_warning_eeprom, temp);
        sprintf((char*)NewValue,"%d", cnt_measurement_temper_warning_eeprom); 
        WriteChangeArcRecord(CHANGE_TEMP_WARNING_CNT, flag_source);
        return _OK;
     }
     else
       return _FALSE;
}
//------------------------------------------------------------------------------
uint8_t PFW_cnt_reverse_flow_eeprom(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"REV_FL_CNT=%d\r\n",
                           cnt_reverse_flow_eeprom);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     uint16_t temp;
     temp = atoi(parString);
     if(temp > 1 && temp < 1001)
     {
        sprintf((char*)OldValue,"%d", cnt_reverse_flow_eeprom);
        Eeprom_WriteWord((uint32_t)&cnt_reverse_flow_eeprom, temp);
        sprintf((char*)NewValue,"%d", cnt_reverse_flow_eeprom); 
        WriteChangeArcRecord(CHANGE_REVERSE_FLOW_CNT, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  }
}
uint8_t PFW_Qfl_reverse_flow(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"REV_FL=%d\r\n",
                           Qfl_reverse_flow);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     uint16_t temp;
     temp = atoi(parString);
     if(temp > 1999 && temp < 10001)
     {
        sprintf((char*)OldValue,"%d", Qfl_reverse_flow);
        Eeprom_WriteWord((uint32_t)&Qfl_reverse_flow, temp);
        sprintf((char*)NewValue,"%d", Qfl_reverse_flow); 
        WriteChangeArcRecord(CHANGE_VALUE_REVERSE_FLOW, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  }
}

uint8_t PFW_serial_number_board(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"NUMBER_BOARD=%s\r\n",
                           serial_number_board);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     uint16_t temp;
     temp = strlen(parString);
     if(temp > 0)
     {
        sprintf((char*)OldValue,"%s", serial_number_board);
        Eeprom_WriteString((uint8_t*)serial_number_board, (uint8_t*)parString, temp);
        sprintf((char*)NewValue,"%s", serial_number_board); 
        WriteChangeArcRecord(CHANGE_SERIAL_NUMBER_BOARD, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  }
}

uint8_t PFW_cnt_error_SGM_eeprom(char* parString)
{
  if(parString ==0)   
  {
     if(while_TransmitEnable() == _FALSE) return OPTIC_ERROR;
     sprintf((char*)TransmitBuffer,"CNT_ERR_SGM=%d\r\n",
                           cnt_error_SGM_eeprom);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK;
  }
  else
  {
     if(CLB_lock != 0x55)
       return OPTIC_ACCESS_DENIED;
     uint16_t temp;
     temp = atoi(parString);
     if(temp > 1 && temp < 260)
     {
        sprintf((char*)OldValue,"%d", cnt_error_SGM_eeprom);
        Eeprom_WriteWord((uint32_t)&cnt_error_SGM_eeprom, temp);
        sprintf((char*)NewValue,"%d", cnt_error_SGM_eeprom);
        WriteChangeArcRecord(CHANGE_CNT_ERROR_SGM_EEPROM, CHANGE_OPTIC);
        return OPTIC_OK;
     }
     else
       return OPTIC_INCORRECT_VALUE;
  }
}
//------------------------------------------------------------------------------
uint8_t PFW_status(char* parString)
{
   uint32_t *ptrStatus;
   ptrStatus = (uint32_t*)&StatusDevice;
   if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"STATUS=%08X\r\n",
             *ptrStatus);  
     Opto_Uart_Transmit(); 
   return 0;
}
//------------------------------------------------------------------------------
uint8_t PFW_LORA_power(char* parString)
{
  uint8_t lo_LORA_power;
  
  lo_LORA_power = atoi(parString);
  if (lo_LORA_power < 2)
  {
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"\r\nLORA_power=%u\r\n",lo_LORA_power); 
     Opto_Uart_Transmit(); 
     if(while_TransmitEnable() == _FALSE) return 1;
     switch(lo_LORA_power)
          {
            case 0: GSM_Set_flag_power(GSM_MUST_DISABLE);
                    Eeprom_WriteDword((uint32_t)&reserved_int, 172800); //48 �����;
                    CalculationNextCommunicationSession();
                    break;
            case 1: GSM_Set_flag_power(GSM_MUST_ENABLE);
                    Eeprom_WriteDword((uint32_t)&reserved_int, 10);
                    count_ExtremalPowerDown = 0;
                    break;
            default:break;                  
           }
     return 0; 
  }
  else 
    return 2;  
}
//------------------------------------------------------------------------------
uint8_t PFW_LORA_reconnect(char* parString)
{
  GSM_Set_flag_power(GSM_MUST_ENABLE);
  flag_opto_reconnect = 1;
  //GSMStatus.registration = LORA_ATJ_NON_REQUEST;
  return 0;
}
//------------------------------------------------------------------------------
uint8_t PFR_LORA_control(char* parString)
{
  uint8_t lo_LORA_control;
  
  lo_LORA_control = atoi(parString);
  if (lo_LORA_control < 2)
  {
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"\r\nLORA_control=%u\r\n",lo_LORA_control); 
     Opto_Uart_Transmit(); 
     if(while_TransmitEnable() == _FALSE) return 1;
     switch(lo_LORA_control)
          {
            case 0: for (int i=1;i<num;i++)
                    {
                      if(while_TransmitEnable() == _FALSE) return 1;
                      memcpy(TransmitBuffer, BufferPause[i], strlen(BufferPause[i]));
                      Opto_Uart_Transmit();
                    }        
                    break;
            case 1: for (int i=0;i<128;i++)
                    {
                      memset((char*)&BufferPause[i+1], 0, sizeof(BufferPause[i+1]));          
                    }
                    break;
            default:break;                  
           }
     return 0; 
  }
  else 
    return 2;   
}
//------------------------------------------------------------------------------
uint8_t PFW_Self_GasBorder(char* parString)
{
  if(parString == 0)   
  {
     if(while_TransmitEnable() == _FALSE) return 1;
     sprintf((char*)TransmitBuffer,"Gas Border =%d m3/h\r\n",
             TransmitOpticTest);  
     Opto_Uart_Transmit(); 
  }
  else
  { 
     if (CLB_lock != 0x55)
      return OPTIC_ACCESS_DENIED;
    uint16_t lo_GasSelfBorder = atoi(parString);
    /*
     sscanf((char*)parString,"%lf", &lo_GasSelfBorder);
     if(lo_GasSelfBorder >= 0 && lo_GasSelfBorder <= 6.0)
     { 
       GasSelfBorder = lo_GasSelfBorder;
     }
     else
        return OPTIC_INCORRECT_VALUE;
    */
    TransmitOpticTest = lo_GasSelfBorder;
  }
  return OPTIC_OK;
}
//------------------------------------------------------------------------------
uint8_t PFW_Set_KalmanFilter(char* parString)
{
 uint16_t lo_EnableKalmanFilter ;
/*
 if (CLB_lock != 0x55)
      return 3;
*/
 if(parString ==0)   
 {
    //while(TransmitEnable == TRANSMIT_DISABLE);
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"EnKalmanFilter=%d\r\n",\
             EnableKalmanFilter);  
     Opto_Uart_Transmit(); 
     return 0;
 }
 else
 {
   if (CLB_lock != 0x55)
      return OPTIC_ACCESS_DENIED;
      
   lo_EnableKalmanFilter = atoi(parString);
   if (lo_EnableKalmanFilter == 1)
      Eeprom_WriteChar((uint32_t)&EnableKalmanFilter, 1);
   else
     if(lo_EnableKalmanFilter == 0)
        Eeprom_WriteChar((uint32_t)&EnableKalmanFilter, 0);
     else
       return OPTIC_ERROR;
   return OPTIC_OK;
 }
}


//------------------------------------------------------------------------------
uint8_t PFW_KalmanK(char* parString)
{

 if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"KalmanK1 = %f KalmanK2 = %f \r\n",
             KalmanK1, KalmanK2);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 else
 {
      if (CLB_lock != 0x55)
      return OPTIC_ACCESS_DENIED;
   float temp1=5.0, temp2=5.0;
   sscanf((char*)parString,"%f;%f", &temp1, &temp2);
   if((temp1 >= 0 && temp1 <= 1) && (temp2 >= 0 && temp2 <= 1))  
   {
     Eeprom_WriteFloat((uint32_t)&KalmanK1, temp1);
     Eeprom_WriteFloat((uint32_t)&KalmanK2, temp2);
     return 0;
   }
   else
    return 2; 
  }
}
//------------------------------------------------------------------------------
uint8_t PFW_KalmanBorder(char* parString)
{
 if(parString ==0)   
  {

    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"KALMAN_BORDER=%d\r\n",
             KalmanBorder);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 else
 {
         if (CLB_lock != 0x55)
      return OPTIC_ACCESS_DENIED;
   uint16_t temp;
   temp = atoi(parString);
   Eeprom_WriteWord((uint32_t)&KalmanBorder, temp);
          return 0;
 }
}
//------------------------------------------------------------------------------
uint8_t PFW_KalmanCounter(char* parString)
{

 if(parString ==0)   
  {

    if(while_TransmitEnable() == _FALSE) return 1;
    sprintf((char*)TransmitBuffer,"KALMAN_COUNTER=%d\r\n",
             KalmanCnt);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 else
 {
   uint8_t temp;
   if (CLB_lock != 0x55)
      return OPTIC_ACCESS_DENIED;
   temp = atoi(parString);
   if(temp > 2)
      Eeprom_WriteChar((uint32_t)&KalmanCnt, temp);
   return 0;
 }
}