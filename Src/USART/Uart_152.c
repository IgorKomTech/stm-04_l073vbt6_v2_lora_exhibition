//=============================================================================
//  
//=============================================================================
/// \file    Uart.c
/// \author 
/// \date   
/// \brief  OPTICAL UART communication
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "Uart_152.h"
#include "stm32l073xx.h"
#include "Typedef.h"
#include "Time_g.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "io.h"
#include "GasMeter.h"
#include "OptoComand.h"
#include "GSMSIM800C.h"
#include "main_function.h"
#include "Global.h"

char TransmitBuffer[TX_BUFFER_MAX_SIZE];
//char TransmitBuffer_temp[TX_BUFFER_MAX_SIZE];
char ReceivedBuffer[RX_BUFFER_MAX_SIZE];
uint16_t _TransmitLength;
uint8_t  TransmitEnable = TRANSMIT_DISABLE; 
uint8_t  DataReceived = 0;
uint16_t ReceivedCounter = 0;

extern SemaphoreHandle_t xRxSemaphore;
extern uint8_t flag_end_task;

const char Error_msg[7][18] = { "OK\r\n",               //0
                            "Error\r\n",            //1 
                            "Incorrect value\r\n",  //2
                            "Access denied\r\n",    //3 
                            "Read only\r\n",         //4 
                            "CRC Error\r\n",         //5
                            "Internal Error\r\n"    //6
                           };    


//-----------------------------------------------------------------------------
 void Opto_Uart_Init(void)
 {  
   tick_count_message_from_OPTIC_msec = xTaskGetTickCount();
 // Enable the peripheral clock of GPIOC
  RCC->IOPENR |= RCC_IOPENR_GPIOCEN;
  
  // GPIO configuration for LPUART signals 
  // Select AF mode (10) on PC10 and PC11
  GPIOC->MODER = (GPIOC->MODER & ~(GPIO_MODER_MODE10|GPIO_MODER_MODE11))\
                 | (GPIO_MODER_MODE10_1 | GPIO_MODER_MODE11_1);
  //AF2 for LPUART signals
  GPIOC->AFR[1] = (GPIOC->AFR[1] &~ (0x0000FFFF));
    // Enable Pullup for TX (PC10)
  MODIFY_REG(GPIOC->PUPDR, GPIO_PUPDR_PUPD10, GPIO_PUPDR_PUPD10_0);
  // Enable Pullup for RX (PC11)
  MODIFY_REG(GPIOC->PUPDR, GPIO_PUPDR_PUPD11, GPIO_PUPDR_PUPD11_0);
  
  // enable PWR clk 
  SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN); 
  // Enable write access to Backup domain
  SET_BIT(PWR->CR, PWR_CR_DBP);
  
  // LSE mapped on LPUART 
  RCC->CCIPR |= RCC_CCIPR_LPUART1SEL;
  
  // Enable the peripheral clock LPUART
  RCC->APB1ENR |= RCC_APB1ENR_LPUART1EN;
  
  // oversampling by 16, 9600 baud
  LPUART1->BRR = 0x369; // 0x6D3 0x369
   
  // LPUART Clock Enable in Stop mode.
//  LPUART1->CR3 |= USART_CR3_UCESM;
  
 // ��� ��  � ��� �������
  {
    uint32_t lo_reg;
    lo_reg = '\n';
    lo_reg = lo_reg << 24;
    LPUART1->CR2 |= lo_reg;
  } 

  
  // 8 data bit, 1 start bit, 1 stop bit, no parity, Transmitter enable, Receiver Enable, stop mode
  LPUART1->CR1 =  USART_CR1_TE | USART_CR1_RE |  USART_CR1_UESM |USART_CR1_UE |USART_CR1_CMIE;
 // 
  SET_BIT(LPUART1->ICR, USART_ICR_TCCF); // Clear Transmit comp. interrapt flag
  
  
  // Enable DMA1 clock
  /*
   if ((RCC->AHBENR & RCC_AHBENR_DMA1EN) != RCC_AHBENR_DMA1EN)
      RCC->AHBENR |=RCC_AHBENR_DMA1EN;
  */
   EnableDisableDMA1(DMA1_ENABLE);
   // USART1_DMA_config
   DMA1_Channel2->CPAR = (uint32_t) &LPUART1->TDR;      //����� �������� ������ �����������
   DMA1_Channel3->CPAR = (uint32_t) &LPUART1->RDR;      //����� �������� ������ ���������
    //����� ������ �����������
   DMA1_Channel2->CMAR = (uint32_t)TransmitBuffer ;
   //����� ������ ���������
   DMA1_Channel3->CMAR = (uint32_t)ReceivedBuffer ;
   DMA1_Channel3->CNDTR = RX_BUFFER_MAX_SIZE;
   
   //����������� ����������� ��������:
   //������ �� ������, ��������� ��������� � ������
   DMA1_Channel2->CCR = DMA_CCR_DIR | DMA_CCR_MINC;
   //������ � ������, ��������� ��������� � ������
   DMA1_Channel3->CCR = DMA_CCR_MINC;
   
   //��������� ���������� �� ���������� ������:
   //����� 2
   DMA1_Channel2->CCR |= DMA_CCR_TCIE;
   DMA1_Channel3->CCR |= DMA_CCR_TCIE;

   //DMA remaped: Chanel2 - LPUART1_TX, Chanel3 - LPUART1_RX 
   DMA1_CSELR->CSELR |= 0x550;
  //������������� USART1 ��� ������ ����� DMA:
  //��������� �����-�������� LUSART1 ����� DMA
   LPUART1->CR3 |= USART_CR3_DMAT|USART_CR3_DMAR;
   SET_BIT(LPUART1->CR3, USART_CR3_EIE); // enable interrupt error
   
   SET_BIT(DMA1_Channel3->CCR,DMA_CCR_EN);
 //  SET_BIT(LPUART1->CR1,USART_CR1_RXNEIE);
   // �������� ���������� � NVIC
 
   NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
   NVIC_SetPriority(DMA1_Channel2_3_IRQn,3);
   NVIC_EnableIRQ(RNG_LPUART1_IRQn);
   NVIC_SetPriority(RNG_LPUART1_IRQn,3);
   if(READ_BIT(GPIOC->IDR, GPIO_IDR_ID12)==0) 
   {
     OptoPort = OPTO_PORT_ENABLE;   
     TransmitEnable =  TRANSMIT_ENABLE;
     SET_BIT(LPUART1->CR1,USART_CR1_UE);
   }  
}

//-----------------------------------------------------------------------------
void Opto_Uart_deInit(void)
{
  //Disable usart
  CLEAR_BIT(LPUART1->CR1,USART_CR1_UE);
  //Power off usart
  MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE10_0, GPIO_MODER_MODE10_0);
  SET_BIT(GPIOC->ODR,GPIO_ODR_OD10);
  CLEAR_BIT(RCC->APB1ENR, RCC_APB1ENR_LPUART1EN);
  // Disable IRQ from usart
  NVIC_DisableIRQ(RNG_LPUART1_IRQn);  
  // Disable DMA  
  CLEAR_BIT(DMA1_Channel2->CCR,DMA_CCR_EN);
  CLEAR_BIT(DMA1_Channel3->CCR,DMA_CCR_EN);
  
//  CLEAR_BIT(RCC->AHBENR, RCC_AHBENR_DMA1EN);
  // Clear buffer
  memset(ReceivedBuffer,0,RX_BUFFER_MAX_SIZE);
  //Set system flag
  TransmitEnable =  TRANSMIT_DISABLE;
  OptoPort = OPTO_PORT_DISABLE;
  EnableDisableDMA1(DMA1_DISABLE);
  // Output from testmode
  ActivateTestMode(TestModeOFF); 
}  

//-----------------------------------------------------------------------------
void  Opto_Uart_Transmit(void)
{
  uint16_t dataLen;
    
  dataLen = strlen((char*)TransmitBuffer);
  if(dataLen)
  {
    CLEAR_BIT(DMA1_Channel2->CCR,DMA_CCR_EN);
    SET_BIT(DMA1->IFCR,DMA_IFCR_CTCIF2);  // ������ ���� �������� ������
    DMA1_Channel2->CNDTR = dataLen; // ����� �������� � ������� ���
//    CLEAR_BIT(LPUART1->CR1,USART_CR1_RE);  
    // ������ ��������
    SET_BIT(DMA1_Channel2->CCR ,DMA_CCR_EN);
    TransmitEnable =  TRANSMIT_DISABLE;
  }  
}    


/*-----------------------------------------------------------------------------
void  Opto_Chek_Transmit(void)
{
  if(TransmitEnable ==  TRANSMIT_DISABLE)
    

//----------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
// ������ � ��������� ������� ����� ������
// ������������ �������� - ������ ����������� �������
// � param �������� ����� ������ ���������� ��� �������
// 
//-----------------------------------------------------------------------------
void OPTO_ReadTask(void const * argument)
{
   uint8_t i=0,sLen=0,ret=0xFF;
   char* newData;
  
   while(1)
   {
      stack_OPTO_ReadTask = uxTaskGetStackHighWaterMark(NULL);
      xSemaphoreTake(xRxSemaphore, portMAX_DELAY);
      flag_end_task |= 0x04;
      ret=0xFF;
      for(i=0; i < MAX_FUNC; i++)   //������������� ������ ������!!
      {
         if(PFunc[i].commandStr[0] == ReceivedBuffer[0])   
         {
            sLen = strlen((char*)PFunc[i].commandStr);
            if(strncmp(PFunc[i].commandStr,(char*)ReceivedBuffer, sLen) != 0)
               continue;  // ��������� ���� ��������
            tick_count_message_from_OPTIC_msec = xTaskGetTickCount();
            newData = strchr((char*)ReceivedBuffer,'=');
            if(newData == NULL) // ������� �����
            {
               newData = strchr((char*)ReceivedBuffer,'(');
               if(newData == NULL)
                  ret = PFunc[i].ParsFunc(0);
               else
                 ret =  PFunc[i].ParsFunc(newData);
            }
            else  // ������� ������
               ret =  PFunc[i].ParsFunc(newData + 1);  //TODO: ������� ������
            i = MAX_FUNC;
         }  // if(PFunc[i].commandStr[0]
      }
      memset(ReceivedBuffer,0,RX_BUFFER_MAX_SIZE);
      DataReceived = 0;
      ReceivedCounter =0;
      CLEAR_BIT(DMA1_Channel3->CCR,DMA_CCR_EN);
      DMA1_Channel3->CMAR = (uint32_t)ReceivedBuffer ;
      DMA1_Channel3->CNDTR = RX_BUFFER_MAX_SIZE;
      SET_BIT(DMA1_Channel3->CCR,DMA_CCR_EN);

      //while(TransmitEnable == TRANSMIT_DISABLE); 
      if(while_TransmitEnable() == _FALSE) continue;
      if(ret == 0xFF) 
         ret = 1; 
      strcpy((char*)TransmitBuffer, Error_msg[ret]);
      Opto_Uart_Transmit(); 
      flag_end_task &= ~0x04;
   }   
}

void EnableDisableDMA1(uint8_t flag)
{
 // static uint8_t cntInput=0;
  if(flag == DMA1_ENABLE)
  {
  //   ++cntInput;
     RCC->AHBENR |=RCC_AHBENR_DMA1EN;
  }
  else
    
    // --cntInput;
  
 // if(!cntInput)
  {
    if (OptoPort == OPTO_PORT_DISABLE && GSM_Get_Status_power() == POWER_NON && GSM_Get_flag_power() == GSM_MUST_DISABLE) 
    
    RCC->AHBENR &= ~RCC_AHBENR_DMA1EN;
  }
}