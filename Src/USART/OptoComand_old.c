//=============================================================================
//  
//=============================================================================
/// \file    OptoComand.c
/// \author 
/// \date   
/// \brief   Comand optical interface
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#include "Uart_152.h"
#include "Typedef.h"
#include "Time_g.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "Configuration.h"
#include "GasMeter.h"
#include "EEPROM.h"
#include "io.h"
#include "time_g.h"
#include "SensorTypeParameters.h"
#include "OptoComand.h"
#include "FlowMeasurement.h"
#include "Archiv.h"
#include "ExtFlash.h"
#include "USART_Modem.h"




TParser PFunc[] = {{(TParsFunc)PFR_DateTime, "DATETIME"},
                   {(TParsFunc)PFR_SWVers,   "SW_VER"},
                   {(TParsFunc)PFW_TestMode, "TestMode"},   // CLB
                   {(TParsFunc)PFW_Qdf,      "QdF"},        // CLB 
                   {(TParsFunc)PFR_DevInfo,  "DevInfo"},
                   {(TParsFunc)PFW_SerialNum,"DEVICE_SN"},   //CLB
                   {(TParsFunc)PFW_MeasTest, "START"},       //CLB
                   {(TParsFunc)PFR_HWVers,   "HW_VER"},
                   {(TParsFunc)PFR_DevName,  "DevName"},
                   {(TParsFunc)PFR_CurVol,   "Volume"},
                   {(TParsFunc)PFW_GasRTime,  "N_GasRecTime"},  //CLB
                   {(TParsFunc)PFW_GasSTime,  "N_GasSMesTime"},  //CLB
                   {(TParsFunc)PFW_GasLTime,  "N_GasLMesTime"},   //CLB
                   {(TParsFunc)PFW_TestRTime,  "Test_GasRecTime"}, //CLB
                   {(TParsFunc)PFW_TestLTime,  "Test_GasLMesTime"}, //CLB
                   {(TParsFunc)PFW_ShortMeassure, "N_ShortMeasUsed"}, //CLB
                   {(TParsFunc)PFR_ReadIntArc,  "ARCHIVE_INT"},
                   {(TParsFunc)PFR_ReadDayArc,  "ARCHIVE_DAY"},
                   {(TParsFunc)PFW_ServerURL,  "SERVER_URL"},
                   {(TParsFunc)PFW_APNAdress,  "APN_ADDRESS"},
                   {(TParsFunc)PFW_APNLogin,   "APN_LOGIN"},
                   {(TParsFunc)PFW_APNPassword, "APN_PASSWORD"},
                   {(TParsFunc)PFW_MaxFlowRate, "Max_FlowRate"},   //CLB
                   {(TParsFunc)PFW_MaxTemp,     "Max_Temp"},       //CLB
                   {(TParsFunc)PFW_MinTemp,     "Min_Temp"},       //CLB  
                   {(TParsFunc)PFR_GetLockState, "LOCK STATE"},  
//                   {(TParsFunc)PFW_ManualK,     "MANUAL_K"},       //CLB  
                   // GasDayBegin!!
};

const uint16_t MAX_FUNC = sizeof(PFunc)/sizeof(PFunc[0]);

extern uint16_t manual_KL;      

                              
//------------------------------------------------------------------------------
uint8_t Str2DataTime(LL_RTC_DateTypeDef* p_Date,LL_RTC_TimeTypeDef* p_Time, char*parString)
{
  char* index;
  char delimiter = '.';
  uint8_t temp_value;
  uint8_t ret=0;
   
    // ������ ���
    index = parString;
    uint8_t i=0;
    while((ret==0) && (i<6))
    {
       switch (i)
       {
        case 0:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 32)
             p_Date->Day = temp_value;
           else 
             ret = 2;
           break;
        case 1:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 13)
             p_Date->Month = temp_value;
           else 
             ret = 2;
           break;
        case 2:
           temp_value = atoi(index);
           if ((temp_value)> 0 && (temp_value)< 99)
             p_Date->Year = temp_value;
           else 
             ret = 2;
           delimiter = ',';
           break;
        case 3:
           temp_value = atoi(index);
           if (temp_value < 25)
           {
             p_Time->Hours = temp_value;
              delimiter = ':';
           }  
           else 
             ret = 0x2;
           break;
        case 4:
           temp_value = atoi(index);
           if (temp_value < 60)
             p_Time->Minutes = temp_value;
           else 
             ret = 0x2;
           break;
        case 5:
           temp_value = atoi(index);
           if (temp_value < 60)
             p_Time->Seconds = temp_value;
           else 
             ret = 0x2;
           break;
       }           
     index = strchr(index,delimiter);
     index++;
     i++;
    }
  return ret;
}



//-----------------------------------------------------------------------------
uint8_t PFR_DateTime(char* parString)
{
  LL_RTC_DateTypeDef loDate;
  LL_RTC_TimeTypeDef loTime;
  uint8_t ret =0;
  
 if(parString ==0)
  {
    GetDate_Time(&loDate,&loTime);
    sprintf((char*)TransmitBuffer,"DATETIME =%0.2u.%0.2u.%0.2u,%0.2u:%0.2u:%0.2u\r\n",\
      loDate.Day,loDate.Month,loDate.Year,\
      loTime.Hours,loTime.Minutes,loTime.Seconds);
    Opto_Uart_Transmit(); 
    return(0); 
  }
 else
 {
   ret = Str2DataTime(&loDate,&loTime,parString);
    if(ret == 0)
    {
      SetDate_Time(&loDate,&loTime);
      return 0;
    }  
   else
     return ret;
 } 

}

//-----------------------------------------------------------------------------
uint8_t PFR_SWVers(char* parString)
{
 if(parString ==0)
  {
    sprintf((char*)TransmitBuffer,"SW_VER=%0.2u.%0.2u\r\n",\
       Device_STM.FW_major,Device_STM.FW_minor);
    Opto_Uart_Transmit();
   return 0; 
  }
  else
    return 4;
} 

//-----------------------------------------------------------------------------
uint8_t PFR_DevInfo(char* parString)
{
 if(parString ==0)
  {
    sprintf((char*)TransmitBuffer,\
    "\r\nSerial N SGM: %s\r\nArticleCode: %s\r\n SF = %d\r\n LUT = %d\r\n",\
       Sensor_Const.SN_SGM,Sensor_Const.Article,Device_STM.SF,Device_STM.Lut);
    Opto_Uart_Transmit();
  return 0; 
  }
  else
    return 4;
}

//-----------------------------------------------------------------------------
uint8_t PFW_SerialNum(char* parString)
{
  if(parString ==0)
  {
      sprintf((char*)TransmitBuffer,"Serial N %s\r\n", Device_STM.SN_Device);
      Opto_Uart_Transmit();
      return 0;
  }
  else
  {
    uint16_t len_buf =0;
    if (CLB_lock != 0x55)
      return 3;

    len_buf = strlen(parString)-2;
    if((len_buf > 20) || (len_buf == 0))  //GasMeter.h  TDevice
      return 2;
    else
    {
      for(uint8_t i=0; i< len_buf;i++)
        if(isdigit(parString[i]))
           ;
         else
          return 0xFF;
        Eeprom_WriteString((uint8_t*)Device_STM.SN_Device,(uint8_t*)parString,len_buf); 
      // strcpy((char*)Device_STM.SN_Device,parString);
    }
  }           
 return 0;         
}
//-----------------------------------------------------------------------------
uint8_t PFW_TestMode(char* parString)
{
  uint8_t loTestMode;
  
  if (CLB_lock != 0x55)
    return 3;
  loTestMode = atoi(parString);
  if (loTestMode < 5)
  {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"\r\nTestMode=%0.1u\r\n",loTestMode); 
     Opto_Uart_Transmit();
     while(TransmitEnable == TRANSMIT_DISABLE);  
 //    SetUsartTE();
     switch(loTestMode)
          {
            case 0: ActivateTestMode(TestModeOFF);
                    break;
            case 1: ActivateTestMode(TestMode_1);
                   break;
            case 2: ActivateTestMode(TestMode_2);
                    break;
            case 4: ActivateTestMode(TestMode_4);
                    break;
           default: ActivateTestMode(TestModeOFF);         
             return 0xFF;         
           }
     return 0; 
  }
  else 
    return 2;  
} 

//-----------------------------------------------------------------------------
 static void OutDQFToOptik(uint8_t value)
 {
      while(TransmitEnable == TRANSMIT_DISABLE);
      sprintf((char*)TransmitBuffer,"%0.2u\t%0.5u\t%0.5u\t%0.2f\t%0.6f\r\n",\
        Qf_param[value].num_param,Qf_param[value].min_Border_Qf,Qf_param[value].max_Border_Qf,\
        Qf_param[value].offset_dQf,Qf_param[value].ctg_dQf);
      Opto_Uart_Transmit();
 }
//-----------------------------------------------------------------------------
uint8_t PFW_Qdf(char* parString)
{
  uint16_t ret=0, str_len=0,paramIndex =0;
  char* index = parString;
  char* newData; 

  if (CLB_lock != 0x55)
    return 3;
  
  if(parString==0)
  {
    newData = strchr((char*)ReceivedBuffer,'[');  
    if (newData !=NULL)
    {
      newData++;
      paramIndex = atoi(newData);
      if(paramIndex >= Qf_count)
        return 2;
      else
      {
        OutDQFToOptik(paramIndex);
        return 0;
      } 
    }
   for (uint8_t i=0; i< Qf_count; i++)
     OutDQFToOptik(i);
   return 0;
   }
  else
   {
    uint16_t lo_num,lo_min,lo_max;
    float lo_diff;
    float lo_tan;
    uint8_t i=0;    
    char delimiter = ';';
    
    str_len = strlen(parString);
    if (str_len <20) 
      return 2;
 
    while((ret==0) && (i<5))
    {
       switch (i)
       {
        case 0:
           lo_num = atoi(index);
           if ((lo_num)> Qf_count )
             ret = 2;
           break;           
        case 1:
           lo_min = atoi(index);
           if(lo_num > 0)
             if (lo_min < Qf_param[lo_num-1].max_Border_Qf)
              ret = 2;
           break;
        case 2:
           lo_max = atoi(index);
           if (lo_max < lo_min)
             ret = 2;
           break;
        case 3:
            lo_diff = atof(index);
            break;
        case 4:
           lo_tan = atof(index);
           break;
        default:
           ret= 2;
        }           
     index = strchr(index,delimiter);
     index++;
     i++;
    }
    if(ret == 0)
       Eeprom_WriteQf(lo_num,lo_min,lo_max,lo_diff,lo_tan);
    
  } // if-else (*parString==0)
 return ret; 
} 

//-----------------------------------------------------------------------------
uint8_t PFW_MeasTest(char* parString)
{
  if (CLB_lock != 0x55)
     return 3;

  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"STOP: %0.2u sek.\t%0.6f\t%0.6f\t%0.4f\t%u\r\n",\
             TestInterval, TEST_VE_L, TEST_VE_Lm3,Test_QF_midle,TEST_QF_Count );  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
       uint16_t lo_TestInterval;
       
       lo_TestInterval = atoi(parString);
       if (lo_TestInterval % Config_GasmeterMeasurementIntervalTestModeFM)
         return 2;  
       if ((lo_TestInterval > 0) && (lo_TestInterval <3600))
       {
         TestInterval=lo_TestInterval;  
         ActivateTestMode(TestMode_3);
        return 0;
       }
       return 2;
   }         
}

//-----------------------------------------------------------------------------
uint8_t PFR_HWVers(char* parString)
{
  if(parString ==0)   
    {
      while(TransmitEnable == TRANSMIT_DISABLE);
      sprintf((char*)TransmitBuffer,"HW_VER=%0.2u.%0.2u\r\n",\
                Device_STM.HW_major,Device_STM.HW_minor);
      Opto_Uart_Transmit();
      return 0; 
    }
  else
   return 4;
 }

//-----------------------------------------------------------------------------
uint8_t PFR_DevName(char* parString)

{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"DeviceName=CMT-G%c\r\n",\
       Sensor_Const.Article[6]);
     Opto_Uart_Transmit();
     return 0; 
   }
  else
     return 4;
}

//-----------------------------------------------------------------------------
uint8_t PFR_CurVol(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"CurrentVolume = %0.4f m3\r\n",\
       GasMeter.VE_Lm3);
     Opto_Uart_Transmit();
     return 0; 
   }
  else
     return 4;
}

//-----------------------------------------------------------------------------
uint8_t PFW_GasRTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"GasRecognitionInterval in Normal mode: %u \r\n",\
             Config_GasmeterGasRecognitionIntervalNormalMode);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;
    
     if (CLB_lock != 0x55)
       return 3;

     lo_TestInterval = atoi(parString);
     if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // ���� 1 ��� � �����
     {
       Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalNormalMode,lo_TestInterval);
       return 0;
     }
    return 2;
   }         
}

//-----------------------------------------------------------------------------
uint8_t PFW_GasSTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"GasShortMeasurementInterval in Normal mode: %u \r\n",\
             Config_GasmeterMeasurementIntervalNormalModeSM);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;

     if (CLB_lock != 0x55)
       return 3;

     lo_TestInterval = atoi(parString);
     if ((lo_TestInterval > Config_GasmeterMeasurementIntervalNormalModeFM)|| \
         (Config_GasmeterMeasurementIntervalNormalModeFM % lo_TestInterval !=0))
       return 2; 
     if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // ���� 1 ��� � �����
     {
       Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalNormalModeSM,lo_TestInterval);
       return 0;
     }
   }
  return 1;
}

//-----------------------------------------------------------------------------
uint8_t PFW_GasLTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"GasLongMeasurementInterval in Normal mode: %u \r\n",\
             Config_GasmeterMeasurementIntervalNormalModeFM);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;
 
     if (CLB_lock != 0x55)
        return 3;

       lo_TestInterval = atoi(parString);
      if(!NoShortMeassure) 
      {
        if ((lo_TestInterval < Config_GasmeterMeasurementIntervalNormalModeSM)|| \
           (lo_TestInterval % Config_GasmeterMeasurementIntervalNormalModeSM !=0))
         return 2; 
      }
       if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // ���� 1 ��� � �����
       {
         Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalNormalModeFM,lo_TestInterval);
         return 0;
       }
    return 1;
   }         
}

//-----------------------------------------------------------------------------
uint8_t PFW_TestLTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"GasLongMeasurementInterval in Test mode: %u \r\n",\
             Config_GasmeterMeasurementIntervalTestModeFM);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;
     if (CLB_lock != 0x55)
       return 3;

     lo_TestInterval = atoi(parString);
     if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // �� ����� 2 ��� � �����
     {
       Eeprom_WriteWord((uint32_t)&Config_GasmeterMeasurementIntervalTestModeFM,lo_TestInterval);
       return 0;
     }
    return 1;
   }         
}

//-----------------------------------------------------------------------------
uint8_t PFW_TestRTime(char* parString)
{
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"GasRecognitionInterval in Test mode: %u \r\n",\
             Config_GasmeterGasRecognitionIntervalTestMode);  
     Opto_Uart_Transmit(); 
     return 0;
   }
   else
   {
     uint16_t lo_TestInterval;
     if (CLB_lock != 0x55)
       return 3;

     lo_TestInterval = atoi(parString);
     if ((lo_TestInterval > 0) && (lo_TestInterval < 43200))   // ���� 1 ��� � �����
     {
       Eeprom_WriteWord((uint32_t)&Config_GasmeterGasRecognitionIntervalTestMode,lo_TestInterval);
       return 0;
     }
    return 1;
   }         
}

//-----------------------------------------------------------------------------
uint8_t PFW_ShortMeassure(char* parString)
{
  const char* ans_YES = "YES";
  const char* ans_NO =  "NO";
  
  char* ansver = (char*)ans_YES;
  
  if(parString ==0)   
   {
     while(TransmitEnable == TRANSMIT_DISABLE);
     if(NoShortMeassure)
       ansver = (char*)ans_NO;
      sprintf((char*)TransmitBuffer,"Used ShortMeassure in NormalMode: %s \r\n",\
             ansver);  
     Opto_Uart_Transmit(); 
     return 0;
   }
  else
  {
    uint16_t lo_NoShortMeassure;
    if (CLB_lock != 0x55)
      return 3;

    lo_NoShortMeassure = atoi(parString);
    if(lo_NoShortMeassure == 0)   // �� ������������ �������� ���������
       lo_NoShortMeassure = 0xFF;
    else
       lo_NoShortMeassure = 0;
    
      Eeprom_WriteWord((uint32_t)&NoShortMeassure,lo_NoShortMeassure);
     return 0;   
  }  
}

//------------------------------------------------------------------------------
void Out_ArcRec(uint8_t p_CRC)
{
  const char* lo_CRC_OK = "CRC OK";
  const char* lo_CRC_ERR = "CRC ERROR";
  char* lo_CRC;
  if (p_CRC)
    lo_CRC=(char*)lo_CRC_ERR;
  else
    lo_CRC=(char*)lo_CRC_OK;
  while(TransmitEnable == TRANSMIT_DISABLE);
   sprintf((char*)TransmitBuffer,"%02u;%02u-%02u-%02u;%02u:%02u:%02u;%0.2f;%0.4f;%u;%d; %s \r\n", \
                 IntArc.ArcRecNo,IntArc.ArcDay,IntArc.ArcMon,IntArc.ArcYear,\
                 IntArc.ArcHour,IntArc.ArcMinute,IntArc.ArcSecond,\
                 IntArc.ArcTemp, IntArc.ArcVolume,IntArc.ArcK,IntArc.ArcFlag, lo_CRC);
   Opto_Uart_Transmit(); 
}  

//-----------------------------------------------------------------------------
uint8_t ReadArcRecord(char* parString, uint8_t arc_type)
{
  uint8_t ret =0;
  uint16_t lo_StartNumInterval, lo_StopNumInterval;
  uint32_t lo_ArcRecNo, lo_MaxArcRecords;
  
  if(arc_type)
  {
     lo_ArcRecNo = DayArcRecNo;
     lo_MaxArcRecords = DayArcRecCount;
     if(lo_ArcRecNo == 0)
       return 1;
  } 
  else
  {
    lo_ArcRecNo = IntArcRecNo;
    lo_MaxArcRecords = IntArcRecCount;
  }
  
  if(parString ==0)  
  {
    lo_StartNumInterval = lo_ArcRecNo-1;
    lo_StopNumInterval =  lo_ArcRecNo-1;

//    ret = ReadIntArc(lo_ArcRecNo-1,arc_type);
//     if (ret==0)
//       Out_ArcRec(ret);
//   return ret; 
  }
  else
  {
     lo_StartNumInterval = atoi(parString);
     if (lo_StartNumInterval >= lo_ArcRecNo) // Chek start position 
       return 2;
     parString = strchr(parString,';');         // Find stop position
     if(parString)
      {
       lo_StopNumInterval = atoi(parString+1); 
       if (lo_StopNumInterval >= lo_ArcRecNo) // Chek stop position 
         return 2;
      }
     else
     {
       lo_StopNumInterval = lo_ArcRecNo-1;
     }
   if((lo_StopNumInterval > lo_MaxArcRecords))
      lo_StartNumInterval = lo_StopNumInterval - lo_MaxArcRecords -1;
  }   // if-else parString   

  SPI_InitHardware();
  for (uint32_t i=lo_StartNumInterval; i<=lo_StopNumInterval ; i++)
  {
   while(IntArcFlag); 

   if (!IS_FLASH_ON())
     SPI_InitHardware();

    ret = ReadIntArc(i,arc_type); 
    Out_ArcRec(ret);
    if(OptoPort==OPTO_PORT_DISABLE)
      break;
      } 
   SPI_DeInitHardware();
   return ret;
}  

//-----------------------------------------------------------------------------
uint8_t PFR_ReadIntArc(char* parString)
{
  return ReadArcRecord(parString,0);
} 
  
//-----------------------------------------------------------------------------
uint8_t PFR_ReadDayArc(char* parString)
{
  return ReadArcRecord(parString,1);
} 

//-----------------------------------------------------------------------------
uint8_t PFW_ServerURL(char* parString)
{
 char* lo_parString;
 uint16_t lo_port, lo_strlen;
 
 if(parString ==0)   
  {
   while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"SERVER_URL = %s \r\n",\
             MdmURL_Server);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_strlen = strlen(parString);
    if (lo_strlen < 64)
    {
      if(lo_parString = strchr(parString,':'))  // find port
      {
        lo_port = atoi(lo_parString);
        if (lo_port < 0xFFFF)
        {
          Eeprom_WriteString((uint8_t*)&MdmURL_Server,(uint8_t*)parString,lo_strlen);
          return 0;
        }  
      }
    }  
   return 2; 
  }
} 
//-----------------------------------------------------------------------------
uint8_t PFW_APNAdress(char* parString)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
   while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"APN_ADDRESS = %s \r\n",\
             MdmAPN_Adress);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_strlen = strlen(parString); 
    if (lo_strlen < 25) 
    {
      Eeprom_WriteString((uint8_t*)&MdmAPN_Adress,(uint8_t*)parString,lo_strlen);
      return 0;
    }
    else
     return 2;  
  }
}

//-----------------------------------------------------------------------------
uint8_t PFW_APNLogin(char* parString)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
   while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"APN_LOGIN = %s \r\n",\
             MdmAPN_Login);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_strlen = strlen(parString); 
    if (lo_strlen <13)
    {
      Eeprom_WriteString((uint8_t*)&MdmAPN_Login,(uint8_t*)parString,lo_strlen);
      return 0;
    } 
    else
      return 2;  
  }
}

//-----------------------------------------------------------------------------
uint8_t PFW_APNPassword(char* parString)
{
  uint16_t lo_strlen;
  
  if(parString ==0)   
  {
   while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"APN_PASSORD = %s \r\n",\
             MdmAPN_Password);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    lo_strlen = strlen(parString); 
    if (lo_strlen < 25 )
    {
      Eeprom_WriteString((uint8_t*)&MdmAPN_Login,(uint8_t*)parString,lo_strlen);
      return 0;
    } 
    else
      return 2;  
  }   
}

//-----------------------------------------------------------------------------
uint8_t PFW_MaxFlowRate(char* parString)
{
 if(parString ==0)   
  {
   while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"Max_FlowRate = %0.4f m3/h\r\n",\
             Device_STM.Max_Q);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 
 return 2;  
}
//-----------------------------------------------------------------------------
uint8_t PFW_MaxTemp(char* parString)
{
  if(parString ==0)   
  {
   while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"Max_Temp = %0.2f C\r\n",\
             Device_STM.Max_T);  
     Opto_Uart_Transmit(); 
     return 0;
  }
 
 return 0xFF;  
} 
//-----------------------------------------------------------------------------
uint8_t PFW_MinTemp(char* parString)
{
  if(parString ==0)   
  {
   while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"Min_Temp = %0.2f C \r\n",\
             Device_STM.Min_T);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  
 return 0xFF;  
} 
//-----------------------------------------------------------------------------
uint8_t PFR_GetLockState(char* parString)
{
  const char* lo_Lock_Open = "Lock OPEN";
  const char* lo_Lock_Close = "Lock CLOSE";
  char* lo_Lock_State = (char*)lo_Lock_Close;
  
  if(parString ==0)   
  {
    if(CLB_lock == 0x55)
      lo_Lock_State = (char*)lo_Lock_Open;

    while(TransmitEnable == TRANSMIT_DISABLE);
    sprintf((char*)TransmitBuffer,"%s \r\n",\
             lo_Lock_State);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
    CLB_lock = 0;
    return 0;
  } 
}  

//-----------------------------------------------------------------------------
uint8_t PFW_ManualK(char* parString)
{
 char const* lo_Auto_K = "Key factor auto = ";
 char const* lo_Sub_K = "Substitution Key factor = ";
 char *lo_K; 
 uint16_t lo_KF;
   
 if(parString ==0)   
  {
    if(manual_KL)
    {
      lo_K  =(char*)lo_Sub_K;
      lo_KF = manual_KL; 
    }  
    else
    {
      lo_K = (char*)lo_Auto_K;
      lo_KF = Sensor.KL;
    }  
    while(TransmitEnable == TRANSMIT_DISABLE);
     sprintf((char*)TransmitBuffer,"%s %u\r\n",\
             lo_K, lo_KF);  
     Opto_Uart_Transmit(); 
     return 0;
  }
  else
  {
   if (CLB_lock != 0x55)
      return 3;
   lo_KF = atoi(parString);
   if (lo_KF > 50000)
     return 2;
   manual_KL = lo_KF;
   return 0; 
  }
return 0xFF; 
} 

