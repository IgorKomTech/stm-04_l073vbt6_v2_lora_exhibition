/*
//%%%b 
��� �������������� �������, � �� ������
//%%%e 
*/
//%%%b 
//#include "FLASH.h"
#include "main_function.h"
#include "EEPROM.h"
#include "Archiv.h"
//%%%e 

//static uint8_t cnt_STR_SEND_OK=0;//###
//#include "stm32l0xx_hal.h"
#include "cmsis_os.h"
#include "GSMSIM800C.h"
#include "Global.h"
#include "ATCom_SIM800.h"
#include "USART_Modem.h"
#include "GasMeter.h"
#include "Configuration.h"
#include "ValveTask.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

// USER BEGIN
//extern _Setting Setting;
extern struct _FlagMessageTMR FlagMessageTMR;

extern TypeStatus StatusDevice;
extern TypeStatus StatusDevice_old_connect_TCP;
extern uint8_t flag_end_task;
extern uint8_t flag_change_server_url_valve;
extern uint8_t flag_opto_reconnect;
extern uint32_t additional_time_message;
// USER END


extern osMessageQId QueueGSM_RX_UARTHandle;
uint8_t cnt_element_QueueGSM_RX_UART;
portBASE_TYPE xStatus;
uint8_t GSMflag_power; // GSM_MUST_ENABLE - ���� �������� ������, GSM_MUST_DISABLE - ���� ��������� ������
Typeflag_BT GSMflag_BT;// BT_MUST_ENABLE - ���� �������� ������, BT_MUST_DISABLE - ���� ��������� ������
Typeflag_TCP GSMflag_TCP;
Typeflag_CALL GSMflag_CALL;
Typeflag_SMSOutput GSMflag_SMSOutput;
Typeflag_CALLOutput GSMflag_CALLOutput;



uint32_t pin_DTR;
GPIO_TypeDef *ptr_PxOUT_DTR;
GPIO_TypeDef *ptr_PxOUT_Power;
uint32_t pin_power;
GPIO_TypeDef *ptr_PxOUT_Pwrkey;
uint32_t pin_pwrkey;
USART_TypeDef *GSM_USART;

uint8_t availability_of_2_SIMcards;
GPIO_TypeDef *ptr_PORT_select_SIMcard;
uint16_t pin_select_SIMcard;
uint8_t  gsm_imei[16];
//int32_t delay_connect_internet = 0;
uint8_t cnt_error_TCP_connect=0;
uint16_t delay_receive_for_gsm_sms_ready;
uint32_t tick_count_message_from_LORA_msec;
uint32_t current_communication_gsm_number=0;
uint32_t tick_count_powerON_LORA_msec = 0;
uint32_t tick_count_5sek = 0;                      // ���������� ��� ������� 5 ��� ����� �������� ������

uint8_t *ptr_buf_data_TCP;
uint8_t flag_buf_data_TCP=0; // 0 - ���������� ���� ����� ��� ���������, 
                             // 1 - ������������ ��������� �� ����� ��� �������� �������� ������ �� ���  �������, ����� �������� send ok
                             // 2 - ������������ ���������.
uint32_t cnt_byte_data_TCP=0;
uint8_t flag_cnt_byte_data_TCP=0;
uint8_t cntSEND_FAIL=0;
uint8_t cnt_error_CGATT_1 = 0;
uint8_t cnt_error_CGATT_0 = 0;
uint8_t cnt_error_CGATT_1_common = 0;
//uint8_t cnt_error_AT_CIICR = 0;
//uint8_t cnt_error_AT_CIFSR = 0;
char transmit_data_LORA_str[220];           // ��������������� ������ ��� �������� ������ �� LORA 
char transmit_data_LORA_str_hex[440];       // ������ �������������� � 16-������ ������� ��� �������� ������ �� LORA 440 - ����. ����� �������� ������� ��������� � ����� ������
char device_EUI[20], application_EUI[20];   // ������ ��� ��������� ���������������
int window_input, port_input, downlink_command; // ���������� ��� �������� ���������� ������ �� �������

uint32_t count_time_ATT_TRANSMIT_old = 0;
uint32_t count_time_ATT_TRANSMIT;
uint32_t pause_transmission;
char num = 1;
char BufferPause[20][64];
char count_ExtremalPowerDown;

const uint32_t mas_speed_uart[CNT_SPEED_UART] = {38400, 57600, 115200, 9600, 19200, 230400};
// 4800 - �� ��������� HAL ���������� ��� ������� ���� 32 ���
// ��� ����������� �������� ������
struct _GSMFlag GSMFlag;
struct _GSMStatus GSMStatus;
struct _GSMInputSMS GSMInputSMS;
struct _DataArchiveTransmitLORA DataArchiveTransmitLORA; // ��������� ��� �������� ������ ���������� ��� ������������ ��������� ������, ��� �������� �� � ���������� �� LORE
struct _GSMFlagMessage GSMFlagMessage;
_GSMFlagMessageTCP GSMFlagMessageTCP;
struct _GSMFlagMessageBT GSMFlagMessageBT;
struct _GSMFlagOutputSMS GSMFlagOutputSMS;
struct _GSMFlagOutputCALL GSMFlagOutputCALL;

#define CNT_ARRAY_STRING_GSMModule  4 
#define SIZE_ARRAY_ONE_STRING_GSMModule   250
uint8_t  GSMstring_GSMModule[CNT_ARRAY_STRING_GSMModule][SIZE_ARRAY_ONE_STRING_GSMModule]; // ������ ��� �������� ����
uint16_t  GSMcnt_string_GSMModule[CNT_ARRAY_STRING_GSMModule]; // ����� ���� � ���������� ���������
//uint8_t RX_Buff;     
uint8_t GSMcnt_message_for_gsm_module;// ����� �������� ���������
uint8_t number_indicator_message_for_gsm= 0;
uint8_t indicator_message_for_gsm = 0; // ������� ��������� �������� � ����������� ���������, 
                                      // ������������ ����� ������ ������ ��������� �� ��� �� ���������
                                      // �� ����������� ������ ������������ 1 ����
uint8_t *GSMptr_rx1; // ��������� �� ���� �������� ���������� �������� �� ������
uint8_t GSM_sequence_string[CNT_ARRAY_STRING_GSMModule] = {0}; // ������ ������������������ �������� ���������
uint8_t temp_number_indicator_message_for_gsm;
uint16_t GSMstatus_delay_init; // ����� ����� ��������������
#define BUF_STR_SIZE 600
uint8_t buf_str[BUF_STR_SIZE]; // ����� ��� ������ � ����������� �� ������� �� TCP
int8_t confirm_transmit_LORA;
uint8_t counter_failed_transmission_LORA = 0; // ������� ��������� ������� ����� ���������
uint8_t GSM_network_name[24];
uint8_t CCID_SIM_card[24];
uint8_t message_SIM_card_balance[50];
uint8_t BufferATTelNumberOutSMSMessages[100]; // �.�. ����� � ������� unicod
//uint8_t BufferATTelNumberOutSMSMessages[]="AT+CMGS=\"+79307000107\"\r";
// ������� (910 123 45 67) 20, 24,  28, 32, 36, 40, 44, 48, 52, 56

//-----------------------------------------------
TypePtrParserCmdLORA PtrParserCmdLORA[]=
{
  {"OK",                MES_LORA_STR_OK},
  {"OK,JOINED",         MES_LORA_STR_ATY_JOINED},
  {"OK,NOTJOINED",      MES_LORA_STR_ATY_NOTJOINED},
  {"EV_TXCOMPLETE,A0",  MES_LORA_STR_ATT_TXCOMPLETE},
  {"EV_TXCOMPLETE,N0",  MES_LORA_STR_ATT_TXNOTCOMPLETE},
  {"EV_RXCOMPLETE",     MES_LORA_STR_ATT_RXCOMPLETE},
  {"OK,049",            MES_LORA_STR_ATJ_GETTINGPARAMETERS},
  {"EV_JOINED",         MES_LORA_STR_ATJ_EV_JOINED},
  {"EV_JOIN_FAILED",    MES_LORA_STR_ATJ_EV_JOIN_FAILED},
  {"ERROR",             MES_LORA_STR_ERROR}  
};
const uint8_t CNT_STR_GSM = sizeof(PtrParserCmdLORA)/sizeof(PtrParserCmdLORA[0]);

//#####################################################################################

void GSM_Init(GPIO_TypeDef *_ptr_PxOUT_Power,              // ��������� �� ������� ����� PxOUT ������ ������������ ������� GSM ������
           uint32_t _pin_power,    // ����� ������ ������������ ������� GSM ������
           GPIO_TypeDef *_ptr_PxOUT_Pwrkey,              // ��������� �� ������� ����� PxOUT ������ PWRKEY
           uint32_t _pin_pwrkey,   // ����� ������
 //          USART_TypeDef *_USART,          // ��. �� ����� ����������� ���������������� UART ������
 //          uint8_t _availability_of_2_SIMcards, // ������� 2-� ����
 //          GPIO_TypeDef *_ptr_PORT_select_SIMcard,
 //          uint16_t _pin_select_SIMcard,
           GPIO_TypeDef *_ptr_PxOUT_DTR,              
           uint32_t _pin_DTR)
{
  
  
  ptr_PxOUT_Power = _ptr_PxOUT_Power;
  pin_power = _pin_power;
  ptr_PxOUT_Pwrkey = _ptr_PxOUT_Pwrkey;
  pin_pwrkey = _pin_pwrkey;
  ptr_PxOUT_DTR = _ptr_PxOUT_DTR;
  pin_DTR = _pin_DTR;
 // GSM_USART = _USART;
 // availability_of_2_SIMcards = _availability_of_2_SIMcards;
 // ptr_PORT_select_SIMcard = _ptr_PORT_select_SIMcard;
 // pin_select_SIMcard = _pin_select_SIMcard;   
  // ��������� ���������
  GSMflag_power = GSM_MUST_DISABLE;
  GSMflag_BT = BT_MUST_DISABLE;
 // HAL_GPIO_WritePin(_ptr_PxOUT_Power, _pin_power, GPIO_PIN_RESET);
//  HAL_GPIO_WritePin(_ptr_PxOUT_Pwrkey, _pin_pwrkey, GPIO_PIN_RESET); // ����� � M660 ON/OFF ����� ���������� � ���������. ���������� � ����, ��� �� ������ ���� ����������� � ���� ����������
//  HAL_GPIO_WritePin(ptr_PxOUT_DTR, pin_DTR, GPIO_PIN_RESET);
  // HAL_UART_Receive_IT(GSM_USART, &RX_Buff, 1);

  GSMStatus.power = POWER_NON;
  GSMStatus.init = INIT_NON;
  GSMStatus.speed = SPEED_NON_FOUND;
  GSMStatus.joined = LORA_ATY_NON_REQUEST_JOINED;
  GSMStatus.transmit = LORA_ATT_NON_TRANSMIT;
  GSMStatus.receive = LORA_ATT_NON_RECEIVE;
  GSMStatus.registration = LORA_ATJ_NON_REQUEST;
  GSMStatus.gprs = GPRS_NON_CONNECT;
  GSMStatus.TCP = TCP_NON_CONNECT;
  // ������������� �������� ������
  
}
//#####################################################################################
void LORAPower()
{
   // ���������
   if(GSMflag_power == GSM_MUST_ENABLE // ����� ������ ���� �������, �� ��� �� ��������� 
      && GSMStatus.power == POWER_NON) 
   {
       Mdm_HardwareInit(); // ������������� GPIO, UART2, DMA1, NVIC, ModemTransmitEnable
       
       // ���������� ��� �����
       additional_time_message = 0;
       memset((uint8_t*)&GSMFlag, 0, sizeof(GSMFlag));
       memset((uint8_t*)&GSMFlagMessage, 0, sizeof(GSMFlagMessage));
       memset((uint8_t*)&GSMStatus, 0, sizeof(GSMStatus)); 
       // ���������� ������������ ������ ����� �� ��������� ������ �� �� ������� ��� �������� � ����������
       // �-� ������ ���������� ����� � ������� ������� ������������. ��� ������ ���������� ������
       tick_count_powerON_LORA_msec = xTaskGetTickCount(); 
     
       GSM_PWR_ON(); // ������ �������� ������ �� ���� GSM Power EN
       //++HiPower_ON;
       flag_event = EVENT_POWER_ON_LORA_MODEM;
       
       ++current_communication_gsm_number;    // ������� ������ �����
       Eeprom_WriteDword((uint32_t)&current_communication_gsm_number_eeprom,  current_communication_gsm_number);
       
       osDelay(1000);
       GSMStatus.power = GSM_POWER_ON;
       TransmitDebugMessageOptic("POWER_ON\r\n"); // ����� ��������� ����� ������ �� ��
       tick_count_message_from_LORA_msec = xTaskGetTickCount(); // ���������� ������������ ������� ������� ������ ����� �������� �� �������, ��� �������� � ���������� �����
       GSMStatus.speed = SPEED_NON_FOUND; // ��� ��������� �� ����� �������� ������
       GSMStatus.joined = LORA_ATY_NON_REQUEST_JOINED; // ��� ��������� �� ����� ����������� �� �����
       GSMStatus.transmit = LORA_ATT_NON_TRANSMIT;
       GSMStatus.receive = LORA_ATT_NON_RECEIVE;
       GSMStatus.registration = LORA_ATJ_NON_REQUEST;
       
   }
   

   // ����������
   if(GSMflag_power == GSM_MUST_DISABLE  // ����� ������ ���� ��������, �� ��� �� ����������
      && GSMStatus.power == GSM_POWER_ON)
   {
         //if (HiPower_ON)
         //  if ((--HiPower_ON) ==0)
              GSM_PWR_OFF();         // ������ ������� ������ �� ���� GSM Power EN 
         flag_event = EVENT_POWER_OFF_LORA_MODEM;
         GSMStatus.power = POWER_NON;
         memset((uint8_t*)&GSMFlagMessage, 0, sizeof(GSMFlagMessage));
         Mdm_HardwareDeInit();

         
         TransmitDebugMessageOptic("POWER_OFF\r\n");
      
   }
}
//#####################################################################################
uint8_t FindSpeedUARTGSMModule()
{
  // ���� �������� ��� �� ���������� � ����� �������
   if((GSMStatus.speed == SPEED_NON_FOUND) && (GSMStatus.power == GSM_POWER_ON))
   {
     // ��������� �� ����, ������ �������� UART �� �������������, ��� ������ � ������ ��������������� ��������
      for(uint8_t i=0; i < CNT_SPEED_UART; i++)
      {
        // ������������� ������ �� ���������� �� ������� mas_speed_uart[i] 
        Mdm_USARTInit(mas_speed_uart[i]); 
        //---------------------------
        for(int j=0; j < 5; j++)
        {
          GSMFlagMessage.AT = SENT_MESSAGE;
          Mdm_Transmit2(AT_AT); 
          osDelay(1000);
          if(GSMFlagMessage.AT == ACCEPTED_MESSAGE) // �������� ����� �� ��
          {
            // ���� ��� ��������� ������� ������� ������� �������� ���������� ������ ������ ������� CalculationNextCommunicationSession();
            successful_transfer_data_to_server = _OK; 
            GSMStatus.speed = SPEED_FOUND;
            TransmitDebugMessageOptic("SPEED_FOUND\r\n"); // ����� ��������� ����� ������ �� ��
            return GSMStatus.speed;
          }
        }
      }
      // �� ���� �������� �� �������. ������.
      GSMStatus.speed = SPEED_FOUND_ERROR;
      TransmitDebugMessageOptic("SPEED_FOUND_ERROR\r\n");
   }
   return GSMStatus.speed;
}
uint8_t RequestRegistration()
{
  if((GSMStatus.joined == LORA_ATY_NON_REQUEST_JOINED) && (GSMStatus.power == GSM_POWER_ON)) // ������� ���� ��� �� ������� ������
  {
        GSMFlagMessage.AT_ATY = SENT_MESSAGE;     // ���� �������� ������� AT_ATY
        GSMStatus.joined = LORA_ATY_REQUEST_JOINED;        
        Mdm_Transmit2(AT_ATY);                    // ������ �� ��������� ���������
        flag_event = EVENT_REQUEST_PARAMETERS_LORA_MODEM;
  }
  return GSMStatus.joined;
}

void TransmitDataLORA()
{
  tick_count_5sek = xTaskGetTickCount();
  if((GSMStatus.transmit == LORA_ATT_NON_TRANSMIT) && (tick_count_5sek - tick_count_powerON_LORA_msec >= 10000)) // ������� ���� ��� �� ���������� ������
  {
    tick_count_powerON_LORA_msec = xTaskGetTickCount();
    uint32_t *ptrStatus;    // ��������� �� ��������� status
    uint8_t *ptrLoraStatus;    // ��������� �� ��������� statusWarning
    uint32_t cntGas;        // ����� ����
    // type_device_int - ��� ��������
    GetDate_Time(&SystDate,&SystTime);           // ��������� ��������� ���� � �������
    ptrStatus = (uint32_t*)&StatusDevice;        // ��������� TypeStatus
    LoraStatus.Qmax_warning = StatusWarning.Qmax_warning;
    LoraStatus.Tmax_min_warning = StatusWarning.Tmax_min_warning;
    ptrLoraStatus = (uint8_t*)&LoraStatus;
    cntGas = (uint32_t)(GasMeter.VE_Lm3 * 1000); // �������� �� 1000 ����� ������������ � ������
    sprintf (transmit_data_LORA_str,"%dL;%s;%.2d%.2d%.2d;%08X;%lu;%02X;%d\r\n", 
                                                type_device_int,                // ��� �������� (int)
                                                (uint8_t*)Device_STM.SN_Device, // �������� ����� ����������
                                                //Device_STM.FW_major,          // (������ ��
                                                Device_STM.FW_minor,            // �������  
                                                FW_GSM_VERSION,                 // �� 4
                                                FW_TEST_VERSION,                // ��������)                                        
                                                *ptrStatus,                     // ��������� �� ��������� ������
                                                cntGas,                         // ������� ������
                                                *ptrLoraStatus,                 // ������ ��������������
                                                Sensor.KL);                     // � ������
    
    TransmitDebugMessageOptic((uint8_t*)&transmit_data_LORA_str);                // ������� � ������ ��������������� ������ ������ ������� ����� ���������� 
    ToHex((uint8_t*)&transmit_data_LORA_str, (uint8_t*)&transmit_data_LORA_str_hex); // ��������� ��������. ������ � ���������������� ������ ��� ��������
    sprintf((char*)buf_str, (char*)AT_ATT, transmit_data_LORA_str_hex);              // ��������� �� ������� �� ������� 
    GSMFlagMessage.AT_ATT = SENT_MESSAGE; // ���� �������� ���������
    GSMStatus.transmit = LORA_ATT_WAIT;   // ������ ������ 
    Mdm_Transmit2(buf_str);               // ����������
    flag_event = EVENT_TRANSMIT_DATA_LORA_MODEM;
  }
  if (GSMFlagMessage.AT_ATT == ACCEPTED_MESSAGE) // ���� ������ ����� �� ATT ������� ������� �����
  {
    osDelay(100);
    if (GSMStatus.transmit == LORA_ATT_TRANSMIT)
    {    
       TransmitDebugMessageOptic("������ ������������� �� �������� �������� ������\r\n\r\n");
       confirm_transmit_LORA = 1;
       flag_event = EVENT_CONFIRM_TRANSMIT_DATA_LORA_MODEM;
         if ( StatusWarning.body_cover_open == 0)
         {
           LoraStatus.body_cover_open = 0;
         }
         if ( StatusWarning.battery_cover_open == 0)
         {
           LoraStatus.battery_cover_open = 0;
         }
         if ( StatusWarning.SIM_card_non == 0)
         {
           LoraStatus.SIM_card_non = 0;
         }
         if ( StatusWarning.battery_not_connected == 0)
         {
           LoraStatus.battery_not_connected = 0;
         }
       // --- ������� ���� ����� ��������� ��������� ---------------------------
       if (count_time_ATT_TRANSMIT_old)
       {
         count_time_ATT_TRANSMIT = xTaskGetTickCount(); // ��������� ������� ����� 
         pause_transmission = count_time_ATT_TRANSMIT - count_time_ATT_TRANSMIT_old;
         if (pause_transmission > 30000) // ���� ������ ������ ���.
         {
           sprintf(BufferPause[num],"%d %02dDay %02d:%02d:%02d; pause=%d; �ount ExtremalPowerDown=%d;\r\n",num,SystDate.Day,SystTime.Hours,SystTime.Minutes,SystTime.Seconds,pause_transmission,count_ExtremalPowerDown);
           num++;
           
         }
       count_time_ATT_TRANSMIT_old = count_time_ATT_TRANSMIT;  
       }
       else  count_time_ATT_TRANSMIT_old = xTaskGetTickCount();
       // ----------------------------------------------------------------------
       // --- ���� ������� downlink ��������� ----------------------------------
       if (GSMStatus.receive == LORA_ATT_RECEIVE)
       {
         GSMStatus.receive = LORA_ATT_NON_RECEIVE;
         TransmitDebugMessageOptic("�������� ������"); 
         sscanf((char*)GSMstring_GSMModule[number_indicator_message_for_gsm-1],"EV_RXCOMPLETE,%d,%d,%d", &window_input, &port_input, &downlink_command); // ���������� downlink ���������
         // --- ���������� �������� --------------------------------------------
         if((downlink_command == 0 || downlink_command == 1))
         {
           sprintf((char*)OldValue,"%d", valve_resolution);
           Eeprom_WriteChar((uint32_t)&valve_resolution, downlink_command);

           sprintf((char*)NewValue,"%d", valve_resolution); 
           WriteChangeArcRecord(CHANGE_VALVE_RESOLUTION, CHANGE_TCP); 

           if(valve_resolution == 1)
           {
             valve_action = VALVE_ACTION_OPEN_TCP;
             TransmitDebugMessageOptic("�������� �������"); 
           }
           else
           {
             valve_action = VALVE_ACTION_CLOSE_TCP;
             TransmitDebugMessageOptic("�������� �������");
           }
           
           flag_change_server_url_valve = _OK;
         }
       }
       // ----------------------------------------------------------------------
       //GSM_Set_flag_power(GSM_MUST_DISABLE);       // ������ ����, ��� ����� ������ ���� �������� 
    }
    if (GSMStatus.transmit == LORA_ATT_NOTTRANSMIT)
    {
       //GSM_Set_flag_power(GSM_MUST_DISABLE);       // ������ ����, ��� ����� ������ ���� ��������
       
       TransmitDebugMessageOptic("������������� �� �������� �������� ������ �� ����\r\n\r\n");       
       confirm_transmit_LORA = 0;
    }
    //--------------------------------------------------------------------------
    GSMStatus.transmit = LORA_ATT_NON_TRANSMIT;
    //--------------------------------------------------------------------------
  } 
}

void RegistrationModemLORA()
{
  // ����������� ��������� ��������� ATJ?
  if((GSMStatus.registration == LORA_ATJ_NON_REQUEST) && (GSMStatus.power == GSM_POWER_ON)) // ������� ���� ��� �� ��������� ������
  {
    GSMFlagMessage.AT_ATJque = SENT_MESSAGE;        // ���� �������� ���������
    GSMStatus.registration = LORA_ATJ_REQUEST;      // ������ ������
    Mdm_Transmit2(AT_ATJque);                       // ���������� ������ ATJ?
    flag_event = EVENT_REGISTRATION_LORA_MODEM;
  }
  // ���������� ��������� ��������� ATJ<...>
  if(GSMStatus.registration == LORA_ATJ_GETTING_PARAMETER) 
  {
    // ������ device EUI, application EUI
    sscanf((char*)GSMstring_GSMModule[number_indicator_message_for_gsm-1],"OK,%[^,],%s", device_EUI, application_EUI);
    //TransmitDebugMessageOptic((uint8_t*)&device_EUI);
    //TransmitDebugMessageOptic((uint8_t*)&application_EUI);
    sprintf((char*)buf_str, (char*)AT_ATJinst, device_EUI, application_EUI);
    GSMFlagMessage.AT_ATJinst = SENT_MESSAGE;              // ���� �������� ���������
    GSMStatus.registration = LORA_ATJ_SETTING_PARAMETER;   // �������� ���� ������
    Mdm_Transmit2(buf_str);                                // ��������    
  }
  // ���������� ���������� ATJ
  if(GSMStatus.registration == LORA_ATJ_READY_FOR_ACTIVATION)
  {
    additional_time_message = 180000;
    GSMFlagMessage.AT_ATJ = SENT_MESSAGE;                  // ���� �������� ���������
    GSMStatus.registration = LORA_ATJ_ACTIVATION_WAIT;     // �������� ���� ������
    Mdm_Transmit2(AT_ATJ);                                 // ��������

  }
  // ����������� ��������� �������
  if(GSMStatus.registration == LORA_ATJ_JOINED)
  {
    additional_time_message = 0;
    GSMStatus.joined = LORA_ATY_NON_REQUEST_JOINED;        // ��� ��� �������� ��������� ������ (ATY) � ��������� ������ (ATT)
    GSMStatus.transmit = LORA_ATT_NON_TRANSMIT;
    flag_opto_reconnect = 0;
  }
  // ����������� ��������� ��������
  if(GSMStatus.registration == LORA_ATJ_JOIN_FAILED)
  {
    //GSM_Set_flag_power(GSM_MUST_DISABLE);                  // ��������� �����
    flag_opto_reconnect = 0;
  }  
}

//#####################################################################################
void InitGSMModule()
{
   if(GSMStatus.power == GSM_POWER_ON
      && GSMStatus.init != INIT_OK
      && GSMStatus.speed == SPEED_FOUND )
   {
      if(GSMFlagMessage.IPR == NON_MESSAGE)
      {
         GSMFlagMessage.IPR = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_IPR_57600, sizeof(AT_IPR_57600)-1);
         Mdm_Transmit2(AT_IPR_38400);
      }
      if(GSMFlagMessage.IPR == ACCEPTED_MESSAGE && GSMFlagMessage.AT_W == NON_MESSAGE)
      {
       //  (*GSM_USART).Init.BaudRate = 57600;
       //  if (HAL_UART_Init(GSM_USART) != HAL_OK)
       //  {
      //      _Error_Handler(__FILE__, 2);
      //   }
      //   HAL_UART_Receive_IT(GSM_USART, &RX_Buff, 1);
         Mdm_USARTInit(SPEED_UART_DEFAULT);
         osDelay(100); // ��������, �.�. �� ������ ����� �� ����� ������������� �������
         GSMFlagMessage.AT_W = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_W, sizeof(AT_W)-1);
         Mdm_Transmit2(AT_W);
      }
/*
      if(GSMFlagMessage.AT_W == ACCEPTED_MESSAGE && GSMFlagMessage.AT_BTHOST == NON_MESSAGE)
      {
         GSMFlagMessage.AT_BTHOST = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_BTHOST, sizeof(AT_BTHOST)-1);
         
         Mdm_Transmit2(AT_BTHOST);
      }
      */
      if(GSMFlagMessage.AT_W == ACCEPTED_MESSAGE && GSMFlagMessage.AT_ATE0 == NON_MESSAGE)
      {
         GSMFlagMessage.AT_ATE0 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_ATE0, sizeof(AT_ATE0)-1);
         Mdm_Transmit2(AT_ATE0);
      }
      // �������� SIM �����
      if(GSMFlagMessage.AT_ATE0 == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CCID == NON_MESSAGE)
      {
        
         //WriteEvent(EVENT_PARAM_GSM_MODEM, EVENT_OK, GSM_Get_current_communication_gsm_number() + 1, EVENT_END);// EVENT_END EVENT_BEGIN
         //flag_event = flag_event_menu = EVENT_CHECK_SIM_CARD;
         //WriteEvent(EVENT_CHECK_SIM_CARD, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
        
         GSMFlagMessage.AT_CCID = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CCID, sizeof(AT_CCID)-1);
         Mdm_Transmit2(AT_CCID);
      }
      if(GSMFlagMessage.AT_CCID == ACCEPTED_MESSAGE_ERROR)
      {
        GSMStatus.init = SIM_CARD_ERROR;
      }
      if(GSMFlagMessage.AT_CCID == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CGSN == NON_MESSAGE)
      {
         GSMFlagMessage.AT_CGSN = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CGSN, sizeof(AT_CGSN)-1);
         Mdm_Transmit2(AT_CGSN);
      }
      if(GSMFlagMessage.AT_CGSN == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CREG == NON_MESSAGE)
      {
         ++current_communication_gsm_number;    
         Eeprom_WriteDword((uint32_t)&current_communication_gsm_number_eeprom,  current_communication_gsm_number);
         //WriteEvent(EVENT_CHECK_SIM_CARD, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN
         //flag_event = flag_event_menu = EVENT_CHECK_REGISTRATION_GSM;
         //WriteEvent(EVENT_CHECK_REGISTRATION_GSM, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
        
         GSMFlagMessage.AT_CREG = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CGSN, sizeof(AT_CGSN)-1);
         Mdm_Transmit2(AT_CREG);
      }
      
      
      
      
      //###
   /*   if(GSMFlagMessage.AT_CGSN == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CSCLK_1 == NON_MESSAGE)
      {// ������� ��� ��������� ������� DTR
         GSMFlagMessage.AT_CSCLK_1 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CSCLK_1, sizeof(AT_CSCLK_1)-1);
         Mdm_Transmit2(AT_CSCLK_1);
      }*/
      // ������� �������
     // if(GSMFlagMessage.AT_CSCLK_1 == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CSQ == NON_MESSAGE)
      if(GSMFlagMessage.AT_CREG == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CSQ == NON_MESSAGE)
      {
         //%%%b
         //WriteEvent(EVENT_CHECK_REGISTRATION_GSM, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN
         //flag_event = flag_event_menu = EVENT_CHECK_LEVEL_GSM_SIGNAL;
         //WriteEvent(EVENT_CHECK_LEVEL_GSM_SIGNAL, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         //%%%e
         
         StatusDevice_old_connect_TCP = StatusDevice;
         
         GSMFlagMessage.AT_CSQ = SENT_MESSAGE;
      //   HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CSQ, sizeof(AT_CSQ)-1);
           Mdm_Transmit2(AT_CSQ);
      }
      // ��� GSM ����
      if(GSMFlagMessage.AT_CSQ == ACCEPTED_MESSAGE && GSMFlagMessage.AT_COPS == NON_MESSAGE)
      {
        
         //WriteEvent(EVENT_CHECK_LEVEL_GSM_SIGNAL, �onfirm_transmit_LORA, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN

        
         GSMFlagMessage.AT_COPS = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_COPS, sizeof(AT_COPS)-1);
          Mdm_Transmit2(AT_COPS);
      }

      // ������ SIM �����
      if(GSMFlagMessage.AT_COPS == ACCEPTED_MESSAGE && GSMFlagMessage.AT_CUSD == NON_MESSAGE)
      {
         if(MdmNumberBalans[0] != 0)
         {
            memset(buf_str, 0, BUF_STR_SIZE);
            sprintf((char*)buf_str, (char*)AT_CUSD, (char*)MdmNumberBalans);
            GSMFlagMessage.AT_CUSD = SENT_MESSAGE;
          //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)buf_str, strlen((char*)buf_str));
            Mdm_Transmit2(buf_str);
         }
         else
         {
            __NOP();
            GSMFlagMessage.AT_CUSD = ACCEPTED_MESSAGE;
            sprintf((char*)message_SIM_card_balance, "%s", "NO NUMBER PHONE BALANCE");
         }
      }      
      //------------------------------------------------
      if(GSMFlagMessage.AT_CUSD == ACCEPTED_MESSAGE)
      {
         GSMStatus.init = INIT_OK;
         //%%%b
    //     WriteArchiveSystemToFlash(GSM_CHECK_GSM, (int8_t)�onfirm_transmit_LORA, SYSTEM_EVENT_END);
    //     WriteArchiveSystemToFlash(GSM_GPRS_UP, 0, SYSTEM_EVENT_BEGIN);
         //%%%e
         GSMstatus_delay_init = 0;      // ������ ������� ������� ����� �������������  
      }
   }
}

//#####################################################################################
void      GSM_Set_TCPFlag(Typeflag_TCP _GSMflag_TCP)
{
  GSMflag_TCP = _GSMflag_TCP;
  if(GSMflag_TCP == TCP_MUST_DISABLE)
  {
     memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
     GSMStatus.gprs = GPRS_NON_CONNECT;
     GSMStatus.TCP = TCP_NON_CONNECT;
  }
}
//#####################################################################################
void GSM_Set_flag_power(uint8_t _GSMflag_power)
{
   if(GSMflag_power == GSM_MUST_DISABLE && _GSMflag_power == GSM_MUST_ENABLE)
      memset((uint8_t*)&GSMStatus, 0, sizeof(GSMStatus));     
   GSMflag_power = _GSMflag_power;
   tick_count_message_from_LORA_msec = xTaskGetTickCount();
   if(GSMflag_power == GSM_MUST_DISABLE)
   {
      GSMflag_BT = BT_MUST_DISABLE;
      GSMflag_TCP = TCP_MUST_DISABLE;
      GSMStatus.BT = BT_NON_CONNECT;
      GSMFlagOutputSMS.status = SMS_NON_TRANSMITTED;
      GSMflag_SMSOutput = SMS_NON_MUST_OUTPUT;
      GSMflag_CALLOutput = CALL_NON_MUST_OUTPUT;
   }
}     
//#####################################################################################
//������/��������� ��������
uint8_t GSM_Get_flag_power()
{
   return GSMflag_power;
}
//#####################################################################################
void GSM_increment_delay()
{
   ++GSMstatus_delay_init;
   //++delay_connect_internet;
   ++delay_receive_for_gsm_sms_ready;
}
//#####################################################################################
void GSM_RX_IT(uint8_t RX_Buff)
{
  
 //  HAL_UART_Receive_IT(GSM_USART, &RX_Buff, 1);
   // uint8_t flag_buf_data_TCP=0; // 0 - ���������� ���� ����� ��� ���������, 
                             // 1 - ������������ ��������� �� ����� ��� �������� �������� ������ �� ���  �������, ����� �������� send ok
                             // 2 - ������������ ���������.
   /* v0.17
   if(flag_buf_data_TCP == 2 && cnt_byte_data_TCP)
   {
     if(RX_Buff == 0x0A && !flag_cnt_byte_data_TCP)
     {// ��� ������ ����� SEND OK 0x0D
        flag_cnt_byte_data_TCP = 1;
     }
     else
     {
       flag_cnt_byte_data_TCP = 1;
        *ptr_buf_data_TCP++ = RX_Buff;
        --cnt_byte_data_TCP;
        if(cnt_byte_data_TCP == 0)
          flag_buf_data_TCP = 0;       
     }
      return;
   }
   */
/*   if(flag_buf_data_TCP == 2)
   { // ����� �������� ����� ������ ��� ������������ ���������� DMA ������� � ��� �������
     flag_buf_data_TCP = 0; 
     return;
   }
     */
   if(!GSMFlag.read_string_gsm)//���� ���� ���������
   {// ��������� �� SIM900 ��� �� �����������
      if(RX_Buff == 0x0D)
      { 
         if(!GSMFlag.simbol_end_0D) // ���� ��� ������ ������ ������ ������
            GSMFlag.simbol_end_0D = 1;// ��������, ��� �� ������
      }
      else if(RX_Buff == 0x0A)
      {
         if(GSMFlag.simbol_end_0D) // ��� ������ 0x0D?
         {
            GSMFlag.simbol_end_0D = 0; // ���������� ����
            GSMFlag.read_string_gsm = 1; // ��������� ������� �� ���� ���������
            if(GSMcnt_message_for_gsm_module == (CNT_ARRAY_STRING_GSMModule-1))
                GSMcnt_message_for_gsm_module = 0; // �������� �������, ������ �� ������������ ���������
            number_indicator_message_for_gsm = Find_null_bite(indicator_message_for_gsm, CNT_ARRAY_STRING_GSMModule); // ���� ������ ���������� ������� ���
            cnt_element_QueueGSM_RX_UART = uxQueueMessagesWaitingFromISR(QueueGSM_RX_UARTHandle);
	    if(number_indicator_message_for_gsm && cnt_element_QueueGSM_RX_UART < CNT_ARRAY_STRING_GSMModule)
            {// ���� ���� ���������� ���������
               if(GSMInputSMS.CMGR_plus)
               {// ��������� ����� ������� �������� ���   
                  GSMptr_rx1 = GSMInputSMS.SMSText;
               }
               else
               {   
                  GSMptr_rx1 = &GSMstring_GSMModule[number_indicator_message_for_gsm-1][0]; // ��������� �� ����� ���������
                  GSMcnt_string_GSMModule[number_indicator_message_for_gsm-1] = 0; // �������� ������� ���� � ���������
               }
            }
            else
            {// ������ �����������
               GSMFlag.read_string_gsm = 0; // ���������� ���������
            }   
         }
      }
      else
      {// ����� �������, ��� ������� ������ ��� ���������   
         number_indicator_message_for_gsm = Find_null_bite(indicator_message_for_gsm, CNT_ARRAY_STRING_GSMModule);
         cnt_element_QueueGSM_RX_UART = uxQueueMessagesWaitingFromISR(QueueGSM_RX_UARTHandle);
         if(number_indicator_message_for_gsm && cnt_element_QueueGSM_RX_UART < CNT_ARRAY_STRING_GSMModule)
         {
            GSMFlag.read_string_gsm = 1; // ��������� ������� �� ���� ���������
       //     if(GSMcnt_message_for_gsm_module == (CNT_ARRAY_STRING_GSMModule-1)) 
       //        GSMcnt_message_for_gsm_module = 0; // �������� �������, ������ �� ������������ ���������
            GSMptr_rx1 = &GSMstring_GSMModule[number_indicator_message_for_gsm-1][0]; // ��������� �� ����� ���������
            GSMcnt_string_GSMModule[number_indicator_message_for_gsm-1] = 1; // �������� ������� ���� � ���������
            *GSMptr_rx1++ = RX_Buff;
            
                     // {// ���� �������� ������ ������ � ��� ���� ������
          if(RX_Buff == 0x3E)
          {
             // ���� �������� ������  ���� ������
             static portBASE_TYPE xHigherPriorityTaskWoken;
             xHigherPriorityTaskWoken = pdFALSE;
             *GSMptr_rx1 = 0;
             //++GSMcnt_message_for_gsm_module; // �������������� ������� �������� ���������
             GSMFlag.read_string_gsm = 0; // ��������� ������ ���������
             Change_bite(&indicator_message_for_gsm, number_indicator_message_for_gsm, 1);
             xQueueSendToBackFromISR(QueueGSM_RX_UARTHandle, &number_indicator_message_for_gsm, &xHigherPriorityTaskWoken);
          }
          
         }
         else
         {
            GSMFlag.read_string_gsm = 0; // ���������� ���������
         }
      }
   }
   else
   {// ������ ����������� ���������  
//---------------------------------------------------------------------------------- 
    // �������������� ���� ���������
       tick_count_message_from_LORA_msec = xTaskGetTickCountFromISR();
       if(RX_Buff == 0x0D || RX_Buff == 0x0A)
       {// ������� ���������� ���������, ��� ��������� ������ 0x0A
       
           if(RX_Buff == 0x0A) // ������, ��� ������ ��������� ������ 0x0A ��� �������������.  
           {// ������ ����� ��� �����������
              if(GSMInputSMS.CMGR_plus)
              {
              GSMInputSMS.SMSTextOK = 1;
              GSMInputSMS.CntByteText = GSMcnt_string_GSMModule[number_indicator_message_for_gsm];
              }
              else
              {
              //#           uint8_t number_sequence_string_SIM900;
               static portBASE_TYPE xHigherPriorityTaskWoken;
               xHigherPriorityTaskWoken = pdFALSE;
              //#  ++GSMcnt_message_for_gsm_module; // �������������� ������� �������� ���������
              
              GSMFlag.read_string_gsm = 0; // ��������� ������ ���������
              
              Change_bite(&indicator_message_for_gsm, number_indicator_message_for_gsm, 1);
              xQueueSendToBackFromISR(QueueGSM_RX_UARTHandle, &number_indicator_message_for_gsm, &xHigherPriorityTaskWoken);
              //# number_sequence_string_SIM900 = Find_null_byte(GSM_sequence_string, CNT_ARRAY_STRING_GSMModule);
              // ��� ��� ������� ������������ ������ �� ����� ����
              //# GSM_sequence_string[number_sequence_string_SIM900-1] = number_indicator_message_for_gsm;
              }
              *GSMptr_rx1 = 0; // ��������� ��������� ����
           }
       } 
       else // ����� ������ �� ���������
       {
          if(GSMcnt_string_GSMModule[number_indicator_message_for_gsm-1] < (SIZE_ARRAY_ONE_STRING_GSMModule-2))
          { 
             *GSMptr_rx1++ = RX_Buff;
             ++GSMcnt_string_GSMModule[number_indicator_message_for_gsm-1];// �������������� ����� ���� � ���������� ���������
          }
         // if((RX_Buff == 0x20)&&(*(GSMptr_rx1-2) == 0x3E))
         // {// ���� �������� ������ ������ � ��� ���� ������
          // {// ���� �������� ������ ���� ������
          if(RX_Buff == 0x3E)
          {
             // ���� �������� ������  ���� ������
             static portBASE_TYPE xHigherPriorityTaskWoken;
             xHigherPriorityTaskWoken = pdFALSE;
             *GSMptr_rx1 = 0;
             //++GSMcnt_message_for_gsm_module; // �������������� ������� �������� ���������
             GSMFlag.read_string_gsm = 0; // ��������� ������ ���������
             Change_bite(&indicator_message_for_gsm, number_indicator_message_for_gsm, 1);
             xQueueSendToBackFromISR(QueueGSM_RX_UARTHandle, &number_indicator_message_for_gsm, &xHigherPriorityTaskWoken);
          }    
       }
   }
}    
//#####################################################################################
void fGSM_RX_UARTTask() // ������ ��� ����������� ������ �� ������
{
   uint8_t rez_find = _FALSE;
   uint16_t size;
   xStatus = xQueuePeek( QueueGSM_RX_UARTHandle, &temp_number_indicator_message_for_gsm, portMAX_DELAY ); //
   flag_end_task |= 0x02;
   if( xStatus == pdPASS )
   {
      // ������ ������� ������� �� ������� ��� ��������� 
      // The data was successfully received from the queue without cutting out
     TransmitDebugMessageOptic((uint8_t*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]);
     size = strlen((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]);
     for(uint8_t i = 0; i < CNT_STR_GSM; i++)
     {
        if(strstr((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], PtrParserCmdLORA[i].Str) != 0)
      //  if(strcmp((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], PtrParserCmdLORA[i].Str) == 0)  
        {// �������
           if((i == 0 && size > 3) /*|| (i == 1 && size > 6)*/)
           { // ��� �� �� ��� ��������, � �� ������ ������
             __NOP();
           }
           else
           {
          //    TransmitDebugMessageOptic((uint8_t*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]);
              ParsingMessageForLORA(PtrParserCmdLORA[i].message_LORA);
              rez_find = _OK;
              break;
           }
        }
     }
     
     if(rez_find == _FALSE)
     {
        if(GSMReadSTR_Digit() == 1) 
          rez_find = _OK;
     }
     if(rez_find == _FALSE && GSMStatus.TCP == TCP_YES_CONNECT )
     {
     //  TransmitDebugMessageOptic((uint8_t*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]);
       if(ReadMessageTMR((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], 
                      GSMcnt_string_GSMModule[temp_number_indicator_message_for_gsm-1], 
                      GSM_TCP) == 0 && size > 5)
         FlagMessageTMR.command = MESSAGE_ERROR;
     }
     ///-----------------------------------------------------------------
     /*
      if(GSMStatus.power == GSM_POWER_ON && GSMStatus.init != INIT_OK)
      {  // ���� ������ �������� ������
         // If only enabled the module
         GSMReadMessageStartGSMModule();
      }
      else
      {
         GSMReadMessageGSMModule();
      }

*/
      //�������� ������ �� �������
      //We cut out the data from the queue
       xStatus = xQueueReceive( QueueGSM_RX_UARTHandle, &temp_number_indicator_message_for_gsm, portMAX_DELAY );
       if( xStatus == pdPASS )
       {
          // ������ ������� ������� �� ������� � ���������� 
          // The data was successfully received from the cut-off queue
          Change_bite(&indicator_message_for_gsm, temp_number_indicator_message_for_gsm, 0);
       }
   }   
}
//#####################################################################################
void ParsingMessageForLORA(uint8_t CmdLORA) // ����� ������� ������ �� ������
{
   switch(CmdLORA)
   {
   case MES_LORA_STR_ATY_JOINED:
        if(GSMFlagMessage.AT_ATY == SENT_MESSAGE)
        {
           GSMFlagMessage.AT_ATY = ACCEPTED_MESSAGE;
           GSMStatus.joined = LORA_ATY_JOINED;
        }
        break;
        
   case MES_LORA_STR_ATY_NOTJOINED:
        if(GSMFlagMessage.AT_ATY == SENT_MESSAGE)
        {
           GSMFlagMessage.AT_ATY = ACCEPTED_MESSAGE; 
           GSMStatus.joined = LORA_ATY_NOTJOINED;
        }
        break;
        
   case MES_LORA_STR_ATT_TXCOMPLETE:
         if(GSMFlagMessage.AT_ATT == SENT_MESSAGE)
        {
           GSMFlagMessage.AT_ATT = ACCEPTED_MESSAGE;
           GSMStatus.transmit = LORA_ATT_TRANSMIT;
        }
        break;
        
   case MES_LORA_STR_ATT_RXCOMPLETE:
         GSMFlagMessage.AT_ATT = ACCEPTED_MESSAGE;
         GSMStatus.receive = LORA_ATT_RECEIVE;
        
        break;
        
   case MES_LORA_STR_ATT_TXNOTCOMPLETE:
        if(GSMFlagMessage.AT_ATT == SENT_MESSAGE)
        {
           GSMFlagMessage.AT_ATT = ACCEPTED_MESSAGE;
           GSMStatus.transmit = LORA_ATT_NOTTRANSMIT;
        }
        break;
        
   case MES_LORA_STR_ATJ_GETTINGPARAMETERS:
        if (GSMFlagMessage.AT_ATJque == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATJque = ACCEPTED_MESSAGE;
          GSMStatus.registration = LORA_ATJ_GETTING_PARAMETER;
        }
        break;
        
   case MES_LORA_STR_ATJ_EV_JOINED:
        if (GSMFlagMessage.AT_ATJ == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATJ = ACCEPTED_MESSAGE;
          GSMStatus.registration = LORA_ATJ_JOINED;
        }
        break;
        
   case MES_LORA_STR_ATJ_EV_JOIN_FAILED:
        if (GSMFlagMessage.AT_ATJ == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATJ = ACCEPTED_MESSAGE;
          GSMStatus.registration = LORA_ATJ_JOIN_FAILED;
        }
        break;
        
   case MES_LORA_STR_OK:
        if(GSMFlagMessage.AT == SENT_MESSAGE)
        {
          GSMFlagMessage.AT = ACCEPTED_MESSAGE;                   // �������� �� �� ������� ��
        }
        if(GSMFlagMessage.AT_ATJinst == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATJinst = ACCEPTED_MESSAGE;           // �������� �� �� ������� AT_ATJinst
          GSMStatus.registration = LORA_ATJ_READY_FOR_ACTIVATION; // ����� ������ ���������
        }        
        break;
        
   case MES_LORA_STR_ERROR:
        if(GSMFlagMessage.AT == SENT_MESSAGE)
        {
          GSMFlagMessage.AT = ACCEPTED_MESSAGE;                   // �������� ERROR �� ������� ��
          TransmitDebugMessageOptic("�������� ERROR �� ������� ��");
        }
        if(GSMFlagMessage.AT_ATY == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATY = ACCEPTED_MESSAGE;                 // �������� ERROR �� ������� AT_ATY
          GSMStatus.joined = LORA_ATY_ERROR;
          TransmitDebugMessageOptic("�������� ERROR �� ������� AT_ATY");
        }
        if(GSMFlagMessage.AT_ATT == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATT = ACCEPTED_MESSAGE;                 // �������� ERROR �� ������� AT_ATT
          GSMStatus.transmit = LORA_ATT_ERROR;                  
          TransmitDebugMessageOptic("�������� ERROR �� ������� AT_ATT");
        }
        if(GSMFlagMessage.AT_ATJque == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATJque = ACCEPTED_MESSAGE;              // �������� ERROR �� ������� AT_ATJque
          GSMStatus.registration = LORA_ATJ_ERROR;
          TransmitDebugMessageOptic("�������� ERROR �� ������� AT_ATJque");
        }
        if(GSMFlagMessage.AT_ATJinst == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATJinst = ACCEPTED_MESSAGE;             // �������� ERROR �� ������� AT_ATJinst
          GSMStatus.registration = LORA_ATJ_ERROR;
          TransmitDebugMessageOptic("�������� ERROR �� ������� AT_ATJinst");
        }
        if(GSMFlagMessage.AT_ATJ == SENT_MESSAGE)
        {
          GSMFlagMessage.AT_ATJ = ACCEPTED_MESSAGE;                 // �������� ERROR �� ������� AT_ATJ
          GSMStatus.registration = LORA_ATJ_ERROR;
          TransmitDebugMessageOptic("�������� ERROR �� ������� _AT_ATJ");
        }
    
        GSMFlagMessage.ERROR = ACCEPTED_MESSAGE;
        //GSM_Set_flag_power(GSM_MUST_DISABLE);                       // �������� ������ ��������� �����
        break;  
   default:break;
   }
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
int DetectHexChr(char *In)
{
int i,ret=1;
for(i=0;i<8;i++)
	{
	if(!(In[i]>='0' && In[i]<='9') && !(In[i]>='A' && In[i]<='F'))
		{
		ret=0;
		break;
		}
	}
return ret;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
unsigned char HexToChr(char *str)
{
int i;
unsigned char chr=0;
for(i=0;i<2;i++)
	{
	chr <<= 4;
	if(str[i] >= '0' && str[i] <= '9')
		chr |=  str[i] - '0';
	else if(str[i] >= 'A' && str[i] <= 'F')
		chr |= str[i] - 'A' + 10;
	else if(str[i] >= 'a' && str[i] <= 'f')
		chr |= str[i] - 'a' + 10;
	}
return chr;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
char GetCP1252Code(uint16_t Ucs)
{
char Chr=(char)Ucs;
// ���������� ��� ������� '�'
if(Ucs==0x2116)
	Chr=0xB9;
// ���������� ��� ������� '�'
else if(Ucs==0x0451)
	Chr=0xB8;
// ���������� ��� ������� '�'
else if(Ucs==0x0401)
	Chr=0xA8;
// ��� ��������� ������� �����
else if(Ucs>=0x0410)
	Chr=Ucs-0x0410+0xC0;
return Chr;
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/

void DecodeUcs2(char *In, char *Out, int SizeOut)
{
uint16_t ChrUcs;
uint16_t len;
uint16_t i,k;
if(DetectHexChr(In))
	{
	len=strlen(In)/4;
	for(i=0,k=0;i<len;i++,k++)
		{
		if(k==SizeOut)
			break;
		ChrUcs=0;
		for(int j=0;j<4;j+=2)
			ChrUcs = (ChrUcs<<8) | HexToChr(In+(i*4)+j);
		Out[k]=GetCP1252Code(ChrUcs);
		}
	}
else
	strncpy(Out,In,SizeOut);
}
//#####################################################################################
uint8_t GSM_TCPConnect(const uint8_t *ptr_IPAddress, const uint8_t *ptr_port,
                       const uint8_t *ptr_APNAddress, const uint8_t *APN_login, const uint8_t *APN_passeord)
{
   if(GSMStatus.TCP != TCP_YES_CONNECT
      && GSMflag_TCP == TCP_MUST_ENABLE
      && GSMflag_BT == BT_MUST_DISABLE
      && GSMStatus.init == INIT_OK
      && GSMStatus.power == GSM_POWER_ON
      && GSMflag_power == GSM_MUST_ENABLE
      && GSMstatus_delay_init >= PAUSE_AFTER_POWER_ON) // ������ n ��� ����� �������������  
   {
      // �����
      if(GSMFlagMessageTCP.AT_CIPSHUT == NON_MESSAGE)
      {
         cnt_error_CGATT_1 = 0;
         cnt_error_CGATT_0 = 0;
         cnt_error_CGATT_1_common = 0;
         //cnt_error_AT_CIICR = 0;
         ///cnt_error_AT_CIFSR = 0;

         //flag_event = flag_event_menu = EVENT_CHECK_REGISTRATION_GPRS;
         //WriteEvent(EVENT_CHECK_REGISTRATION_GPRS, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
        
         //delay_connect_internet = 0;  //+++ v 0.03
         GSMStatus.TCP =  TCP_CONNECTING;
         //successful_transfer_data_to_server = _FALSE;
         GSMFlagMessageTCP.AT_CIPSHUT = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSHUT, sizeof(AT_CIPSHUT)-1);
         Mdm_Transmit2(AT_CIPSHUT);
      }
      // 1 ����������
      if(GSMFlagMessageTCP.AT_CIPSHUT == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CIPMUX_0 == NON_MESSAGE)
      {
         //LedBlueMode = LED_MODE_TCP_CONNECTION;
         osDelay(200);
         GSMFlagMessageTCP.AT_CIPMUX_0 = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPMUX_0, sizeof(AT_CIPMUX_0)-1);
         Mdm_Transmit2(AT_CIPMUX_0);
      }
      
      if(GSMFlagMessageTCP.AT_CIPMUX_0 == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CGATT_CHANGE == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CGATT_CHANGE = SENT_MESSAGE;
         Mdm_Transmit2(AT_CGATT_CHANGE);
      }
      
      if(GSMFlagMessageTCP.AT_CIICR == ACCEPTED_MESSAGE_ERROR 
         || GSMFlagMessageTCP.AT_CIFSR == ACCEPTED_MESSAGE_ERROR
         || GSMFlagMessageTCP.CONNECT_FAIL == ACCEPTED_MESSAGE)
      {
        /*
         GSMFlagMessageTCP.AT_CIICR = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CIFSR = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CIPSTART = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CGATT_CHANGE = ACCEPTED_MESSAGE_ERROR;
         GSMFlagMessage.ERROR = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CGATT_1 = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CGATT_0 = NON_MESSAGE;
        */
         memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
         GSMFlagMessage.ERROR = NON_MESSAGE;
         cnt_error_CGATT_0 = 0;
         cnt_error_CGATT_1 = 8;
         StatusDevice.GSMmodem_non_registration_GPRS = 1;
      }      

      // ���������� ������
      if((GSMFlagMessageTCP.AT_CGATT_CHANGE == ACCEPTED_MESSAGE 
         || GSMFlagMessageTCP.AT_CGATT_CHANGE == ACCEPTED_MESSAGE_ERROR)
         && GSMFlagMessageTCP.AT_CGATT_1 != ACCEPTED_MESSAGE)
      {
         if(GSMFlagMessageTCP.AT_CGATT_CHANGE == ACCEPTED_MESSAGE)
            GSMFlagMessageTCP.AT_CGATT_1 = ACCEPTED_MESSAGE;
         else
         {
            if(GSMFlagMessage.ERROR)
            {
               osDelay(7000);
               GSMFlagMessage.ERROR = 0;
               GSMFlagMessageTCP.AT_CGATT_1 = NON_MESSAGE;
               GSMFlagMessageTCP.AT_CGATT_0 = NON_MESSAGE;
               ++cnt_error_CGATT_1;
            }
            if(GSMFlagMessageTCP.AT_CGATT_1 == NON_MESSAGE)
            {
               if(cnt_error_CGATT_0 > 2)
               {
                  memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
                  GSMStatus.TCP = TCP_ERROR_CONNECT_INTERNET;
                //  GSMFlagMessageTCP.AT_CGATT_1 = ACCEPTED_MESSAGE;
                  StatusDevice.GSMmodem_non_registration_GPRS = 1;
               }
               else
               {
                  if(cnt_error_CGATT_1 < 8)
                  {
                     GSMFlagMessageTCP.AT_CGATT_1 = SENT_MESSAGE;
                     Mdm_Transmit2(AT_CGATT_1);
                     ++cnt_error_CGATT_1_common;
                     StatusDevice.GSMmodem_non_registration_GPRS = 0;
                     if(cnt_error_CGATT_1 == 7 && cnt_error_CGATT_0 == 2)
                       cnt_error_CGATT_0 = 3;
                     
                  }
                  else
                  {
                     GSMFlagMessageTCP.AT_CGATT_0 = SENT_MESSAGE;
                     GSMFlagMessageTCP.AT_CGATT_1 = ACCEPTED_MESSAGE_ERROR;
                     Mdm_Transmit2(AT_CGATT_0);
                     cnt_error_CGATT_1 = 0;
                     ++cnt_error_CGATT_0;
                     StatusDevice.GSMmodem_non_registration_GPRS = 1;
                  }
               }
            }
         }
      }
/*
      if(GSMFlagMessageTCP.CONNECT_FAIL == ACCEPTED_MESSAGE)
      {
        // GSMStatus.TCP = TCP_ERROR_CONNECT_INTERNET;
         StatusDevice.error_connect_server = 1;
         GSMFlagMessageTCP.AT_CGATT_1 = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CSTT = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CIICR = ACCEPTED_MESSAGE_ERROR;
      }
      */
      // Start Task and Set APN, USER NAME, PASSWORD
      if(GSMFlagMessageTCP.AT_CGATT_1 == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CSTT == NON_MESSAGE)
      {
        
         cnt_error_TCP_connect = 0;
         
         StatusDevice.GSMmodem_non_registration_GPRS = 0;
         
         osDelay(200);
         memset(buf_str, 0, BUF_STR_SIZE);
         sprintf((char*)buf_str, (char*)AT_CSTT, ptr_APNAddress, APN_login, APN_passeord);
         GSMFlagMessageTCP.AT_CSTT = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)buf_str, strlen((char*)buf_str));
           Mdm_Transmit2(buf_str);
         
      }
      // ����������

      
      if(GSMFlagMessageTCP.AT_CSTT == ACCEPTED_MESSAGE && 
         /*(*/GSMFlagMessageTCP.AT_CIICR == NON_MESSAGE/* || GSMFlagMessageTCP.AT_CIICR == ACCEPTED_MESSAGE_ERROR)*/)
      {
       //  if(cnt_error_AT_CIICR < 10)
       //  {
            osDelay(200);
            GSMFlagMessageTCP.AT_CIICR = SENT_MESSAGE;
            // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIICR, sizeof(AT_CIICR)-1);
            Mdm_Transmit2(AT_CIICR);
        // }
        // else
        /// {
          // GSMFlagMessageTCP.AT_CIICR = ACCEPTED_MESSAGE;
        // }
      }
      // �������� IP �����
      if(GSMFlagMessageTCP.AT_CIICR == ACCEPTED_MESSAGE 
         && /*(*/GSMFlagMessageTCP.AT_CIFSR == NON_MESSAGE /*|| GSMFlagMessageTCP.AT_CIFSR == ACCEPTED_MESSAGE_ERROR)*/)
      {
      //   if(cnt_error_AT_CIFSR < 21)
      //   {
            osDelay(200);
            GSMFlagMessageTCP.AT_CIFSR = SENT_MESSAGE;
        //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIFSR, sizeof(AT_CIFSR)-1);
            Mdm_Transmit2(AT_CIFSR);
       //  }
       // else
       //  {
       //    GSMFlagMessageTCP.AT_CIFSR = ACCEPTED_MESSAGE;
       //  }
      }
      // ��������� ��������� TCP �������
      if(GSMFlagMessageTCP.AT_CIFSR == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CIPSTART == NON_MESSAGE)
      {
         //WriteEvent(EVENT_CHECK_REGISTRATION_GPRS, cnt_error_CGATT_1_common, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
         //flag_event = flag_event_menu = EVENT_CHECK_REGISTRATION_SERVER;
         //WriteEvent(EVENT_CHECK_REGISTRATION_SERVER, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         osDelay(180);
         
         StatusDevice.error_connect_server = 0;
         
         memset(buf_str, 0, BUF_STR_SIZE);
         sprintf((char*)buf_str, (char*)AT_CIPSTART, ptr_IPAddress, ptr_port);
         GSMFlagMessageTCP.AT_CIPSTART = SENT_MESSAGE;
        // WriteArchiveSystemToFlash(GSM_GPRS_UP, SYSTEM_EVENT_OK, SYSTEM_EVENT_END);
       //  WriteArchiveSystemToFlash(GSM_TCP_UP, SYSTEM_EVENT_OK, SYSTEM_EVENT_BEGIN);
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)buf_str, strlen((char*)buf_str));
         Mdm_Transmit2(buf_str);
         
      }
      if(GSMFlagMessageTCP.CONNECT_OK)
      {
         GSMStatus.TCP =  TCP_YES_CONNECT;
         
         StatusDevice.error_connect_server = 0;
         
         //WriteEvent(EVENT_CHECK_REGISTRATION_SERVER, EVENT_OK, GSM_Get_current_communication_gsm_number(), EVENT_END);// EVENT_END EVENT_BEGIN 
         //flag_event = flag_event_menu = EVENT_START_DATA_SERVER;
         //WriteEvent(EVENT_START_DATA_SERVER, 0, 0, EVENT_BEGIN);// EVENT_END EVENT_BEGIN
         //%%%b
      //   WriteArchiveSystemToFlash(GSM_TCP_UP, SYSTEM_EVENT_OK, SYSTEM_EVENT_END);
       //  WriteArchiveSystemToFlash(GSM_TCP_MESSAGE_START, 0, SYSTEM_EVENT_BEGIN);
         //%%%e
      }
   }
   return GSMStatus.TCP; // GSMStatus.TCP == TCP_YES_CONNECT
}
//#####################################################################################
uint8_t GSMCloseTCP()
{
   if(GSMStatus.TCP == TCP_YES_CONNECT) 
   {
      // �����
      if(GSMFlagMessageTCP.AT_CIPSHUT_Close == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSHUT_Close = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSHUT, sizeof(AT_CIPSHUT)-1);
         Mdm_Transmit2(AT_CIPSHUT);
      }
      if(GSMFlagMessageTCP.AT_CIPSHUT_Close == ACCEPTED_MESSAGE)
      {
          //GSMFlagMessageTCP.AT_CIPSHUT_Close = ACCEPTED_MESSAGE;
          memset((uint8_t*)&GSMFlagMessageTCP, 0, sizeof(GSMFlagMessageTCP));
          GSMStatus.TCP = TCP_NON_CONNECT;
          return 1;
      }
   }
   return 0;
}
//#####################################################################################
uint8_t GSMTransmitMessageTCP(uint8_t *ptr_buf)
{// ������ ������ ��������� ��������� ���� 0x1A
   if(GSMStatus.TCP == TCP_YES_CONNECT)
   {
      if(GSMFlagMessageTCP.AT_CIPQSEND_0 == NON_MESSAGE)
      {// ������� � �������� ��������� ����� ����������� �������� ������
         GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
         GSMFlagMessageTCP.AT_CIPQSEND_0 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPQSEND_0, sizeof(AT_CIPQSEND_0)-1);
         Mdm_Transmit2(AT_CIPQSEND_0);
      }
      if(GSMFlagMessageTCP.AT_CIPQSEND_0 == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CIPSEND == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND = SENT_MESSAGE;
         //HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSEND, sizeof(AT_CIPSEND)-1);
         Mdm_Transmit2(AT_CIPSEND);
      }
      if(GSMFlagMessageTCP.AT_CIPSEND == ACCEPTED_MESSAGE && GSMFlagMessageTCP.MessageTX == NON_MESSAGE)
      {
         GSMFlagMessageTCP.MessageTX = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, ptr_buf, strlen((char*)ptr_buf));
         Mdm_Transmit2(ptr_buf);
      }
      if(GSMFlagMessageTCP.MessageTX == ACCEPTED_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
         GSMFlagMessageTCP.AT_CIPQSEND_0 = NON_MESSAGE;
         GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = NON_MESSAGE;
         return 1;
      }
   }
   return 0;
}
//####################################################################################################################
// Description :                                                                                         **
//####################################################################################################################
// In  :                                                                                          **
// Out :                                                                                     **
//####################################################################################################################
uint8_t GSMTransmitMessageTCPParts(uint8_t *ptr_buf, uint8_t last_line)
{
   if(GSMStatus.TCP == TCP_YES_CONNECT)
   {
      if(GSMFlagMessageTCP.AT_CIPQSEND_1 == NON_MESSAGE)
      {
         GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
         GSMFlagMessageTCP.AT_CIPQSEND_1 = SENT_MESSAGE;
       //  HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPQSEND_1, sizeof(AT_CIPQSEND_1)-1);
         Mdm_Transmit2(AT_CIPQSEND_1);
      }
      if(GSMFlagMessageTCP.AT_CIPQSEND_1 == ACCEPTED_MESSAGE && GSMFlagMessageTCP.AT_CIPSEND == NON_MESSAGE)
      {
         GSMFlagMessageTCP.AT_CIPSEND = SENT_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = SENT_MESSAGE;
        // HAL_UART_Transmit_IT(GSM_USART, (uint8_t*)AT_CIPSEND, sizeof(AT_CIPSEND)-1);
         Mdm_Transmit2(AT_CIPSEND);
      }
      
      if(GSMFlagMessageTCP.AT_CIPSEND == ACCEPTED_MESSAGE && GSMFlagMessageTCP.MessageTX == NON_MESSAGE/* && !last_line*/)
      {
         osDelay(50);//###
        // cntSEND_FAIL = 0;
         strcat((char*)FlagMessageTMR.buf_message_tcp, "\x1A");
        // GSMFlagMessageTCP.AT_CIPSEND = SENT_MESSAGE; // ��� ������ ���� �����
         GSMFlagMessageTCP.MessageTX = SENT_MESSAGE;
         //HAL_UART_Transmit(GSM_USART, ptr_buf, strlen((char*)ptr_buf) , 100);
         Mdm_Transmit2(ptr_buf);
         /*
         if(last_line)
         {
            GSMFlagMessageTCP.AT_CIPQSEND_1 = NON_MESSAGE;
            GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
         }
         GSMFlagMessageTCP.TransmitMessage = NON_MESSAGE;
         return 1;
         */
      }
      if(GSMFlagMessageTCP.MessageTX == SENT_MESSAGE 
         && GSMFlagMessageTCP.SEND_FAIL == ACCEPTED_MESSAGE)
      {
         ++cntSEND_FAIL;
         if(cntSEND_FAIL > 3)
         {
           // WriteArchiveSystemToFlash(NULL, SYSTEM_EVENT_ERROR, SYSTEM_EVENT_END);
            FlagMessageTMR.command = MESSAGE_TCP_CLOSED;
            GSMFlagMessageTCP.AT_CIPQSEND_1 = NON_MESSAGE;
            GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
            GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
            cntSEND_FAIL = 0;
         }
         else
         {
            GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
            GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
            osDelay(200);
         }
         GSMFlagMessageTCP.SEND_FAIL = NON_MESSAGE;
      }
      if(GSMFlagMessageTCP.MessageTX == ACCEPTED_MESSAGE)
      {
         if(last_line)
         {
            GSMFlagMessageTCP.AT_CIPQSEND_1 = NON_MESSAGE;
            cntSEND_FAIL = 0;
         }
         GSMFlagMessageTCP.AT_CIPSEND = NON_MESSAGE;
         GSMFlagMessageTCP.MessageTX = NON_MESSAGE;
         GSMFlagMessageTCP.TransmitMessage = NON_MESSAGE;
         return 1;
      }
   }
   return 0;
}
//---------------------------------------
uint8_t GSM_Get_Status_AT_CIPSEND()
{
   if(GSMFlagMessageBT.TransmitMessage == NON_MESSAGE
      && GSMFlagMessageTCP.TransmitMessage == NON_MESSAGE)
      return NON_MESSAGE;
   else
      return SENT_MESSAGE;
}

//#####################################################################################
uint8_t GSMReadSTR_Digit()
{
  if(GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0] >= '0'
     && GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0] <= '9')
  {
     if(GSMFlagMessageTCP.AT_CIFSR == SENT_MESSAGE)
        GSMFlagMessageTCP.AT_CIFSR = ACCEPTED_MESSAGE;
     if(GSMFlagMessage.AT_CGSN == SENT_MESSAGE)
     { 
          memset(gsm_imei, 0, 16);
          memcpy(gsm_imei, &GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], 15);
     }
     if(GSMFlagMessage.AT_CCID == SENT_MESSAGE)
     {
          memset(CCID_SIM_card, 0, sizeof(CCID_SIM_card));
          memcpy(CCID_SIM_card, &GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0], strlen((char*)&GSMstring_GSMModule[temp_number_indicator_message_for_gsm-1][0]));
     }
     
     
     return 1;
  }
  return 0;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_network_name()
{
   return (char*)GSM_network_name;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_CCID_SIM_card()
{
   return (char*)CCID_SIM_card;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_IMEI()
{
   return (char*)gsm_imei;
}

/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
char* GSM_Get_ptr_message_SIM_card_balance()
{
   return (char*)message_SIM_card_balance;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
int8_t Get_confirm_transmit_LORA()
{
   return confirm_transmit_LORA;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint8_t GSM_Get_Status_power()// GSM_POWER_ON
{
   return GSMStatus.power;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint32_t LORA_Get_tick_count_message_from_LORA_msec()
{
   return tick_count_message_from_LORA_msec;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
void GSM_ExtremalPowerDown()
{
  count_ExtremalPowerDown++;
  //uint8_t add_connection = 0;
  flag_opto_reconnect = 1;
  //timeGSMUnixStart = timeSystemUnix;
  //timeUnixGSMNextConnect = timeGSMUnixStart + reserved_int;
  flag_event_menu = EVENT_OFF_EXTREMAL_GSM_MODEM;
  confirm_transmit_LORA = EVENT_OFF_EXTREMAL_GSM_MODEM; // ������� -3 ��� ������������� ���������� ���������
  //if(flag_event < EVENT_CHECK_REGISTRATION_GSM)
    //add_connection = 1;
  //WriteEvent(flag_event, EVENT_OFF_EXTREMAL_GSM_MODEM, GSM_Get_current_communication_gsm_number() + add_connection, EVENT_END);// EVENT_END EVENT_BEGIN 
  // HAL_GPIO_WritePin(ptr_PxOUT_Pwrkey, pin_pwrkey, GPIO_PIN_SET); // ���������� ON/OFF � '0'
  TransmitDebugMessageOptic("ExtremalPowerDown");
   GSM_PWRKEY_OFF();
  // HAL_GPIO_WritePin(ptr_PxOUT_Power, pin_power, GPIO_PIN_RESET);
  //if (HiPower_ON)
   //        if ((--HiPower_ON) ==0)
              GSM_PWR_OFF(); 
//   HAL_GPIO_WritePin(ptr_PxOUT_Pwrkey, pin_pwrkey, GPIO_PIN_RESET); // ���������� ON/OFF � '0'
   GSM_PWRKEY_ON();
   
  memset((uint8_t*)&GSMFlagMessage, 0, sizeof(GSMFlagMessage));
  GSMflag_power = GSM_MUST_DISABLE;
  GSMStatus.power = POWER_NON;
  GSMStatus.init = INIT_NON;
  GSMStatus.speed = SPEED_NON_FOUND;
  GSMStatus.joined = LORA_ATY_NON_REQUEST_JOINED;
  GSMStatus.transmit = LORA_ATT_NON_TRANSMIT;
  GSMStatus.receive = LORA_ATT_NON_RECEIVE;
  GSMStatus.registration = LORA_ATJ_NON_REQUEST;
  GSMStatus.gprs = GPRS_NON_CONNECT;
  GSMStatus.TCP = TCP_NON_CONNECT;
  
  StatusDevice.GSM_Modem_Power_mode = _FALSE;
  StatusDevice.GSM_Modem_Power_optic = _FALSE;
  StatusDevice.GSM_Modem_Power_switch = _FALSE;
  
  Mdm_HardwareDeInit();

  // WriteArchiveSystemToFlash(GSM_POWER_DOWN, SYSTEM_EVENT_OK, SYSTEM_EVENT_END);
  // LedBlueMode = LED_MODE_GSM_POWER_OFF;
  osDelay(10000); 

}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint16_t GSM_Get_current_communication_gsm_number()
{
   return current_communication_gsm_number;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint8_t GSM_Get_Status_init()
{
   return GSMStatus.init;
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
void LORA_Update_tick_count_message_from_LORA_msec()
{
   tick_count_message_from_LORA_msec = xTaskGetTickCount();
}
/*******************************************************************
** InitRFChip : This routine initializes the RFChip registers     **
**              Using Pre Initialized variables                   **
********************************************************************
** In  : -                                                        **
** Out : -                                                        **
*******************************************************************/
uint32_t LORA_Get_tick_count_powerON_LORA_msec()
{
   return tick_count_powerON_LORA_msec;
}
