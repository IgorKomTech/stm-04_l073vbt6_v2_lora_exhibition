//=============================================================================
/// \file    ValveTask .c
/// \author  MVA
/// \date    30-Jan-2019
/// \brief   Implementing the Vlave-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "stm32l073xx.h"
#include "stm32l0xx.h"
#include "Time_g.h"
#include "USART_Modem.h"
#include "ValveTask.h"
#include "cmsis_os.h"
#include "gasmeter.h"
#include "global.h"
#include "OptoComand.h"
#include "eeprom.h"
#include "FlowMeasurement.h"
#include "Io.h"
#include "main_function.h"
#include "Archiv.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//------------------------------------------------------------------------------
//TODO:  �������� ��������� ������� ����� ���������� �������
//       �������� ���� ���������� �������� �������
//       �������� ������� �������� ������� � GPRS ������.
//------------------------------------------------------------------------------


#define VALVE_OPEN_ON()   SET_BIT(GPIOA->ODR, GPIO_ODR_OD5)  
#define VALVE_OPEN_OFF()  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_OD5)

#define VALVE_CLOSE_ON()   SET_BIT(GPIOA->ODR, GPIO_ODR_OD4)  
#define VALVE_CLOSE_OFF()  CLEAR_BIT(GPIOA->ODR, GPIO_ODR_OD4)


//const  uint8_t valve_presence  @".eeprom" = 0; // 1 - there is
//const  uint8_t valve_status @".eeprom" = VALVE_UNKNOWN; // 0 - close, 1 - open
//const  uint8_t valve_resolution @".eeprom" = 1; // 0 - close, 1 - open

const uint16_t time_to_open             @".eeprom" = 3000;
const uint16_t time_to_litte_open       @".eeprom" = 500;
const uint16_t time_to_close            @".eeprom" = 3000;
const uint16_t valve_open_delay         @".eeprom" = 5;  // ����� � �������� ���������
const float    valve_min                @".eeprom" = 0.015;

//const char* Ansver[] = {"CLOSE", "OPEN", "CLOSE_REVERSE_FLOW", "CLOSE_Q_MAX", "CLOSE_NON_NULL", "NON_POWER", "UNKNOWN"};
const char message_valve_status[7][19] = {"CLOSE", "OPEN", "CLOSE_REVERSE_FLOW", "CLOSE_Q_MAX", "CLOSE_NON_NULL", "NON_POWER", "UNKNOWN"};

uint8_t Valve_Open(uint8_t p_Fors);
uint8_t Valve_Close(void);

uint8_t valve_action = VALVE_ACTION_NON; // valve action to be performed depending on where the command came from


//const uint8_t Valve_status @".eeprom" = VALVE_UNKNOWN; 


//#define _VALVE_OK 0
//#define _VALVE_NOT_OPEN 3
//#define _VALVE_NOT_ENABLE 4
//#define _VALVE_NOT_POWER  8
//#define _VALVE_ERR 1


//-----------------------------------------------------------------------------
// �������� p_Fors - ������������� �������� ������� ��� �������� ������
//-----------------------------------------------------------------------------
uint8_t Valve_Open(uint8_t p_Fors)
{
 // uint8_t  lo_Valve_status;
  uint8_t lo_FlagFlow = 3 ;   // ����� �� �������
  
  if (valve_status == VALVE_OPEN)
   return VALVE_OPEN;  //
  
  

  if(!READ_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN)) // 
     SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN); // enable RCC port IO D

   
  Set_valve_status(VALVE_UNKNOWN);
  
   CalculateMotoHourceValve(MOTO_HOURCE_START);
   GSM_PWR_ON();  // enable power value
   ++HiPower_ON;  

   osDelay(200);   

   if(READ_BIT(GPIOD->IDR, GPIO_IDR_ID2) == RESET)  // �� - �� �������!
   {
      //if(HiPower_ON)
        //if ((--HiPower_ON) == 0)
           //GSM_PWR_OFF();
     Set_valve_status(VALVE_NON_POWER);
     return VALVE_NON_POWER;
   }
   
   // ���������� ������
   VALVE_OPEN_ON();   
   if(p_Fors == VALVE_OPEN_FORS_YES)
   {
    //  osDelay(time_to_open); 
    //  VALVE_OPEN_OFF();
    //  Set_valve_status(VALVE_OPEN);
    //  lo_Valve_status = VALVE_OPEN; 
      lo_FlagFlow = 0;
   }   
   else
   {
      osDelay(time_to_litte_open);
      VALVE_OPEN_OFF();    
   
      for (int cnt = 0; cnt < valve_open_delay; cnt++)  
      {

         osDelay(Config_GasmeterMeasurementIntervalNormalModeFM * 500);

         if(GasMeter.QgL_m3 < valve_min  )   // ����� ��������? 
         {
           lo_FlagFlow-- ;         //����� ������� !
           if (lo_FlagFlow == 0)
              break;
         }  
         else
            lo_FlagFlow = 3;
      } 
    }
   
    if(lo_FlagFlow)       // ���� ������ - ������� ������
    {
       VALVE_CLOSE_ON();   
       osDelay(time_to_close); 
       VALVE_CLOSE_OFF();  
       //lo_Valve_status = VALVE_CLOSE_NON_NULL;
       Set_valve_status(VALVE_CLOSE_NON_NULL);
    }
    else
    {
       VALVE_OPEN_ON();   
       osDelay(time_to_open); 
       VALVE_OPEN_OFF(); 
    //   lo_Valve_status = VALVE_OPEN;
       Set_valve_status(VALVE_OPEN);
  } 
 
  //if(HiPower_ON)
     //if ((--HiPower_ON) == 0)
       //GSM_PWR_OFF();   
  CalculateMotoHourceValve(MOTO_HOURCE_STOP);
   //Eeprom_WriteChar((uint32_t)&valve_status , lo_Valve_status);
   
   return lo_FlagFlow; 
}  
    
//-----------------------------------------------------------------------------
uint8_t Valve_Close(void)
{
 
//  uint8_t lo_FlagFlow = _VALVE_NOT_CLOSED;   // ����� �� �������

//  uint8_t lo_Valve_status; 

  
  if (valve_status == VALVE_CLOSE)
    return VALVE_CLOSE;  

  Set_valve_status(VALVE_UNKNOWN);
  

  if(!READ_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN))
     SET_BIT(RCC->IOPENR,RCC_IOPENR_IOPDEN);  // enable RCC port IO D
   CalculateMotoHourceValve(MOTO_HOURCE_START);
   ++HiPower_ON;
   GSM_PWR_ON(); 
   
   osDelay(200);

   
   if(READ_BIT(GPIOD->IDR, GPIO_IDR_ID2) == RESET)  // �� - �� �������!
   {
      //if(HiPower_ON)
        //if ((--HiPower_ON) == 0)
           //GSM_PWR_OFF();
      Set_valve_status(VALVE_NON_POWER);
      return VALVE_NON_POWER;
   }

   VALVE_CLOSE_ON();   
   osDelay(time_to_close);
    
   VALVE_CLOSE_OFF();    
   
   //if (HiPower_ON)
      //if ((--HiPower_ON) ==0)
         //GSM_PWR_OFF();  
   CalculateMotoHourceValve(MOTO_HOURCE_STOP);
/*
  for (int cnt = 0; cnt < Valve_Open_delay; cnt++)  
    {
      osDelay(Config_GasmeterMeasurementIntervalNormalModeFM +500);
      if(GasMeter.QgL ==0.0)   // ����� ��������? 
      {
        lo_FlagFlow-- ;         //����� ������� !
        if (lo_FlagFlow == 0)
          break;
      }  
      else
        lo_FlagFlow = _VALVE_NOT_CLOSED;
      } 
    }

   if (lo_FlagFlow)
     lo_Valve_status = VALVE_UNKNOWN;
   else  */
   
  // lo_Valve_status = VALVE_CLOSE;   
   
   //Eeprom_WriteChar((uint32_t)&valve_status , lo_Valve_status);
   Set_valve_status(VALVE_CLOSE);
   return VALVE_CLOSE;
}
     
//------------------------------------------------------------------------------
uint8_t PFW_valve(char* parString)
{
  //  uint8_t lo_Valve_status;
 //   const char* Ansver[] = {"CLOSE", "OPEN", "CLOSE_REVERSE_FLOW", "CLOSE_Q_MAX", "CLOSE_NON_NULL", "NON_POWER", "UNKNOWN"};
  //  const char* lo_ErrMsg[] = {"������ ������!\r\n", "��� ������� �������!!\r\n",\
  //          "��� ����������!!\r\n","������ �� ������\r\n", "������ �������\r\n"};
 //   char* lo_message; 
    
   if(parString ==0)   
   {
    if(while_TransmitEnable() == _FALSE) return 1;
    /*
    switch (valve_status)
    {
    case VALVE_CLOSE:  
         lo_message = (char*)lo_Ansver[0];
         break;
    case VALVE_OPEN:  lo_message = (char*)lo_Ansver[1];
             break;
    case VALVE_CLOSE_REVERSE_FLOW:  lo_message = (char*)lo_Ansver[2];
            break;
    case VALVE_CLOSE_Q_MAX:  lo_message = (char*)lo_Ansver[0];
             break;
    case VALVE_CLOSE_NON_NULL:  lo_message = (char*)lo_Ansver[1];
             break;
    case VALVE_NON_POWER:  lo_message = (char*)lo_Ansver[2];
             break;
    default:
    case VALVE_UNKNOWN:  lo_message = (char*)lo_Ansver[2];
             break;   
    }         
 */
    sprintf((char*)TransmitBuffer,"Valve_status=%d;%s\r\n", valve_status, message_valve_status[valve_status]);  
    Opto_Uart_Transmit(); 
    return OPTIC_OK; 
  }
  else
  {
     if(*parString == '0')
     {

        sprintf((char*)OldValue,"%d", valve_status);
        sprintf((char*)NewValue,"%d", 0);

        valve_action = VALVE_ACTION_CLOSE_OPTIC;
     }
     else 
        if(*parString == '1')
        {
           sprintf((char*)OldValue,"%d", valve_status);
           sprintf((char*)NewValue,"%d", 1);
           valve_action = VALVE_ACTION_OPEN_OPTIC;
        }
        else 
           return OPTIC_INCORRECT_VALUE; 
     WriteChangeArcRecord(CHANGE_VALVE_OPTIC, CHANGE_OPTIC);

  }
    
    
    /*
    
    
    if (strncmp(lo_Ansver[1],parString,2) == 0)
       lo_Valve_status = Valve_Open(0);
    else
      if (strncmp(lo_Ansver[2],parString,2) == 0)
        lo_Valve_status = Valve_Close();
      else
        return OPTIC_ERROR;
     
       switch (lo_Valve_status) 
       {
         case _VALVE_OK:
              return OPTIC_OK; 
         case _VALVE_NOT_OPEN:
               lo_message = (char*)lo_ErrMsg[0];
               break;
       case _VALVE_NOT_ENABLE :     
               lo_message = (char*)lo_ErrMsg[2];
               break;
       case _VALVE_NOT_POWER :     
               lo_message = (char*)lo_ErrMsg[1];
               break;
       default:
               lo_message = (char*)lo_ErrMsg[4];
               break;
       }               
              sprintf((char*)TransmitBuffer,"%s", lo_message);  
              Opto_Uart_Transmit(); 
     } 
    */
       return OPTIC_OK; 
}

//------------------------------------------------------------------------------
uint8_t PFW_valve_time(char* parString)
{
  float lo_Time_Close, lo_Time_Open, lo_Time_LOpen;
  
  if(parString == 0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;
    lo_Time_Close = (float)time_to_close /1000.0;
    lo_Time_Open = (float)time_to_open /1000.0;
    lo_Time_LOpen = (float)time_to_litte_open /1000.0;
    
    sprintf((char*)TransmitBuffer,"Valve time(sek):\r\nOpen = %0.2f \r\nClose = %0.2f \r\nLitte open = %0.2f\r\n ", \
      lo_Time_Open, lo_Time_Close, lo_Time_LOpen);  
    Opto_Uart_Transmit(); 
    return OPTIC_OK; 
  }
  else
  {
    if(CLB_lock == 0x55)
    {
       lo_Time_Open = atof(parString);
      // if(lo_Time_Open == 0.0)
      //   lo_Time_Open = (float)time_to_open /1000.0;
       if(lo_Time_Open < 1.0 || lo_Time_Open > 20.0)
         return OPTIC_INCORRECT_VALUE;
      /// ��������� ������������ �������� !!!!  

       parString = strchr(parString,';');
       if(parString)
       { 
          lo_Time_Close = atof(++parString);
   //   if(lo_Time_Close == 0.0)
   //     lo_Time_Close = (float)time_to_close /1000.0;
          if(lo_Time_Close < 1.0 || lo_Time_Close > 20.0)
             return OPTIC_INCORRECT_VALUE;
       }
       parString = strchr(parString,';');
       if(parString)
       { 
          lo_Time_LOpen = atof(++parString);
    //  if(lo_Time_LOpen == 0.0)
    //    lo_Time_LOpen = (float)time_to_close /1000.0;
          if(lo_Time_LOpen < 0.5 || lo_Time_LOpen > 2.0)
             return OPTIC_INCORRECT_VALUE;
       }

       uint16_t lo_iTime_Close, lo_iTime_Open, lo_iTime_LOpen;
       
       lo_iTime_Close = (unsigned int)(lo_Time_Close * 1000.0);  
       lo_iTime_Open  = (unsigned int)(lo_Time_Open * 1000.0);
       lo_iTime_LOpen = (unsigned int)(lo_Time_LOpen * 1000.0);
      
       Eeprom_WriteWord((uint32_t)&time_to_close,lo_iTime_Close);
       Eeprom_WriteWord((uint32_t)&time_to_open,lo_iTime_Open);
       Eeprom_WriteWord((uint32_t)&time_to_litte_open,lo_iTime_LOpen);
    }
    else
      return OPTIC_ACCESS_DENIED;
  
  }
 return OPTIC_OK; 
 }

//------------------------------------------------------------------------------
uint8_t PFW_valve_check_open_time(char* parString)
{
  uint16_t  lo_Time_Open;
  
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;
    lo_Time_Open = valve_open_delay * Config_GasmeterMeasurementIntervalNormalModeFM ;

    sprintf((char*)TransmitBuffer,"Valve check time(sek) = %u \r\n ", \
      lo_Time_Open);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {
    if(CLB_lock == 0x55)
    {
       lo_Time_Open = atoi(parString);

       if(lo_Time_Open < 6 || lo_Time_Open > 600)
          return OPTIC_INCORRECT_VALUE;
    /// ��������� ������������ �������� !!!!  
       lo_Time_Open  = lo_Time_Open/Config_GasmeterMeasurementIntervalNormalModeFM ;
 
       Eeprom_WriteWord((uint32_t)&valve_open_delay,lo_Time_Open);
    }
    else
      return OPTIC_ACCESS_DENIED;

   }  
  
 return OPTIC_OK; 
}

// Valve_min
//------------------------------------------------------------------------------
uint8_t PFW_Valve_min(char* parString)
{
  float lo_Valve_min;
  
  if(parString ==0)   
  {
    if(while_TransmitEnable() == _FALSE) return 1;

    sprintf((char*)TransmitBuffer,"Valve min flow(m3/hour) = %0.4f \r\n ", \
      valve_min);  
     Opto_Uart_Transmit(); 
     return OPTIC_OK; 
  }
  else
  {
    if(CLB_lock == 0x55)
    {
       lo_Valve_min = atof(parString);
    /// ��������� ������������ �������� !!!!  
       if(lo_Valve_min > 0.301)
          return OPTIC_INCORRECT_VALUE;
    
       Eeprom_WriteFloat((uint32_t)&valve_min,lo_Valve_min);
    }
    else
      return OPTIC_ACCESS_DENIED;
  }  
  
  return OPTIC_OK; 
}
//------------------------------------------------------------------------------
uint8_t PFW_Valve_set(char* parString)
{
  if(parString ==0)   
  {
     return OPTIC_INCORRECT_VALUE; 
  }
  else
  { 
     if(CLB_lock == 0x55)
     {
        uint16_t temp;
        temp = atol(parString);
        if(temp == 0 || temp == 1)
        {
           sprintf((char*)OldValue,"%d", valve_presence);
           Eeprom_WriteChar((uint32_t)&valve_presence, temp);
           sprintf((char*)NewValue,"%d", valve_presence);
           WriteChangeArcRecord(CHANGE_VALVE_PRESENCE, CHANGE_OPTIC);
        }
        else 
           return OPTIC_INCORRECT_VALUE; 
     }
     else
       return OPTIC_ACCESS_DENIED;
     
  }
  return OPTIC_OK; 
}

/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
void Valve_Parsing()
{
   if(valve_presence) // this valve?
   {
      if(valve_action == VALVE_ACTION_OPEN_OPTIC)
      {
          if(valve_resolution)
            Valve_Open(VALVE_OPEN_FORS_NON);
      }
      if(valve_action == VALVE_ACTION_OPEN_TCP)
      {

      //   if(valve_status != VALVE_CLOSE_REVERSE_FLOW
      //      && valve_status != VALVE_CLOSE_Q_MAX)
           Valve_Open(VALVE_OPEN_FORS_NON);
      }
      if(valve_action == VALVE_ACTION_CLOSE_OPTIC || valve_action == VALVE_ACTION_CLOSE_TCP)

      {
         Valve_Close();
      }
      if(StatusDevice.reverse_flow)
      {
         if(Valve_Close() == VALVE_CLOSE)
            Set_valve_status(VALVE_CLOSE_REVERSE_FLOW);
      }
      if(StatusDevice.going_beyond_Q)
      {
         if(Valve_Close() == VALVE_CLOSE)
            Set_valve_status(VALVE_CLOSE_Q_MAX);
      }     
      valve_action = VALVE_ACTION_NON;
   }
}
/**********************************************************************************************************
** Description :                                                                                         **
***********************************************************************************************************
** In  :                                                                                          **
** Out :                                                                                     **
**********************************************************************************************************/
void Set_valve_status(uint8_t temp_status)
{
   Eeprom_WriteChar((uint32_t)&valve_status , temp_status);
}
