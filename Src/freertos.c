
/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"


#include "Typedef.h"
#include "Error.h"
#include "Time_g.h"
#include "Io.h"
#include "I2c.h"
#include "lcd_8s.h"
#include "Utils.h"
#include "System.h"
#include "Sgm.h"
#include "Gasmeter.h"
#include "FlowMeasurement.h"
#include "Menu.h"
#include "UART_152.h"
#include "ExtFlash.h"
#include "Archiv.h"
#include <string.h>
#include "OptoComand.h"
#include "USART_Modem.h"

#include "GSMSIM800C.h"
#include "Global.h"
#include "main_function.h"

#define CONFIG_USE_CATEGORY_MAIN
#define CONFIG_USE_CATEGORY_WELMEC
#include "Configuration.h"



extern uint8_t successful_transfer_data_to_server;
uint8_t cnt_repetition_connect_TCP;

/* Variables -----------------------------------------------------------------*/
osThreadId GasmeterTaskHandle;
osThreadId OPTO_ReadTaskHandle;
osThreadId GSMTaskHandle;
osThreadId GSM_RX_UARTTaskHandle;
osThreadId TCP_IPTaskHandle;

SemaphoreHandle_t xRTCSemaphore; // = NULL;
SemaphoreHandle_t xRxSemaphore;// = NULL;
osMessageQId QueueGSM_RX_UARTHandle;

Error Gl_Error;
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void GasmeterTask(void const * argument);
static Error InitModules(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */
static void LogError(Error error);
void StartGSMTask(void const * argument);
void StartGSM_RX_UARTTask(void const * argument);
void StartTCP_IPTask(void const * argument);
/* USER CODE BEGIN FunctionPrototypes */


/* USER CODE END FunctionPrototypes */

/* Hook prototypes */
extern const char MdmPort_Server[6];
extern uint32_t tick_count_message_from_server_msec;
extern struct _FlagMessageTMR FlagMessageTMR;
extern int32_t delay_connect_GSMTCP_s;
/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
Error error;
  System_Init();
  Time_InitHardware();
  LCD8_Init();
  Io_InitHardware();  //// Init OptoSwitch and Key 
  I2c_InitHardware();
  Sgm_InitHardware();
  Opto_Uart_Init();

  LCD8_Menu(TEH_MENU_SW,0);
   error = InitModules();
  
 
   
  if(Error_IsError(error))
    Error_SetFatal(&error);
  //Restart programm!!!
 //  ErrorLog_IncrementErrorCounter(ERRORLOG_BOOT_FAILURE);
  
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  
  	xRTCSemaphore = xSemaphoreCreateBinary();
    xRxSemaphore =  xSemaphoreCreateBinary();
  
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(GasmeterTask, GasmeterTask, osPriorityNormal, 0, 128); // High
  GasmeterTaskHandle = osThreadCreate(osThread(GasmeterTask), NULL);
  
  osThreadDef(OPTO_ReadTask, OPTO_ReadTask, osPriorityNormal, 0, 128); // BelowNormal
  OPTO_ReadTaskHandle = osThreadCreate(osThread(OPTO_ReadTask), NULL);
 // vTaskSuspend(OPTO_ReadTaskHandle); // ������������ ������

  /* definition and creation of GSMTask */
  osThreadDef(GSMTask, StartGSMTask, osPriorityIdle, 0, 128);
  GSMTaskHandle = osThreadCreate(osThread(GSMTask), NULL);

  /* definition and creation of GSM_RX_UARTTask */
  osThreadDef(GSM_RX_UARTTask, StartGSM_RX_UARTTask, osPriorityIdle, 0, 128);
  GSM_RX_UARTTaskHandle = osThreadCreate(osThread(GSM_RX_UARTTask), NULL);

  /* definition and creation of TCP_IPTask */
  osThreadDef(TCP_IPTask, StartTCP_IPTask, osPriorityIdle, 0, 128);
  TCP_IPTaskHandle = osThreadCreate(osThread(TCP_IPTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */
  
  /* Create the queue(s) */
  /* definition and creation of QueueGSM_RX_UART */
  osMessageQDef(QueueGSM_RX_UART, 5, uint8_t);
  QueueGSM_RX_UARTHandle = osMessageCreate(osMessageQ(QueueGSM_RX_UART), NULL);
  
  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}
//-----------------------------------------------------------------------------
/* Start GasmeterTask function */
void GasmeterTask(void const * argument)
{
 //+++ portTickType xLastWakeTime;
  /* USER CODE BEGIN StartDefaultTask */
  

  
 //+++ xLastWakeTime = xTaskGetTickCount();
  WriteIntArcRecord(1);
  /* Infinite loop */
 while(!Error_IsFatal(Gl_Error)) 
  {
    xSemaphoreTake(xRTCSemaphore, portMAX_DELAY);
    System_WatchdogRefresh();
    Gl_Error = Gasmeter_Process();

    if(Error_IsError(Gl_Error))
      LCD8_Menu(TEH_MENU_ERROR,0);
    else
      LCD8_Menu(0,0);     

    if(IntArcFlag)
    {
      WriteIntArcRecord(0);
      IntArcFlag =0;
    } 
    ///////////////////////////
      // �������, ���� �� ��������� �����.
    if(delay_connect_GSMTCP_s <= 0)
    {
       delay_connect_GSMTCP_s = 0xFFFFFF; // ��� �� �� ������
       GSM_Set_TCPFlag(TCP_MUST_ENABLE);   
       GSM_Set_flag_power(GSM_MUST_ENABLE);      
    }
    ///////////////////////////
    EnterSTOPMode();
    
//    vTaskDelayUntil( &xLastWakeTime, ( 1000 / portTICK_RATE_MS ) );
//    Time_WaitUntilNextSecond();

//    osDelay(1);
  }
  LogError(Gl_Error);
  System_Reset();
  
  /* USER CODE END StartDefaultTask */
}
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
/* StartGSMTask function */
void StartGSMTask(void const * argument)
{
  
  for(;;)
  {
    GSMPower();
    if(GSM_Get_flag_power() == GSM_MUST_ENABLE ) 
    {
        if(FindSpeedUARTGSMModule() == SPEED_FOUND_ERROR)
        {
            GSM_Set_flag_power(GSM_MUST_DISABLE);
        }
    
        InitGSMModule();
    }
    //------------------------------------------------------------------------
    uint8_t rez_GSM_TCPConnect;
    rez_GSM_TCPConnect = GSM_TCPConnect((uint8_t*)MdmURL_Server, (uint8_t*)MdmPort_Server, (uint8_t*)MdmAPN_Adress, (uint8_t*)MdmAPN_Login, (uint8_t*)MdmAPN_Password);
    if(rez_GSM_TCPConnect == TCP_YES_CONNECT)
    {
    //  tick_count_message_from_server_msec = xTaskGetTickCount(); //###
       if(DifferenceTickCount(xTaskGetTickCount(), tick_count_message_from_server_msec) > WAITING_TIME_MESSAGE_FROM_SERVER_MSEC)
       {// �������� ������� �������� ������� �� �������
        // ������� TCP ����������
        // ���������� ���� �������
        // �������� � ������, � ����� ��������� �������
         // ���������������� ������� ��������� ������� �����
         /*
          if(GSMCloseTCP())
          {
             FlagMessageTMR.command = MESSAGE_BT_TCP_NON;
             //++number_communication_gsm_fail;
             //number_communication_gsm_fail += 2; //###
             GSM_Set_TCPFlag(TCP_MUST_DISABLE);

            
         
          }
       */
       }
       ParsingMessageTMR();
     }
     else
     {// ��� TCP ����������
        if(rez_GSM_TCPConnect == TCP_CONNECTING)
           successful_transfer_data_to_server = _FALSE;
     }
    //-------------------------------------------------------------------------------------------
    if(rez_GSM_TCPConnect == TCP_NON_CONNECT && successful_transfer_data_to_server != _NULL2)
    {
       if(successful_transfer_data_to_server == _OK)
       {
          cnt_repetition_connect_TCP = 0;

       }
    }
    //-------------------------------------------------------------------------------------------
    //ParsingMessageTMR();

    osDelay(1);
  }
}
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 
  // GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM GSM 


  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
/* StartGSM_RX_UARTTask function */
void StartGSM_RX_UARTTask(void const * argument)
{
    for(;;)
  {
     fGSM_RX_UARTTask(); // ���������� �������� ���������
     osDelay(1);
  }
}
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 
  // GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART GSM UART 

  // TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP 
  // TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP 
  // TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP 

/* StartTCP_IPTask function */
void StartTCP_IPTask(void const * argument)
{
  for(;;)
  {
    osDelay(1);
  }
}
  // TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP 
  // TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP 
  // TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP TCP 


//-----------------------------------------------------------------------------
static Error InitModules(void)
{
  Error error;
 
  error = Sgm_InitModule();
  
  if(Error_IsNone(error))
    error = FlowMeas_InitModule();
  
  if(Error_IsNone(error)) 
    error = Gasmeter_InitModule();
  
  error = Sgm_DeinitModule();
  return error;
}

//-----------------------------------------------------------------------------
static void LogError(Error error)
{
  if(Gl_Error.Raw != error.Raw)
  {
    Gl_Error.Raw = error.Raw;
//    WriteIntArcRecord(2);   /// Log if Error
  }  
  // Add Error in EventArc
}

void vApplicationIdleHook( void )
{
  if (KeyPressed == KEY_PRESSED_SHORT)
      Switch_Next_Menu();
//  if ((OptoPort==OPTO_PORT_DISABLE) || (READ_BIT(GPIOC->IDR, GPIO_IDR_ID12))) 
   __WFI();
//  EnterSTOPMode();
} 
