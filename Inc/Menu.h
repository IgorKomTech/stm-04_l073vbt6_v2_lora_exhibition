//=============================================================================
//   
//=============================================================================
/// \file    menu.h
/// \author  MVA
/// \date    12-Dec-2017
/// \brief   Module for Menu Switch
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#ifndef MENU_H
#define MENU_H

// main menu
#define MAIN_MENU_VOLUME  0 //  + 1
#define MAIN_MENU_FLOW    1 // + 2
#define MAIN_MENU_DATE    2 // + 3
#define MAIN_MENU_VALVE   3

#define TEH_MENU_WARNINGS 10
#define TEH_MENU_ERROR    11  // указатель -
#define TEH_MENU_CRASH    12

#define MAIN_MENU_GASTYPE 13 // -
#define MAIN_MENU_TEMPER  14 // + 2
#define MAIN_MENU_VOLUME_FLOAT   15 // + 3
#define MAIN_MENU_TIME    16 // + 1
#define TEH_MENU_MDMTST   17 // -
#define MENU_RESIDUAL_CAPACITY_BATTERY    18
#define TEH_SERIAL_DEVICE 19 // + 4
#define TEH_MENU_SW       20 // -
#define TEH_MENU_CHKSUMM  21 // -
#define MAIN_MENU_DATE_VERIFICATION       22
#define MAIN_MENU_DATE_NEXT_VERIFICATION  23
#define TEH_ALL_SEGMENT   24 //











#define MAX_MAIN_MENU  MAIN_MENU_VALVE
#define MAX_TEH_MENU   TEH_ALL_SEGMENT


extern uint8_t Global_Menu;
void Switch_Next_Menu(void);
void Switch_Sub_Menu(void);
void LCD8_Menu(uint8_t pMenu, uint8_t pParam);

#endif /* MENU_H */

