//=============================================================================
//  
//=============================================================================
/// \file    OptoComand.h
/// \author 
/// \date   
/// \brief   Comand optical interface
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#include "UART_152.h"
#ifndef OPTOCOMAND_H
#define OPTOCOMAND_H
/*
const char* Error_msg[] = { "OK\r\n",               //0
                            "Error\r\n",            //1 
                            "Incorrect value\r\n",  //2
                            "Access denied\r\n",    //3 
                            "Read only\r\n",         //4 
                            "CRC Error\r\n",         //5
                            "Internal Error\r\n",    //6
                           };    

*/

enum
{
  OPTIC_OK = 0,
  OPTIC_ERROR,
  OPTIC_INCORRECT_VALUE,
  OPTIC_ACCESS_DENIED,
  OPTIC_READ_ONLY,
  OPTIC_CRC_ERROR,
  OPTIC_INTERNAL_ERROR
};


uint8_t PFR_DateTime(char* parString);
uint8_t PFR_SWVers(char* parString);
uint8_t PFW_TestMode(char* parString);
uint8_t PFW_Qdf(char* parString);
uint8_t PFR_DevInfo(char* parString);
uint8_t PFW_SerialNum(char* parString);
uint8_t PFW_MeasTest(char* parString);
uint8_t PFR_HWVers(char* parString);
uint8_t PFR_DevName(char* parString);
uint8_t PFW_GasRTime(char* parString);
uint8_t PFR_CurVol(char* parString);
uint8_t PFW_GasLTime(char* parString);
uint8_t PFR_ReadArc(char* parString);
uint8_t PFW_ServerURL(char* parString, uint8_t number);
uint8_t PFW_ServerURL1(char* parString);
uint8_t PFW_ServerURL2(char* parString);
uint8_t PFW_ServerURL3(char* parString);
uint8_t PFW_APNAdress(char* parString);
uint8_t PFW_APNLogin(char* parString);
uint8_t PFW_APNPassword(char* parString);
uint8_t PFW_MaxFlowRate(char* parString);
uint8_t PFW_MaxTemp(char* parString);
uint8_t PFW_MinTemp(char* parString);
uint8_t PFR_ReadDayArc(char* parString);
uint8_t PFR_ReadChangeArc(char* parString);
uint8_t PFR_GetLockState(char* parString);
uint8_t PFW_GasDayBorter (char* parString);
uint8_t PFW_ModemModeTransfer (char* parString);
uint8_t PFW_ModemBalansePhohe(char* parString);
uint8_t PFR_ReadEventArc(char* parString);
uint8_t PFW_MdmStart(char* parString);
uint8_t PFR_ArcNumRecords(char* parString);
uint8_t PFW_MdmToOpto(char* parString);
uint8_t PFR_GroupInfo(char* parString);
uint8_t PFW_ModemReservedInterval(char* parString);
uint8_t PFW_ClearArc(char* parString);
uint8_t PFW_ClearCount(char* parString);
uint8_t PFW_ClearMdmCnt(char* parString);
uint8_t PFW_Gl_Error(char* parString);
uint8_t PFW_QposLimit(char* parString);
uint8_t PFW_type_device(char* parString);
uint8_t PFW_hw_version(char* parString);
uint8_t PFW_sms_phone(char* parString);
uint8_t PFW_pressure_abs(char* parString);
uint8_t PFW_QnegLimit(char* parString);
uint8_t PFW_all_segment(char* parString);
uint8_t PFW_SN_SGM_E(char* parString);
uint8_t PFW_QGL_MIN(char* parString);
uint8_t PFW_QGL_MAX(char* parString);
uint8_t PFW_QFL_LIMIT(char* parString);
uint8_t PFW_KONTRAST(char* parString);
uint8_t PFW_VOLUME_INST(char* parString);
uint8_t PFW_MOTO_INFO(char* parString);
uint8_t PFW_DATE_VERIFIC_NEXT(char* parString);
uint8_t PFW_MOTO_DATE_VERIFIC(char* parString);
uint8_t PFW_Qmax_warning(char* parString);
uint8_t PFW_Qmax_min_warning(char* parString);
uint8_t PFW_Qmax_warning_cnt_measurement(char* parString);
uint8_t PFW_Qmax_cnt_measurement(char* parString);
uint8_t PFW_DQF_END(char* parString);
uint8_t PFW_cnt_measurement_temper_eeprom(char* parString);
uint8_t PFW_Tmax_warning(char* parString);
uint8_t PFW_Tmin_warning(char* parString);
uint8_t PFW_cnt_measurement_temper_warning_eeprom(char* parString);
uint8_t PFW_cnt_reverse_flow_eeprom(char* parString);
uint8_t PFW_Qfl_reverse_flow(char* parString);
uint8_t PFW_serial_number_board(char* parString);
uint8_t PFW_cnt_error_SGM_eeprom(char* parString);
uint8_t PFW_TestRTime(char* parString);
uint8_t PFW_TestLTime(char* parString);
uint8_t PFW_status(char* parString);
uint8_t PFW_LORA_power(char* parString);
uint8_t PFW_LORA_reconnect(char* parString);
uint8_t PFR_LORA_control(char* parString);

uint8_t PFW_Self_GasBorder(char* parString);
uint8_t PFW_Set_KalmanFilter(char* parString);
uint8_t PFW_EnTFilter(char* parString);
uint8_t PFW_KalmanK(char* parString);
uint8_t PFW_KalmanBorder(char* parString);
uint8_t PFW_KalmanCounter(char* parString);

uint8_t PFW_N_GasRTime(char* parString);
uint8_t PFW_GasRange(char* parString);

extern const uint16_t MAX_FUNC;
extern const char UnitsName[];
extern const TParser PFunc[];
extern uint16_t NoLogToOptik;
extern uint32_t tick_count_message_from_OPTIC_msec;

uint8_t while_TransmitEnable ();
uint8_t Str2DataTime(LL_RTC_DateTypeDef* p_Date,LL_RTC_TimeTypeDef* p_Time, char*parString);
uint32_t Get_tick_count_message_from_OPTIC_msec();
uint8_t change_Qmax_warning_cnt_measurement(char* parString, uint8_t flag_source);
uint8_t change_Qmax_min_warning(char* parString, uint8_t flag_source);
uint8_t change_Qmax_cnt_measurement(char* parString, uint8_t flag_source);
uint8_t change_Tmax_warning(char* parString, uint8_t flag_source);
uint8_t change_Tmin_warning(char* parString, uint8_t flag_source);
uint8_t change_cnt_measurement_temper_warning_eeprom(char* parString, uint8_t flag_source);
uint8_t change_GasRecognitionInterval(char* parString, uint8_t flag_source);
uint8_t change_Kfactor_range(char* parString, uint8_t flag_source);
uint8_t change_Kfactor_measure_cnt_eeprom(char* parString, uint8_t flag_source);

#endif