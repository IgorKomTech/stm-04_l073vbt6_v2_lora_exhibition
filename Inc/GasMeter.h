//=============================================================================
/// \file    GasMeter.h
/// \author  RFU
/// \date    05-Feb-2016
/// \brief   Gasmeter functionality
//=============================================================================
//=============================================================================

#ifndef GASMETER_H
#define GASMETER_H

#include "Typedef.h"
#include "Error.h"
#include "sgm.h"
#include "SensorTypeParameters.h"
#include "stm32l0xx_ll_rtc.h"


//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_GASMETER_UNKNOWN_SENSOR     (ERROR_MODULE_GASMETER | 0x00)
#define ERROR_GASMETER_INVALID_FLOW_UNIT  (ERROR_MODULE_GASMETER | 0x01)
#define ERROR_GASMETER_INIT_GASREC        (ERROR_MODULE_GASMETER | 0x02)


 
typedef struct
	{
          double QgL;         // ������ long ���� � ������
          double QgS;         // ������ short ���� � ������
          double dQf;         // ������� �������������� ������
          double QgL_m3;      // +++ MVA ������ long � ����� � ���
          double QgS_m3;      // +++ MVA ������ long � ����� � ���
          double QgL_Disp;    // ������ ��� �������  
          double var_T;       // ����������� � �������  
          double VE_L;        // ����� long ����������� ������ � �����
          double VE_S;        // ����� short ����������� ������ � �����  
          double VE_Lm3;      // ����� long ����������� ������ � �3
          double VE_Sm3;      // ����� short ����������� ������ � �3  
          double VE_Pulse;    // ����� �� �������� ���������
          float  VDD_power;    // ���������� ������� �������
  } TGasMeter;


typedef struct
	{
 	  uint16_t SF;    // ���������� ����������� 
    uint16_t Lut;         // �����
    float   CalkValue;    // � ��������� � ���������� �����
    uint16_t FW_major;   // ������ �� ��������
    uint16_t FW_minor;   // ������ �� ��������
    uint16_t HW_major;   
    uint16_t HW_minor;   // ������ ������ ��������    
    LL_RTC_DateTypeDef DateOfManufact; // ���� ������������ 
    uint8_t  SN_Device[20];
  //  float Max_Q;
  //  float Min_Q;
    int8_t Max_T;
    int8_t Min_T; 
	} TDevice;

enum
{
  KFACTOR_MODE_NORM,
  KFACTOR_MODE_MEASURE,
  KFACTOR_MODE_ERROR
};

typedef enum  { 
                TestModeOFF = 0,
                TestMode_1  = 1, 
                TestMode_2  = 2,
                TestMode_3  = 3,  
//                TestMode_4  = 4,
                TestMode_5  = 5,
              } T_TestMode ;

extern const TDevice Device_STM;
extern TGasMeter GasMeter;
extern TSensor Sensor;

extern double TEST_VE_L;
extern double TEST_VE_Pulse;   //�����!!!!       
extern double TEST_VE_Lm3;

extern uint16_t TestInterval;

extern uint32_t TEST_QF_summ;
extern uint16_t TEST_QF_Count;
extern float Test_QF_midle;

extern float Int_Flow_Max;
extern double Int_Temp_Midl;
extern uint16_t Int_Temp_Count;
extern float Int_Temp_Max;
extern float Int_Temp_Min;
extern double GasSelfBorder;
extern uint8_t TransmitOpticTest;
extern T_TestMode TestModeActive;
extern T_TestMode TestModeActive_previous;

extern const uint16_t NoShortMeassure;


                                

//-----------------------------------------------------------------------------
Error Gasmeter_InitModule(void);
//-----------------------------------------------------------------------------
Error Gasmeter_Process(void);
void ActivateTestMode(T_TestMode pTestMode);
//-----------------------------------------------------------------------------


#endif /* GASMETER_H */
