//=============================================================================
//   
//=============================================================================
/// \file    Time.h
/// \author  LWI
/// \date    18-Dec-2014
/// \brief   Module for Timing and MCU Low Power Wait functions
//=============================================================================
// COPYRIGHT
//=============================================================================
//=============================================================================

#ifndef TIME_H
#define TIME_H

#include "Typedef.h"
#include "stm32l0xx_ll_rtc.h"

//----------------------------------------------------------------------------
/// \brief Init the core frequency and the second alarm of RTC.
void Time_InitHardware(void);

//-----------------------------------------------------------------------------
void Time_RegisterBeforeSleepCallback(void (*func)(bool*));

//----------------------------------------------------------------------------
/// \brief MCU goes to stop mode and return on next second alarm.
void Time_WaitUntilNextSecond(void);

//----------------------------------------------------------------------------
/// \brief Get the system up time since power up.
/// \retval System up time [s]
uint32_t Time_GetSystemUpTimeSecond(void);

//----------------------------------------------------------------------------
/// \brief Small delay, the MCU is not going to low power mode.
/// \param delay   delay for wait [us]
void Time_DelayMicroSeconds(uint32_t delay);

//----------------------------------------------------------------------------
/// \brief Low Power delay, the MCU is going to stop mode for given time.
/// \param delay   delay for wait [ms]
void Time_DelayMilliSeconds(uint32_t delay);

void EnterSTOPMode(void);

void GetDate_Time(LL_RTC_DateTypeDef* Date,LL_RTC_TimeTypeDef* Time);
void SetDate_Time(LL_RTC_DateTypeDef* NewDate,LL_RTC_TimeTypeDef* NewTime);

void initPeriodicTimer(void);
void DeInitPeriodicTimer(void);
void startPeriodicTimer(void);
uint16_t stopPeriodicTimer(void);

extern LL_RTC_TimeTypeDef SystTime;
extern LL_RTC_DateTypeDef SystDate;
extern uint8_t IntArcFlag;


#endif /* TIME_H */
