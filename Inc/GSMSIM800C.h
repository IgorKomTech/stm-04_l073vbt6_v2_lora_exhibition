#ifndef __GSMSIM800C_H
#define __GSMSIM800C_H

#define GSM_MUST_ENABLE 1
#define GSM_MUST_DISABLE 0



#define ONE_SIM_CARD 0
#define TWO_SIM_CARD 1

#define NON_MESSAGE 0
#define ACCEPTED_MESSAGE 1
#define SENT_MESSAGE 2
#define ACCEPTED_MESSAGE_ERROR 3
#define SENT_MESSAGE_PART 4

#define SPEED_NON_FOUND 0
#define SPEED_FOUND 1
#define SPEED_FOUND_ERROR 2

#define LORA_ATY_NON_REQUEST_JOINED 0
#define LORA_ATY_REQUEST_JOINED 1
#define LORA_ATY_JOINED 2
#define LORA_ATY_NOTJOINED 3
#define LORA_ATY_ERROR 4

#define LORA_ATT_NON_TRANSMIT 0
#define LORA_ATT_WAIT 1
#define LORA_ATT_TRANSMIT 2           
#define LORA_ATT_NOTTRANSMIT 3
#define LORA_ATT_ERROR 4

#define LORA_ATT_NON_RECEIVE 0
#define LORA_ATT_RECEIVE 1

#define LORA_ATJ_NON_REQUEST 0         // ��� �� ���� ������� ���������������
#define LORA_ATJ_REQUEST 1             // ��������� ��������������
#define LORA_ATJ_GETTING_PARAMETER 2   // ������ ����� �� ������
#define LORA_ATJ_SETTING_PARAMETER 3   // �������� ��������������
#define LORA_ATJ_READY_FOR_ACTIVATION 4// ������ � ���������
#define LORA_ATJ_ACTIVATION_WAIT 5     // ���� ���������
#define LORA_ATJ_JOINED 6              // �����������
#define LORA_ATJ_JOIN_FAILED 7         // �� �����������
#define LORA_ATJ_ERROR 8               // ������



#define PAUSE_AFTER_POWER_ON 7000

#define SPEED_UART_DEFAULT  38400
#define CNT_SPEED_UART  6




typedef enum
{
   CALL_NON_MUST_OUTPUT,
   CALL_MUST_OUTPUT
}Typeflag_CALLOutput;

typedef enum
{
   SMS_NON_MUST_OUTPUT,
   SMS_MUST_OUTPUT_PDU
}Typeflag_SMSOutput;

typedef enum
{
   BT_MUST_DISABLE,
   BT_MUST_ENABLE
}Typeflag_BT;

typedef enum
{
   BT_NON_CONNECT,
   BT_CONNECTING,
   BT_YES_CONNECT,
   BT_YES_POWER,
   BT_DISCONNECT
 //  TCP_ERROR_CONNECT_INTERNET,
//   TCP_ERROR_CONNECT_SERVER
}TypeStatus_BT;

typedef enum
{
   GPRS_MUST_DISABLE,
   GPRS_MUST_ENABLE
}Typeflag_GPRS;

typedef enum
{
   TCP_MUST_DISABLE,
   TCP_MUST_ENABLE
}Typeflag_TCP;

typedef enum
{
   CALL_MUST_DISABLE,
   CALL_MUST_ENABLE
}Typeflag_CALL;

typedef enum
{
   DISABLE_GSM_CHANNEL,
   ENABLE_GSM_CHANNEL
}TypFlagEnblGsmChannel;


// USER END

enum // ������������ status_power � status_init
{
  POWER_NON,      // ������� �� ������
  POWER_ON_PART1, // 
  POWER_ON_PART2, //
  POWER_ON_PART3, 
  GSM_POWER_ON,
  POWER_OFF_PART1,
  POWER_OFF_PART2,
  GSMStatusERROR,
  GSMStatusOK
};

enum
{
   GPRS_NON_CONNECT,
   GPRS_YES_CONNECT,
   
   GPRS_ERROR_CONNECT
};

enum
{
   TCP_NON_CONNECT,
   TCP_CONNECTING,
   TCP_YES_CONNECT,
   TCP_ERROR_CONNECT_INTERNET,
   TCP_ERROR_CONNECT_SERVER
};

enum // ������������ status_power � status_init
{
   INIT_NON,
   INIT_OK,
   SIM_CARD_ERROR
};

__packed  struct _GSMFlag
{
   uint32_t read_string_gsm :1; // ���� ���� ���������/ 0 - ��������� �� SIM900 �� �����������
   uint32_t simbol_end_0D   :1; //      
};

__packed  struct _GSMFlagMessage
{
//   uint32_t wait_OK:1; // // ����� �������� �� ������� � ������ ����� ��������� ����� ��, ������ ��� �������� ����� �������
   uint32_t AT_ATY:2;
   uint32_t AT_ATT:2;
   uint32_t AT_ATJque:2;
   uint32_t AT_ATJinst:3;
   uint32_t AT_ATJ:3;
   uint32_t CFUN_1:1;
   uint32_t CALL_READY:1;
   uint32_t SMS_READY:1;
   uint32_t ERROR:1;     
   uint32_t CPIN:2; // 0- ������ ���������, 1 - ���� ��� �����, 2 - ��� ��� �����
   uint32_t AT:2;
   
   uint32_t IPR:2;
   uint32_t AT_W:2;
 //  uint32_t AT_SAPBR_3_1_Contype:2;
   uint32_t AT_SAPBR_3_1_APN:2;
   
 //  uint32_t AT_SAPBR_3_1_USER:2;
 //  uint32_t AT_SAPBR_3_1_PWD:2;
   uint32_t AT_SAPBR_1_1:2;
   uint32_t AT_ATE0:2;
   
   uint32_t AT_CGSN:2;
   uint32_t AT_CPOWD_1:2;
   uint32_t AT_CSQ:2;
   uint32_t AT_BTHOST:2;
   
   uint32_t AT_CUSD:2; 
   uint32_t AT_CSCLK_1:2; 
   uint32_t AT_COPS:2;    
   uint32_t AT_CCID:2;    
   
   uint32_t CPIN_NOT_INSERTED:2;  
   uint32_t AT_CREG_0:2;  
   uint32_t AT_CREG:4;   
};
      
__packed struct _GSMFlagMessageGPRS
{
 //  uint32_t AT_SAPBR_3_1_Contype:2;
   uint32_t AT_SAPBR_3_1_APN:2;
 //  uint32_t AT_SAPBR_3_1_USER:2;
 //  uint32_t AT_SAPBR_3_1_PWD:2;
   uint32_t AT_SAPBR_1_1:2;   
};

//__packed struct _GSMFlagMessageTCP
__packed typedef struct
{
   uint32_t AT_CIPSHUT:2;
   uint32_t AT_CIPSHUT_Close:2;
   uint32_t AT_CIPMUX_0:2;
   uint32_t AT_CGATT_1:2;
   uint32_t AT_CGATT_0:2;   
   uint32_t AT_CGATT_CHANGE:2;
   uint32_t PLUS_CGATT:2;
   uint32_t AT_CSTT:2;
   uint32_t AT_CIICR:2;
   uint32_t AT_CIFSR:2;
   uint32_t AT_CIPSTART:2;
   uint32_t CONNECT_OK:2;  
   uint32_t CONNECT_FAIL:2;
   uint32_t AT_CIPSEND:2; 
   uint32_t SEND_FAIL:2;    
   uint32_t AT_CIPQSEND_1:2; 
   uint32_t AT_CIPQSEND_0:2; 
   uint32_t MessageTX:2;  
   uint32_t TransmitMessage:2; // ������ ��������
//};
}_GSMFlagMessageTCP;
__packed struct _GSMFlagMessageBT
{
   uint32_t AT_BTPOWER:2; 
   uint32_t AT_BTPOWER_0:2;
   uint32_t plus_BTPAIRING:2;
   uint32_t AT_BTPAIR_1_1:2;
   uint32_t plus_BTCONNECTING_SPP:2;
   uint32_t plus_BTCONNECT_SPP:2;
   uint32_t plus_BTDISCONN_SPP:2;
   uint32_t AT_BTACPT_1:2;
   uint32_t AT_BTSPPSEND:2;
   uint32_t MessageTX:2;
   uint32_t TransmitMessage:2;
};

__packed struct _GSMStatus
{
   uint8_t power; // 
   uint8_t init;// 
   uint8_t speed; //
   uint8_t joined;
   uint8_t transmit;
   uint8_t receive;
   uint8_t registration;
   uint8_t gprs;
   uint8_t TCP;
   TypeStatus_BT BT;
};

__packed struct _GSMInputSMS
{
    uint8_t InputSMS:      1; // 1 - ������ ���
    uint8_t AT_CMGR:       1; // 1 - ���������� ������� ��������� 
    uint8_t CMGR_plus:1;        // ������ ����� ��������, � ��������� ��������� ����� �����
    uint8_t SMSTextOK:1;        // ������ ����� ���������
    uint8_t PhoneNumber[12];
    uint8_t SMSText[1];  // ����� ��� ��� ������
    uint16_t CntByteText;// ����� ���� � ������
};

__packed struct _DataArchiveTransmitLORA
{
    double temperature;   // ������ long ���� � ������
    uint32_t volume;      // ������ short ���� � ������
    uint16_t k_factor;    // � ������
    uint16_t hour;        // ����
}; 


typedef enum
{
   SMS_NON_TRANSMITTED,
   SMS_TRANSMITTED,
   SMS_TRANSMIT_OK
   
}TypeSMSOutputStatus;

typedef enum
{
   CALL_NON_TRANSMITTED,
   CALL_TRANSMITTED,
   CALL_TRANSMIT_OK,
   CALL_TRANSMIT_ERROR
   
}TypeCALLOutputStatus;

__packed struct _GSMFlagOutputSMS
{
    uint16_t AT_CSMP_17_167_0_25: 2;
    uint16_t AT_CSCS_UCS2:2;
    uint16_t AT_CMGS:       2; // 1 - ���������� ������� ������� ���������� 
    uint16_t AT_CMGF_ON:       2; // 1 - ���������� ������� ������� ����������     
    uint16_t MessageTX:2; // ������������ �� ���������
    TypeSMSOutputStatus status;
};

__packed struct _GSMFlagOutputCALL
{
    uint16_t ATD: 2;
    uint16_t ATH0: 2;
    TypeCALLOutputStatus status;
};

typedef struct
  {
   const char* Str;    
   uint8_t   message_LORA;
  } TypePtrParserCmdLORA;
//#############################################################################

extern uint32_t current_communication_gsm_number;

void GSM_Init(GPIO_TypeDef *_ptr_PxOUT_Power,              // ��������� �� ������� ����� PxOUT ������ ������������ ������� GSM ������
           uint32_t _pin_power,    // ����� ������ ������������ ������� GSM ������
           GPIO_TypeDef *_ptr_PxOUT_Pwrkey,              // ��������� �� ������� ����� PxOUT ������ PWRKEY
           uint32_t _pin_pwrkey,   // ����� ������
 //          USART_TypeDef *_USART,          // ��. �� ����� ����������� ���������������� UART ������
 //          uint8_t _availability_of_2_SIMcards, // ������� 2-� ����
 //          GPIO_TypeDef *_ptr_PORT_select_SIMcard,
 //          uint16_t _pin_select_SIMcard,
           GPIO_TypeDef *_ptr_PxOUT_DTR,              
           uint32_t _pin_DTR);
  
  void      InitGSMModule();
  TypeStatus_BT GSM_BluetoothConnect(TypFlagEnblGsmChannel FlagEnblGsmChannel);
  void      GSM_increment_delay();
  uint8_t*  GSM_Get_IMEI();
  Typeflag_BT GSM_Get_flag_BT();
  uint8_t   GSM_Get_flag_power();
  int8_t   Get_confirm_transmit_LORA();
  TypeStatus_BT   GSM_Get_Status_BT();
  uint8_t   GSM_Get_Status_power();  
  uint8_t   GSM_Get_Status_AT_CIPSEND();
  TypeSMSOutputStatus GSM_GetStatusOutSMS();
  uint8_t   GSM_Get_flag_buf_data_TCP();
   Typeflag_TCP  GSM_Get_TCPFlag();
  uint8_t GSM_Get_Status_init();
  char* GSM_Get_ptr_network_name();
  char* GSM_Get_ptr_CCID_SIM_card();
  char* GSM_Get_ptr_message_SIM_card_balance();
  char* GSM_Get_ptr_IMEI();
  
  uint8_t   GSM_OpenGPRS();
  void      LORAPower();  
  void      GSM_Reset();
  void      GSMReadMessageStartGSMModule(); 
  void      GSMReadMessageGSMModule();
  void      ParsingMessageForLORA(uint8_t CmdLORA);
  uint8_t   GSMReadSTR_CONNECT_OK();
  uint8_t   GSMReadSTR_CONNECT_FAIL();
  uint8_t   GSMReadSTR_Digit();  
  void   GSMReadSTR_OK();
  uint8_t   GSMReadSTR_SHUT_OK();
  uint8_t   GSMReadSTR_PLUS_BTSPPDATA();
  uint8_t   GSMReadSTR_PLUS_COPS();
 // uint8_t   GSMReadUSERTCP();
  
  void      GSM_Set_TCPFlag(Typeflag_TCP _GSMflag_TCP);
  void      GSM_Set_flag_BT(Typeflag_BT _GSMflag_BT);
  void      GSM_Set_flag_power(uint8_t _GSMflag_power);
  void      GSM_Set_ptr_buf_data_TCP(uint8_t *_ptr_buf_data_TCP, uint32_t _cnt_byte_data_TCP);
  void GSM_Set_flag_SMSOutput(Typeflag_SMSOutput flag);
  uint32_t  LORA_Get_tick_count_message_from_LORA_msec();
  void      GSMSetPowerDown();
  uint8_t   GSM_TCPConnect(const uint8_t *ptr_IPAddress, const uint8_t *ptr_port,
                       const uint8_t *ptr_APNAddress, const uint8_t *APN_login, const uint8_t *APN_passeord);
  uint8_t   GSMCloseTCP();
  uint8_t   GSMTransmitMessageBT(uint8_t *ptr_buf);
  uint8_t   GSMTransmitMessageTCP(uint8_t*);
  uint8_t   GSMTransmitMessageTCPParts(uint8_t *ptr_buf, uint8_t last_line);
  uint8_t   FindSpeedUARTGSMModule();
  void TransmitDataLORA();
  void RegistrationModemLORA();
  uint8_t RequestRegistration();
  void      fGSM_RX_UARTTask();
  void      GSM_RX_IT(uint8_t RX_Buff);
  void      GSM_ExtremalPowerDown();
  uint16_t  GSM_Get_current_communication_gsm_number();
  void      GSM_Set_current_communication_gsm_number(uint32_t cnt);
  void      CopyTelNumberOutSMS(uint8_t *ptr);
  void      GSM_OutSMS(uint8_t *ptr_textSMS);
  _GSMFlagMessageTCP GSM_GetFlagMessageTCP();
  uint16_t  GSM_Get_current_communication_gsm_number();
  void      GSM_Set_CALLFlag(Typeflag_CALL _GSMflag_CALL);
  Typeflag_CALL  GSM_Get_CALLFlag();
  void      GSM_Set_flag_CALLOutput(Typeflag_CALLOutput flag);
  void      GSM_OutCALL(uint8_t *ptr_textSMS);
  void      DecodeUcs2(char *In, char *Out, int SizeOut);
  //uint16_t GSM_Get_current_communication_gsm_number();
  void LORA_Update_tick_count_message_from_LORA_msec();
  uint32_t LORA_Get_tick_count_powerON_LORA_msec();
#endif