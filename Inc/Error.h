//=============================================================================
//   
//=============================================================================
/// \file    Error.h
/// \author 
/// \date   
/// \brief   Definition of Error Flags
//=============================================================================
// COPYRIGHT
//=============================================================================
//=============================================================================

#ifndef ERROR_H
#define ERROR_H

#include "Typedef.h"

//-----------------------------------------------------------------------------
// Definition of modules with error handling. Add here new modules. 
// Modules from 0x01 to 0x1F are allowed.
#define ERROR_MODULE_MAIN        (0x01 << 5)
#define ERROR_MODULE_SGM         (0x02 << 5)
#define ERROR_MODULE_VDDCOMP     (0x03 << 5)
#define ERROR_MODULE_GASREC      (0x04 << 5)
#define ERROR_MODULE_FLOWMEAS    (0x05 << 5)
#define ERROR_MODULE_GASMETER    (0x06 << 5)
#define ERROR_MODULE_DATASTORAGE (0x07 << 5)
#define ERROR_MODULE_I2C         (0x08 << 5)
#define ERROR_MODULE_EEPROM      (0x09 << 5)
#define ERROR_MODULE_NVM         (0x0A << 5)
#define ERROR_MODULE_UTILS       (0x0B << 5)


#define MODEM_ERROR              (0x01 << 4)     // ������ ������ ��� �����������


typedef union {
  struct {
    uint32_t LastError    : 10;
    uint32_t InnerError   : 10;
    uint32_t PrimaryError : 10;
    uint32_t Overflow     : 1;
    uint32_t FatalError   : 1;
  } Items;
  uint32_t Raw;
} Error;

extern Error Gl_Error;
//-----------------------------------------------------------------------------
/// \brief  Returns a none error for initialization of a error variable.
Error Error_None(void);

//-----------------------------------------------------------------------------
/// \brief  Adds a error to an error variable.
///         If PrimaryError is none, PrimaryError = modulError,
///         LastError = modulError.
///         If PrimaryError is already set, InnerError = LastError,
///         LastError = modulError.
/// \param  error    Pointer to the error variable.
/// \param  uint16_t The module error to add.
/// \return Same as *error.
Error Error_Add(Error* error, uint16_t modulError);

//-----------------------------------------------------------------------------
/// \brief  Adds a error if the error variable contains already an error.
/// \param  error   Pointer to the error variable.
/// \param  uint16_t The module error to add.
/// \return Same as *error.
Error Error_AppendIfError(Error* error, uint16_t modulError);

//-----------------------------------------------------------------------------
/// \brief  Sets the fatal-flag in the error variable.
/// \param  error   Pointer to the error variable.
/// \return Same as *error.
Error Error_SetFatal(Error* error);

//-----------------------------------------------------------------------------
/// \brief  Returns true if the error variable contains no error.
bool Error_IsNone(Error error);

//-----------------------------------------------------------------------------
/// \brief  Returns true if the error variable contains an error.
bool Error_IsError(Error error);

//-----------------------------------------------------------------------------
/// \brief  Returns true if the fatal-flag is set in the error variable.
bool Error_IsFatal(Error error);

#endif /* ERROR_H */
