//=============================================================================
/// \file    SensorTypeParameters.h
/// \author  
/// \date    29-Oct-2015
/// \brief   Module for sensor specific parameters
//=============================================================================

#ifndef SENSORTYPEPARAMTER_H
#define SENSORTYPEPARAMTER_H

#include "Typedef.h"

typedef struct {
  float   Q_negSaturation;
  float   Q_negLimit;
  float   Q_posLimit;
  float   Q_min;
  float   Q_t;
  float   Q_max;
  float   Q_posSaturation;
//  uint8_t decimalPlaces;
} SensorTypeParameters;

  // ��������� ������
typedef struct
	{
   uint16_t Qfl;     //Long parametr
   uint16_t Qfs;     //Short parametr 
   uint16_t Qfl_FA;  //Long parametr Fine adjust
   uint16_t Qfs_FA;  //Short parametr Fine adjust
   uint16_t KS;      //�������� ���� short
   uint16_t KL;      //�������� ���� long 
   uint16_t Traw;    //�������� ����������� 
   short   dQVDD;    //��������� �� �������
  } TSensor;

typedef struct
	{
    uint16_t FlowUnit;
    uint8_t  SN_SGM[20];
    uint8_t  Article[30];
  } TSens_Const;


typedef struct {
   uint16_t num_param;      //2
   uint16_t min_Border_Qf;  //2
   uint16_t max_Border_Qf;  //2
   float offset_dQf;        //4
   float ctg_dQf;           //4
} TQf_param;

extern TSensor Sensor;
extern TSens_Const Sensor_Const;
extern SensorTypeParameters sensorTypeParameters[];

extern const uint16_t Qf_count;
extern const TQf_param Qf_param[];

#endif /* SENSORTYPEPARAMTER_H */
