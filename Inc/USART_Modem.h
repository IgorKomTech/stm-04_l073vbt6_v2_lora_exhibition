//=============================================================================
/// \file    USART_Modem.h
/// \author  MNA
/// \date    23-Apr-2018
/// \brief   Module for SGM modem
//=============================================================================
// COPYRIGHT
//=============================================================================
// Power key
#define GSM_PWRKEY_ON()  SET_BIT(GPIOD->BSRR, GPIO_BSRR_BR_0) //+++ RESET 
#define GSM_PWRKEY_OFF() SET_BIT(GPIOD->BSRR, GPIO_BSRR_BS_0) //+++ SET

#define GSM_DTR_ON()  SET_BIT(GPIOD->BSRR, GPIO_BSRR_BS_7 ) 
#define GSM_DTR_OFF() SET_BIT(GPIOD->BSRR, GPIO_BSRR_BR_7) //

// On-off power 
#define GSM_PWR_ON()   SET_BIT(GPIOD->BSRR,GPIO_BSRR_BS_1) // SET
#define GSM_PWR_OFF()  SET_BIT(GPIOD->BSRR,GPIO_BSRR_BR_1) // RESET

// On-off power 
//#define POWER_MCU_BAT3_ON()     SET_BIT(GPIOC->BSRR,GPIO_BSRR_BR_8) // RESET
//#define POWER_MCU_BAT1_2_ON()  POWER_MCU_BAT3_ON()// SET_BIT(GPIOC->BSRR,GPIO_BSRR_BS_8) // SET

//#define SIZE_MdmURL_Server      16
//#define SIZE_MdmPort_Server     6
//#define SIZE_MdmAPN_Adress      24
//#define SIZE_MdmAPN_Login       12
//#define SIZE_MdmAPN_Password    12
///#define SIZE_MdmNumberBalans    25
//#define SIZE_EEPROM_SN_SGM      20


/*
typedef union {
  struct {
          uint8_t Mdm_PowerON : 1;   // ������� ������ ��������
          uint8_t Mdm_Ready   : 1;   // ����� � ���� GSM 
          uint8_t Mdm_TestOk  : 1;   // ��������� ���� ��������� IMEI ��������
          uint8_t Mdm_SimON   : 1;   // ��� ����� � �������
          uint8_t Mdm_Reserv  : 4;
          } Items;
   uint8_t Mdm_Raw;
} TMdm_Flags;
*/
void  Mdm_HardwareInit(void);
//void Mdm_Transmit(void);
void Mdm_Transmit2(uint8_t const *ptr_str);
uint8_t Mdm_Test(uint8_t modemSpeed);
void Mdm_HardwareDeInit(void);
void Mdm_USARTInit(uint32_t BaudRate);
void TransmitDebugMessageOptic(const uint8_t *ptr_message);
void Waiting_transfer_GSM(uint16_t limit);

extern uint8_t GPRS_flag;
extern uint16_t ModemTransmitEnable;

//extern const char MdmURL_Server[][SIZE_MdmURL_Server];
//extern const char MdmPort_Server[][SIZE_MdmPort_Server];
//extern const char MdmAPN_Adress[SIZE_MdmAPN_Adress];
//extern const char MdmAPN_Login [SIZE_MdmAPN_Login];
//extern const char MdmAPN_Password[SIZE_MdmAPN_Password];
//extern const char MdmNumberBalans[SIZE_MdmNumberBalans];




extern const uint8_t MdmTransferMode;
extern const uint8_t MdmTransferHour;
extern const uint8_t MdmTransferMinutes;
extern const uint8_t MdmTransferDay;

extern const uint32_t reserved_int;
extern const uint32_t auto_switching_mode;

