#include "stm32l0xx.h"
#include "Time_g.h"
#include "Archiv.h"
#include <time.h>

#define BUF_MESSAGE_GSM_MODULE_SIZE 100

#define WAITING_TIME_MESSAGE_FROM_SERVER_MSEC 30000
#define WAITING_TIME_MESSAGE_FROM_GSM_MSEC    30000//90000 // 1.5 ��� �� �������� ���������
#define WAITING_TIME_CONNECTING_SERVER_GSM_MSEC    420000 // 7 ���, 5 ��� �� ����������� + 2 �� �������� ������ ���  �������� ������
#define WAITING_TIME_CONNECTING_OPTIC_MSEC    3000000

#define MESSAGE_NON_PARTS 0
#define MESSAGE_PARTS 1

#define EVENT_INT_BEGIN         0x00000000
#define EVENT_INT_END           0x80000000

#define EVENT_INT_CURRENT_FLOW                  0x00000000  // +
#define EVENT_INT_ERROR_SGM_MODULE              0x00000001  // +
#define EVENT_INT_ERROR_EL_COMPONENT            0x00000002
#define EVENT_INT_ERROR_CS_DQF                  0x00000004 // +
#define EVENT_INT_BATTERY_COVER_OPEN            0x00000008 // +
#define EVENT_INT_MAX_Q                         0x00000010  // +
#define EVENT_INT_MAX_T                         0x00000020  // +
#define EVENT_INT_OPEN_CASE                     0x00000040
#define EVENT_INT_OPEN_CALIB_SWITCH             0x00000080  // +
#define EVENT_INT_UNAUTHORIZED_INTERVENTION     0x00000100
#define EVENT_INT_BATARY_TELEMETRY_NON          0x00000200
#define EVENT_INT_SIM_CARD_NON                  0x00000400 // +
#define EVENT_INT_BATTERY_TELEMETRY_MIN_10      0x00000800 // +
#define EVENT_INT_BATTERY_COUNTER_MIN_10        0x00001000
#define EVENT_INT_ABAUT_Q_MAX                   0x00002000 // +
#define EVENT_INT_ABAUT_T_MAX                   0x00004000 // +
#define EVENT_INT_GOING_BEYOND_K_FACTOR         0x00008000 // +
#define EVENT_INT_REVERSE_FLOW                  0x00010000 // +
#define EVENT_INT_CLEAR_REG_STORY               0x00020000 

#define EVENT_INT_ERROR_CS_PO                   0x40000000


#define SIZE_MdmURL_Server      16
#define SIZE_MdmPort_Server     6
#define SIZE_MdmAPN_Adress      24
#define SIZE_MdmAPN_Login       12
#define SIZE_MdmAPN_Password    12
#define SIZE_MdmNumberBalans    25
#define SIZE_EEPROM_SN_SGM      20



//��� ������� ������ 
//#define  MAX_CNT_COMMUNICATION_GSM  3000


/*
E01 - ����� �����
�02 - � ������
�03 - Q max
E04 - �����������
�05 - ������� 10%
�06 - ������
*/
enum
{
  STR_ALL_END,
  STR_PART_END
};

enum
{
  ARCHIVE_HOURLY = 0,
  ARCHIVE_DAILY = 1
};

enum
{
  MODE_ONCE_AN_HOUR     = 1,
  MODE_ONCE_A_DAY       = 2,  
  MODE_ONCE_A_MONTH     = 4,   
  MODE_ONCE_A_DECADE   = 5  
};
__packed struct _Mode_Transfer // 4�����
{
   uint8_t mode;  // 1 - ������ ���
                  // 2 - ��� � ����
                  // 3 - ��� � ������
                  // 4 - ��� � �����
   uint8_t hour;
   uint8_t minute;
   uint8_t day;   // mode = 3, ���� ������. ����� = 1
                  // mode = 4, ����� ������
};
/*
__packed typedef struct 
{
   uint8_t     addressAPN[24]; // 30
   uint8_t     loginAPN[12]; // 42
   uint8_t     passwordAPN[12]; // 54
   uint8_t     ipserver[64]; // 118, 63 �� ����� + ������� ����, ��� ���������� �����
   uint8_t     portserver[6]; // 124, 5 �� ���� + ������� ����
   struct _Mode_Transfer Mode_Transfer; // 154
   uint32_t    SN_device; // 158
   uint32_t    reserved_int; // 323
   uint8_t     gas_day; // 324 ������� ����. ����� (����) ��� ������������ ��������� ������
   uint8_t     number_balans[6]; // 330
   uint8_t     rez[147]; // 499 �������� ��� ��������� � ��������� �� ˸��)
   uint8_t     CS; // 500 // ���� ������ �����������, ������� ������� ������, �.�. ���������� �������� �������� �������
}_Setting;
*/
enum
{
    MESSAGE_BT_TCP_NON,
    MESSAGE_TCP_KEY,
    MESSAGE_TCP_GET_GROUP_INFO,
    MESSAGE_TCP_ARCHIVE,
    MESSAGE_TCP_ARCHIVE_HOURLY,
    MESSAGE_TCP_ARCHIVE_DAILY,
    MESSAGE_TCP_ARCHIVE_SYSTEM,
    MESSAGE_TCP_ARCHIVE_CHANGES_SETTING,
    MESSAGE_TCP_ARCHIVE_EVENT,    
    MESSAGE_TCP_DATETIME,
    MESSAGE_TCP_DATETIME_SET,
    MESSAGE_TCP_APN_ADDRESS,    
    MESSAGE_TCP_APN_ADDRESS_SET,
    MESSAGE_TCP_APN_LOGIN,
    MESSAGE_TCP_APN_LOGIN_SET,
    MESSAGE_TCP_APN_PASSWORD,
    MESSAGE_TCP_APN_PASSWORD_SET,
    MESSAGE_TCP_VOLUME_PULSE,
    MESSAGE_TCP_VOLUME_PULSE_SET,
    MESSAGE_TCP_SERVER_URL,
    MESSAGE_TCP_SERVER_URL_SET,
    MESSAGE_TCP_SERVER_URL2,
    MESSAGE_TCP_SERVER_URL2_SET,
    MESSAGE_TCP_SERVER_URL3,
    MESSAGE_TCP_SERVER_URL3_SET, 
    MESSAGE_TCP_MODE_TRANSFER,
    MESSAGE_TCP_MODE_TRANSFER_SET,
    MESSAGE_TCP_SMS_PHONE1,
    MESSAGE_TCP_SMS_PHONE1_SET,
    MESSAGE_TCP_SMS_PHONE2,
    MESSAGE_TCP_SMS_PHONE2_SET,
    MESSAGE_TCP_LOCK_STATE,
    MESSAGE_TCP_LOCK_STATE_SET,
    MESSAGE_TCP_COUNT_SESSION,
    MESSAGE_TCP_COUNT_SESSION_SET,
    MESSAGE_TCP_ERROR_SESSION,
    MESSAGE_TCP_ERROR_SESSION_SET,    
    MESSAGE_TCP_MAX_SESSION,
    MESSAGE_TCP_MAX_SESSION_SET,     
    MESSAGE_TCP_COUNT_FAIL_SIM,
    MESSAGE_TCP_COUNT_FAIL_SIM_SET,    
    MESSAGE_TCP_COUNT_FAIL_SPEED,
    MESSAGE_TCP_COUNT_FAIL_SPEED_SET,    
    MESSAGE_TCP_EXIT,
    MESSAGE_TCP_PABS,
    MESSAGE_TCP_PABS_SET,
    MESSAGE_TCP_CLOSED,
    MESSAGE_SN_DEVICE,    
    MESSAGE_SN_DEVICE_SET,
    MESSAGE_UPDATE,
    MESSAGE_GET_FLASH,
    MESSAGE_GET_SETTING,
    MESSAGE_GET_FLASH_UPDATE,
    MESSAGE_GET_ARHIVE_INTERVALE, // UART,BT
    MESSAGE_GET_ARHIVE_DAILY,    // UART,BT 
    MESSAGE_GET_ARHIVE_SYSTEM, // UART,BT
    MESSAGE_GET_ARHIVE_CHANGE, // UART,BT
    MESSAGE_GET_ARHIVE_EVENT, // UART,BT
    MESSAGE_ERASE_FLASH, // UART,BT
    MESSAGE_CLEAR_BATTERY_VOLTAGE,
    MESSAGE_GET_ALL_RF_SENSOR,    // UART,BT
    MESSAGE_START_ADD_RF_SENS,// UART,BT
    MESSAGE_STOP_ADD_RF_SENS,// UART,BT
    MESSAGE_SET_TYPE_RF_SENS,
    MESSAGE_SET_TYPE_ALARM_RF_SENS,    
    MESSAGE_DEL_RF_SENS,
    MESSAGE_DEL_ALL_RF_SENS,    
    MESSAGE_RESERVED_INT,
    MESSAGE_RESERVED_INT_SET,
    MESSAGE_START_DEBUG, // UART,BT
    MESSAGE_STOP_DEBUG,   // UART,BT 
    MESSAGE_SETTINGS_DEFAULT,
    MESSAGE_GAS_DAY,
    MESSAGE_GAS_DAY_SET,
    MESSAGE_GET_GSM_INFO,
    MESSAGE_BALANCE_PHONE,    
    MESSAGE_BALANCE_PHONE_SET,
    MESSAGE_AUTO_SWITCH_MODE,    
    MESSAGE_AUTO_SWITCH_MODE_SET,
    MESSAGE_REPLACE_BATTERY,
    MESSAGE_ERROR,
    MESSAGE_TCP_VALVE,
    MESSAGE_TCP_VALVE_SET,
    MESSAGE_MOTO_INFO,
    MESSAGE_MOTO_INFO_SET,
    MESSAGE_TCP_QMAX_WAR_CNT_MEAS,
    MESSAGE_TCP_QMAX_WAR_CNT_MEAS_SET,
    MESSAGE_TCP_QMAX_MIN_WARNING,
    MESSAGE_TCP_QMAX_MIN_WARNING_SET,
    MESSAGE_TCP_TMAX_WARNING,
    MESSAGE_TCP_TMAX_WARNING_SET,
    MESSAGE_TCP_TMIN_WARNING,
    MESSAGE_TCP_TMIN_WARNING_SET,
    MESSAGE_TCP_T_WARN_CNT,
    MESSAGE_TCP_T_WARN_CNT_SET,
    MESSAGE_TCP_N_GAS_REC_TIME,
    MESSAGE_TCP_N_GAS_REC_TIME_SET,
    MESSAGE_TCP_GAS_RANGE,
    MESSAGE_TCP_GAS_RANGE_SET,
    MESSAGE_TCP_N_GAS_REC,
    MESSAGE_TCP_N_GAS_REC_SET,
    MESSAGE_TCP_EN_KALMAN_FILTER,
    MESSAGE_TCP_EN_KALMAN_FILTER_SET,
    MESSAGE_TCP_EN_SELF_FILTER,
    MESSAGE_TCP_EN_SELF_FILTER_SET,
    MESSAGE_TCP_EN_T_FILTER,
    MESSAGE_TCP_EN_T_FILTER_SET,
    MESSAGE_TCP_KALMAN_K,
    MESSAGE_TCP_KALMAN_K_SET,
    MESSAGE_TCP_KALMAN_COUNTER,
    MESSAGE_TCP_KALMAN_COUNTER_SET,
    MESSAGE_TCP_KALMAN_BORDER,
    MESSAGE_TCP_KALMAN_BORDER_SET, 

};

enum
{
  SYSTEM_CHANGE_STATUS = 11
};

enum
{
  EVENT_POWER_ON_LORA_MODEM=1,   // ��������� ������ ��������� �� ������� � ������� �� ������ ��������
  EVENT_REQUEST_PARAMETERS_LORA_MODEM,
  EVENT_REGISTRATION_LORA_MODEM,  
  EVENT_TRANSMIT_DATA_LORA_MODEM,
  EVENT_CONFIRM_TRANSMIT_DATA_LORA_MODEM,
  EVENT_POWER_OFF_LORA_MODEM   
};

enum
{
  TELEM_EVENT_START_GSM_MODEM=1, // +
  TELEM_EVENT_PARAM_GSM_MODEM, //  +
  TELEM_EVENT_CHECK_SIM_CARD,   // +
  TELEM_EVENT_CHECK_REGISTRATION_GSM, // +
  TELEM_EVENT_CHECK_LEVEL_GSM_SIGNAL, // +
  TELEM_EVENT_CHECK_REGISTRATION_GPRS, // +
  TELEM_EVENT_CHECK_REGISTRATION_SERVER, // +
  TELEM_EVENT_START_DATA_SERVER, // +
  TELEM_EVENT_DATA_SERVER,   // + 
  TELEM_EVENT_OFF_GSM_MODEM,  //10 +
  TELEM_EVENT_CHANGE_STATUS,   //11 -----------
  TELEM_EVENT_ADD_REGISTRATION_GPRS = 13 , // +
  TELEM_EVENT_TRANSMIT_SMS = 14 ,
  TELEM_EVENT_START_GSM_FOR_TIME,   // +
  TELEM_EVENT_START_GSM_FOR_BUTTON, // +
  TELEM_EVENT_START_GSM_FOR_OPTIC,  // +
  TELEM_EVENT_POWER_ON, // +
  TELEM_EVENT_POWER_OFF, // + 
  TELEM_EVENT_OPTIC_UP,
  TELEM_EVENT_OPTIC_DOWN,
  TELEM_EVENT_BEGIN_CSD,
  TELEM_EVENT_END_CSD,
};
enum
{
  EVENT_OK = 1,
  EVENT_GSM_ERROR = -1,
  EVENT_ERROR = -2,
  EVENT_OFF_EXTREMAL_GSM_MODEM = -3
    
};


typedef struct
{
   const char* commandStr;    
   uint8_t   message_TCP;
} TypePtrParserCmdTCP;

typedef enum
{
   GSM_TCP,
   GSM_BT,
   UART_SETTING   
}TypSourceMessageTMR;

typedef struct 
{
  uint8_t end_part_message:1;
  uint8_t end_message:1;
  uint8_t rez:6;
}_ResultReply;
struct _FlagMessageTMR
{
   uint8_t command; //
   TypSourceMessageTMR source_message; //
   uint8_t buf_message_tcp[BUF_MESSAGE_GSM_MODULE_SIZE];
};

__packed typedef struct 
{
   LL_RTC_DateTypeDef Date;   
   LL_RTC_TimeTypeDef Time;
}TypeDateTimeFind;

__packed typedef struct 
{
  uint16_t unauthorized_intervention: 1;  // going beyond K factor
  uint16_t residual_capacity_battery_min10: 1;  
  uint16_t battery_not_connected: 1;  
  uint16_t body_cover_open: 1;
  
  uint16_t GSMmodem_non_registration_GSM: 1; // 
  uint16_t GSMmodem_non_registration_GPRS: 1; //
  uint16_t error_connect_server: 1; //  
  uint16_t start_program: 1; // ��� ������ ��������� ����������� 1 ����� ������������ 
  
  uint16_t flash_memory_error: 1;
  uint16_t calibration_lock_open: 1;
  uint16_t optical_port_communication_session: 1; // ���������� �� ������
  uint16_t GSM_Modem_Power_mode: 1;

  uint16_t error_in_SGM_module:1; // ��� ����� � ������ ���������   
  uint16_t going_beyond_T: 1;    // going beyond temperature
  uint16_t going_beyond_Q: 1;    // ���������� ������� ����
  uint16_t reverse_flow:1;       // �������� ����� ������ 10000 (�.�. ������ ���� 10000-10000)
  
  uint8_t RCC_CSR;
  
  uint8_t hard_fault:1;

  uint16_t GSM_Modem_Power_optic: 1;
  uint16_t GSM_Modem_Power_switch: 1;
  uint8_t check_sn_sgm_false:1;  

  uint8_t check_sn_sgm2_false:1;
  uint8_t check_sn_sgm3_false:1;
  uint8_t check_sn_sgm4_false:1;
  uint8_t check_crc_dQf_false:1;  

}TypeStatus;

__packed typedef struct 
{
  uint8_t Qmax_warning:                1;
  uint8_t Tmax_min_warning:            1;     
  uint8_t SIM_card_non:                1;
  uint8_t battery_cover_open:          1;
  uint8_t battery_not_connected:       1;
  uint8_t body_cover_open:             1;
  //uint32_t rez2:                      29;
  
}TypeStatusWarning;

__packed typedef struct 
{    
  uint8_t Qmax_warning:                1;
  uint8_t Tmax_min_warning:            1; 
  uint8_t SIM_card_non:                1;
  uint8_t battery_cover_open:          1;
  uint8_t battery_not_connected:       1;
  uint8_t body_cover_open:             1;
  //uint32_t rez2:                      29;
  
}TypeLoraStatus;


typedef struct 
{
 // uint64_t time_workingCPU;
  uint64_t time_StopCPU;
  uint64_t time_workingGSM; // +
  uint64_t time_workingOPTIC;  // + current optic to stop mode add 30 ���
  uint64_t time_workingSGM; // +
  uint64_t time_workingValve; // + 
  float    workingCPU_ma;
  float    StopCPU_mkA;
  float    workingGSM_ma;
  float    workingOPTIC_ma;  
  float    workingSGM_ma;  
  float    workingValve_ma;    
  uint16_t capacity_battery_ma;
}_TypeMotoHoursToMils;

enum
{
  MOTO_HOURCE_START,
  MOTO_HOURCE_STOP
};

__packed typedef struct 
{
  uint32_t time_workingCPU;
  uint32_t time_StopCPU; 
  uint32_t time_workingGSM; 
  uint32_t time_workingOPTIC;   
  uint32_t time_workingSGM; 
  uint32_t time_workingValve;  
  uint8_t  flag_start_time_workingCPU:1;
  uint8_t  flag_start_time_stopCPU:1;
  uint8_t  flag_start_time_workingGSM:1;
  uint8_t  flag_start_time_workingOPTIC:1;
  uint8_t  flag_start_time_workingSGM:1;
  uint8_t  flag_start_time_sworkingValve:1;
}_TypeMotoHoursTempToMils;
/*
E01 - ����� �����
�02 - � ������
�03 - Q max
E04 - �����������
�05 - ������� 10%
�06 - ������ (Device_STM.Lut)
*/
/*
DateTimeFindBegin
DateTimeFindEnd
*/
extern uint8_t cnt_error_SGM;
extern uint8_t flag_check_crc_dQf;
extern uint8_t Qmax_warning_temp;
extern uint8_t going_beyond_T;
extern uint8_t going_beyond_Q;
extern uint8_t going_beyond_T_warning;
extern uint8_t source_change_CLB_LOCK;
extern uint8_t number_server_transmit;
extern int8_t flag_event;
extern int8_t flag_event_menu;
extern uint64_t   timeUnixGSMNextConnect;
extern uint64_t   timeGSMUnixStart;
extern   uint32_t PressKeyTime;
extern float residual_capacity_battery_percent;
extern const uint8_t telephone_number_SMS1[] @".eeprom";
extern  const int8_t type_device @".eeprom";
extern  const uint8_t hard_fault @".eeprom";
extern  const  uint16_t hw_version @".eeprom";
extern  const  double pressure_abs @".eeprom";
extern  double pressure_abs_gas;
extern  const  char EEPROM_SN_SGM [] @".eeprom";
extern  const int8_t optic_kontrast @".eeprom";
extern const char MdmURL_Server[][SIZE_MdmURL_Server];
extern const char MdmPort_Server[][SIZE_MdmPort_Server];
extern const char MdmAPN_Adress[SIZE_MdmAPN_Adress];
extern const char MdmAPN_Login [SIZE_MdmAPN_Login];
extern const char MdmAPN_Password[SIZE_MdmAPN_Password];
extern const char MdmNumberBalans[SIZE_MdmNumberBalans];
extern const LL_RTC_DateTypeDef DateVerification @".eeprom";
extern const LL_RTC_DateTypeDef DateNextVerification @".eeprom";
extern const float Qmax_warning @".eeprom";
extern const float Qmax_min_warning @".eeprom";
extern const uint16_t Qmax_warning_cnt_measurement @".eeprom";
extern const uint16_t Qmax_cnt_measurement @".eeprom";
extern const uint16_t cnt_measurement_temper_eeprom @".eeprom";
extern const int8_t Tmax_warning @".eeprom";
extern const int8_t Tmin_warning @".eeprom";
extern const uint16_t cnt_measurement_temper_warning_eeprom @".eeprom";
extern const uint32_t crc_dQf_eeprom @".eeprom";
extern const uint16_t cnt_reverse_flow_eeprom @".eeprom";
extern const uint16_t Qfl_reverse_flow @".eeprom";
extern const uint8_t serial_number_board[] @".eeprom";
extern const uint16_t cnt_error_SGM_eeprom @".eeprom";
extern const uint16_t Kfactor_min @".eeprom";
extern const uint16_t Kfactor_average @".eeprom";
extern const uint16_t Kfactor_max @".eeprom";
extern const uint8_t Kfactor_measure_cnt_eeprom @".eeprom";
extern uint32_t event_int_story;
extern uint32_t stack_GasmeterTask;//
extern uint32_t stack_OPTO_ReadTask; //
extern uint32_t stack_ReadWriteARC;//
extern uint32_t stack_task_GSM;//
extern uint32_t stack_GSM_RX_UARTTask;//
extern uint32_t stack_IDLE;//
extern uint32_t number_communication_gsm_fail;
extern const  uint32_t number_communication_gsm_fail_eeprom @".eeprom"; 
extern const  uint32_t current_communication_gsm_number_eeprom @".eeprom"; 
extern uint8_t successful_transfer_data_to_server;

extern uint8_t type_device_int;
extern TypeStatus StatusDevice_buf;
extern TypeStatus StatusDevice;
extern TypeStatusWarning StatusWarning;
extern TypeLoraStatus LoraStatus;

extern uint16_t cnt_str_transmit;
extern uint64_t   timeSystemUnix;
extern uint64_t   time_record_Volume;

extern _TypeMotoHoursToMils MotoHoursToMils;
extern _TypeMotoHoursTempToMils MotoHoursTempToMils;

extern IntArcTypeDef frameArchiveInt;

extern int8_t HiPower_ON;

extern const  uint8_t EnableKalmanFilter;



uint8_t ReadMessageTMR(char *ptrMessageTMR, uint8_t cnt_char_message, TypSourceMessageTMR sourceMessageTMR);
uint8_t TransmitMessageTMR(uint8_t *ptr_buf, uint8_t last_line, uint8_t parts, uint8_t end_part_message);
void ParsingMessageTMR();
void CalculationNextCommunicationSession();
void  CalculationTimeOnceADay();
void  CalculationTimeOnceADecade();
void  CalculationTimeOnceAMonth();
void Response_TCP(uint8_t response);
void TransmitDebugMessageOptic(const uint8_t *ptr_message);
void WriteTelemetry(uint16_t p_Event_Code, 
                int8_t p_Event_Result, 
                uint16_t p_Sesion_Number, 
                uint8_t type_record);
uint8_t QueueSendToBack(uint8_t* ptr_buf_arh, uint8_t _flag_detection_archive, uint32_t _numberArcRecFind);
void sprintfArchive(uint8_t typeArchive, uint8_t flagBegin);
uint8_t ParsingMESSAGE_TCP_LOCK_STATE_SET(uint8_t checkcrc32);
void FormationGROUP_INFO(uint8_t *ptr_buf, uint8_t part);
void sprintfEndMessage(uint8_t flag);
uint8_t ParsingMESSAGE_TCP_APN_SET(TCHANG_Type MdmAPN, uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_ERROR_SESSION_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_COUNT_SESSION_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_COUNT_FAIL_SPEED_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_COUNT_FAIL_SIM_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_MAX_SESSION_SET(uint8_t checkcrc32);
uint8_t ParsingMESSAGE_TCP_VALVE_SET(uint8_t checkcrc32);
_ResultReply  ParsingMESSAGE_MOTO_INFO(uint8_t *ptr_buf, uint8_t part);
uint8_t ParsingMESSAGE_MOTO_INFO_SET(char *ptr_buf_str, uint8_t checkcrc32, uint8_t flag_source);
void ClearCntSession(uint8_t change_source, uint32_t cnt_all, uint32_t cnt_fail, uint32_t cnt_fail_sim, uint32_t cnt_fail_speed);



void change_transfermode_to_5(uint8_t flag_change);
uint8_t ChangeURLPortServer(uint8_t *ptr_str, uint8_t index_server, uint8_t source);
_ResultReply TransmitARCHIVE(uint8_t *ptr_buf_in, uint8_t *ptr_buf_out, uint32_t size_buf_out);
uint32_t CalculateCRC32(uint8_t *ptr_buf, uint32_t cnt_byte_data, uint8_t flag_process);
void CRC_Start();
uint32_t CRC_Stop(uint8_t *pBuffer, uint32_t cnt_byte_data, uint32_t i);
void MX_LPTIM1_Init(void);

void CalculateMotoHourceGSM(uint8_t flag);
void CalculateMotoHourceOPTIC(uint8_t flag);
void CalculateMotoHourceSGM(uint8_t flag);
void CalculateMotoHourceValve(uint8_t flag);
void CalculateMotoHourceStopCPU(uint8_t flag);
void Calculation_residual_capacity_battery_percent();
int ToHex(uint8_t *In, uint8_t *Out);