//=============================================================================
/// \file    FlowMeasurement.h
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Flow-Measurement
//=============================================================================
// COPYRIGHT
//=============================================================================

#ifndef FLOW_MEASUREMENT_H
#define FLOW_MEASUREMENT_H

#include "Typedef.h"
#include "Error.h"
#include "Sgm.h"



//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_FLOWMEAS_LUT_NUMBER_OUT_OF_RANGE (ERROR_MODULE_FLOWMEAS | 0x00)
#define ERROR_FLOWMEAS_TOO_FEW_SAMPLINGPOINTS  (ERROR_MODULE_FLOWMEAS | 0x01)
#define ERROR_FLOWMEAS_TOO_MANY_SAMPLINGPOINTS (ERROR_MODULE_FLOWMEAS | 0x02)
#define ERROR_FLOWMEAS_LUT_VALUES_INCORRECT    (ERROR_MODULE_FLOWMEAS | 0x03)
#define ERROR_FLOWMEAS_UNKOWN_FLOW_UNIT        (ERROR_MODULE_FLOWMEAS | 0x04)
#define ERROR_FLOWMEAS_SENSOR_CONFIGURATION    (ERROR_MODULE_FLOWMEAS | 0x05)
#define ERROR_FLOWMEAS_TIMEOUT                 (ERROR_MODULE_FLOWMEAS | 0x06)
#define ERROR_FLOWMEAS_MEASURE_FLOW            (ERROR_MODULE_FLOWMEAS | 0x09)
#define ERROR_FLOWMEAS_MEASURE_FLOW_CHECK      (ERROR_MODULE_FLOWMEAS | 0x0A) 

//-----------------------------------------------------------------------------
/// \brief flow unit
typedef enum {
  FLOWMEAS_UNIT_UNKOWN,
  FLOWMEAS_UNIT_NORM_LITER_PER_MINUTE,        ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_20_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_15_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_25_PER_MINUTE, ///< 
  FLOWMEAS_UNIT_NORM_LITER_PER_HOUR,          ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_20_PER_HOUR,   ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_15_PER_HOUR,   ///< 
  FLOWMEAS_UNIT_STANDARD_LITER_25_PER_HOUR,   ///< 
} FlowMeas_Unit;


// ----------------------------------------------------------------------------
/// \brief Initialize Flow-Measurement-Module.
Error FlowMeas_InitModule(void);

// ----------------------------------------------------------------------------
/// \brief Perform a flow measurement.
/// \param value       Pointer to return the flow measurment value
/// \param k           TODO
/// \return Error flags
Error FlowMeas_MeasureFlow(void);

//Error FlowMeas_MeasureFlowShort(void); 

// -- Normal Mode (default) ---------------------------------------------------
/** \name Normal Mode (default) */
extern const uint16_t Config_GasmeterMeasurementIntervalNormalModeFM;
extern const uint16_t Config_GasmeterMeasurementIntervalNormalModeSM;
//const Config_GasmeterVDDRecognitionIntervalNormalMode @".eeprom" = 86400  ;
extern const uint16_t Config_GasmeterGasRecognitionIntervalNormalMode;
extern const uint16_t Config_GasmeterGasRecognitionIntervalMeasureMode @".eeprom"; // 
extern const uint16_t Config_GasmeterGasRecognitionIntervalErrorMode @".eeprom"; // 

extern const uint16_t Config_GasmeterGasRecognitionRetryIntervalNormalMode; // 5 �����

//#define Config_GasmeterBackupIntervalNormalMode 1800 // 30min

// -- Test Mode ---------------------------------------------------------------
extern const uint16_t Config_GasmeterMeasurementIntervalTestModeFM;
extern const uint16_t Config_GasmeterGasRecognitionIntervalTestMode;

extern const uint16_t QposLimit;
extern const uint16_t QnegLimit;
extern uint8_t reverse_flow;
// ----------------------------------------------------------------------------
extern float percent_QgL_Disp_min;
extern float percent_QgL_Disp_max;
extern uint16_t Qfl_limit_QgL_Disp;
extern const uint16_t Qf_sizeof;
extern const float KalmanK1 @".eeprom";
extern const float KalmanK2 @".eeprom";
extern const uint16_t KalmanBorder @".eeprom";
extern const uint8_t KalmanCnt @".eeprom";
extern uint16_t Sensor_Qfl;
/// \brief Get the unit of the flow-measurement.
///
/// Unit is only valid for flow measurements!
/// \return The unit of the flow-measurement
// FlowMeas_Unit FlowMeas_GetFlowUnit(void);

//Error ReadDeviceVDD(void);

#endif /* FLOW_MEASUREMENT_H */
