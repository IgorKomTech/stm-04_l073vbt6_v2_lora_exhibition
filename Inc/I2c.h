//=============================================================================
// 
//=============================================================================
/// \file    I2c.h
/// \author  SUL
/// \date    09-Dec-2014
/// \brief   Module for I2c-communication
//=============================================================================
// 
//=============================================================================
// 
//=============================================================================

#ifndef I2C_H
#define I2C_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Typedef.h"
#include "Error.h"
  
//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_I2C_ACK            (ERROR_MODULE_I2C | 0x00)
#define ERROR_I2C_DATA_LINE_LOW  (ERROR_MODULE_I2C | 0x01)

/// Enum-typedef for manipulations of I2c-headers
typedef enum {
  I2C_WRITE   = 0x00, ///< Write bit in header
  I2C_READ    = 0x01, ///< Read bit in header
  I2C_RW_MASK = 0x01  ///< Bit position read/write bit in header
} I2c_Header;

/// I2c acknowledge
typedef enum {
  ACK         = 0, ///< Signals acknowledge
  NO_ACK      = 1, ///< Signals no acknowledge
} I2c_Ack;

//----------------------------------------------------------------------------
/// \brief Initialize the port for I2c.
void I2c_InitHardware(void);

//----------------------------------------------------------------------------
/// \brief Enable I2c-lines.
void I2c_EnableLines(void);

//----------------------------------------------------------------------------
/// \brief Disable I2c-lines.
void I2c_DisableLines(void);

//----------------------------------------------------------------------------
/// \brief Send an I2c start condition.
Error I2c_StartCondition(void);

//----------------------------------------------------------------------------
/// \brief Send an I2c stop condition.
void I2c_StopCondition(void);

//----------------------------------------------------------------------------
/// \brief Write a single byte to the i2c bus.
/// \param data   Data to write to i2c bus
/// \retval Error flags
Error I2c_WriteByte(uint8_t data);

//----------------------------------------------------------------------------
/// \brief Read a single byte from the i2c bus.
/// \param  ack   Wether to send an ack (ACK) or not (NOT_ACK)
/// \retval Byte read from i2c
uint8_t I2c_ReadByte(I2c_Ack ack);

//----------------------------------------------------------------------------
/// \brief Make a write-header.
/// \param address The I2c-address to make the header from
/// \retval the I2c-header
uint8_t I2c_MakeWriteHeader(uint8_t address);
//----------------------------------------------------------------------------
/// \brief Make a read-header.
/// \param address The I2c-address to make the header from
/// \retval the I2c-header
uint8_t I2c_MakeReadHeader(uint8_t address);

#ifdef __cplusplus
}
#endif

#endif /* I2C_H */
