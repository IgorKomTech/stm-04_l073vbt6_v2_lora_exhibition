//=============================================================================
/// \file    Archiv.h
/// \author  MVA
/// \date    13-Mar-2018
/// \brief   Functions for archive
//=============================================================================
// COPYRIGHT
//=============================================================================

#include "stm32l0xx_ll_rtc.h"

#ifndef ARC_H
#define ARC_H


/* ������ ���������� ������� � ExtFlash */
#define IntArcMemBorder_Start     (uint32_t) 0
#define IntArcMemBorder_End       (uint32_t)(IntArcMemBorder_Start  + 0x20000)  // 2048 frame // 0 - 0x20000
#define DayArcMemBorder_Start     (uint32_t)IntArcMemBorder_End 
#define DayArcMemBorder_End       (uint32_t)(DayArcMemBorder_Start  + 0x10000) // 1024 frame // 0x20000 - 0x30000
#define EventArcMemBorder_Start   (uint32_t)DayArcMemBorder_End 
#define EventArcMemBorder_End     (uint32_t)(EventArcMemBorder_Start  + 0x40000) // 4096 frame // 0x30000 - 0x70000
#define ChangeArcMemBorder_Start  (uint32_t)EventArcMemBorder_End
#define ChangeArcMemBorder_End    (uint32_t)(ChangeArcMemBorder_Start + 0x00100000) // 4096 frame // 0x70000 - 0x00170000
#define SystemArcMemBorder_Start  (uint32_t)ChangeArcMemBorder_End
#define SystemArcMemBorder_End    (uint32_t)(SystemArcMemBorder_Start + 0x00010000) // 1024 frame // 0x00170000 - 0x00180000
#define TelemetryArcMemBorder_Start  (uint32_t)SystemArcMemBorder_End
#define TelemetryArcMemBorder_End    (uint32_t)(TelemetryArcMemBorder_Start + 0x00010000) // 1024 frame // 0x00180000 - 0x00190000

// MAX 0x007FFFFF

__packed typedef struct
 { 
   uint8_t  Day;             //1 /* ���� ������ */
   uint8_t  Mon;             //2
   uint16_t Year;            //4
   uint8_t  Hour;            //5 /* ����� ������ */
   uint8_t  Minute;          //6
   uint8_t  Second;          //7
 }TypeTime;



enum {
               ARC_INT  = 1,
               ARC_DAY  = 2,
               ARC_CHANG =3,
               ARC_EVENT =4, 
               ARC_SYSTEM =7
               } ;

typedef enum 
{
    TYPE_ARCHIVE_HOURLY_TIME = 1,
    TYPE_ARCHIVE_DAILY_TIME = 2,
    TYPE_ARCHIVE_CHANG_NUMBER = 3,
    TYPE_ARCHIVE_TELEMETRY_NUMBER = 4, 
    //TYPE_ARCHIVE_EVENT_NUMBER = 5,    
    TYPE_ARCHIVE_DAILY_NUMBER = 6,    
    TYPE_ARCHIVE_HOURLY_NUMBER = 8,
    TYPE_ARCHIVE_CHANG_TIME = 9,
    //TYPE_ARCHIVE_EVENT_TIME = 10,
    TYPE_ARCHIVE_TELEMETRY_TIME = 11,   
    TYPE_ARCHIVE_SYSTEM_NUMBER = 12,
    TYPE_ARCHIVE_SYSTEM_TIME = 13
}TArchiv_type;

typedef enum {
              ARC_WRITE = 0,
              ARC_READ  = 1
}TARC_RW;


 

__packed typedef struct
 { 
   uint32_t ArcRecNo;           //4 /* ����� ������ */
   uint64_t TimeBegine;         //12
   uint64_t TimeEnd;            //20   
   uint16_t Event_Code;         //22    /* ��� ������� */
   int8_t Event_Result;         //23    /* ��� ������ ������� */
   uint16_t Sesion_Number;      //25    /* ����� ������ �����, 0 - �����. ������� �������� */ 
   uint32_t Status;             //29
   uint32_t ArcCRC;             //33
 } EventSystemArcTypeDef;//
#define EventSystemArcTypeDefSIZE    33

__packed typedef struct
{
                uint32_t ArcRecNo;     //4      /* ����� ������ */  
                uint64_t Index_Time;   //12      /* ����� ������ � Unix ������� */
                double ArcVolume;      //20      /* ����������� ����� */
                float  ArcTemp;        //24      /* ������� ����������� �� �������� */ 
              //  double QgL_m3_midl;    //32
                uint32_t event;        //36
                uint32_t event_story;  //40
                uint32_t num_int;      //44
                uint32_t num_change;   //48
                uint32_t num_tel;      //52
                uint32_t num_system;    //56
                uint16_t ArcK;         //58
                uint16_t rez[9];          //60
                uint32_t ArcCRC;       //64    
                
              //  uint32_t ArcError;     //52      /* ������ ��� */                
              //  float  ArcMaxT;        //40
              //  float  ArcMinT;        //44                
              //  float  ArcMaxQ;        //36      /* ������������ ������ �� �������� */                
              //  uint32_t Reserv;       //58
              //  uint16_t Reserv2;      //60

}IntArcTypeDef; // 

__packed typedef struct
{ 
               uint32_t  ArcRecNo;         // 4   /* ����� ������ */  
               uint64_t Index_Time;        // 12      /* ����� ������ � Unix ������� */
               uint8_t  ID_Param;          // 13   /* ID ���������  14*/
               uint8_t  ID_Change;         // 14 ID ��������� ���������
               char OldValue[30];          // 44  /* ������ �������� */
               char NewValue[30];          // 74  /* ����� �������� */
               uint8_t  ArcCLB_Lock;       // 75   /* ��������� ������ ����� */ 
               uint8_t  status_cover;       // 76   /* ��������� ������ */
           //    uint8_t  rez[110];           // 
               uint32_t ArcCRC;            // 80
} ChangeArcTypeDef;
#define ChangeArcTypeDefSIZE    80

__packed typedef struct
{ 
               uint32_t  ArcRecNo;         // 4   /* ����� ������ */  
               uint64_t Index_Time;        // 12      /* ����� ������ � Unix ������� */
               uint64_t Index_Time_end;        // 12      /* ����� ������ � Unix ������� */
} NoAndTimeArcTypeDef;

typedef union 
{
  IntArcTypeDef          IntArc; // 64
  ChangeArcTypeDef       ChangeArc; // 146
  EventSystemArcTypeDef  EventSystemArc; // 33
  NoAndTimeArcTypeDef    NoAndTimeArcType;
  uint8_t               *ptr_read;
  uint8_t                beginArc;
}enumArcTypeDef;

/*
typedef struct
{
  TArchiv_type Arc_Type;
  TARC_RW      Arc_RW;
  TARC_SOURCE  Arc_Source;
  uint8_t*     Arc_Buffer;
  uint32_t     Arc_RecNo;
} TARC_Queue; 
*/
typedef struct
{
  TArchiv_type Arc_Type;
  TARC_RW      Arc_RW;
//  TARC_SOURCE  Arc_Source;
  enumArcTypeDef     Arc_Buffer;
  uint32_t     Arc_RecNo;
} TARC_Queue;

enum
{
  EVENT_BEGIN = 1,
  EVENT_END  
};

enum
{
  ARC_READ_STATUS_IN_RANGE_OK = 0,
  ARC_READ_STATUS_NOT_RANGE_ERROR,
  ARC_READ_STATUS_TIME_FAIL_ERROR,
  ARC_READ_STATUS_CRC_ERROR,
  ARC_READ_STATUS_END_FIND
};

enum
{
  LABEL_DATE,
  LABEL_NUMBER
};

typedef enum {
              SOURCE_OPTIC = 0,
              SOURCE_MDM   = 1
} TARC_SOURCE;




typedef enum {                        // optic tcp progr
              CHANGE_DATE_TIME = 5,   // + +
              CHANGE_APN_ADDRESS = 6, // + +
              CHANGE_APN_LOGIN = 7,   // + +
              CHANGE_APN_PASWORD = 8, // + +
          //    CHANGE_TCP_SERVER = 9, // + +
              CHANGE_SMS_NUMBBER = 10, // + +
              CHANGE_TRANSFER_MODE = 11, // + + +
              CHANGE_SERIAL_NUMBER = 15, // +
              CHANGE_BALANCE_NUMBER = 16, // + +
              CHANGE_GAS_DAY = 19, // + +
              CHANGE_AUTO_MODE = 21, // - +
              CHANGE_CNT_SESSION = 29, // + +
              CHANGE_RESERVED_INT = 31, // + +
              CHANGE_MAX_CNT_COMMUNICATION_GSM  = 35, // - +
              CHANGE_MOTO_INFO = 42,
              CHANGE__dQF = 51,    // + // ���� ������
              CHANGE_REC_TIME  = 52, // + // ������ ��������� � �������
            //  CHANGE_S_MES_TIME = 53,    // Short ���������
              CHANGE_L_MES_TIME = 54,     // + Long ���������  
              CHANGE_TEST_REC_TIME = 55,  // ������ ��������� � �������,�������� �����
              CHANGE_TEST_L_MES_TIME = 56, // ������ long ��������� ,�������� �����   
        //      CHANGE_SHORT_MEAS = 57,    // ������������ �������� ��������� 
              CHANGE_MAX_FLOW  = 58,    // ������� ���� ������ 
              CHANGE_TEMP_PAR_MAX  = 59,    // ����-��� ������� �����������
              CHANGE_CLB_LOCK  = 60,    // + + ��������� ������� �������������� �����
              CHANGE_TYPE_DEVICE  = 61,    // +
              CHANGE_STATUS  = 62, // - - +
              CHANGE_PRESSURE_ABS  = 63, // + +
              CHANGE_K_FACTOR  = 64, // + +
              CHANGE_VALVE_RESOLUTION  = 65, // - +
              CHANGE_VALVE_PRESENCE  = 66, // 
              CHANGE_SERIAL_NUMBER_SGM  = 67, //
              CHANGE_VALVE_OPTIC  = 68,
              CHANGE_TCP_SERVER1 = 69, // + +
              CHANGE_TCP_SERVER2 = 70, // + +
              CHANGE_TCP_SERVER3 = 71, // + +
              CHANGE_QPOS_LIMIT = 72, // +        
              CHANGE_QNEG_LIMIT = 73, // +  
              CHANGE_KONTRAST = 74, // +  
              CHANGE_SERIAL_NUMBER_SGM1  = 75, //
              CHANGE_SERIAL_NUMBER_SGM2  = 76, //
              CHANGE_SERIAL_NUMBER_SGM3  = 77, //
              CHANGE_SERIAL_NUMBER_SGM4  = 78, //   
              CHANGE_DATE_VERIFIC = 79,
              CHANGE_DATE_VERIFIC_NEXT = 80,      
              CHANGE_QMAX_WARNING = 81,  
              CHANGE_QMAX_MIN_WARNING = 82,  
              CHANGE_QMAX_WARNING_CNT_MEASUREMENT = 83,  
              CHANGE_QMAX_CNT_MEASUREMENT = 84, 
              CHANGE_CNT_MEASUREMENT_TEMPER_EEPROM = 85,
              CHANGE_TEMP_PAR_MIN = 86,
              CHANGE_TEMP_PAR_MAX_WARNING = 87,
              CHANGE_TEMP_PAR_MIN_WARNING = 88,
              CHANGE_TEMP_WARNING_CNT = 89,
              CHANGE_REVERSE_FLOW_CNT = 90,
              CHANGE_VALUE_REVERSE_FLOW = 91,
              CHANGE_SERIAL_NUMBER_BOARD = 92,
              CHANGE_CNT_ERROR_SGM_EEPROM = 93,
              CHANGE_GAS_RANGE = 94,
              CHANGE_GAS_CNT_ERROR = 95,
              CHANGE_BODY_COVER_OPEN = 96,
              CHANGE_BATTERY_COVER_OPEN = 97,
              CHANGE_TEST_MODE = 98,   
              CHANGE_CLEAR_ARHIVE = 99, 
              CHANGE_CLEAR_COUNTER = 100,
              CHANGE_SMS_NUMBBER2 = 101,
              
              
} TCHANG_Type;

enum
{
   CHANGE_PROGRAM = 0,
   CHANGE_TCP,
   CHANGE_OPTIC
};




// extern const uint32_t IntArcMem_Size; //?
extern const uint16_t IntArcRecCount;
extern const uint16_t IntArcRecordPerSector; 

// extern const uint32_t DayArcMem_Size; //?
extern const uint16_t DayArcRecCount;

// extern const uint32_t EventArcMem_Size; //?
extern const uint16_t EvArcRecCount;
extern const uint16_t EventArcRecordPerSector; 
extern const uint16_t ChangeRecordPerSector;

//extern const uint32_t ChangeArcMem_Size; //? 
extern const uint16_t ChangeArcRecCount;
extern const uint16_t SystemArcRecCount;
extern const uint16_t TelemetryArcRecCount;

extern const uint8_t IntArcLen;
extern const uint8_t ChangeArcLen;
extern const uint8_t EventArcLen; 
extern const uint8_t SystemArcLen; 
extern const uint8_t TelemetryArcLen; 

extern const uint32_t  IntArcRecNo;
extern const uint32_t  DayArcRecNo;
//extern const uint32_t  EvArcRecNo;
extern const uint32_t ChangeArcRecNo;
extern const uint32_t SystemArcRecNo;
extern const uint32_t TelemetryArcRecNo;

extern const uint16_t  GasDayBorder;

extern char OldValue[];
extern char NewValue[];
extern uint64_t EventTimeBegin;
extern  uint64_t EventTimeEnd;
extern double Int_QgL_m3_midl;




//extern EventArcTypeDef frameArchiveEvent;
//extern ChangeArcTypeDef frameArchiveChange;

/*
extern IntArcTypeDef    IntArc;
extern EventArcTypeDef  EvArc;
extern ChangeArcTypeDef ChangeArc;
*/


uint8_t WriteIntEventArcRecord(uint32_t  ArcFlag, uint32_t flag_begin_end);
uint8_t ReadIntArc(uint32_t p_IntArcRecNo, uint8_t p_arcType);
uint64_t Get_IndexTime(LL_RTC_DateTypeDef p_ArcDate, LL_RTC_TimeTypeDef p_ArcTime);
uint32_t CalcCRC(uint8_t* pRecord, uint16_t pSize);
uint8_t WriteTelemetryArcRecord(uint16_t p_Event_Code, int8_t p_Event_Result, uint16_t p_Sesion_Number);
void SetDateTimeToArchiveChanges();
uint8_t WriteChangeArcRecord(TCHANG_Type p_Parametr, uint8_t _ID_Change)   ;
void WriteChangeArcStatus(uint8_t flag_end);
uint8_t WriteSystemArcRecord(uint16_t p_Event_Code, uint16_t p_Sesion_Number);
void ReadArchiveParsing(enumArcTypeDef *ptr_Arc_Buffer, TArchiv_type archiv_type, uint8_t      *ptr_out);
uint8_t ReadArchiveQueue(TArchiv_type archiv_type, 
                    uint32_t     n_record,
                    uint64_t     label_begin,
                    uint64_t     label_end,
                    uint8_t      *ptr_out);

//extern const uint16_t DisableArcRecord;  
#endif // ARC_H




