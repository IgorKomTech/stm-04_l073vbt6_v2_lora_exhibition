//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    FlowCorrectionLut.h
/// \author  RFU
/// \date    24-Jul-2015
/// \brief   Lookup tables for flow correction
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef FLOWCORRECTIONLUT_H
#define FLOWCORRECTIONLUT_H

#include "Typedef.h"

#define FLOW_CORR_MAX_NUMBER_OF_SAMPLING_POINTS  20

typedef struct {
  uint16_t x;
  int16_t  y;
} LutSamplingPoint;

typedef struct {
  uint16_t length;
  LutSamplingPoint samplingPoints[FLOW_CORR_MAX_NUMBER_OF_SAMPLING_POINTS];
} FlowCorrectionLut;

#endif /* FLOWCORRECTIONLUT_H */
