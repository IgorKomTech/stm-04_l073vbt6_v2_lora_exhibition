//=============================================================================
/// \file    ValveTask.h
/// \author  MVA
/// \date    30-Jan-2019
/// \brief   Implementing the Vlave-functionality.
//=============================================================================
// COPYRIGHT
//=============================================================================
/*
4.1	����������� ����������� �������� ������ ������������� ����������� 
����������� ������� �� �������� ����������� ��������� ������� � ��������� �������:
�	���������� �������� ������� ���� ����� �������� ������� ��������� 
0,015�3/� � �������� ������� ��������� ������� ���� ����� �������� ������� 
������ ���� �� ����� 1 ���; ��� ����������� ������� ���� ������ ������ ���� 
�	���������� ������������� ����������� ������� ���� ( Q ? 1,2 Qmax); 
����������� �������� �������, ����� �������� ������� ������ ������� ������ 
( 1��� � 1��� ), ��������������� ����������; 
���������� ���������� �������� ��������� ���������� ����� ����, 
��������� ����� ������� �� ������������� ����������� �������� �������. 
*/
uint8_t Valve_Open(uint8_t p_Fors);
uint8_t Valve_Close(void);

#ifndef VALVETASK_H
#define VALVETASK_H

//#define VALVE_UNKNOWN 0
//#define VALVE_OPEN 1
//#define VALVE_CLOSE 2

#define VALVE_CLOSE                     0
#define VALVE_OPEN                      1
#define VALVE_CLOSE_REVERSE_FLOW        2
#define VALVE_CLOSE_Q_MAX               3
#define VALVE_CLOSE_NON_NULL            4
#define VALVE_NON_POWER                 5
#define VALVE_UNKNOWN                   6

//// valve action to be performed depending on where the command came from
enum
{
   VALVE_ACTION_NON=0,
   VALVE_ACTION_OPEN_TCP,
   VALVE_ACTION_OPEN_OPTIC,
   VALVE_ACTION_CLOSE_TCP,
   VALVE_ACTION_CLOSE_OPTIC
};

enum
{
   VALVE_OPEN_FORS_NON = 0,
   VALVE_OPEN_FORS_YES
};

extern const  uint8_t valve_presence; // 1 - there is
extern const uint8_t valve_status; 
extern const uint8_t valve_resolution; // 0 - close, 1 - open
extern uint8_t valve_action;

uint8_t PFW_valve(char* parString);
uint8_t PFW_valve_time(char* parString);
uint8_t PFW_valve_check_open_time(char* parString);
uint8_t PFW_Valve_min(char* parString);
uint8_t PFW_Valve_set(char* parString);
void Valve_Parsing();
void Set_valve_status(uint8_t temp_status);

#endif