//=============================================================================
/// \file    Sgm.c
/// \author  RFU
/// \date    13-Apr-2016
/// \brief   Module for Sensirion Gasmeter Sensor
//=============================================================================
// COPYRIGHT
//=============================================================================

#ifndef SGM_H
#define SGM_H

#include "Typedef.h"
#include "Error.h"
#include "SensorTypeParameters.h"
  

extern TSensor Sensor;

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_SGM_CRC                   (ERROR_MODULE_SGM | 0x00)
#define ERROR_SGM_SENSOR_EEPROM_READ    (ERROR_MODULE_SGM | 0x01)
#define ERROR_SGM_UNKOWN_SGM_TYPE       (ERROR_MODULE_SGM | 0x02)
#define ERROR_SGM_WAIT_FOR_PRAM_TIMEOUT (ERROR_MODULE_SGM | 0x03)  

//-----------------------------------------------------------------------------
/// \brief Sensor types indexes.
/// Must match with SensorTypeParameters in SensorTypeParameters.c!
typedef enum {
  SGM_TYPE_UNDEFINED = -1,
  SGM_TYPE_SGM4      =  0,
  SGM_TYPE_SGM6      =  1,
  SGM_TYPE_SGM10     =  2    
} Sgm_Type;


extern Sgm_Type _SgmType;
//-----------------------------------------------------------------------------
/// \brief Initialize the sensor power pin.
void Sgm_InitHardware(void);

//-----------------------------------------------------------------------------
/// \brief Restart sensor and read the sensor type.
///
/// Performs a power-cycle to reset the sensor and waits until the sensor is
/// ready for communication through I2c.
/// \return Error flags
Error Sgm_InitModule(void);
Error Sgm_DeinitModule(void);

void SGM_PowerOn(void);
void SGM_PowerOff(void);


//-----------------------------------------------------------------------------
/// \brief Gets the sensor id number.
Error Sgm_GetId(uint16_t* id);

//-----------------------------------------------------------------------------
/// \brief Performs a flow measurement.
//Error Sgm_MeasFlow(uint16_t k, uint16_t* rawFlow, uint16_t* rawQfa);
/*Error Sgm_MeasFlow(uint16_t k, // ��� ������
					uint16_t* qLutShort, // ��������� ��� ��������� Short 
					uint16_t* qLutLong,  // ��������� ��� ��������� Long
					uint16_t* k8T8,      // R ������ ��� Sort ��������� 
					uint16_t* qFA_long,  // ������ Long
					uint16_t* qFA_short, // ������ Short
					uint16_t* Traw);       // �����������
*/
Error Sgm_MeasFlow(TSensor* pSensor);
//-----------------------------------------------------------------------------
//Error Sgm_MeasFlowShort(uint16_t k8T8, uint16_t* qShort, uint16_t* qFA_Short);
Error Sgm_MeasFlowShort(TSensor* pSensor);
//-----------------------------------------------------------------------------
/// \brief Performs a gas recognition and returns the k value.
Error Sgm_GasRecognition(uint16_t* pGasRec);


//-----------------------------------------------------------------------------
/// \brief Gets the scale factor, flow unit and offset.
Error Sgm_GetSf_Offset(uint16_t* scaleFactor, uint16_t* flowUnit, uint16_t* offset);

//-----------------------------------------------------------------------------
/// \brief Gets the product serial number from the sensor.
Error Sgm_GetProductSerialNumber(uint64_t* serialNumber);

//-----------------------------------------------------------------------------
/// \brief Gets the article code from the sensor.
Error Sgm_GetArticleCode(uint8_t* buffer);

//-----------------------------------------------------------------------------
/// \brief Gets the sensor specific parameters.
/// \return sensor parameters
SensorTypeParameters* Sgm_GetParameters(void);

//-----------------------------------------------------------------------------
/// \brief Gets the sensor specific parameters.
/// \return sensor parameters
Error Sgm_GetVDD(short* p_dQVDD, uint16_t* p_VDD);


#endif /* SGM_H */
