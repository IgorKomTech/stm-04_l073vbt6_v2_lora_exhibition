//=============================================================================
//    S E N S I R I O N   AG,  Laubisruetistr. 50, CH-8712 Staefa, Switzerland
//=============================================================================
/// \file    Utils.h
/// \author  SUL
/// \date    11-Dec-2014
/// \brief   Module with miscellaneous utility functions
//=============================================================================
// COPYRIGHT
//=============================================================================
// Copyright (c) 2014, Sensirion AG
// All rights reserved.
// The content of this code is confidential. Redistribution and use in source 
// and binary forms, with or without modification, are not permitted.
// 
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
// DAMAGE.
//=============================================================================

#ifndef UTILS_H
#define UTILS_H

#include "Typedef.h"
#include "Error.h"

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_UTILS_CRC                   (ERROR_MODULE_UTILS | 0x00)

//-----------------------------------------------------------------------------
/// \brief Calculates a CRC-32 over the program memory area and checks it
///        with the passed reference CRC-32.
Error Utils_ProgramMemoryCheck(uint32_t* crc);

//-----------------------------------------------------------------------------
/// \brief Calculates a CRC-32 over the parameter memory area and checks it
///        with the passed reference CRC-32.
Error Utils_ParameterMemoryCheck(uint32_t* crc);

//-----------------------------------------------------------------------------
/// \brief Compares two CRC-32 and returns an error if they are not the same.
//Error Utils_Crc32Check(uint32_t crc1, uint32_t crc2);

//-----------------------------------------------------------------------------
/// \brief Calculate a CRC-8.
/// \param data         Pointer to data-chunk for which to calculate the CRC-8
/// \param length       The length of the data-chunk
/// \param polynomial   The polynomial used for CRC calculation
/// \param startVector  The start vector for CRC calculation
/// \return The calculated CRC
uint8_t Utils_CalculateCrc(const uint8_t* data, uint8_t length,
                           uint8_t polynomial, uint8_t startVector);

//-----------------------------------------------------------------------------
/// \brief Calculate a CRC-32.
/// \param data         Pointer to data-chunk for which to calculate the CRC-32
/// \param length       The length of the data-chunk
/// \param polynomial   The polynomial used for CRC calculation
/// \return The calculated CRC-32
uint32_t Utils_CalculateCrc32(const uint8_t* data, uint32_t length, uint32_t polynomial);

#ifdef __cplusplus
extern "C" {
#endif

//-----------------------------------------------------------------------------
/// \brief Make a 16-bit word from two bytes.
/// \param highByte   High-byte of the word to build
/// \param lowByte    Low-byte of the word to build
/// \return The 16-bit word
uint16_t Utils_MakeWord(uint8_t highByte, uint8_t lowByte);

//-----------------------------------------------------------------------------
/// \brief Get the high-byte of a 16-bit word.
/// \param word   The 16-bit word to get the high-byte from
/// \return The high-byte
uint8_t Utils_GetHighByte(uint16_t word);

//-----------------------------------------------------------------------------
/// \brief Get the low-byte of a 16-bit word.
/// \param word   The 16-bit word to get the low-byte from
/// \return The low-byte
inline uint8_t Utils_GetLowByte(uint16_t word)
{
  return ((uint8_t)(word & 0x00FF));
}

#ifdef __cplusplus
}
#endif

#endif /* UTILS_H */
