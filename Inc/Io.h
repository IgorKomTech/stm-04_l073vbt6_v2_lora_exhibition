//=============================================================================
//   
//=============================================================================
/// \file    Io.h
/// \author  SUL
/// \date    08-Dec-2014
/// \brief   Module for Io (jumpers, pushbuttons on Gasmeter Reference Design)
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#ifndef IO_H
#define IO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "Typedef.h"

typedef enum {
  KEY_PRESSED_NOT    = 0x00,
  KEY_PRESSED_SHORT  = 0x11,
  KEY_PRESSED_LONG   = 0x88,
  KEY_PRESSED_DOWN   = 0x22  
} TKeyPressed;

#define OPTO_PORT_ENABLE 0xFF
#define OPTO_PORT_DISABLE 0x0
  
// ----------------------------------------------------------------------------
/// \brief Initialize the Io-Module.
void Io_InitHardware(void);

// ----------------------------------------------------------------------------
/// \brief Whether jumper1 is set or not.
/// \retval true if set, false if not  
bool Io_IsSetJumper1(void);

// ----------------------------------------------------------------------------
/// \brief Whether jumper2 is set or not.
/// \retval true if set, false if not  
//bool Io_IsSetJumper2(void);

// ----------------------------------------------------------------------------
/// \brief Whether jumper3 is set or not.
/// \retval true if set, false if not  
bool Io_IsSetJumper3(void);

// ----------------------------------------------------------------------------
/// \brief Whether switch1 is closed.
/// \retval true if closed, false if not  
bool Io_IsSwitch1Closed(void);

// ----------------------------------------------------------------------------
/// \brief Init pulse timer
/// \retval none
void InitOtputTimer(void);
void DeInitOtputTimer(void);

// ----------------------------------------------------------------------------
/// \brief Start pulse timer
/// \retval none
void startOutputTimer(uint16_t pPeriod);

// ----------------------------------------------------------------------------
/// \brief Stop pulse timer
/// \retval none
void stopOutputTimer();

void GPIO_InitHardware(void);


extern TKeyPressed KeyPressed;
extern uint16_t  Pulse;
extern uint8_t OptoPort;
extern uint32_t PressKeyDelay;
extern uint8_t CLB_lock; 

extern uint32_t PressKeyDelay;
extern uint32_t LockPressKeyDelay;
extern TKeyPressed KeyPressed;
extern TKeyPressed LockKeyState;


#ifdef __cplusplus
}
#endif

#endif /* IO_H */
