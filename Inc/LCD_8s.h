/**
  ******************************************************************************
  * File Name          : LCD.h
  * Description        : This file provides code for the configuration
  *                      of the LCD instances.
  ******************************************************************************
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#ifndef LCD_H
#define LCD_H

#include <stdint.h>

void LCD8_Init(void);
void LCD8_Update(void);
void LCD8_SWversion(uint8_t Major, uint8_t Minor);
void LCD8_clear(void);
void LCD8_Qg(double pQg);
void LCD8_testSym(void);
void display_buffer(uint8_t pLCD_menu_point, uint8_t blink_point);

extern char LCD_string[];

#endif /* LCD_H */

