//=============================================================================
//  
//=============================================================================
/// \file    ExtFlash.h
/// \author 
/// \date   
/// \brief   External Flash interface
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#define  _buf_SIZE  256

void SPI_InitHardware(void);
void SPI_DeInitHardware(void);
void FLASH_Reset(void);
//void SPI_Init(void);
//void SPI_DeInit(void);
bool IS_FLASH_ON (void);
uint8_t SPI_Read_Data(uint32_t spi_adress, uint16_t p_size, uint8_t* pBuffer);
uint8_t SPI_Write(uint32_t p_adress,uint8_t* p_data, uint16_t p_bytes_to_write);
uint8_t SPI_GET_Register(uint8_t p_register, uint8_t answer_cnt);
void SPI_get_chip_info(void);
uint8_t SPI_Erase_4K(uint32_t p_adress);

void ReadWriteARC(void const * argument);

//extern uint8_t _buf[];
extern uint8_t  *_buf;
extern uint8_t  _buf_get_registr;