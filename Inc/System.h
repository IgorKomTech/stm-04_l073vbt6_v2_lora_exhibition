//=============================================================================
//  
//=============================================================================
/// \file    System.h
/// \author  RFU
/// \date    05-Feb-2016
/// \brief   Watchdog timer, which restarts the device on unexpected hang up.
//=============================================================================
// COPYRIGHT
//=============================================================================
//=============================================================================

#ifndef SYSTEM_H
#define SYSTEM_H

#include "Typedef.h"

//-----------------------------------------------------------------------------
/// \brief  Reads the reset flags and initialize the watchdog timer.
void System_Init(void);

//-----------------------------------------------------------------------------
/// \brief  Resets the watch dog timer.
void System_WatchdogRefresh(void);

//-----------------------------------------------------------------------------
/// \brief  Restarts the microcontroller (software reset).
void System_Reset(void);

//-----------------------------------------------------------------------------
/// \brief  Returns true if the last reset was a watch dog reset.
bool System_IsWatchdogResetOccurred(void);

//-----------------------------------------------------------------------------
/// \brief  Returns true if the last reset was a software reset.
bool System_IsSoftwareResetOccurred(void);

void System_RestartAfterReset(void);
void System_SaweBeforeReset(void);

static void StartWatchdog(void);
#endif /* SYSTEM_H */
