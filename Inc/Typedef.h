//=============================================================================
//  
//=============================================================================
/// \file    Typedef.h
/// \author  RFU
/// \date    07-Jan-2016
/// \brief   Definition of the types used in this project
//=============================================================================
// COPYRIGHT
//=============================================================================
//=============================================================================

#ifndef TYPEDEF_H
#define TYPEDEF_H

#include "stdint.h"
#include "stdbool.h"

typedef uint8_t (*TParsFunc)(char* param);

typedef struct
  {
   TParsFunc ParsFunc;
   char* commandStr;
  } TParser;


#endif /* TYPEDEF_H */
