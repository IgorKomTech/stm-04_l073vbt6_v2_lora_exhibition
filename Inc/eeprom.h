//=============================================================================
//  
//=============================================================================
/// \file    Eeprom.h
/// \author  RFU
/// \date    07-Jan-2016
/// \brief   Module for the uC internal EEPROM
//=============================================================================
// COPYRIGHT
//=============================================================================
// 
//=============================================================================

#ifndef EEPROM_H
#define EEPROM_H

#include "Typedef.h"
#include "Error.h"


#define FLASH_PEKEY1        0x89ABCDEF
#define FLASH_PEKEY2        0x02030405

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_NVM_READ               (ERROR_MODULE_NVM | 0x00)
#define ERROR_NVM_WRITE              (ERROR_MODULE_NVM | 0x01)
#define ERROR_NVM_INDEX_OUT_OF_RANGE (ERROR_MODULE_NVM | 0x02)
#define ERROR_NVM_LUT_IS_TOO_LARGE   (ERROR_MODULE_NVM | 0x03)
#define ERROR_NVM_NOT_IMPLEMENTED    (ERROR_MODULE_NVM | 0x1F)

//-----------------------------------------------------------------------------
// Definition of module errors. Add here new module errors. 
// Module errors from 0x00 to 0x1F are allowed.
#define ERROR_EEPROM_ADDRESS_OUT_OF_RAMGE (ERROR_MODULE_EEPROM | 0x00)
#define ERROR_EEPROM_ADDRESS_ALIGNMENT    (ERROR_MODULE_EEPROM | 0x01)

//-----------------------------------------------------------------------------
/// \brief Addresses in the non-volatile memory.
typedef enum {
  NVM_ADDRESS_BOOT_COUNTER             = 0x0000, // 0x0000-0x0003, size: 4
  NVM_ADDRESS_CRC32_PROGRAM            = 0x0004, // 0x0004-0x0007, size: 4
  NVM_ADDRESS_CRC32_PARAMETERS         = 0x0008, // 0x0008-0x000B, size: 4
  NVM_ADDRESS_TEST_MODE_PASSWORD       = 0x000C, // 0x000C-0x000F, size: 4
  NVM_ADDRESS_LAST_SUCCESSFUL_BOOT     = 0x0010, // 0x0010-0x0013, size: 4
  NVM_ADDRESS_RESERVE                  = 0x0014, // 0x0014-0x005F, size: 76
  NVM_ADDRESS_ERROR_COUNTERS           = 0x0060, // 0x0060-0x007F, size: 32
  NVM_ADDRESS_BACKUP_BLOCKS            = 0x0080, // 0x0080-0x00FF, size: 128
  NVM_ADDRESS_LAST_BATTERY_REPLACEMENT = 0x0100, // 0x0100-0x01FF, size: 4
  NVM_ADDRESS_PARAMETERS_RESERVE       = 0x0104, // 0x0104-0x01FF, size: 252
  NVM_ADDRESS_CORRECTION_LUTS          = 0x0200, // 0x0200-0x03FF, size: 512
  NVM_ADDRESS_ERROR_LOGS               = 0x0400, // 0x0400-0x07FF, size: 1024
} Nvm_Addresses;                 

#define FLOW_CORR_MAX_NUMBER_OF_SAMPLING_POINTS  20

//-----------------------------------------------------------------------------
/// \brief Writes a 16-bit word to the microcontroller internal EEPROM.
/// \param address      The address where to write to the EEPROM.
/// \param data         The data to write to the EEPROM.
int Eeprom_WriteWord(uint32_t p_address, uint16_t p_data);

//-----------------------------------------------------------------------------
/// \brief Reads a 32-bit double word from the microcontroller internal EEPROM.
/// \param address      The address from where to read the EEPROM
/// \param data         Pointer to return the read data from the EEPROM.
int Eeprom_ReadDword(uint32_t p_address, uint32_t* p_data);

//-----------------------------------------------------------------------------
/// \brief Writes a 32-bit double word to the microcontroller internal EEPROM.
/// \param address      The address where to write to the EEPROM.
/// \param data         The data to write to the EEPROM.
int Eeprom_WriteDword(uint32_t p_address, uint32_t p_data);

//-----------------------------------------------------------------------------
/// \brief Reads a 64-bit quad word from the microcontroller internal EEPROM.
/// \param address      The address from where to read the EEPROM
/// \param data         Pointer to return the read data from the EEPROM.
int Eeprom_ReadQword(uint32_t p_address, uint64_t* p_data);

//-----------------------------------------------------------------------------
/// \brief Writes a 64-bit quad word to the microcontroller internal EEPROM.
/// \param address      The address where to write to the EEPROM.
/// \param data         The data to write to the EEPROM.
int Eeprom_WriteQword(uint32_t p_address, uint64_t p_data);
int Eeprom_WriteDouble(uint32_t p_address, double p_data);

int Eeprom_WriteQf(uint16_t p_num,uint16_t p_min,uint16_t p_max,float p_diff, float p_tan);
int Eeprom_WriteString(uint8_t* p_StrData, uint8_t* p_DataToCopy, uint16_t p_maxData);
int Eeprom_WriteFloat(uint32_t p_address, float p_data);
int Eeprom_WriteChar(uint32_t p_address, uint8_t p_data);

#endif /* EEPROM_H */
